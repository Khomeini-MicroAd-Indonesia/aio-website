 //Animate Method
jQuery.fn.extend({
	animateCss: function (animationName) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        $(this).addClass('animated ' + animationName + ' show').one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);
        });
    }
});

//Method DOM
var base = {
	initialize : function() {
		this.events();
	},

	events : function(){
		 // NAVIGATION RESPOSNIVE HANDLER
	    jQuery(".mainMenu").clone(false).appendTo("#mobileNav").removeClass();
	    var mobileNav = jQuery('#mobileNav'),
	        shadow = jQuery('.js-nav-shadow');
	    jQuery("#mobileNav li").each(function () {
	      if (jQuery('ul', this).length == 1) {
	        jQuery(this).append('<i class="fa fa-angle-down" aria-hidden="true"></i>');
	        jQuery('ul', this).addClass('sub-menuNav');
	        jQuery(this).click(function () {
	          jQuery('ul', this).slideToggle();
	        });
	      }
	    });

	    jQuery(".js-btn-mobileNav").click(function () {
	      mobileNav.animateCss('slideInLeft');
	      jQuery('body').addClass('nav-opened');
	      shadow.fadeIn('fast');
	      return false;
	    });

	    jQuery(document).click(function(event) {
	      var target = jQuery(event.target);
	      if (target.is('.js-nav-shadow')) {
	        mobileNav.removeClass('show');
	        $('body').removeClass('nav-opened');
	        shadow.fadeOut('fast');
	      }
	    });

	    jQuery('.close-sidebar').on('click', function () {
	      mobileNav.removeClass('show');
	      $('body').removeClass('nav-opened');
	      shadow.fadeOut('fast');
	    });

	    $('.btn-src').on('click', function () {
	    	$('.search-box').toggleClass('show fadeInDown');
	    });

	    $('.distribList li').on('click', function (e) {
	    	e.preventDefault();
	    	$('ul', this).slideToggle();
	    });
	}
};

$(window).on('resize', function () {
	base.initialize();
});
$(window).resize();

//GOOGLE MAP
// var mapData = [
// 		{titles:"Bekasi",address:"<p>Jl. Cut Mutiah Km.102 No.25 Kel. Sepanjang Jaya<br \/>\r\nRawa Lumbu Bekasi Timur&nbsp;<br \/>\r\n17144<br \/>\r\n(021) 8241-4815 \/ 82416613<\/p>\r\n",lat:"-6.26122",lng:"106.997184"},
// 		{titles:"Bekasi",address:"<p>Jababeka Innovation Center<br \/>\r\nJl. Samsung I Blok A001&nbsp;<br \/>\r\nJababeka Cikarang<\/p>\r\n",lat:"-6.287997",lng:"107.157882"},
// 		{titles:"Cileungsi",address:"<p>Jl. Raya Narogong Cileugsi Kidul&nbsp;<\/p>\r\n",lat:"-6.411363",lng:"106.961766"},
// 		{titles:"Tangerang",address:"<p>Komplek VIVO Business Park<br \/>\r\nJl. Pembangunan 3 Blok B No. 2&nbsp;<br \/>\r\nKelurahan Karanganyar<br \/>\r\nKecamatan Neglasari<br \/>\r\nKota Tangerang<br \/>\r\n(021) 296-62611 \/ 021 296-62537<\/p>\r\n",lat:"-6.152821",lng:"106.641211"},
// 		{titles:"Tangerang",address:"<p>Komplek Pergudangan CIBADAK INDAH<br \/>\r\nNo. 9 J Jl. Raya Serang Km. 19&nbsp;<br \/>\r\nTangerang<\/p>\r\n",lat:"-6.208132",lng:"106.481549"},
// 		{titles:"Tangerang",address:"<p>Jl. Raya Serang Km. 3.8 Desa Pasir Jaya Kecamatan Cikupa Kabupaten Tanggerang 15710<\/p>\r\n",lat:"-6.220243",lng:"106.530450"},
// 		{titles:"Jakarta Timur",address:"<p>Jl. Rawa Gelam I No. 2&nbsp;<br \/>\r\nKawasan Industri Pulo Gadung<br \/>\r\n(021) 4682-4494<\/p>\r\n",lat:"-6.196531",lng:"106.912189"},
// 		{titles:"Jakarta Timur",address:"<p>Jl. TB Simatupang No. 54 A Rt 011 Rw 02&nbsp;<br \/>\r\nKelurahan Susukan Kecamatan Ciracas<br \/>\r\nJakarta Timur<br \/>\r\n(021) 8778-3145\/47<\/p>\r\n",lat:"-6.309125",lng:"106.870545"},
// 		{titles:"Jakarta Utara",address:"<p>Komplek Caraka Martadinata<br \/>\r\nBlok G\/H\/I<br \/>\r\nJL Laksana RE Martadinata<br \/>\r\nTanjung Priok Jakarta Utara 14310<br \/>\r\n(021) 430-6420 \/ 439 03 453<\/p>\r\n",lat:"-6.118351",lng:"106.868211"},
// 		{titles:"Depok",address:"<p>Pergudangan Sawangan Indah&nbsp;<br \/>\r\nBlok B 10 Jl. Raya Cinangka<br \/>\r\nSawangan Depok<br \/>\r\n(021) 2944-6213<\/p>\r\n",lat:"-6.372200",lng:"106.744046"},
// 		{titles:"Bandung",address:"<p>Jl. Terusan Kiara Condong No. 98 B<br \/>\r\nBandung - Jawa Barat<br \/>\r\n(022) 7514-638&nbsp;<\/p>\r\n",lat:"-6.948875",lng:"107.640681"},
// 		{titles:"Cimahi",address:"<p>Jl. Leuwigajah No. 67 Desa Utama<br \/>\r\nKec. Cimahi Selatan&nbsp;<br \/>\r\nKabupaten Bandung - Jawa Barat<\/p>\r\n",lat:"-6.902522",lng:"107.539994"},
// 		{titles:"Surabaya",address:"<p>Komplek Pergudangan Platinum Garden Siwalan Panji<br\/>\r\nJl. Raya Lingkar Timur Blok D Kav 2 Sidoarjo<br \/>\r\n(031) 9900-5818<\/p>\r\n",lat:"-7.430797",lng:"112.736866"},
// 		{titles:"Surabaya",address:"<p>Komplek Pergudangan Romokalisari 82<br \/>\r\nJl. Romokalisari No. 82M&nbsp;<br \/>\r\nKel. Romokalisari<br \/>\r\nKec. Benowo - Surabaya<br \/>\r\n(031) 7496127<\/p>\r\n",lat:"-7.199222",lng:"112.642368"},
// 		{titles:"Pamekasan",address:"<p>Jl. Raya Blumbungan<br \/>\r\nDesa Tambak, Pamekasan<br \/>\r\n(0324) 325-852<\/p>\r\n",lat:"-7.130703",lng:"113.532251",phone : 'No data'},
// 		{titles:"Medan",address:"<p>Komplek Krakatau Multi Center Blok F<br \/>\r\nJl. Gunung Krakatau Ujung - Medan<br \/>\r\n(061) 664-5412<\/p>\r\n",lat:"3.627746",lng:"98.680680"},
// 		{titles:"Langkat",address:"<p>Jl. KH. Zainul Arifin<br \/>\r\nKomplek Stabat City Blok A15-A16 Stabat<br \/>\r\nKab. Langkat - SUMUT<br \/>\r\n(061) 420 89032<\/p>\r\n",lat:"3.757354",lng:"98.452171"},
// 		{titles:"Tebing Tinggi",address:"<p>Jl. Mayjend Sutoyo No. 42&nbsp;<br \/>\r\nKelurahan Rambung&nbsp;<br \/>\r\nKota Madya Tebing Tinggi - SUMUT<br\/>\r\n(061) 326689<\/p>\r\n",lat:"3.323943",lng:"99.166803"},
// 		{titles:"Bogor",address:"<p>Jl. Raya Pemda No. 188 Pasir Jambu Pangkalan III Bogor 16710<\/p>\r\n",lat:"-6.536767",lng:"106.806726"},
// 		{titles:"Sukabumi",address:"<p>Jl. Raya Pelabuhan II KM 3 RT 01 \/ RW 19 Kel Dayeuh Luhur Kec Warudoyong<\/p>\r\n",lat:"-6.948670",lng:"106.914361"},
// 		{titles:"Karawang",address:"<p>JL.Taruma Negara Interchange Karawang Barat RT.12\/07 Desa Purwadana Kecamatan Teluk Jambe Timur (Samping RM Alam Sari) Karawang<\/p>\r\n",lat:"-6.300423",lng:"107.281438"},
// 		{titles:"Purwakarta",address:"<p>JL. Raya Cikampek - Purwakarta No.366 Desa Cilosari Kecamatan Bungur Sari Kabupaten Purwakrta (Samping RS.Thamrin)<\/p>\r\n",lat:"-6.494602",lng:"107.473411"},
// 		{titles:"Tasikmalaya",address:"<p>Jl. Perintis Kemerdekaan No. 37A RT 06 RW 01 Kelurahan Sambong Jaya Kecamatan Mangkubumi Tasikmalaya - Jawa Barat<\/p>\r\n",lat:"-7.357349",lng:"108.217061"},
// 		{titles:"Garut",address:"<p>Jl. Jend. Sudirman No. 99B<\/p>\r\n",lat:"-7.203191",lng:"107.915162"},
// 		{titles:"Sumedang",address:"<p>Jl. Serma Muhtar No. 88<\/p>\r\n",lat:"-6.844225",lng:"107.918315"},
// 		{titles:"Ciamis",address:"<p>Jl. Raya Rancah KM 01, Dusun Cisaga Kota, Desa Cisaga Kab. Ciamis - 48386<\/p>\r\n",lat:"-7.341005",lng:"108.516531"},
// 		{titles:"Cirebon",address:"<p>Jl. Buyut No.2A, Gambir Baru Kel.Pegambiran, Kec.Lemahwungkuk<\/p>\r\n",lat:"-6.709403",lng:"108.564656"},
// 		{titles:"Cirebon",address:"<p>Jl. Buyut No.2A, Gambir Baru Kel.Pegambiran, Kec.Lemahwungkuk<\/p>\r\n",lat:"-6.481604",lng:"108.298320"},
// 		{titles:"Yogyakarta",address:"<p>Jl. Lowanu No. 20\/93 &nbsp;Sorosutan Umbulharjo Yogyakarta&nbsp;<\/p>\r\n",lat:"-7.822339",lng:"110.377855"},
// 		{titles:"Magelang",address:"<p>Jl. Mayor unus depan Perumnas kali Negoro<\/p>\r\n",lat:"-7.583636",lng:"110.207586"},
// 		{titles:"Gunung Kidul",address:"<p>Siyono Wetan RT.57 \/ RW.10 Logandeng Playen - Gunung Kidul<\/p>\r\n",lat:"-7.946242",lng:"110.585216"},
// 		{titles:"Purwokerto",address:"<p>Jl. KH Wahid Hasyim 129 RT 01 RW 01 Karang Klesem Purwokerto - Jawa Tengah<\/p>\r\n",lat:"-7.444784",lng:"109.241796"},
// 		{titles:"Cilacap",address:"<p>Jl. Rawa Bendungan No. 17 Desa Mertasinga Cilacap Utara<\/p>\r\n",lat:"-7.676834",lng:"109.053158"},
// 		{titles:"Banjarnegara",address:"<p>Jl. Perubahan No. 43 Desa Purwonegoro Banjarnegara<\/p>\r\n",lat:"-7.437607",lng:"109.555194"},
// 		{titles:"Kebumen",address:"<p>Jl. Tentara Pelajar No. 47, Kebumen<\/p>\r\n",lat:"-7.670911",lng:"109.670522"},
// 		{titles:"Tegal",address:"<p>Jl. Mataram No.3 Rt 3\/ Rw 11 Desa Muara Reja - Tegal ( Sebelah utara terminal bus tegal )<\/p>\r\n",lat:"-6.848829",lng:"109.109204"},
// 		{titles:"Pekalongan",address:"<p>Jl. Dharma Bakti &nbsp;No 2, Medono-Pekalongan<\/p>\r\n",lat:"-6.902189",lng:"109.673887"},
// 		{titles:"Solo",address:"<p>Jl. Solo-Purwodadi KM. 7 (depan GKJ selokaton) Desa Wonorejo, Kecamatan Gondangrejo kabupaten Karanganyar<\/p>\r\n",lat:"-7.463859",lng:"110.806579"},
// 		{titles:"Wonogiri",address:"<p>Jl. Ahmad Yani No. 213 Gilingan Banjarsari, Surakarta<\/p>\r\n",lat:"-7.553155",lng:"110.822715"},
// 		{titles:"Semarang",address:"<p>Jl. Soekarno Hatta No. 97 A Kecamatan Pedurungan Kelurahan Tlogosari Kulon Semarang (50196)<\/p>\r\n",lat:"-6.982447",lng:"110.453181"},
// 		{titles:"Kendal",address:"<p>Jl. S. Parman No. 21 Tegal Sari Tegal Barat Kota Tegal Jawa Tengah<\/p>\r\n",lat:"-6.855841",lng:"109.133446"},
// 		{titles:"Pati",address:"<p>Jl. Pati Tayu Km 2 Bongsri Rt 01 Rw 01 Mulyoharjo Pati Jawa Tengah 59151<\/p>\r\n",lat:"-6.730712",lng:"111.048616"},
// 		{titles:"Purwodadi",address:"<p>Jl. Wr Supratman No.1 Rt 01 Rw 02 Kauman Juwana Pati Jawa Tengah<\/p>\r\n",lat:"-6.716778",lng:"111.144526"},
// 		{titles:"Bojonegoro",address:"<p>Jl. Lettu Suyitno No. 25 Kelurahan Banjarejo Kecamatan Bojonegoro&nbsp;<\/p>\r\n",lat:"-7.135930",lng:"111.920828"},
// 		{titles:"Lamongan ",address:"<p>Jl. Mayjen Sungkono No. 05 B-05 Gresik<\/p>\r\n",lat:"-7.178455",lng:"112.622556"},
// 		{titles:"Malang",address:"<p>Jl. Raya Pakisjajar no 7, RT 04 RW 12 Desa Pakisjajar, Kec. Pakis 65154 Kab. Malang, Jawa Timur<\/p>\r\n",lat:"-7.961829",lng:"112.720379"},
// 		{titles:"Probolinggo",address:"<p>Jl. Kalianget 100 Surabaya Kelurahan Perak Utara Kecamatan Pabean Cantian Surabaya<\/p>\r\n",lat:"-7.201484",lng:"112.734460"},
// 		{titles:"Pasuruan",address:"<p>Jl. Simo Pomahan No.45 Kelurahan Simomulyo Kecamatan Sukomanunggal Surabaya<\/p>\r\n",lat:"-7.266690",lng:"112.704754"},
// 		{titles:"Madiun",address:"<p>Jl. Thamrin No. 82 Rt.03 Rw.01 Kelurahan Klegen Kecamatan Kartoharjo Madiun - Jawa Timur<\/p>\r\n",lat:"-7.629303",lng:"111.532465"},
// 		{titles:"Ponorogo",address:"<p>Jl. Raya Ponorogo &ndash; Ngebel, Dusun Tenggang, RT 2 RW 3, Desa Ngrupit, Kec. Jenangan Kab. Ponorogo, Jawa Timur&nbsp;<\/p>\r\n",lat:"-7.815952",lng:"111.560643"},
//
//
//
// 	// {
// 	// 	lat: -6.415091,
// 	//     lng: 106.963944,
// 	//     titles:'CILEUNGSI DEPO',
// 	//     address: 'JL. Laksana RE Martadinata Tanjung Priok Jakarta Utara 14310 ',
// 	//     phone : '0812 - 8249 6990'
// 	// },{
// 	// 	lat: -6.118394,
// 	//     lng: 106.868297,
// 	//     titles:'SUNTER',
// 	//     address: 'Jl. Raya Narogong km 22,5 Cileungsi Kidul.',
// 	//     phone : '430-6420 / 439 03 453 '
// 	// },{
// 	// 	lat: -6.307988,
// 	//     lng: 106.874029,
// 	//     titles:'PASAR REBO',
// 	//     address: 'Jl. TB Simatupang,Ciracas Jakarta Timur.',
// 	//     phone : '021 - 8778-3145/47'
// 	// },
// 	// {
// 	// 	lat:  -5.371042,
// 	//     lng: 105.268667,
// 	//     titles:'Bandar Lampung',
// 	//     address: 'No data',
// 	//     phone : 'No data'
// 	// },{
// 	// 	lat:  -8.281787,
// 	//     lng: 115.093317,
// 	//     titles:'Bali',
// 	//     address: 'No data',
// 	//     phone : 'No data'
// 	// },{
// 	// 	lat:   -7.516154,
// 	//     lng: 112.371131,
// 	//     titles:'Jawa Timur',
// 	//     address: 'No data',
// 	//     phone : 'No data'
// 	// }
// ];

// var mapData = [;/

var mapData = [];
function mapData(callback){

$.ajax({
		url: 'get-data-distributor',
		data: 'province=#',
		method: 'post',
		dataType: 'json',
		success: function(response) {
					var d = $.parseJSON(JSON.stringify(response));
					var arr = [];
					for (var i in d)
					{
							Data={
								lat: d[i].latitude,
								lng: d[i].longitude,
								titles:d[i].kota,
								address:d[i].address,
								phone:'No Data'
							};
						arr.unshift(Data);
					i++;
					};
 				mapData = JSON.stringify(arr);
				console.log(mapData);
				callback(mapData);
				// return mapData;
		}
});
}
//
// $.post('get-data-distributor', {province:'#'},function(response){
// 			// var d = $.parseJSON(JSON.stringify(response));
// 					var arr = [];
// 					for (var i in response)
// 					{
// 							Data={
// 								lat: response[i].latitude,
// 								lng: response[i].longitude,
// 								titles:response[i].kota,
// 								address:response[i].address,
// 								phone:'No Data'
// 							};
// 						arr.unshift(Data);
// 					i++;
// 					};
//  				mapData = JSON.stringify(arr);
// 				// return mapData;
// 				console.log(data);
// });


var styleArray = [
            {elementType: 'geometry', stylers: [{color: '#e3e9dd'}]},
            {elementType: 'labels.text.stroke', stylers: [{color: ''}]},
            {elementType: 'labels.text.fill', stylers: [{color: '#000000'}]},
            {
              featureType: 'administrative.locality',
              elementType: 'labels.text.fill',
              stylers: [{color: '#262626'}]
            },
            {
              featureType: 'poi',
              elementType: 'labels.text.fill',
              stylers: [{color: '#d59563'}]
            },
            {
              featureType: 'poi.park',
              elementType: 'geometry',
              stylers: [{color: '#263c3f'}]
            },
            {
              featureType: 'poi.park',
              elementType: 'labels.text.fill',
              stylers: [{color: '#6b9a76'}]
            },
            {
              featureType: 'road',
              elementType: 'geometry',
              stylers: [{color: '#38414e'}]
            },
            {
              featureType: 'road',
              elementType: 'geometry.stroke',
              stylers: [{color: '#212a37'}]
            },
            {
              featureType: 'road',
              elementType: 'labels.text.fill',
              stylers: [{color: '#9ca5b3'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'geometry',
              stylers: [{color: '#746855'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'geometry.stroke',
              stylers: [{color: '#1f2835'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'labels.text.fill',
              stylers: [{color: '#f3d19c'}]
            },
            {
              featureType: 'transit',
              elementType: 'geometry',
              stylers: [{color: '#2f3948'}]
            },
            {
              featureType: 'transit.station',
              elementType: 'labels.text.fill',
              stylers: [{color: '#d59563'}]
            },
            {
              featureType: 'water',
              elementType: 'geometry',
              stylers: [{color: '#335197'}]
            },
            {
              featureType: 'water',
              elementType: 'labels.text.fill',
              stylers: [{color: '#ffffff'}]
            },
            {
              featureType: 'water',
              elementType: 'labels.text.stroke',
              stylers: [{color: '#17263c'}]
            }
          ];

//Var Declarations
var pinsMap, mapOption, map, marker, i, bounds, infoWindow;

function initMap (){
	displayMap(); //Display a map on the page
	displayMarkers(); //Show  Multiple  Markers
}

var displayMap = function () {
	mapOption = {
		center: new google.maps.LatLng(-0.652155, 113.941975),
	 	zoom: 5,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false,
        navigationControl: false,
        mapTypeControl: false,
        scaleControl: true,
        draggable: true,
        styles: styleArray
   	};
	map = new google.maps.Map(document.getElementById('map'), mapOption);
	map.setTilt(0);
};
//
// $('.link').

var displayMarkers = function () {

	infoWindow = new google.maps.InfoWindow();

	google.maps.event.addListener(map, 'click', function () {
        infoWindow.close();
    });

	bounds = new google.maps.LatLngBounds();
	$.ajax({
			url: 'get-data-distributor',
			data: 'province=#',
			method: 'post',
			dataType: 'json',
			success: function(response) {
						var d = $.parseJSON(JSON.stringify(response));
						var arr = [];
						for (var i in d)
						{
								Data={
									lat: d[i].latitude,
									lng: d[i].longitude,
									titles:d[i].kota,
									address:d[i].address,
									phone:'No Data'
								};
							arr.unshift(Data);
						i++;
						};
	 				mapData = $.parseJSON(JSON.stringify(arr));
					for (var i in mapData) {
						pinPosition = new google.maps.LatLng(mapData[i].lat, mapData[i].lng);
						mapInfoWindow(i);
						bounds.extend(pinPosition);
						i++;
					}
			}
	});

};

var mapInfoWindow = function (i) {
	var image = './../assets/img/pin-map.png';
	marker = new google.maps.Marker({
		position: pinPosition,
		map: map,
		icon: image
			// animation: google.maps.Animation.BOUNCE
	});

	var infoWindowContent = '<div class="iw_container">' +
	'<h4 class="iw_area">' + mapData[i].titles  + '</h4>' +
    '<p class="iw_address">' + mapData[i].address  + '</p>' +
    '<p class="iw_phone">' + mapData[i].phone  + '</p>' +
    '</div>';

	google.maps.event.addListener(marker, 'click', (function () {
    	infoWindow.setContent(infoWindowContent);
    	infoWindow.open(map, this);
    	map.setZoom(15);
    })); // Allow each marker to have an info window
};
