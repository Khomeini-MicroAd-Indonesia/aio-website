//Animate Method
jQuery.fn.extend({
	animateCss: function (animationName) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        $(this).addClass('animated ' + animationName + ' show').one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);
        });
    }
});

//Method DOM
var base = {
	initialize : function() {
		this.vendorInit();
		this.events();
	},

	events : function(){
		 // NAVIGATION RESPOSNIVE HANDLER
	    jQuery(".mainMenu").clone(false).appendTo("#mobileNav").removeClass();
	    var mobileNav = jQuery('#mobileNav'),
	        shadow = jQuery('.js-nav-shadow');
	    jQuery("#mobileNav li").each(function () {
	      if (jQuery('ul', this).length == 1) {
	        jQuery(this).append('<i class="fa fa-angle-down" aria-hidden="true"></i>');
	        jQuery('ul', this).addClass('sub-menuNav');
	        jQuery(this).click(function () {
	          jQuery('ul', this).slideToggle();
	        });
	      }
	    });

	    jQuery(".js-btn-mobileNav").click(function () {
	      mobileNav.animateCss('slideInLeft');
	      jQuery('body').addClass('nav-opened');
	      shadow.fadeIn('fast');
	      return false;
	    });

	    jQuery(document).click(function () {
	      var target = jQuery(event.target);
	      if (target.is('.js-nav-shadow')) {
	        mobileNav.removeClass('show');
	        $('body').removeClass('nav-opened');
	        shadow.fadeOut('fast');
	      }
	    });

	    jQuery('.close-sidebar').on('click', function () {
	      mobileNav.removeClass('show');
	      $('body').removeClass('nav-opened');
	      shadow.fadeOut('fast');
	    });

	    $('.btn-src').on('click', function () {
	    	$('.search-box').toggleClass('show fadeInDown');
	    });

	    $('ul.distribList > li a').on('click', function (e) {
	    	e.preventDefault();
	    	$(this).next().stop().slideToggle();
	    });

		$('.js-brandTrigger').each(function () {
			$('.js-brandTrigger').on('click', function (){
				selected_tab = $(this).data("brand");
				$target = $('.js-prodBrand');
				$target.find('li').removeClass('active');
				$target.find('#' + selected_tab).addClass('active');
				return false;
			});
		});

		$('.tabs-content-container .tabs-content:first').show();

		$('.js-prodBrand li').each(function (){
			$('.js-prodBrand li').click(function()
			 {
				var $panel = $(this).closest('.js-prodBrand'),
				    $panelContent = $('.tabs-content-container');
					selected_tab = $(this).attr("id");
				$panel.find("li").removeClass('active');
				$(this).addClass("active");
				$(".tabs-content").hide();
				$panelContent.find('.' +selected_tab).stop().slideDown('600');
				$panelContent.find('.scroll-pane').jScrollPane();

				return false;
			});
		});

		$('.js-trigerFirstTab a').on('click', function () {
			$('.tabs-content-container .tabs-content').hide();
			$('.tabs-content-container .tabs-content:first').stop().slideDown('600');
			return false;
		});
	},

	vendorInit : function () {
		//Jascroll pane
		$('.scroll-pane').jScrollPane();
	}
};

$(window).on('resize', function () {
	base.initialize();
	$('.js-brandTrigger').each(function () {
		$('.js-brandTrigger').on('click', function (){
			selected_tab = $(this).data("brand");
			$target = $('.js-prodBrand');
			$target.find('li').removeClass('active');
			$target.find('#' + selected_tab).addClass('active');
			return false;
		});
	});

	$('.tabs-content-container .tabs-content:first').show();

	$('.js-prodBrand li').each(function (){
		$('.js-prodBrand li').click(function()
		 {
			var $panel = $(this).closest('.js-prodBrand'),
					$panelContent = $('.tabs-content-container');
				selected_tab = $(this).attr("id");
			$panel.find("li").removeClass('active');
			$(this).addClass("active");
			$(".tabs-content").hide();
			$panelContent.find('.' +selected_tab).stop().slideDown('600');
			$panelContent.find('.scroll-pane').jScrollPane();

			return false;
		});
	});

	$('.js-trigerFirstTab a').on('click', function () {
		$('.tabs-content-container .tabs-content').hide();
		$('.tabs-content-container .tabs-content:first').stop().slideDown('600');
		return false;
	});
});
$(window).resize();

$('.distribList-content li').each(function () {
	var dataLat = $(this).data('lat');
	var dataLang = $(this).data('lang');

	console.log(dataLat,dataLang);
});

//GOOGLE MAP
var mapData = [
	{
		lat: -6.415091,
	    lng: 106.963944,
	    titles:'CILEUNGSI DEPO',
	    address: 'JL. Laksana RE Martadinata Tanjung Priok Jakarta Utara 14310 ',
	    phone : '0812 - 8249 6990'
	},{
		lat: -6.118394,
	    lng: 106.868297,
	    titles:'SUNTER',
	    address: 'Jl. Raya Narogong km 22,5 Cileungsi Kidul.',
	    phone : '430-6420 / 439 03 453 '
	},{
		lat: -6.307988,
	    lng: 106.874029,
	    titles:'PASAR REBO',
	    address: 'Jl. TB Simatupang,Ciracas Jakarta Timur.',
	    phone : '021 - 8778-3145/47'
	},
	{
		lat:  -5.371042,
	    lng: 105.268667,
	    titles:'Bandar Lampung',
	    address: 'No data',
	    phone : 'No data'
	},{
		lat:  -8.281787,
	    lng: 115.093317,
	    titles:'Bali',
	    address: 'No data',
	    phone : 'No data'
	},{
		lat:   -7.516154,
	    lng: 112.371131,
	    titles:'Jawa Timur',
	    address: 'No data',
	    phone : 'No data'
	}
];

var styleArray = [
            {elementType: 'geometry', stylers: [{color: '#e3e9dd'}]},
            {elementType: 'labels.text.stroke', stylers: [{color: ''}]},
            {elementType: 'labels.text.fill', stylers: [{color: '#000000'}]},
            {
              featureType: 'administrative.locality',
              elementType: 'labels.text.fill',
              stylers: [{color: '#262626'}]
            },
            {
              featureType: 'poi',
              elementType: 'labels.text.fill',
              stylers: [{color: '#d59563'}]
            },
            {
              featureType: 'poi.park',
              elementType: 'geometry',
              stylers: [{color: '#263c3f'}]
            },
            {
              featureType: 'poi.park',
              elementType: 'labels.text.fill',
              stylers: [{color: '#6b9a76'}]
            },
            {
              featureType: 'road',
              elementType: 'geometry',
              stylers: [{color: '#38414e'}]
            },
            {
              featureType: 'road',
              elementType: 'geometry.stroke',
              stylers: [{color: '#212a37'}]
            },
            {
              featureType: 'road',
              elementType: 'labels.text.fill',
              stylers: [{color: '#9ca5b3'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'geometry',
              stylers: [{color: '#746855'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'geometry.stroke',
              stylers: [{color: '#1f2835'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'labels.text.fill',
              stylers: [{color: '#f3d19c'}]
            },
            {
              featureType: 'transit',
              elementType: 'geometry',
              stylers: [{color: '#2f3948'}]
            },
            {
              featureType: 'transit.station',
              elementType: 'labels.text.fill',
              stylers: [{color: '#d59563'}]
            },
            {
              featureType: 'water',
              elementType: 'geometry',
              stylers: [{color: '#335197'}]
            },
            {
              featureType: 'water',
              elementType: 'labels.text.fill',
              stylers: [{color: '#ffffff'}]
            },
            {
              featureType: 'water',
              elementType: 'labels.text.stroke',
              stylers: [{color: '#17263c'}]
            }
          ];

//Var Declarations
var pinsMap, mapOption, map, marker, i, bounds, infoWindow;

function initMap (){
	displayMap(); //Display a map on the page
	displayMarkers(); //Show  Multiple  Markers
}

var displayMap = function () {
	mapOption = {
		center: new google.maps.LatLng(-0.652155, 113.941975),
	 	zoom: 5,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false,
        navigationControl: false,
        mapTypeControl: false,
        scaleControl: true,
        draggable: true,
        styles: styleArray
   	};
	map = new google.maps.Map(document.getElementById('map'), mapOption);
	map.setTilt(0);
};

var displayMarkers = function () {

	infoWindow = new google.maps.InfoWindow();

	google.maps.event.addListener(map, 'click', function () {
        infoWindow.close();
    });

	bounds = new google.maps.LatLngBounds();

	for (i = 0; i < mapData.length; i++) {
		pinPosition = new google.maps.LatLng(mapData[i].lat, mapData[i].lng);
		mapInfoWindow(i);
		bounds.extend(pinPosition);
	}
};

var mapInfoWindow = function (i) {
	var image = './assets/img/pin-map.png';
	marker = new google.maps.Marker({
		position: pinPosition,
		map: map,
		icon: image
			// animation: google.maps.Animation.BOUNCE
	});

	var infoWindowContent = '<div class="iw_container">' +
	'<h4 class="iw_area">' + mapData[i].titles  + '</h4>' +
    '<p class="iw_address">' + mapData[i].address  + '</p>' +
    '<p class="iw_phone">' + mapData[i].phone  + '</p>' +
    '</div>';

	google.maps.event.addListener(marker, 'click', (function () {
    	infoWindow.setContent(infoWindowContent);
    	infoWindow.open(map, this);
    	this.setZoom(5);
    })); // Allow each marker to have an info window
};
