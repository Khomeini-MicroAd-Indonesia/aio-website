var events = {
	'11-21-2016' : [{content: 'Great American Smokeout', url: 'http://www.wincalendar.com/Great-American-Smokeout', allDay: true}],
	'11-13-2016' : [{content: 'Ashura [An example of an complete date event (11-13-2013)]', allDay: true}],
	'11-11-2016' : [{content: 'Remembrance Day (Canada)', url: 'http://www.wincalendar.com/Remembrance-Day', allDay: true}],
	'11-04-2016' : [{content: 'Islamic New Year', repeat: 'YEARLY', allDay: true, endDate: '12-31-2016'}],
	'12-25-2016' : [{content: 'Christmas Day', repeat: 'YEARLY', allDay: true, endDate: '12-31-2016'}],
	'01-01-2016' : [{content: 'New Year\'s', repeat: 'YEARLY', allDay: true, endDate: '12-31-2016'}],
	'03-02-2016' : [{content: 'Yeah Monthly', repeat: 'MONTHLY', allDay: true, endDate: '12-31-2016'}],
	'01-07-2016' : [{content: 'Monthly And Yearly', repeat: 'MONTHLY', allDay: true, endDate: '12-31-2016'}],
	'03-02-2016' : [{content : 'Graduation Exams', repeat: 'INTERVAL', allDay: true, endDate: '12-31-2016'}],
	'04-01-2016' : [{content : 'MONDAY (WEEKLY)', repeat: 'MON', allDay: true, endDate: '12-31-2016'}],
    '04-01-2016' : [{content : 'MONDAY (WEEKLY)', repeat: 'MON', allDay: true, endDate: '12-31-2016'}],
//    '04-01-2016' : ['<a href="">event one</a>', '<span>event two</span>'],
    '05-18-2016' : [{content : 'MONDAY (WEEKLY)', repeat: 'MON', allDay: true, endDate: '12-31-2016'}]
},
t = new Date(),
//Creation of today event
today = ((t.getMonth() + 1) < 10 ? '0' + (t.getMonth() + 1) : (t.getMonth() + 1)) + '-' + (t.getDate() < 10 ? '0' + t.getDate() : t.getDate()) + '-' +t.getFullYear();
events[today] = [{content: 'TODAY', allDay: true}];
