/*
 * jQuery DP Calendar v2.3.4
 *
 * Copyright 2011, Diego Pereyra
 *
 * @Web: http://www.dpereyra.com
 * @Email: info@dpereyra.com
 *
 * Depends:
 *	jquery.ui.core.js
 *	jquery.ui.datepicker.js
 */

(function ($) {
    $.fn.dp_calendar = function (options, lang) {


        /* Setting vars*/
        var opts, events_array, date_selected, order_by, format_ampm, show_datepicker, show_time, link_color, show_priorities, show_sort_by, onChangeMonth, onChangeDay, onClickMonthName, onClickEvents, DP_LBL_NO_ROWS, DP_LBL_SORT_BY, DP_LBL_TIME, DP_LBL_TITLE, DP_LBL_PRIORITY, div_main_date, main_date, prev_month, toggleDP, next_month, div_dates, list_days, clear, div_clear, day_name, calendar_list, h2_sort_by, cl_sort_by, li_time, li_title, li_priority, ul_list, $dp, curr_day, curr_day_name, curr_date, curr_month_name_short, curr_month, curr_month_name, curr_year, ul_list_days, added_events, recurring_frecuency_active = false;
        var selected_lang = lang;
        
        opts = $.extend({}, $.fn.dp_calendar.defaults, options);

        events_array = opts.events_array;
        date_selected = opts.date_selected;
        order_by = opts.order_by;
        format_ampm = opts.format_ampm;
        show_datepicker = opts.show_datepicker;
        link_color = opts.link_color;
        show_priorities = opts.show_priorities;
//		show_sort_by = opts.show_sort_by;
        show_time = opts.show_time;
        onChangeMonth = opts.onChangeMonth;
        onChangeDay = opts.onChangeDay;
        onClickMonthName = opts.onClickMonthName;
        onClickEvents = opts.onClickEvents;
        date_range_start = opts.date_range_start;
        date_range_end = opts.date_range_end;

        /* Labels & Messages*/
        DP_LBL_EVENTS = $.fn.dp_calendar.regional['']['DP_LBL_EVENTS'];
        DP_LBL_NO_ROWS = $.fn.dp_calendar.regional['']['DP_LBL_NO_ROWS'];
        DP_LBL_SORT_BY = $.fn.dp_calendar.regional['']['DP_LBL_SORT_BY'];
        DP_LBL_TIME = $.fn.dp_calendar.regional['']['DP_LBL_TIME'];
        DP_LBL_TITLE = $.fn.dp_calendar.regional['']['DP_LBL_TITLE'];
        DP_LBL_PRIORITY = $.fn.dp_calendar.regional['']['DP_LBL_PRIORITY'];

        /* Padding function */
        function dp_str_pad(input, pad_length, pad_string, pad_type) {
            var half = '',
                pad_to_go,
                str_pad_repeater;

            str_pad_repeater = function (s, len) {
                var collect = '',
                    i;

                while (collect.length < len) {
                    collect += s;
                }
                collect = collect.substr(0, len);

                return collect;
            };

            input += '';
            pad_string = pad_string !== undefined ? pad_string : ' ';

            if (pad_type !== 'STR_PAD_LEFT' && pad_type !== 'STR_PAD_RIGHT' && pad_type !== 'STR_PAD_BOTH') {
                pad_type = 'STR_PAD_RIGHT';
            }
            if ((pad_to_go = pad_length - input.length) > 0) {
                if (pad_type === 'STR_PAD_LEFT') {
                    input = str_pad_repeater(pad_string, pad_to_go) + input;
                } else if (pad_type === 'STR_PAD_RIGHT') {
                    input = input + str_pad_repeater(pad_string, pad_to_go);
                } else if (pad_type === 'STR_PAD_BOTH') {
                    half = str_pad_repeater(pad_string, Math.ceil(pad_to_go / 2));
                    input = half + input + half;
                    input = input.substr(0, pad_length);
                }
            }

            return input;
        }

        /* in_array function */
        function dp_in_array (needle, haystack, argStrict) {
            var key = '',
                strict = !! argStrict;

            if (strict) {
                for (key in haystack) {
                    if (haystack[key] === needle) {
                        return true;
                    }
                }
            } else {
                for (key in haystack) {
                    if (haystack[key] == needle) {
                        return true;
                    }
                }
            }

            return false;
        }

        /* calculeDates() Core function */
        function calculeDates() {
            /* Setting vars */
            var newLI, newText, i;

            curr_day = date_selected.getDay();
            curr_day_name = $.fn.dp_calendar.regional['lang_'+selected_lang].dayNames[curr_day];
            curr_date = date_selected.getDate();
            curr_month = date_selected.getMonth();
            curr_month_name = $.fn.dp_calendar.regional['lang_'+selected_lang].monthNames[curr_month];
            curr_month_name_short = $.fn.dp_calendar.regional['lang_'+selected_lang].monthNamesShort[curr_month];
            curr_year = date_selected.getFullYear();

            //Set defaults options
            $.fn.dp_calendar.date_selected = date_selected;
            $.fn.dp_calendar.order_by = order_by;
            $.fn.dp_calendar.format_ampm = format_ampm;
            $.fn.dp_calendar.curr_day = curr_day;
            $.fn.dp_calendar.curr_day_name = curr_day_name;
            $.fn.dp_calendar.curr_date = curr_date;
            $.fn.dp_calendar.curr_month = curr_month;
            $.fn.dp_calendar.curr_month_name = curr_month_name;
            $.fn.dp_calendar.curr_month_name_short = curr_month_name_short;
            $.fn.dp_calendar.curr_year = curr_year;

            /* Clean the list of days */
            while (ul_list_days.firstChild) { ul_list_days.removeChild(ul_list_days.firstChild); }
            $(ul_list_days).html("");

            if(order_by === 1) {
                events_array.sort(function(a,b) {

                    if (a["sectioncss"] == null){
                        ax = Date.UTC(0, 0, 0, a["startDate"].getHours(), a["startDate"].getMinutes());
                        bx  = Date.UTC(0, 0, 0, b["startDate"].getHours(), b["startDate"].getMinutes());
                        return ax == bx ? 0 : (ax < bx ? -1 : 1)
                    }
                    else{
                        a = a["sectioncss"];
                        b = b["sectioncss"];
                        return a == b ? 0 : (a < b ? -1 : 1)

                    }
                });
            }
            if(order_by === 2) {
                events_array.sort(function(a,b) {
                    a = a["title"].toLowerCase();
                    b = b["title"].toLowerCase();

                    return a == b ? 0 : (a < b ? -1 : 1)
                });

            }
            if(order_by === 3) {
                events_array.sort(function(a,b) {
                    a = a["priority"];
                    b = b["priority"];
                    return a == b ? 0 : (a > b ? -1 : 1)
                });

            }

            var firstDayOfWeek = new Date(curr_year, curr_month, 1).getDay();
            for (var x=0; x<firstDayOfWeek; x++) {
                newLI = $('<li />');
                newText = document.createTextNode("");
                $(newLI).html(newText);
                $(ul_list_days).append(newLI);
            }

            /* Update the list of days*/
            for (i = 1; i <= new Date(curr_year, (curr_month + 1), 0).getDate(); i++) {
                newLI = $('<li />');

                if (curr_date === i) {
                    newLI.addClass("active");
                }
                newText = document.createTextNode(dp_str_pad(i, 2, "0", "STR_PAD_LEFT"));

                $(newLI).html(newText).attr('id', 'dpEventsCalendar_li_'+Date.UTC(curr_year, curr_month, i));

                var objCurrDate = new Date(curr_year, curr_month, i);
                var div_node = $('<div />').attr('class', 'node');
                $(events_array).each(function(j){
                    var startDate = this["startDate"];
                    if((Date.UTC(objCurrDate.getFullYear(), objCurrDate.getMonth(), objCurrDate.getDate()) === Date.UTC(startDate.getFullYear(), startDate.getMonth(), startDate.getDate()))){
                        $(div_node).append("<div class='node-sign pilih' data-shift=\""+this['shift']+"\"></div>");
                    }
                });
                
                for (idx_shift=1;idx_shift<=3;idx_shift++) {
                    if((objCurrDate.getDay() === 6) || (objCurrDate.getDay() === 0)){
                        $(div_node).append("<div class='node-sign libur' data-shift=\""+idx_shift+"\"></div>");
                    }else{
                        if (typeof $(div_node).find('div[data-shift='+idx_shift+']').html() === 'undefined') {
                            if (idx_shift === 2 && objCurrDate.getDay() === 5) {
                                $(div_node).append("<div class='node-sign pilih' data-shift=\""+idx_shift+"\"></div>");
                            } else {
                                $(div_node).append("<div class='node-sign' data-shift=\""+idx_shift+"\"></div>");
                            }
                        }
                    }
                }

                div_node.find('div').sort(function(a, b) {
                    return +a.dataset.shift - +b.dataset.shift;
                }).appendTo(div_node);
                newLI.append(div_node);

                $(ul_list_days).append(newLI);
            }

            jQuery($(div_dates).find("li")).css("color", link_color);
            jQuery($(cl_sort_by).find("li")).css("color", link_color);
            //ul_list_days.style.width = (new Date(curr_year, (curr_month + 1), 0).getDate() * 14) + "px";

            /* Check Date Range */
            if(date_range_start != null) {
                if(date_range_start.getMonth() == curr_month) {
                    jQuery($(div_dates).find("li:lt("+(date_range_start.getDate() - 1)+")")).addClass("dp_calendar_edisabled");
                    $(prev_month).hide();
                } else {
                    $(prev_month).show();
                }
                $dp.datepicker("option", {minDate: date_range_start});
            }

            if(date_range_end != null) {
                if(date_range_end.getMonth() == curr_month) {
                    jQuery($(div_dates).find("li:gt("+(date_range_end.getDate() - 1)+")")).addClass("dp_calendar_edisabled");
                    $(next_month).hide();
                } else {
                    $(next_month).show();
                }
                $dp.datepicker("option", {maxDate: date_range_end});
            }


            /* Onclick Days Event*/
            $($(ul_list_days).find("li:not(.dp_calendar_edisabled)")).click(function (e) {
                date_selected = new Date(curr_year, curr_month, $(this).text());
                $($(ul_list_days).find("li:not(.dp_calendar_edisabled)")).each(function (i) {
                    this.className = "";
                });
                this.className = "active";
                calculeDates();
                onChangeDay();
            });

            /* Days and Months Labels*/
            if(selected_lang == "id"){
                title_visit = "JADWAL KUNJUNGAN";
            }else{
                title_visit = "VISIT SCHEDULE";
            }
            $(day_name).html("");
            $(day_name).append("<h1>"+title_visit+"</h1>");

            $dp.datepicker("setDate", date_selected);
            $(toggleDP).html(curr_month_name + " " + curr_year);

            /* Preloader Message */
            $(ul_list).html("<div class='loading'></div>");

            /* Events Request */
            added_events = 0;

            $(events_array).each(function(i){

                if(typeof(this) == "object") {
                    var startDate = this["startDate"], endDate = this["endDate"];
                    event_to_time = Date.UTC(startDate.getFullYear(), startDate.getMonth(), startDate.getDate());
                    event_to_time_date_selected = Date.UTC(date_selected.getFullYear(), date_selected.getMonth(), date_selected.getDate());
                    if(typeof(endDate) == "object") {
                        event_to_time_end = Date.UTC(endDate.getFullYear(), endDate.getMonth(), endDate.getDate());
                    } else {
                        event_to_time_end = event_to_time_date_selected + 9999999999;
                    }
                    event_date = startDate.getDate();
                    event_month = startDate.getMonth();
                    event_year = startDate.getFullYear();
                    /* Recurring Events */
                    recurring_frecuency_active = false;
                    if(event_to_time_date_selected >= Date.UTC(startDate.getFullYear(), startDate.getMonth(), startDate.getDate()) && this["frecuency"] > 0){
                        switch(this["frecuency"]) {
                            case 1:
                                if(event_to_time_date_selected <= event_to_time_end) {
                                    recurring_frecuency_active = true;
                                }
                                break;
                            case 2:
                                calc_multiplo = ((event_to_time_date_selected - event_to_time) % 7);
                                if( calc_multiplo == 0 && event_to_time_date_selected <= event_to_time_end) { recurring_frecuency_active = true; }
                                break;
                            case 3:
                                if( date_selected.getDate() == startDate.getDate() && event_to_time_date_selected <= event_to_time_end) { recurring_frecuency_active = true; }
                                break;
                            case 4:
                                if( date_selected.getDate() == startDate.getDate() && date_selected.getMonth() == startDate.getMonth() && event_to_time_date_selected <= event_to_time_end) { recurring_frecuency_active = true; }
                                break;
                        }
                    }
                    /* Set classes */
                    /*if(curr_year === startDate.getFullYear() && curr_month === startDate.getMonth()){
                        $(ul_list_days).children("li")[(startDate.getDate() - 1)].className = $(ul_list_days).children("li:not(.dp_calendar_edisabled)")[(startDate.getDate() - 1)].className == "active" ? "active" : "has_events";
                    }*/ // [MAI - Peter]: take out this (unused)

                    if(this["frecuency"] > 0) {

                        switch(this["frecuency"]) {
                            case 1:
                                $(ul_list_days).find('li').each(function(i) { var li_events_time = $(this).attr('id').replace('dpEventsCalendar_li_', ''); if(li_events_time > event_to_time && li_events_time <= event_to_time_end) { $(this).not('.dp_calendar_edisabled').addClass( $(this).hasClass('active') ? "active" : "has_events" ); } });
                                break;
                            case 2:
                                $(ul_list_days).find('li').each(function(i) { var li_events_time = $(this).attr('id').replace('dpEventsCalendar_li_', ''); if((li_events_time > event_to_time && li_events_time <= event_to_time_end) && ((li_events_time - event_to_time) % 7 == 0)) { $(this).not('.dp_calendar_edisabled').addClass( $(this).hasClass('active') ? "active" : "has_events" ); } });
                                break;
                            case 3:
                                $(ul_list_days).find('li').each(function(i) { var li_events_time = $(this).attr('id').replace('dpEventsCalendar_li_', ''); if((li_events_time > event_to_time && li_events_time <= event_to_time_end) && (new Date(parseInt(li_events_time)+86400000).getDate() == event_date)) { $(this).not('.dp_calendar_edisabled').addClass( $(this).hasClass('active') ? "active" : "has_events" ); } });
                                break;
                            case 4:
                                $(ul_list_days).find('li').each(function(i) { var li_events_time = $(this).attr('id').replace('dpEventsCalendar_li_', ''); if((li_events_time > event_to_time && li_events_time <= event_to_time_end) && (new Date(parseInt(li_events_time)).getDate() == event_date) && (new Date(parseInt(li_events_time)).getMonth() == event_month)) { $(this).not('.dp_calendar_edisabled').addClass( $(this).hasClass('active') ? "active" : "has_events" ); } });
                                break;
                        }
                    }

                    /* Load the events list*/
                    if((Date.UTC(date_selected.getFullYear(), date_selected.getMonth(), date_selected.getDate()) === Date.UTC(startDate.getFullYear(), startDate.getMonth(), startDate.getDate())) || recurring_frecuency_active){

                        var li_event, li_event_title, li_event_description;

                        if(added_events === 0) {
                            $(ul_list).html("");
                        }

                        added_events++;

                        li_event = $('<li />');
                        li_event_title = $('<p>');
                        $(li_event).addClass("low medium-4 small-4 columns");
                        $(li_event).attr('data-shift',+this["shift"]);
                        $(li_event_title).append(this["title"]);
                        $(li_event).append(li_event_title);
                        $(ul_list).append(li_event);
                    }
                }
            });
            
            $('.shift', '#calendar .calendar_list').each(function(){
                if($( "#calendar" ).hasClass( "kejayan" )){
                    switch (date_selected.getDay()) {
                        case 5:
                            if ($(this).index() == 0) {
                                $(this).text('09.00 WIB :');
                            } else if ($(this).index() == 1) {
                                $(this).text('-');
                            } else if ($(this).index() == 2) {
                                $(this).text('13.00 WIB :');
                            }
                            break;
                        default:
                            if ($(this).index() == 0) {
                                $(this).text('09.00 WIB :');
                            } else if ($(this).index() == 1) {
                                $(this).text('11.00 WIB :');
                            } else if ($(this).index() == 2) {
                                $(this).text('13.00 WIB :');
                            }
                    }
                }else{
                    switch (date_selected.getDay()) {
                        case 5:
                            if ($(this).index() == 0) {
                                $(this).text('08.30 WIB :');
                            } else if ($(this).index() == 1) {
                                $(this).text('-');
                            } else if ($(this).index() == 2) {
                                $(this).text('13.00 WIB :');
                            }
                            break;
                        default:
                            if ($(this).index() == 0) {
                                $(this).text('08.30 WIB :');
                            } else if ($(this).index() == 1) {
                                $(this).text('10.00 WIB :');
                            } else if ($(this).index() == 2) {
                                $(this).text('13.00 WIB :');
                            }
                    }
                }
            });

            if(selected_lang == "id"){
                title_avail = "Tersedia";
                title_unavail = "Tidak Tersedia";
                title_choose_slot = "Pilih Shift Ini";
            }else{
                title_avail = "Available";
                title_unavail = "Unavailable";
                title_choose_slot = "Choose This Shift"
            }
            title_weekend = "-";
            
            for (idx_shift=1;idx_shift<=3;idx_shift++) {
                if((date_selected.getDay() === 6) || (date_selected.getDay() === 0)){
                    $(ul_list).append("<li class='low medium-4 small-4 columns' data-shift=\""+idx_shift+"\"><p style=\"color:black;\">"+title_weekend+"</p></li> ");
                }else{
                    if (idx_shift === 2 && date_selected.getDay() === 5) {
                        $(ul_list).append("<li class='low medium-4 small-4 columns' data-shift=\""+idx_shift+"\"><p style=\"color:red;\">"+title_unavail+"</p></li> ");
                    } else if (typeof $(ul_list).find('li[data-shift='+idx_shift+']').html() === 'undefined') {
                        $(ul_list).append("<li class='low medium-4 small-4 columns slot' data-shift=\""+idx_shift+"\"><a href='javascript:;'>"+title_choose_slot+"</a></li> ");
                    }
                }
            }

            ul_list.find('li').sort(function(a, b) {
                return +a.dataset.shift - +b.dataset.shift;
            }).appendTo(ul_list);

        }


        /* CREATING THE HTML CODE */

        this.addClass("dp_calendar");
        this.html("");

        div_main_date = $('<div />').addClass('div_main_date');

        main_date = $('<div />').addClass('main_date');

        $(div_main_date).append(main_date);

        prev_month = $('<a />').attr({href : 'javascript:void(0);', id: 'prev_month'}).html('&laquo;');


        toggleDP = $('<a />').attr({href : 'javascript:void(0);', id: 'toggleDP'});

        next_month = $('<a />').attr({href : 'javascript:void(0);', id: 'next_month'}).html('&raquo;');

        if(selected_lang == "id"){
                title_avail = "Tersedia";
                title_unavail = "Tidak Tersedia";
        }else{
                title_avail = "Available";
                title_unavail = "Unavailable";
        }
            
        avaliable = $("<div class='medium-6 small-6 columns available'>" + "<p>" + title_avail + "</p>" + "</div>");
        notavailable = $("<div class='medium-6 small-6 columns notavailable'>" + "<p>" + title_unavail + "</p>" + "</div>");
        $(div_main_date).append(avaliable);
        $(div_main_date).append(notavailable);
        $(div_main_date).append()
        $(main_date).append(prev_month);
        $(main_date).append(toggleDP);
        $(main_date).append(next_month);
        this.append(div_main_date);

        div_dates = $('<div />').addClass('div_dates');

        list_days = $('<ul />').attr('id', 'list_days');
        ul_list_days = list_days;

        clear = $('<div />').addClass('clear');

        day_name = $('<div />').addClass('row day_name').attr('id', 'day_name');

        $(div_dates).append("<ul class='week-day'><li>"+$.fn.dp_calendar.regional['lang_'+selected_lang].dayNamesShort[0]+"</li><li>"+$.fn.dp_calendar.regional['lang_'+selected_lang].dayNamesShort[1]+"</li><li>"+$.fn.dp_calendar.regional['lang_'+selected_lang].dayNamesShort[2]+"</li><li>"+$.fn.dp_calendar.regional['lang_'+selected_lang].dayNamesShort[3]+"</li><li>"+$.fn.dp_calendar.regional['lang_'+selected_lang].dayNamesShort[4]+"</li><li>"+$.fn.dp_calendar.regional['lang_'+selected_lang].dayNamesShort[5]+"</li><li>"+$.fn.dp_calendar.regional['lang_'+selected_lang].dayNamesShort[6]+"</li></ul>");

        $(div_dates).append(ul_list_days);
        div_clear = $('<div />').addClass('clear');
        $(div_dates).append(div_clear);
        $(div_dates).append(day_name);
        div_clear = $('<div />').addClass('clear');
        $(div_dates).append(div_clear);
        this.append(div_dates);

        calendar_list = $('<div />').addClass('calendar_list');

        cl_sort_by = $('<ul />').attr('id', 'cl_sort_by');

        li_time = $('<li />');
        if (order_by === 1) {
            li_time.addClass("active");
        }
        $(li_time).html(DP_LBL_TIME);
        li_title = $('<li />');
        if (order_by === 2) {
            li_title.addClass("active");
        }
        $(li_title).html(DP_LBL_TITLE);

        li_priority = $('<li />');
        if (order_by === 3) {
            li_priority.addClass("active");
        }
        $(li_priority).html(DP_LBL_PRIORITY);


        $(cl_sort_by).append(li_time);
        $(cl_sort_by).append(li_title);
        if(show_priorities) {
            $(cl_sort_by).append(li_priority);
        }

        ul_list = $('<ul />').attr('id', 'list');

        if(show_sort_by) {
            h2_sort_by = $('<h2 />');
            $(h2_sort_by).html(DP_LBL_SORT_BY);

            $(calendar_list).append(h2_sort_by);
            $(calendar_list).append(cl_sort_by);
        } else {
            h2_sort_by = $('<h2 />');
            $(h2_sort_by).html(DP_LBL_EVENTS);
            $(calendar_list).append("<div class='medium-4 small-4 columns shift'>10.00 WIB :</div>" + "<div class='medium-4 small-4 columns shift'>13.00 WIB :</div>" + "<div class='medium-4 small-4 columns shift'>16.00 WIB :</div> ");
        }
        $(calendar_list).append(clear);
        $(calendar_list).append(ul_list);
        this.append(calendar_list);


        $dp = $("<input type='text' />").hide().datepicker({
            onSelect: function (dateText, inst) {
                date_selected = new Date(dateText);
                calculeDates();
            }
        }).appendTo('body');


        $(toggleDP).click(function (e) {
            if (show_datepicker === true) {
                if ($dp.datepicker('widget').is(':hidden')) {
                    $dp.datepicker("show");
                    $dp.datepicker("widget").position({
                        my: "top",
                        at: "top",
                        of: this
                    });
                } else {
                    $dp.hide();
                }

            }
            onClickMonthName();

            e.preventDefault();
        });


        calculeDates();

        $(next_month).click(function (e) {
            date_selected = date_selected.add(1).month();
            calculeDates();
            onChangeMonth();
        });

        $(prev_month).click(function (e) {
            date_selected = date_selected.add(-1).month();
            calculeDates();
            onChangeMonth();
        });

        $($(cl_sort_by).find("li")).click(function (e) {
            $($(cl_sort_by).find("li")).each(function (i) {
                this.className = "";
            });
            this.className = "active";
            $($(cl_sort_by).find("li")).each(function (i) {
                if (this.className === "active") { order_by = (i + 1); }
            });
            calculeDates();
        });


    };

    /* Default Parameters and Events */
    $.fn.dp_calendar.defaults = {
        events_array: new Array(),
        date_selected: new Date(),
        order_by: 1,
        show_datepicker: true,
        show_priorities: true,
        show_sort_by: true,
        show_time: true,
        format_ampm: false,
        onChangeMonth: function () {},
        onChangeDay: function () {},
        onClickMonthName: function () {},
        onClickEvents: function () {},
        link_color: '#929292',
        date_range_start: null,
        date_range_end: null
    };

    /* Global parameters */
    $.fn.dp_calendar.date_selected = $.fn.dp_calendar.defaults.date_selected;
    $.fn.dp_calendar.order_by = $.fn.dp_calendar.defaults.order_by;
    $.fn.dp_calendar.format_ampm = $.fn.dp_calendar.defaults.format_ampm;
    $.fn.dp_calendar.curr_day = "";
    $.fn.dp_calendar.curr_day_name = "";
    $.fn.dp_calendar.curr_date = "";
    $.fn.dp_calendar.curr_month = "";
    $.fn.dp_calendar.curr_month_name = "";
    $.fn.dp_calendar.curr_month_name_short = "";
    $.fn.dp_calendar.curr_year = "";
    $.fn.dp_calendar.regional = [];
    $.fn.dp_calendar.regional[''] = {
        closeText: 'Done',
        prevText: 'Prev',
        nextText: 'Next',
        currentText: 'Today',
        monthNames: ['January','February','March','April','May','June',
            'July','August','September','October','November','December'],
        monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
        dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        dayNamesMin: ['Su','Mo','Tu','We','Th','Fr','Sa'],
        DP_LBL_EVENTS: 'Events',
        DP_LBL_NO_ROWS: 'No results were found in this date.',
        DP_LBL_SORT_BY: 'SORT BY:',
        DP_LBL_TIME: 'TIME',
        DP_LBL_TITLE: 'TITLE',
        DP_LBL_PRIORITY: 'PRIORITY'};
    
    $.fn.dp_calendar.regional["lang_id"] = {
        closeText: 'Done',
        prevText: 'Prev',
        nextText: 'Next',
        currentText: 'Hari Ini',
        monthNames: ['Januari','Februari','Maret','April','Mei','Juni',
            'Juli','Agustus','September','Oktober','November','Desember'],
        monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun',
            'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des'],
        dayNames: ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'],
        dayNamesShort: ['Min', 'Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
        dayNamesMin: ['Su','Mo','Tu','We','Th','Fr','Sa'],
        DP_LBL_EVENTS: 'Events',
        DP_LBL_NO_ROWS: 'No results were found in this date.',
        DP_LBL_SORT_BY: 'SORT BY:',
        DP_LBL_TIME: 'TIME',
        DP_LBL_TITLE: 'TITLE',
        DP_LBL_PRIORITY: 'PRIORITY'};

    $.fn.dp_calendar.regional["lang_en"] = {
        closeText: 'Done',
        prevText: 'Prev',
        nextText: 'Next',
        currentText: 'Today',
        monthNames: ['January','February','March','April','May','June',
            'July','August','September','October','November','December'],
        monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
        dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        dayNamesMin: ['Su','Mo','Tu','We','Th','Fr','Sa'],
        DP_LBL_EVENTS: 'Events',
        DP_LBL_NO_ROWS: 'No results were found in this date.',
        DP_LBL_SORT_BY: 'SORT BY:',
        DP_LBL_TIME: 'TIME',
        DP_LBL_TITLE: 'TITLE',
        DP_LBL_PRIORITY: 'PRIORITY'};

    /* setDate(date) Method */
    $.fn.dp_calendar.setDate = function (date) {
        $.fn.dp_calendar({
            date_selected: date
        });
    };

    /* setDay(day) Method */
    $.fn.dp_calendar.setDay = function (day) {
        $.fn.dp_calendar({
            date_selected: new Date($.fn.dp_calendar.curr_year, $.fn.dp_calendar.curr_month, day)
        });
    };

    /* setMonth(month) Method */
    $.fn.dp_calendar.setMonth = function (month) {
        $.fn.dp_calendar({
            date_selected: new Date($.fn.dp_calendar.curr_year, month, $.fn.dp_calendar.curr_date)
        });
    };

    /* setYear(year) Method */
    $.fn.dp_calendar.setYear = function (year) {
        $.fn.dp_calendar({
            date_selected: new Date(year, $.fn.dp_calendar.curr_month, $.fn.dp_calendar.curr_date)
        });
    };

    /* getDate() Method */
    $.fn.dp_calendar.getDate = function () {
        return $.fn.dp_calendar.date_selected;
    };

    /* getDay() Method */
    $.fn.dp_calendar.getDay = function () {
        return $.fn.dp_calendar.curr_date;
    };

    /* getMonth() Method */
    $.fn.dp_calendar.getMonth = function () {
        return $.fn.dp_calendar.curr_month;
    };

    /* getYear() Method */
    $.fn.dp_calendar.getYear = function () {
        return $.fn.dp_calendar.curr_year;
    };

})(jQuery);