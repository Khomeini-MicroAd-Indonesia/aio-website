<?php

/* soyjoy.twig */
class __TwigTemplate_5403e363c96eb1249e05425304e08a15af475a572435cc9b9f4a091c59cdec6c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        echo Asset::css("custom/animations.css");
        echo "
    ";
        // line 5
        $this->displayParentBlock("frontend_css", $context, $blocks);
        echo "
    ";
        // line 6
        echo Asset::css("custom/product.css");
        echo "
";
    }

    // line 8
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 9
        echo "    <div class=\"soyjoy columns\">
        <div class=\"columns sub-menu-product\" data-equalizer=\"menu-ionessense\">
            <div class=\"medium-2 medium-offset-3 small-4 columns\">
                <a href=\"";
        // line 12
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/brand/pocari-sweat\"  data-equalizer-watch=\"menu-ionessense\">
                <div class=\"columns pocari-sweat-color\">
                        Pocari Sweat
                </div>
                </a>
            </div>
            <div class=\"medium-2 small-4 columns\">
                <a href=\"";
        // line 19
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/brand/soyjoy\" data-equalizer-watch=\"menu-ionessense\">
                    <div class=\"columns soyjoy-color\">
                            Soy Joy
                        <span class=\"active-menu\"></span>
                    </div>
                </a>
            </div>
            <div class=\"medium-2 small-4 end columns\">
                <a href=\"";
        // line 27
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/brand/ionessence\" data-equalizer-watch=\"menu-ionessense\">
                    <div class=\"columns ionesence-color\">
                            Ionessence
                    </div>
                </a>
            </div>
        </div>
        <div class=\"columns banner text-center\">
            <img data-interchange=\"[";
        // line 35
        echo Uri::base();
        echo "/assets/img/product/banner-soyjoy-small.png, small], [";
        echo Uri::base();
        echo "/assets/img/product/banner-soyjoy-medium.png, medium], [";
        echo Uri::base();
        echo "/assets/img/product/banner-soyjoy.png, large]\">
        </div>
        <div class=\"row\">
            <div class=\"columns product-detail\" data-equalizer=\"product-detail\">
                <div class=\"medium-6 columns\">
                    <div class=\"columns product-desc animatedParent\" data-equalizer-watch=\"product-detail\">
                        <p class=\"animated fadeInUp\"><b>SOYJOY</b> merupakan pelopor cemilan sehat yang terbuat sepenuhnya dari tepung kedelai dan buah-buahan asli yang dikeringkan. Kedelai dan buah sebagai bahan dasar SOYJOY mengandung nutrisi penting seperti protein, serat, dan vitamin. SOYJOY juga dibuat tanpa pemanis buatan dan tanpa pengawet.</p>
                    </div>
                </div>
                <div class=\"medium-6 columns animatedParent\">
                    <div class=\"columns product-desc-img animated fadeInUp delay-250\">
                        <a href=\"http://soyjoy.co.id/chocodrama/#\" target=\"_blank\"><img src=\"";
        // line 46
        echo Uri::base();
        echo "/assets/img/product/soyjoy-detail.jpg\" data-equalizer-watch=\"product-detail\"></a>
                    </div>
                </div>
            </div>
            <div class=\"columns\">
                <div class=\"columns\">
                <div class=\"product-variant columns\">
                    <div class=\"medium-6 columns variant-display animatedParent\" data-sequence=\"200\">
                        <div class=\"columns\">
                            <div class=\"small-2 columns animated fadeInUpShort\" data-id=\"1\"><img src=\"";
        // line 55
        echo Uri::base();
        echo "/assets/img/product/variant/soyjoy-variant-stroberry.png\"></div>
                            <div class=\"small-2 columns animated fadeInUpShort\" data-id=\"2\"><img src=\"";
        // line 56
        echo Uri::base();
        echo "/assets/img/product/variant/soyjoy-variant-peanut.png\"></div>
                            <div class=\"small-2 columns animated fadeInUpShort\" data-id=\"3\"><img src=\"";
        // line 57
        echo Uri::base();
        echo "/assets/img/product/variant/soyjoy-variant-raisin-almond.png\"> </div>
                            <div class=\"small-2 columns animated fadeInUpShort\" data-id=\"4\"><img src=\"";
        // line 58
        echo Uri::base();
        echo "/assets/img/product/variant/soyjoy-variant-banana.png\"> </div>
                            <div class=\"small-2 columns animated fadeInUpShort\" data-id=\"5\"><img src=\"";
        // line 59
        echo Uri::base();
        echo "/assets/img/product/variant/soyjoy-variant-hawthorn-berry.png\"> </div>
                            <div class=\"small-2 columns animated fadeInUpShort\" data-id=\"6\"><img src=\"";
        // line 60
        echo Uri::base();
        echo "/assets/img/product/variant/soyjoy-almond-and-chocolate.png\"> </div>
                        </div>
                    </div>
                    <div class=\"medium-6 columns\">
                        <div class=\"columns\">
                            <p class=\"variant-title\">VARIAN RASA</p>
                            <div class=\"line\"></div>
                            <ul class=\"variant-name medium-6 end columns animatedParent\" data-sequence=\"100\">
                                <li class=\"animated fadeInUp\" data-id=\"1\">Strawberry</li>
                                <li class=\"animated fadeInUp\" data-id=\"2\">Peanut</li>
                                <li class=\"animated fadeInUp\" data-id=\"3\">Raisin Almond</li>
                                <li class=\"animated fadeInUp\" data-id=\"4\">Banana</li>
                                <li class=\"animated fadeInUp\" data-id=\"5\">Hawthorn Berry</li>
                                <li class=\"animated fadeInUp\" data-id=\"6\">Almond & Chocolate</li>
                            </ul>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <div class=\"columns\">
                <div class=\"columns\">
                    <div class=\"columns product-link\">
                        <div class=\"product-button\">
                            <a href=\"http://www.soyjoy.co.id/home\" target=\"_blank\" class=\"button\">GO TO <br/>PRODUCT WEBSITE</a>
                            <div style=\"border-bottom: 4px solid #ffffff ; width: 60px\"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
";
    }

    // line 93
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 94
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    ";
        // line 95
        echo Asset::js("css3-animate-it.js");
        echo "
";
    }

    public function getTemplateName()
    {
        return "soyjoy.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  186 => 95,  181 => 94,  178 => 93,  141 => 60,  137 => 59,  133 => 58,  129 => 57,  125 => 56,  121 => 55,  109 => 46,  91 => 35,  79 => 27,  67 => 19,  56 => 12,  51 => 9,  48 => 8,  42 => 6,  38 => 5,  33 => 4,  30 => 3,);
    }
}
