<?php

/* ionessence.twig */
class __TwigTemplate_a52d2caa6c60dc1e379c4a8e8f552b37560b6af140173ada3c21beb2b7bff565 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        echo Asset::css("custom/animations.css");
        echo "
    ";
        // line 5
        $this->displayParentBlock("frontend_css", $context, $blocks);
        echo "
    ";
        // line 6
        echo Asset::css("custom/product.css");
        echo "
";
    }

    // line 8
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 9
        echo "    <div class=\"ionessense columns\">
        <div class=\"columns banner text-center\">
            <img data-interchange=\"[";
        // line 11
        echo Uri::base();
        echo "/assets/img/product/banner-ionessense-small.png, small], [";
        echo Uri::base();
        echo "/assets/img/product/banner-ionessense-medium.png, medium], [";
        echo Uri::base();
        echo "/assets/img/product/banner-ionessense-new.png, large]\">
            <div class=\"columns sub-menu-product\" data-equalizer=\"menu-ionessense\">
                <div class=\"medium-2 medium-offset-3 small-4 columns\">
                    <a href=\"";
        // line 14
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/brand/pocari-sweat\">
                        <div class=\"columns pocari-sweat-color\" data-equalizer-watch=\"menu-ionessense\">
                            Pocari Sweat
                        </div>
                    </a>
                </div>
                <div class=\"medium-2 small-4 columns\">
                    <a href=\"";
        // line 21
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/brand/soyjoy\">
                        <div class=\"columns soyjoy-color\" data-equalizer-watch=\"menu-ionessense\">
                            Soy Joy
                        </div>
                    </a>
                </div>
                <div class=\"medium-2 small-4 end columns\">
                    <a href=\"";
        // line 28
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/brand/ionessence\">
                        <div class=\"columns ionesence-color\" data-equalizer-watch=\"menu-ionessense\">
                            Ionessence
                            <span class=\"active-menu\"></span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"columns product-detail\" data-equalizer=\"product-detail\">
                <div class=\"medium-6 columns\">
                    <div class=\"columns product-desc animatedParent\" data-sequence=\"300\" data-equalizer-watch=\"product-detail\">
                        <div class=\"title  animated fadeInUp\" data-id=\"1\">
                            <p>IONESSENCE</p>
                            <div style=\"width: 110px; height: 3px;background: #ffffff;margin-top: 23px;margin-bottom: 15px\"></div>
                        </div>
                        <p class=\"animated fadeInUp\" data-id=\"2\">IONESSENCE adalah minuman hipotonik rendah kalori (11 kcal/100mL) Untuk mencegah dehidrasi dan menjaga kelembapan kulit.</p>
                    </div>
                </div>
                <div class=\"medium-6 columns animatedParent\">
                    <div class=\"columns product-desc-img animated fadeInUp delay-250\">
                        <img src=\"";
        // line 50
        echo Uri::base();
        echo "/assets/img/product/ionessence.jpg\" data-equalizer-watch=\"product-detail\">
                    </div>
                </div>
            </div>
            <div class=\"columns\">
                <div class=\"columns\">
                    <div class=\"product-variant animatedParent\">
                        <a href=\"https://ionessence.co.id/#\"><img class=\"animated fadeInUp\" data-interchange=\"[";
        // line 57
        echo Uri::base();
        echo "/assets/img/product/variant/ionessence-desc-small.jpg, small], [";
        echo Uri::base();
        echo "/assets/img/product/variant/ionessence-desc-medium.jpg, medium], [";
        echo Uri::base();
        echo "/assets/img/product/variant/ionessence-desc.jpg, large]\"></a>
                    </div>
                </div>
            </div>
            <div class=\"columns\">
                <div class=\"columns\">
                    <div class=\"columns product-link\">
                        <div class=\"product-button\">
                            <a href=\"https://ionessence.co.id/\" target=\"_blank\" class=\"button\">GO TO <br/>PRODUCT WEBSITE</a>
                            <div style=\"border-bottom: 4px solid #ffffff ; width: 60px\"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
";
    }

    // line 75
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 76
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    ";
        // line 77
        echo Asset::js("css3-animate-it.js");
        echo "
";
    }

    public function getTemplateName()
    {
        return "ionessence.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  157 => 77,  152 => 76,  149 => 75,  123 => 57,  113 => 50,  87 => 28,  76 => 21,  65 => 14,  55 => 11,  51 => 9,  48 => 8,  42 => 6,  38 => 5,  33 => 4,  30 => 3,);
    }
}
