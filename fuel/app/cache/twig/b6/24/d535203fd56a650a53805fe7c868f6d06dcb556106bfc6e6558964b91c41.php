<?php

/* life_at_otsuka_new.twig */
class __TwigTemplate_b624d535203fd56a650a53805fe7c868f6d06dcb556106bfc6e6558964b91c41 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend_new.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend_new.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("frontend_css", $context, $blocks);
        echo "
    ";
        // line 5
        echo Asset::css("custom/lifeatotsuka_new.css");
        echo "
";
    }

    // line 7
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 8
        echo "    <div class=\"expanded row custom-left career\">
        <div class=\"medium-12 columns title\">
            <h2>LIFE AT AMERTA INDAH OTSUKA</h2>
        </div>
        <div class=\"row banner\">
            <div class=\"medium-12 columns end nopadd\">
                <img src=\"";
        // line 14
        echo Uri::base();
        echo "/assets/css/images/banner-lifeatotsuka.jpg\"/>
                <div class=\"medium-4 small-10 columns banner-desc\">
                    <div class=\"desc-content\">
                        ";
        // line 17
        echo Lang::get("lao_title");
        echo "
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class=\"expanded row\">
        <div class=\"medium-8 medium-offset-2 end columns custom-content\" data-equalizer=\"direktur\" data-equalize-on=\"large\">
            <h2>";
        // line 25
        echo Lang::get("lao_vision");
        echo "</h2>
            ";
        // line 26
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["leadership_vision"]) ? $context["leadership_vision"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["vision"]) {
            // line 27
            echo "                <div class=\"large-6 columns\" data-equalizer-watch=\"direktur\">
                    ";
            // line 29
            echo "                    <img src=\"";
            echo Uri::base();
            echo "/assets/css/images/prayugo-gunawan.jpg\"/>
                </div>
                <div class=\"large-6 columns presiden-quote\" data-equalizer-watch=\"direktur\" style=\"position: relative;background: #FFFFFF\">
                    <div>
                        <div class=\"quote-content\">
                            ";
            // line 34
            echo Lang::get("lao_msg");
            echo "
                            <span>";
            // line 35
            echo $this->getAttribute((isset($context["vision"]) ? $context["vision"] : null), "name");
            echo ", ";
            echo $this->getAttribute((isset($context["vision"]) ? $context["vision"] : null), "position");
            echo "</span>
                        </div>
                    </div>
                    <div class=\"ornamen\" style=\"\"></div>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['vision'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 41
        echo "        </div>
        <div class=\"medium-8 medium-offset-2 end columns custom-content lao-filosofi\">
            <h2>";
        // line 43
        echo Lang::get("lao_filosofi2");
        echo "</h2>
            <div class=\"title-section\">
                ";
        // line 45
        echo Lang::get("lao_filosofi");
        echo "
            </div>
            <div class=\"row\">
            ";
        // line 48
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["leadership_philosophy"]) ? $context["leadership_philosophy"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["philosophy"]) {
            // line 49
            echo "                <div class=\"medium-4 columns\">
                    <img style=\"width: 100%\" src=\"";
            // line 50
            echo Uri::base();
            echo "media/lao-photo/";
            echo $this->getAttribute((isset($context["philosophy"]) ? $context["philosophy"] : null), "image");
            echo "\" alt=\"";
            echo $this->getAttribute((isset($context["philosophy"]) ? $context["philosophy"] : null), "title");
            echo "\"/>
                    <div class=\"caption\">";
            // line 51
            echo $this->getAttribute((isset($context["philosophy"]) ? $context["philosophy"] : null), "title");
            echo "</div>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['philosophy'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 54
        echo "                ";
        // line 62
        echo "            </div>
            <div class=\"desc\">
                ";
        // line 64
        echo Lang::get("lao_desc");
        echo "
            </div>
            <div class=\"row sub-title\">
                <h2>";
        // line 67
        echo Lang::get("lao_title2");
        echo "</h2>
                <div class=\"title-section\">
                    ";
        // line 69
        echo Lang::get("lao_desc2");
        echo "
                </div>
            </div>
            <div class=\"row sub-title\">
                <h2>";
        // line 73
        echo Lang::get("lao_title3");
        echo "</h2>
                <div class=\"title-section\">
                    ";
        // line 75
        echo Lang::get("lao_desc3");
        echo "
                </div>
            </div>
        </div>
    </div>
    <div class=\"expanded row cerita\" data-equalizer=\"cerita\">
        <div class=\"medium-8 medium-offset-2 end columns custom-content \">
            <div class=\"medium-12 columns\">
                <div class=\"columns\">
                    <div class=\"columns title\">";
        // line 84
        echo Lang::get("lao_story");
        echo "</div>
                </div>
            </div>
            <div style=\"clear: both;\"></div>
            <div class=\"row collapse show-for-medium\">
                <div class=\"medium-12 columns cerita-wrap\">
                    <ul class=\"tabs\" id=\"example-vert-tabs\" data-tabs>
                    ";
        // line 91
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["lao_data"]) ? $context["lao_data"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["lao"]) {
            // line 92
            echo "                        <li class=\"medium-3 columns tabs-title ";
            echo (($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "first")) ? ("is-active") : (""));
            echo "\">
                            <a href=\"#panel";
            // line 93
            echo $this->getAttribute((isset($context["lao"]) ? $context["lao"] : null), "count");
            echo "v\" aria-selected=\"true\" data-equalizer-watch=\"cerita\">
                                <div>
                                    <img src=\"";
            // line 95
            echo Uri::base();
            echo "media/lao-photo/";
            echo $this->getAttribute((isset($context["lao"]) ? $context["lao"] : null), "photo");
            echo "\" alt=\"";
            echo $this->getAttribute((isset($context["lao"]) ? $context["lao"] : null), "name");
            echo "\"/>
                                    <div class=\"columns bio\">
                                        <p>";
            // line 97
            echo $this->getAttribute((isset($context["lao"]) ? $context["lao"] : null), "name");
            echo "</p>
                                        <p>";
            // line 98
            echo $this->getAttribute((isset($context["lao"]) ? $context["lao"] : null), "job");
            echo ", ";
            echo $this->getAttribute((isset($context["lao"]) ? $context["lao"] : null), "location");
            echo "</p>
                                    </div>
                                    <div style=\"clear: both\"></div>
                                </div>
                            </a>
                        </li>
                    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lao'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 105
        echo "                    </ul>
                </div>
                <div class=\"medium-12 columns \">
                    <div class=\"tabs-content\" data-tabs-content=\"example-vert-tabs\">
                    ";
        // line 109
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["lao_data"]) ? $context["lao_data"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["lao"]) {
            // line 110
            echo "                        <div class=\"tabs-panel ";
            echo (($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "first")) ? ("is-active") : (""));
            echo "\" id=\"panel";
            echo $this->getAttribute((isset($context["lao"]) ? $context["lao"] : null), "count");
            echo "v\">
                            <img class=\"quote\" src=\"";
            // line 111
            echo Uri::base();
            echo "/assets/img/home/quote.jpg\"/>
                            <p>";
            // line 112
            echo $this->getAttribute((isset($context["lao"]) ? $context["lao"] : null), "story");
            echo "</p>
                        </div>
                    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lao'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 115
        echo "                    </div>
                </div>
            </div>
            <div class=\"row collapse hide-for-medium\">
                <ul class=\"accordion\" data-accordion>
                    ";
        // line 120
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["lao_data"]) ? $context["lao_data"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["lao"]) {
            // line 121
            echo "                        <li class=\"accordion-item ";
            echo (($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "first")) ? ("is-active") : (""));
            echo "\" data-accordion-item>
                            <a href=\"#\" class=\"accordion-title\">";
            // line 122
            echo $this->getAttribute((isset($context["lao"]) ? $context["lao"] : null), "name");
            echo "</a>
                            <div class=\"accordion-content\" data-tab-content>
                                <div>
                                    <div class=\"text-center\">
                                        <img src=\"";
            // line 126
            echo Uri::base();
            echo "media/lao-photo/";
            echo $this->getAttribute((isset($context["lao"]) ? $context["lao"] : null), "photo");
            echo "\" alt=\"";
            echo $this->getAttribute((isset($context["lao"]) ? $context["lao"] : null), "name");
            echo "\"/>
                                    </div>
                                    <div class=\"columns bio\">
                                        <p>";
            // line 129
            echo $this->getAttribute((isset($context["lao"]) ? $context["lao"] : null), "name");
            echo "</p>
                                        <p>";
            // line 130
            echo $this->getAttribute((isset($context["lao"]) ? $context["lao"] : null), "job");
            echo ", ";
            echo $this->getAttribute((isset($context["lao"]) ? $context["lao"] : null), "location");
            echo "</p>
                                        <div class=\"lao-story\">
                                            ";
            // line 132
            echo $this->getAttribute((isset($context["lao"]) ? $context["lao"] : null), "story");
            echo "
                                        </div>
                                    </div>
                                    <div style=\"clear: both\"></div>
                                </div>
                            </div>
                        </li>
                    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lao'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 140
        echo "                    <!-- ... -->
                </ul>
            </div>
            <div class=\"lao-button\">
                <a href=\"";
        // line 144
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/career-new\" class=\"button\">";
        echo Lang::get("lao_join");
        echo " <span><img src=\"";
        echo Uri::base();
        echo "/assets/css/images/explorion-arrow.png\" alt=\"\" title=\"\"/></span></a>
            </div>
        </div>
    </div>
";
    }

    // line 149
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 150
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    <script>
        \$( window ).load(function() {
        });
    </script>

";
    }

    public function getTemplateName()
    {
        return "life_at_otsuka_new.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  416 => 150,  413 => 149,  399 => 144,  393 => 140,  371 => 132,  364 => 130,  360 => 129,  350 => 126,  343 => 122,  338 => 121,  321 => 120,  314 => 115,  297 => 112,  293 => 111,  286 => 110,  269 => 109,  263 => 105,  240 => 98,  236 => 97,  227 => 95,  222 => 93,  217 => 92,  200 => 91,  190 => 84,  178 => 75,  173 => 73,  166 => 69,  161 => 67,  155 => 64,  151 => 62,  149 => 54,  140 => 51,  132 => 50,  129 => 49,  125 => 48,  119 => 45,  114 => 43,  110 => 41,  96 => 35,  92 => 34,  83 => 29,  80 => 27,  76 => 26,  72 => 25,  61 => 17,  55 => 14,  47 => 8,  44 => 7,  38 => 5,  33 => 4,  30 => 3,);
    }
}
