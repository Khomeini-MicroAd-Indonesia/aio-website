<?php

/* home.twig */
class __TwigTemplate_b6d3fc619b889a2a8f86d7bf8dde9907aeb6df7b11ca31ea70fbc82fc745cee9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'back_to_top' => array($this, 'block_back_to_top'),
            'frontend_js' => array($this, 'block_frontend_js'),
            'back_to_top_js' => array($this, 'block_back_to_top_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        echo Asset::css("custom/animations.css");
        echo "
    ";
        // line 5
        $this->displayParentBlock("frontend_css", $context, $blocks);
        echo "
    ";
        // line 6
        echo Asset::css("custom/home.css");
        echo "
    <link href=\"";
        // line 7
        echo Uri::base();
        echo "assets/foundation/foundation-icons/foundation-icons.css\" rel=\"stylesheet\" type=\"text/css\">
";
    }

    // line 9
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 10
        echo "    <div class=\"home-banner\">
        <div class=\"row\" data-equalizer=\"homeslide\">
            <div class=\"large-9 medium-8 small-12 columns no-padding\">
                <div class=\"orbit\" role=\"region\" aria-label=\"Favorite Space Pictures\" data-orbit data-options=\"animInFromLeft:fade-in; animInFromRight:fade-in;\">
                    <ul class=\"orbit-container\">
                        <button class=\"orbit-previous\"><span class=\"show-for-sr\">Previous Slide</span>&#9664;&#xFE0E;</button>
                        <button class=\"orbit-next\"><span class=\"show-for-sr\">Next Slide</span>&#9654;&#xFE0E;</button>
                        ";
        // line 17
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["banner_detail"]) ? $context["banner_detail"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["banner"]) {
            // line 18
            echo "                            <li class=\"is-active orbit-slide callout panel\">
                                <img class=\"orbit-image\" src=\"";
            // line 19
            echo Uri::base();
            echo "/media/homebanner/";
            echo $this->getAttribute((isset($context["banner"]) ? $context["banner"] : null), "image");
            echo "\" data-equalizer-watch=\"homeslide\">
                            </li>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['banner'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "                    </ul>
                    <nav class=\"orbit-bullets\">
                        ";
        // line 24
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["banner_detail"]) ? $context["banner_detail"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["banner"]) {
            // line 25
            echo "                            <button class=\"is-active\" data-slide=\"0\"><span class=\"show-for-sr\">First slide details.</span><span class=\"show-for-sr\">Current Slide</span></button>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['banner'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 27
        echo "                    </nav>
                </div>
            </div>
            <div class=\"large-3 medium-4 columns no-padding thumbs-panel\" data-equalizer-watch=\"homeslide\">
                <div class=\"callout\" data-equalizer=\"bar\">
                    ";
        // line 32
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["sideimage_detail"]) ? $context["sideimage_detail"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["sideimage"]) {
            // line 33
            echo "                        <div class=\"callout thumbs\" data-equalizer-watch=\"bar\">
                            <a href=\"";
            // line 34
            echo Uri::base();
            echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
            echo $this->getAttribute((isset($context["sideimage"]) ? $context["sideimage"] : null), "anchor");
            echo "\">
                                <div class=\"thumbs-wrap\">
                                    <p class=\"title\">";
            // line 36
            echo $this->getAttribute((isset($context["sideimage"]) ? $context["sideimage"] : null), "title");
            echo "</p>
                                    <p class=\"desc\">";
            // line 37
            echo $this->getAttribute((isset($context["sideimage"]) ? $context["sideimage"] : null), "teaser");
            echo "</p>
                                    <span class=\"read-more\"><img src=\"";
            // line 38
            echo Uri::base();
            echo "/assets/img/icon-thumbs.png\"></span>
                                </div>
                                <div class=\"gap\"></div>
                            </a>
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sideimage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 44
        echo "                </div>
            </div>
        </div>
    </div>
    <div class=\"life-at-otsuka\" data-equalizer=\"life-at-otsuka\">
        <div class=\"large-7 medium-7 small-12 columns about\"  data-equalizer-watch=\"life-at-otsuka\">
            ";
        // line 50
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["president_detail"]) ? $context["president_detail"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["detail"]) {
            // line 51
            echo "                <div class=\"about-content\">
                    <div class=\"large-7 large-offset-2 medium-7 columns end\">
                        <div class=\"about-us-title\"><p>About us</p></div>
                        <div class=\"about-us\">";
            // line 54
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "msg1");
            echo "</div>
                        <div class=\"detail\">";
            // line 55
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "msg2");
            echo "</div>
                        <hr/>
                    </div>
                    <div class=\"large-6 large-offset-3 medium-6 medium-offset-2 small-10 small-offset-1 columns end\">
                        <div class=\"sub-detail\">";
            // line 59
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "msg3");
            echo "</div>
                        <div class=\"quote\">";
            // line 60
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "msg4");
            echo "</div>
                        <div class=\"name-quote\">";
            // line 61
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "name");
            echo ", ";
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "job");
            echo " Amerta Indah Otsuka</div>
                        <a href=\"";
            // line 62
            echo (isset($context["base_url"]) ? $context["base_url"] : null);
            echo "about-us\" class=\"selengkapnya\">";
            echo Lang::get("txt_home07");
            echo "</a>
                    </div>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['detail'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 66
        echo "        </div>
        <div class=\"large-5 medium-5 hide-for-small-only columns about-right\">
            <div  data-equalizer-watch=\"life-at-otsuka\">

            </div>
        </div>
        <div class=\"\">
            <div class=\"large-7 large-offset-5 medium-8 medium-offset-4 small-12 columns index-top\" data-equalizer=\"comment-otsuka\">
                <div class=\"row comment-otsuka\">
                    <div class=\"large-5 medium-6 small-12 columns comment-left animatedParent\" data-equalizer-watch=\"comment-otsuka\" data-sequence=\"500\">
                        <div class=\"animated fadeInUpShort\" data-id=\"1\">
                            <div class=\"left-title\">Life at Otsuka</div>
                            <hr/>
                            ";
        // line 79
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["loa_detail"]) ? $context["loa_detail"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["loa"]) {
            // line 80
            echo "                                <div class=\"left-desc\">
                                    ";
            // line 81
            echo twig_slice($this->env, $this->getAttribute((isset($context["loa"]) ? $context["loa"] : null), "story_lead"), 0, 120);
            echo "...
                                </div><br class=\"br\"/>
                                <div class=\"name-quote\">";
            // line 83
            echo $this->getAttribute((isset($context["loa"]) ? $context["loa"] : null), "name");
            echo ", ";
            echo $this->getAttribute((isset($context["loa"]) ? $context["loa"] : null), "job");
            echo " Amerta Indah Otsuka ";
            echo $this->getAttribute((isset($context["loa"]) ? $context["loa"] : null), "location");
            echo ".</div>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['loa'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 85
        echo "                        </div>
                        <div>
                            <a href=\"";
        // line 87
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "lifeatotsuka\" class=\"selengkapnya animated growIn\" data-id=\"2\">";
        echo Lang::get("txt_home07");
        echo "</a>
                        </div>
                    </div>
                    <div class=\"large-7 medium-6 small-12 columns comment-right\" data-equalizer-watch=\"comment-otsuka\">
                        <img src=\"";
        // line 91
        echo Uri::base();
        echo "assets/img/home/life-at-otsuka-large.jpg\">
                    </div>
                </div>
                <div class=\"new-vacancy animatedParent animateOnce\" data-equalizer=\"new-vacancy\">
                    <div class=\"large-7 medium-7 columns\" data-equalizer-watch=\"new-vacancy\">
                        <div class=\"comment-left\">
                            <div class=\"animated fadeInUpShort\">
                                <p class=\"title\">NEW VACANCY</p>
                                <hr/>
                                <p class=\"sub-title\">";
        // line 100
        echo Lang::get("txt_home01");
        echo "</p>
                                <p class=\"sub-title-bold\">";
        // line 101
        echo Lang::get("txt_home02");
        echo "</p>
                                <ul>
                                    ";
        // line 103
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["career_detail"]) ? $context["career_detail"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["detail"]) {
            // line 104
            echo "                                        ";
            if (($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index0") <= 2)) {
                // line 105
                echo "                                            <li>";
                echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "name");
                echo " - ";
                echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "level");
                echo " (";
                echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "location");
                echo ")</li>
                                        ";
            }
            // line 107
            echo "                                    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['detail'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 108
        echo "                                </ul>
                                <p style=\"text-align: right\"><a href=\"";
        // line 109
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "career\" class=\"link-other\">>";
        echo Lang::get("txt_home04");
        echo "</a></p>
                            </div>
                        </div>
                    </div>
                    <div class=\"large-5 medium-5 columns comment-right\" data-equalizer-watch=\"new-vacancy\">
                        <p class=\"title text-center\">";
        // line 114
        echo Lang::get("txt_home03");
        echo "</p>
                        <div class=\"large-10 medium-10 columns no-padding join-us-content\" data-equalizer=\"email\">
                            <input id=\"email\" type=\"email\" style=\"padding: 2px;\" pattern=\"^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}\$\" title=\"Please input your email\"/>
                            <div class=\"large-5 medium-5 small-6 columns no-padding join-us\" data-equalizer-watch=\"email\">
                                <button id=\"btn-join\" class=\"selengkapnya-small\" onclick=\"subscribe();\">JOIN US</button>
                            </div>
                            <div class=\"large-7 medium-7 small-6 columns no-padding\" data-equalizer-watch=\"email\">
                                <img src=\"";
        // line 121
        echo Uri::base();
        echo "assets/img/home/new-vacancy.jpg\">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class=\"large-12 medium-12 columns our-product\">
        <div class=\"large-5 large-offset-7 medium-5 medium-offset-7 columns animatedParent containingBlock\">
            <div class=\"animated fadeInUp containingBlock-desc\">
                <p class=\"title\">OUR<br/>PRODUCTS</p>
                <p class=\"sub-title\">WE CREATE QUALITY PRODUCTS<br/> FOR YOUR BETTER HEALTH</p>
            </div>
        </div>
    </div>
    <div class=\"large-12 medium-12 small-12 columns thumb-product animatedParent animateOnce\" data-sequence=\"300\">
        <div class=\"large-4 medium-4 small-4 columns animated fadeInUpShort\" data-id=\"1\">
            <a href=\"";
        // line 139
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/brand/pocari-sweat\">
                <img src=\"";
        // line 140
        echo Uri::base();
        echo "assets/img/home/pocari-sweat.jpg\">
            </a>
        </div>
        <div class=\"large-4 medium-4 small-4 columns animated fadeInUpShort\" data-id=\"2\">
            <a href=\"";
        // line 144
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/brand/soyjoy\">
                <img src=\"";
        // line 145
        echo Uri::base();
        echo "assets/img/home/soyjoy.jpg\">
            </a>
        </div>
        <div class=\"large-4 medium-4 small-4 columns animated fadeInUpShort\" data-id=\"3\">
            <a href=\"";
        // line 149
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/brand/ionessence\">
                <img src=\"";
        // line 150
        echo Uri::base();
        echo "assets/img/home/ionessence.jpg\">
            </a>
        </div>
    </div>
    <div class=\"factory-visit grey\">
        <div class=\"row\">
            ";
        // line 157
        echo "            <div class=\"title\">
                <p>EXPLORION TOUR</p>
            </div>
            <div class=\"factory-content\">
                <div class=\"large-12 columns animatedParent\">
                    <div class=\"large-5 medium-5 small-12 end columns animated fadeInUpShort\">
                        ";
        // line 163
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["factorytour_detail"]) ? $context["factorytour_detail"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["detail"]) {
            // line 164
            echo "                            <p class=\"sub-title\">";
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "title");
            echo "</p>
                            <div class=\"description\">";
            // line 165
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "content");
            echo "</div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['detail'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 167
        echo "                    </div>
                </div>
            </div>
            <div class=\"columns\" data-equalizer=\"factory-map\">
                <div class=\"large-6 medium-12 small-12 columns factory-map aligner\" data-equalizer-watch=\"factory-map\">
                    <div class=\"large-7 medium-7 small-12 columns Aligner-item Aligner-item--fixed\">
                        <div class=\"small-6 columns\">
                            <img src=\"";
        // line 174
        echo Uri::base();
        echo "/assets/img/home/factory-visit-1.jpg\">
                        </div>
                        <div class=\"small-6 columns\">
                            <img src=\"";
        // line 177
        echo Uri::base();
        echo "/assets/img/home/factory-visit-2.jpg\">
                        </div>
                    </div>
                    <div class=\"large-5 medium-5 small-12 columns description animatedParent\" data-sequence=\"500\">
                        <div class=\"animated fadeInUpShort\" data-id=\"1\">
                            <p class=\"title\">";
        // line 182
        echo Lang::get("txt_home05");
        echo "</p>
                            <p class=\"sub-title\">";
        // line 183
        echo Lang::get("txt_home06");
        echo "</p>
                        </div>
                        <a href=\"";
        // line 185
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "explorion-advanture\" class=\"selengkapnya-small animated growIn\" data-id=\"2\">";
        echo Lang::get("txt_home08");
        echo "</a>
                    </div>
                </div>
                <div class=\"large-6 medium-12 small-12 columns gallery-tour\">
                    <div data-equalizer=\"gallery-tour\">
                        <div class=\"large-3 medium-3 small-12 columns gallery-tour-content\"  data-equalizer-watch=\"factory-map\">
                            <a href=\"#\">
                                <div class=\"all aligner\">
                                    <div class=\"Aligner-item Aligner-item--fixed\">
                                        <p class=\"title\">GALLERY<br/>EXPLORION TOUR</p>
                                        <a href=\"";
        // line 195
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "gallery-detail\" class=\"see-all\">SEE ALL ></a>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <div class=\"large-9 medium-9 small-12 columns thumb\" data-equalizer-watch=\"factory-map\">
                            ";
        // line 202
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["factorytour_images"]) ? $context["factorytour_images"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
            // line 203
            echo "                                <div class=\"large-6 medium-6 small-6 columns thumb-gallery first\">
                                    <a href=\"";
            // line 204
            echo Uri::base();
            echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
            echo "/gallery-detail\">
                                        ";
            // line 205
            if (($this->getAttribute((isset($context["image"]) ? $context["image"] : null), "seq") < 3)) {
                // line 206
                echo "                                            <img src=\"";
                echo Uri::base();
                echo "media/gallery/";
                echo ("thumb-" . $this->getAttribute((isset($context["image"]) ? $context["image"] : null), "filename"));
                echo "\">
                                        ";
            }
            // line 208
            echo "                                    </a>
                                </div>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 211
        echo "                        </div>
                    </div>
                </div>
            </div>
            ";
        // line 216
        echo "        </div>
    </div>
    <div class=\"csr\" data-equalizer=\"csr\">
        <div class=\"large-6 medium-12 columns csr-banner animatedParent\">
            <img src=\"";
        // line 220
        echo Uri::base();
        echo "assets/img/home/csr-banner.jpg\" data-equalizer-watch=\"csr\">
            <div class=\"large-12 medium-12 columns title-section animated fadeInUpShort\">
                <p class=\"title\">CORPORATE<br/>SOCIAL<br/>RESPONSIBILITY</p>
            </div>
        </div>
        <div class=\"large-6 medium-12 small-12 columns csr-thumb\" data-equalizer-watch=\"csr\">
            <div class=\"columns animatedParent animateOnce\"  style=\"background: #fff\" data-equalizer=\"csr-thumb\" data-sequence=\"1000\">
                ";
        // line 227
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["csr_detail"]) ? $context["csr_detail"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["detail"]) {
            // line 228
            echo "                    <div class=\"columns\" data-equalizer-watch=\"csr-thumb\">
                        ";
            // line 230
            echo "                        <div class=\"large-12 medium-12 columns aligner white\">
                            <div class=\"large-6 medium-12 small-12 columns csr-desc aligner-item aligner-item--fixed\">
                                <div class=\"animated fadeInUpShort\" data-id=\"1\">
                                    <div class=\"title\">
                                        ";
            // line 234
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "title");
            echo "
                                    </div>
                                    <div class=\"date\">
                                        ";
            // line 237
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "date");
            echo "
                                    </div>
                                    ";
            // line 239
            $context["highlight"] = twig_split_filter($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "content"), ".");
            // line 240
            echo "                                    <div class=\"description\">
                                        ";
            // line 241
            echo $this->getAttribute((isset($context["highlight"]) ? $context["highlight"] : null), 0, array(), "array");
            echo ".. <a href=\"";
            echo Uri::base();
            echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
            echo "/our-event/\" class=\"red\">Read more</a>
                                    </div>

                                </div>
                            </div>
                            <div class=\"large-6 medium-6 small-12 columns csr-pic\">
                                <img src=\"";
            // line 247
            echo Uri::base();
            echo "media/csr/thumbnail/";
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "img");
            echo "\">
                            </div>
                        </div>

                        ";
            // line 252
            echo "                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['detail'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 254
        echo "            </div>
            <div class=\"large-12 medium-12 columns\">
                <a href=\"";
        // line 256
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/our-event\">
                    <p class=\"lain\">";
        // line 257
        echo Lang::get("txt_home04");
        echo "</p>
                </a>
            </div>
        </div>
        <div class=\"show-for-small-only\">
            ";
        // line 262
        $this->displayBlock('back_to_top', $context, $blocks);
        // line 265
        echo "        </div>
    </div>
    <div class=\"reveal\" id=\"exampleModal1\" data-reveal>
        <p>Terima kasih atas partisipasi Anda telah berlangganan newsletter kami</p>
        <button class=\"close-button\" data-close aria-label=\"Close modal\" type=\"button\">
            <span aria-hidden=\"true\">&times;</span>
        </button>
    </div>
    <div class=\"reveal\" id=\"exampleModal2\" data-reveal>
        <p>Mohon hanya masukkan alamat e-mail, dengan format: youremail@emailaddress.com </p>
        <button class=\"close-button\" data-close aria-label=\"Close modal\" type=\"button\">
            <span aria-hidden=\"true\">&times;</span>
        </button>
    </div>

";
    }

    // line 262
    public function block_back_to_top($context, array $blocks = array())
    {
        // line 263
        echo "                ";
        $this->displayParentBlock("back_to_top", $context, $blocks);
        echo "
            ";
    }

    // line 281
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 282
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    ";
        // line 283
        echo Asset::js("jquery.maskedinput.js");
        echo "
    ";
        // line 284
        echo Asset::js("css3-animate-it.js");
        echo "
    <script>


        \$(document).foundation({
            equalizer: {
                equalize_on_stack: true
            }
        });
        
        function subscribe(){

            var regex = /^([a-zA-Z0-9_.+-])+\\@(([a-zA-Z0-9-])+\\.)+([a-zA-Z0-9]{2,4})+\$/;
            var email = document.getElementById('email').value;
            ";
        // line 299
        echo "
            if(regex.test(email) === true) {

                \$('#exampleModal1').foundation('open');
                \$.ajax({
                    type: \"POST\",
                    url: \"";
        // line 305
        echo Uri::base();
        echo "subscribe?email=\"+email, // get-city?id=\"+str,
                    data: {
                        'email': email
                    },
                    success: function(){
                        document.getElementById('email').value = '';
                    },
                    error: function(){
                        event.preventDefault();
                    }
                });

            }else {

                \$('#exampleModal2').foundation('open');

            }


        }

        \$('input').blur(function(evt) {
            evt.target.checkValidity();
        }).bind('invalid', function(event) {
            alert('Put in the correct email form: username@email.com');
        });
        ";
        // line 331
        $this->displayBlock('back_to_top_js', $context, $blocks);
        // line 334
        echo "    </script>

";
    }

    // line 331
    public function block_back_to_top_js($context, array $blocks = array())
    {
        // line 332
        echo "        ";
        $this->displayParentBlock("back_to_top_js", $context, $blocks);
        echo "
        ";
    }

    public function getTemplateName()
    {
        return "home.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  709 => 332,  706 => 331,  700 => 334,  698 => 331,  669 => 305,  661 => 299,  644 => 284,  640 => 283,  635 => 282,  632 => 281,  625 => 263,  622 => 262,  603 => 265,  601 => 262,  593 => 257,  588 => 256,  584 => 254,  577 => 252,  568 => 247,  556 => 241,  553 => 240,  551 => 239,  546 => 237,  540 => 234,  534 => 230,  531 => 228,  527 => 227,  517 => 220,  511 => 216,  505 => 211,  497 => 208,  489 => 206,  487 => 205,  482 => 204,  479 => 203,  475 => 202,  465 => 195,  450 => 185,  445 => 183,  441 => 182,  433 => 177,  427 => 174,  418 => 167,  410 => 165,  405 => 164,  401 => 163,  393 => 157,  384 => 150,  379 => 149,  372 => 145,  367 => 144,  360 => 140,  355 => 139,  334 => 121,  324 => 114,  314 => 109,  311 => 108,  297 => 107,  287 => 105,  284 => 104,  267 => 103,  262 => 101,  258 => 100,  246 => 91,  237 => 87,  233 => 85,  221 => 83,  216 => 81,  213 => 80,  209 => 79,  194 => 66,  182 => 62,  176 => 61,  172 => 60,  168 => 59,  161 => 55,  157 => 54,  152 => 51,  148 => 50,  140 => 44,  128 => 38,  124 => 37,  120 => 36,  113 => 34,  110 => 33,  106 => 32,  99 => 27,  92 => 25,  88 => 24,  84 => 22,  73 => 19,  70 => 18,  66 => 17,  57 => 10,  54 => 9,  48 => 7,  44 => 6,  40 => 5,  35 => 4,  32 => 3,);
    }
}
