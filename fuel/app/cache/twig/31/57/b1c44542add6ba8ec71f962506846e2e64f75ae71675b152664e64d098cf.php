<?php

/* csr_peduli_lingkungan.twig */
class __TwigTemplate_3157b1c44542add6ba8ec71f962506846e2e64f75ae71675b152664e64d098cf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'about_menu_us' => array($this, 'block_about_menu_us'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'back_to_top' => array($this, 'block_back_to_top'),
            'frontend_js' => array($this, 'block_frontend_js'),
            'back_to_top_js' => array($this, 'block_back_to_top_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("frontend_css", $context, $blocks);
        echo "
    ";
        // line 5
        echo Asset::css("custom/csr_new.css");
        echo "
";
    }

    // line 7
    public function block_about_menu_us($context, array $blocks = array())
    {
        echo ".test";
    }

    // line 8
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 9
        echo "    <div class=\"csr-content\">
        ";
        // line 10
        $this->env->loadTemplate("template_csr_new.twig")->display($context);
        // line 11
        echo "        <div class=\"row\">
            <div class=\"columns csr-description\">
            <p class=\"title\">SATU HATI PEDULI LINGKUNGAN</p>
            <div style=\"border-bottom: 1px solid #ced2d6\"></div>
                <div class=\"sub-title\">
                    <p>Keadaan alam memberikan pengaruh yang besar pada kehidupan manusia. Melihat kenyataan ini, PT Amerta Indah Otsuka ikut memikul tanggung jawab dalam pelestarian lingkungan alam demi terbangunnya kehidupan yang lebih bermutu.</p>
                </div>
                <div class=\"banner-csr\"><img src=\"";
        // line 18
        echo Uri::base();
        echo "/assets/img/csr-new/satu-hati-peduli-lingkungan.jpg\"/></div>
            </div>
            <div class=\"columns\">
                <div class=\"desc-text\">
                    <div class=\"sub-title-program\">
                        <p>";
        // line 23
        echo Lang::get("txt_csr01");
        echo "</p>
                    </div>
                    <p>";
        // line 25
        echo Lang::get("txt_csr02");
        echo "</p>
                    <div class=\"sub-title-program\">
                        <p>";
        // line 27
        echo Lang::get("txt_csr03");
        echo "</p>
                    </div>
                    <p>";
        // line 29
        echo Lang::get("txt_csr04");
        echo "</p>
                </div>
            </div>
        </div>
        <div class=\"show-for-small-only\">
            ";
        // line 34
        $this->displayBlock('back_to_top', $context, $blocks);
        // line 37
        echo "        </div>
    </div>
";
    }

    // line 34
    public function block_back_to_top($context, array $blocks = array())
    {
        // line 35
        echo "                ";
        $this->displayParentBlock("back_to_top", $context, $blocks);
        echo "
            ";
    }

    // line 41
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 42
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    <script type=\"text/javascript\">
        ";
        // line 44
        $this->displayBlock('back_to_top_js', $context, $blocks);
        // line 47
        echo "    </script>
";
    }

    // line 44
    public function block_back_to_top_js($context, array $blocks = array())
    {
        // line 45
        echo "        ";
        $this->displayParentBlock("back_to_top_js", $context, $blocks);
        echo "
        ";
    }

    public function getTemplateName()
    {
        return "csr_peduli_lingkungan.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 45,  135 => 44,  130 => 47,  128 => 44,  122 => 42,  119 => 41,  112 => 35,  109 => 34,  103 => 37,  101 => 34,  93 => 29,  88 => 27,  83 => 25,  78 => 23,  70 => 18,  61 => 11,  59 => 10,  56 => 9,  53 => 8,  47 => 7,  41 => 5,  36 => 4,  33 => 3,);
    }
}
