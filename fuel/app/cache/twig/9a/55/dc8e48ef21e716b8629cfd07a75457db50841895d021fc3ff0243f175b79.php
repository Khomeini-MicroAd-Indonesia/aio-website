<?php

/* list.twig */
class __TwigTemplate_9a55dc8e48ef21e716b8629cfd07a75457db50841895d021fc3ff0243f175b79 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend_new.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend_new.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("frontend_css", $context, $blocks);
        echo "
    ";
        // line 5
        echo Asset::css("custom/vendor/bootstrap/bootstrap.css");
        echo "
    ";
        // line 6
        echo Asset::css("custom/vendor/animate.min.css");
        echo "
    ";
        // line 7
        echo Asset::css("custom/main.css");
        echo "
";
    }

    // line 9
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 10
        echo "    <section class=\"herosub\" style=\"background-image: url('";
        echo Uri::base();
        echo "assets/img/bg-news.jpg')\">
        <div class=\"container\">
            ";
        // line 12
        if (((isset($context["flag"]) ? $context["flag"] : null) == 1)) {
            // line 13
            echo "                <h2 class=\"herosub-title\"><span>SIARAN PERS</span></h2>
            ";
        } else {
            // line 15
            echo "                <h2 class=\"herosub-title\"><span>BERITA TERKINI</span></h2>
            ";
        }
        // line 17
        echo "        </div>
    </section>
    <!-- //hersub -->

    <div id=\"main\" role=\"main\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-md-10 col-md-offset-1\">
                    <section class=\"news-posts\">
                    ";
        // line 26
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["articles"]) ? $context["articles"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["article"]) {
            echo "        
                        <article class=\"news-post-item\">
                            <figure>
                                <a href=\"";
            // line 29
            echo (((Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null)) . "/news/detail/") . $this->getAttribute((isset($context["article"]) ? $context["article"] : null), "slug"));
            echo "\"><img src=\"";
            echo (((Uri::base() . $this->getAttribute((isset($context["article"]) ? $context["article"] : null), "get_image_path", array(), "method")) . "media/press-room/thumbnail/") . $this->getAttribute((isset($context["article"]) ? $context["article"] : null), "image"));
            echo "\" alt=\"";
            echo $this->getAttribute((isset($context["article"]) ? $context["article"] : null), "title");
            echo "\"></a>
                            </figure>
                            <div class=\"desc\">
                                <header>
                                    <span class=\"post-category\"><a href=\"#\">";
            // line 33
            echo $this->getAttribute((isset($context["article"]) ? $context["article"] : null), "category");
            echo "</a></span>
                                    <h3>
                                        <a href=\"";
            // line 35
            echo (((Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null)) . "/news/detail/") . $this->getAttribute((isset($context["article"]) ? $context["article"] : null), "slug"));
            echo "\">";
            echo $this->getAttribute((isset($context["article"]) ? $context["article"] : null), "title");
            echo "</a>
                                    </h3>
                                    <span class=\"post-date\">Posted on ";
            // line 37
            echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["article"]) ? $context["article"] : null), "date"), "F d, Y");
            echo "</span>
                                </header>
                                <p>";
            // line 39
            echo $this->getAttribute((isset($context["article"]) ? $context["article"] : null), "highlight");
            echo "</p>
                                <div class=\"link-more\">
                                    <a href=\"";
            // line 41
            echo (((Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null)) . "/news/detail/") . $this->getAttribute((isset($context["article"]) ? $context["article"] : null), "slug"));
            echo "\">Baca Selengkapnya</a>
                                </div>
                            </div>
                        </article>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['article'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 46
        echo "                        <!-- //news-post-item -->

                        ";
        // line 100
        echo "                        <!-- //news-post-item -->
                    </section>
                    ";
        // line 102
        if (((isset($context["flag"]) ? $context["flag"] : null) == 1)) {
            // line 103
            echo "                        <div class=\"other-news row\">
                            <article class=\"col-md-5\">
                                <h5 class=\"title-xs\">NARA HUBUNG MEDIA</h5>
                                <p>
                                    Idayatih (Corporate Communication Department)<br> Idayatih@aio.co.id
                                </p>
                            </article>

                            <div class=\"btn-otherpost\">
                                <a href=\"#\">lihat berita lainnya</a>
                            </div>
                        </div>
                    ";
        } else {
            // line 116
            echo "                        <div class=\"btn-otherpost\">
                            <a href=\"#\">lihat berita lainnya</a>
                        </div>
                    ";
        }
        // line 120
        echo "                </div>
            </div>
        </div>
    </div>
    <!-- //main -->
";
    }

    // line 126
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 127
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    ";
        // line 128
        echo Asset::js("custom/libs.min.js");
        echo "
    <script async defer src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyB4b1bKwY-EX4Jl46J4eNz83Te-g8okhgY&callback=initMap\"></script>
    ";
        // line 130
        echo Asset::js("custom/global.js");
        echo "
";
    }

    public function getTemplateName()
    {
        return "list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  187 => 130,  182 => 128,  177 => 127,  174 => 126,  165 => 120,  159 => 116,  144 => 103,  142 => 102,  138 => 100,  134 => 46,  123 => 41,  118 => 39,  113 => 37,  106 => 35,  101 => 33,  90 => 29,  82 => 26,  71 => 17,  67 => 15,  63 => 13,  61 => 12,  55 => 10,  52 => 9,  46 => 7,  42 => 6,  38 => 5,  33 => 4,  30 => 3,);
    }
}
