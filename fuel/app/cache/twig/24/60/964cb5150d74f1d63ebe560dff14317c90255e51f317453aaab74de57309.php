<?php

/* email_factorytour.twig */
class __TwigTemplate_2460964cb5150d74f1d63ebe560dff14317c90255e51f317453aaab74de57309 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'frontend_content' => array($this, 'block_frontend_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('frontend_content', $context, $blocks);
    }

    public function block_frontend_content($context, array $blocks = array())
    {
        // line 2
        echo "    <html>
    <body>
    <div>
        <p>Kepada PT Amerta Indah Otsuka,</p>
        <p>Berikut data yang masuk melalui form Factory Tour di website</p>
        <p>Nama : ";
        // line 7
        echo (isset($context["name"]) ? $context["name"] : null);
        echo "</p>
        <p>Kunjungan ke : ";
        // line 8
        echo (isset($context["factory"]) ? $context["factory"] : null);
        echo "</p>
        <p>Tanggal Kunjungan : ";
        // line 9
        echo (isset($context["visit_date_full"]) ? $context["visit_date_full"] : null);
        echo "</p>
        <p>Waktu Kunjungan : Shift ";
        // line 10
        echo (isset($context["visit_shift"]) ? $context["visit_shift"] : null);
        echo "</p>
        <p>Nama Institusi : ";
        // line 11
        echo (isset($context["institution"]) ? $context["institution"] : null);
        echo "</p>
        <p>Alamat : ";
        // line 12
        echo (isset($context["address"]) ? $context["address"] : null);
        echo "</p>
        <p>Jumlah Pengunjung : ";
        // line 13
        echo (isset($context["visitor"]) ? $context["visitor"] : null);
        echo "</p>
        <p>HP : ";
        // line 14
        echo (isset($context["mobile"]) ? $context["mobile"] : null);
        echo "</p>
        <p>Telepon : ";
        // line 15
        echo (isset($context["landline"]) ? $context["landline"] : null);
        echo "</p>
        <p>Email: ";
        // line 16
        echo (isset($context["email"]) ? $context["email"] : null);
        echo "</p>
        <p>Pesan & Keterangan : ";
        // line 17
        echo (isset($context["message"]) ? $context["message"] : null);
        echo "</p>
    </div>
    </body>
    </html>
";
    }

    public function getTemplateName()
    {
        return "email_factorytour.twig";
    }

    public function getDebugInfo()
    {
        return array (  73 => 17,  69 => 16,  65 => 15,  61 => 14,  57 => 13,  53 => 12,  49 => 11,  45 => 10,  41 => 9,  37 => 8,  33 => 7,  26 => 2,  20 => 1,);
    }
}
