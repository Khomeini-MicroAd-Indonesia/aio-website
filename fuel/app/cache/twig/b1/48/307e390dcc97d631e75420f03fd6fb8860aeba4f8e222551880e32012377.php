<?php

/* shcb.twig */
class __TwigTemplate_b148307e390dcc97d631e75420f03fd6fb8860aeba4f8e222551880e32012377 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend_new.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend_new.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("frontend_css", $context, $blocks);
        echo "
    ";
        // line 5
        echo Asset::css("custom/vendor/bootstrap/bootstrap.css");
        echo "
    ";
        // line 6
        echo Asset::css("custom/vendor/animate.min.css");
        echo "
    ";
        // line 7
        echo Asset::css("custom/main.css");
        echo "
";
    }

    // line 9
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 10
        echo "    <section class=\"herosub\" style=\"background-image: url('";
        echo Uri::base();
        echo "assets/img/banner-csr-shcb.jpg')\">
        <div class=\"container\">
            <h2 class=\"herosub-title\"><span>Tanggung Jawab Sosial Perusahaan⁠⁠⁠⁠</span></h2>
        </div>

        <article class=\"herosub-text\">
            <h2>SATU HATI </h2>
            <p>Wujud kepedulian PT Amerta Indah Otsuka terhadap alam, pendidikan, dan kesehatan masyarakat</p>
        </article>

        <div class=\"csr-nav\">
            <ul>
                <li>
                    <a href=\"";
        // line 23
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/csr/shcb\" class=\"active\"><img src=\"";
        echo Uri::base();
        echo "assets/img/icon-csr-1.png\" alt=\"\"></a>
                </li>
                <li>
                    <a href=\"";
        // line 26
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/csr/shpl\"><img src=\"";
        echo Uri::base();
        echo "assets/img/icon-csr-2.png\" alt=\"\"></a>
                </li>
                <li>
                    <a href=\"";
        // line 29
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/csr/shsb\"><img src=\"";
        echo Uri::base();
        echo "assets/img/icon-csr-3.png\" alt=\"\"></a>
                </li>
            </ul>
        </div>
    </section>
    <!-- //hersub -->

    <div id=\"main\" role=\"main\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-md-10 col-md-offset-1 section-csr\">
                    <h3 class=\"title-xs text-uppercase\">";
        // line 40
        echo Lang::get("shcb_main_title");
        echo "</h3>
                    <p>";
        // line 41
        echo Lang::get("shcb_first_paragraph");
        echo "</p>
                    <p>";
        // line 42
        echo Lang::get("shcb_second_paragraph");
        echo "</p>
                    <div class=\"space45\"></div>
                    <h4 class=\"title-xs text-uppercase\">";
        // line 44
        echo Lang::get("shcb_title");
        echo "</h4>
                    <div class=\"space20\"></div>
                ";
        // line 46
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["activities"]) ? $context["activities"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["activity"]) {
            echo " 
                ";
            // line 47
            if (($this->getAttribute((isset($context["activity"]) ? $context["activity"] : null), "category") == "0")) {
                // line 48
                echo "                    <article class=\"post-item\">
                        <figure>
                            <img src=\"";
                // line 50
                echo Uri::base();
                echo "media/csr/";
                echo $this->getAttribute((isset($context["activity"]) ? $context["activity"] : null), "image");
                echo "\" alt=\"\">
                        </figure>
                        <div class=\"desc\">
                            <h4 class=\"title-xs\">";
                // line 53
                echo $this->getAttribute((isset($context["activity"]) ? $context["activity"] : null), "title");
                echo "</h4>
                            <p>";
                // line 54
                echo $this->getAttribute((isset($context["activity"]) ? $context["activity"] : null), "content");
                echo "</p>
                        </div>
                    </article>
                ";
            }
            // line 58
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['activity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 59
        echo "                </div>
            </div>
        </div>
    </div>
    <!-- //main -->
";
    }

    // line 65
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 66
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    ";
        // line 67
        echo Asset::js("custom/libs.min.js");
        echo "
    <script async defer src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyB4b1bKwY-EX4Jl46J4eNz83Te-g8okhgY&callback=initMap\"></script>
    ";
        // line 69
        echo Asset::js("custom/global.js");
        echo "
";
    }

    public function getTemplateName()
    {
        return "shcb.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  181 => 69,  176 => 67,  171 => 66,  168 => 65,  159 => 59,  153 => 58,  146 => 54,  142 => 53,  134 => 50,  130 => 48,  128 => 47,  122 => 46,  117 => 44,  112 => 42,  108 => 41,  104 => 40,  88 => 29,  80 => 26,  72 => 23,  55 => 10,  52 => 9,  46 => 7,  42 => 6,  38 => 5,  33 => 4,  30 => 3,);
    }
}
