<?php

/* csr_cerdaskan_bangsa.twig */
class __TwigTemplate_10adda5b3830e0ca3d8e73183d069d2b356e23462afe7e8db052d8a9cab55990 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'about_menu_us' => array($this, 'block_about_menu_us'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'back_to_top' => array($this, 'block_back_to_top'),
            'frontend_js' => array($this, 'block_frontend_js'),
            'back_to_top_js' => array($this, 'block_back_to_top_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        echo Asset::css("custom/animations.css");
        echo "
    ";
        // line 5
        $this->displayParentBlock("frontend_css", $context, $blocks);
        echo "
    ";
        // line 6
        echo Asset::css("custom/csr_new.css");
        echo "
";
    }

    // line 8
    public function block_about_menu_us($context, array $blocks = array())
    {
        echo ".test";
    }

    // line 9
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 10
        echo "    <div class=\"csr-content\">
        ";
        // line 11
        $this->env->loadTemplate("template_csr_new.twig")->display($context);
        // line 12
        echo "
        <div class=\"row\">
            <div class=\"columns csr-description\">
            <p class=\"title\">SATU HATI CERDASKAN BANGSA</p>
            <div style=\"border-bottom: 1px solid #ced2d6\"></div>
                <div class=\"sub-title\">
                    <p>Pendidikan adalah pilar dasar pembangunan bangsa. PT Amerta Indah Otsuka berupaya untuk  turut meningkatkan kualitas hidup masyarakat melalui Yayasan Satu Hati Cerdaskan Bangsa.</p>
                </div>
                <div class=\"banner-csr\"><img src=\"";
        // line 20
        echo Uri::base();
        echo "/assets/img/csr-new/satu-hati-cerdaskan-bangsa.png\"/></div>
            </div>
            <div class=\"columns\">
                <div class=\"desc\">
                    <div class=\"desc-text\">
                        <p>Bagi PT Amerta Indah Otsuka pendidikan merupakan modal utama dalam pembentukan Sumber Daya Manusia (SDM) yang berkualitas dan memiliki daya saing global. Otsuka-people creating new products for better health worldwide merupakan filosofi perusahaan dimana kami selalu berkomitmen untuk menghasilkan produk inovatif yang memberikan manfaat  kesehatan bagi masyarakat. Hal ini menjadi landasan kegiatan Corporate Social Responsibility PT. Amerta Indah Otsuka untuk peduli di bidang kesehatan, pendidikan dan lingkungan masyarakat Indonesia.</p>
                        <div class=\"sub-title-program\">
                            <p>Perpustakaan Satu Hati Cerdaskan Bangsa</p>
                        </div>
                        <p>PT Amerta Indah Otsuka berkontribusi dalam meningkatkan kualitas pendidikan di Indonesia dengan membangun 28 perpustakaan dengan lebih dari 124.000 buku yang tersebar mulai dari Sumatera hingga Papua.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"show-for-small-only\">
            ";
        // line 35
        $this->displayBlock('back_to_top', $context, $blocks);
        // line 38
        echo "        </div>
    </div>
";
    }

    // line 35
    public function block_back_to_top($context, array $blocks = array())
    {
        // line 36
        echo "                ";
        $this->displayParentBlock("back_to_top", $context, $blocks);
        echo "
            ";
    }

    // line 42
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 43
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    ";
        // line 44
        echo Asset::js("css3-animate-it.js");
        echo "
    <script type=\"text/javascript\">
        ";
        // line 46
        $this->displayBlock('back_to_top_js', $context, $blocks);
        // line 49
        echo "    </script>
";
    }

    // line 46
    public function block_back_to_top_js($context, array $blocks = array())
    {
        // line 47
        echo "        ";
        $this->displayParentBlock("back_to_top_js", $context, $blocks);
        echo "
        ";
    }

    public function getTemplateName()
    {
        return "csr_cerdaskan_bangsa.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  134 => 47,  131 => 46,  126 => 49,  124 => 46,  119 => 44,  114 => 43,  111 => 42,  104 => 36,  101 => 35,  95 => 38,  93 => 35,  75 => 20,  65 => 12,  63 => 11,  60 => 10,  57 => 9,  51 => 8,  45 => 6,  41 => 5,  36 => 4,  33 => 3,);
    }
}
