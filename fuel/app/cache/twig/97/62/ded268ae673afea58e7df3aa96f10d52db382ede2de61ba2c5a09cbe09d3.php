<?php

/* factory_tour.twig */
class __TwigTemplate_9762ded268ae673afea58e7df3aa96f10d52db382ede2de61ba2c5a09cbe09d3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_head_js' => array($this, 'block_frontend_head_js'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'back_to_top' => array($this, 'block_back_to_top'),
            'frontend_js' => array($this, 'block_frontend_js'),
            'back_to_top_js' => array($this, 'block_back_to_top_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        echo Asset::css("custom/animations.css");
        echo "
    ";
        // line 5
        $this->displayParentBlock("frontend_css", $context, $blocks);
        echo "
    ";
        // line 7
        echo "    ";
        echo Asset::css("custom/factory_tour.css");
        echo "
    ";
        // line 8
        echo Asset::css("custom/dp_calendar.css");
        echo "
    ";
        // line 9
        echo Asset::css("custom/demo.css");
        echo "

";
    }

    // line 12
    public function block_frontend_head_js($context, array $blocks = array())
    {
        // line 13
        echo "     ";
        $this->displayParentBlock("frontend_head_js", $context, $blocks);
        echo "
     ";
        // line 14
        echo Asset::js("jquery-ui-1.12.0.js");
        echo "
     ";
        // line 15
        echo Asset::js("date.js");
        echo "
     ";
        // line 16
        echo Asset::js("jquery.dp_calendar.js");
        echo "
 ";
    }

    // line 18
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 19
        echo "<div class=\"factory\">
<div class=\"banner animatedParent animateOnce\">
    <img src=\"";
        // line 21
        echo Uri::base();
        echo "/assets/img/factory-tour/factory-banner.jpg\"/>
    <div class=\"banner-desc animated fadeInUpShort\">
        <p>EXPLORION<br/>TOUR</p>
        <hr/>
    </div>
</div>
<div class=\"row factory-detail\">
    <div class=\"menu-factory\">
        <div class=\"columns\">
            <div class=\"medium-6 small-6 columns no-padd tabs-title\">
                <a href=\"";
        // line 31
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/explorion-advanture\">
                    <img src=\"";
        // line 32
        echo Uri::base();
        echo "/assets/img/factory-tour/icon-explorion-adventure.png\"/>
                    <div class=\"active\"></div>
                </a>
            </div>
            <div class=\"medium-6 small-6 columns no-padd tabs-title is-active\">
                <a href=\"";
        // line 37
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/factory-tour\" aria-selected=\"true\">
                    <img src=\"";
        // line 38
        echo Uri::base();
        echo "/assets/img/factory-tour/icon-factory-tour.png\"/>
                    <div class=\"active\"></div>
                </a>
            </div>
        </div>
    </div>

    <div class=\"columns factory-detail\">
        <div class=\"medium-12 columns\">
            <img src=\"";
        // line 47
        echo Uri::base();
        echo "/assets/img/factory-tour/banner-factorytour.jpg\"/>
        </div>
        <div class=\"medium-12 columns desc\">
            <div class=\"columns no-padd sub-desc\">
                <p>";
        // line 51
        echo Lang::get("factory_title");
        echo "</p>
            </div>
            <div class=\"columns no-padd detail\">
                <p>";
        // line 54
        echo Lang::get("factory_desc_1");
        echo "</p>
                <p>";
        // line 55
        echo Lang::get("factory_desc_2");
        echo "</p>
                <p>";
        // line 56
        echo Lang::get("factory_desc_3");
        echo "</p>
            </div>
        </div>
    </div>
</div>
<div class=\"row visiting-form custom-padd\">
    <div  class=\"title\">
        <hr/>
        <p>KEJAYAN</p>
        <p>VISITING FORM</p>
    </div>
    <div class=\"visiting-detail\">
        <ul class=\"tabs\" data-tabs id=\"visiting-detail\">
            <li class=\"tabs-title\"><a href=\"#panel1\" aria-selected=\"true\">";
        // line 69
        echo Lang::get("txt_tour01");
        echo "</a></li>
            <li class=\"tabs-title  is-active\"><a href=\"#panel2\">";
        // line 70
        echo Lang::get("txt_tour02");
        echo "</a></li>
        </ul>
        <div class=\"tabs-content\" data-tabs-content=\"visiting-detail\">
            <div class=\"tabs-panel\" id=\"panel1\">
                <ol>
                    <li>";
        // line 75
        echo Lang::get("txt_tourtnc01");
        echo "</li>
                    <li>";
        // line 76
        echo Lang::get("txt_tourtnc02");
        echo "</li>
                    <li>";
        // line 77
        echo Lang::get("txt_tourtnc03");
        echo "</li>
                    <li>";
        // line 78
        echo Lang::get("txt_tourtnc04");
        echo "</li>
                    <li>";
        // line 79
        echo Lang::get("txt_tourtnc05");
        echo "</li>
                    <li>";
        // line 80
        echo Lang::get("txt_tourtnc06");
        echo "</li>
                    <li>";
        // line 81
        echo Lang::get("txt_tourtnc07");
        echo "</li>
                </ol>
                <p style=\"color: #ffffff;background: #01b2e5;display: inline-block;padding: 10px 20px 10px 60px;margin-left: -27px\">";
        // line 83
        echo Lang::get("txt_tourtnc13");
        echo "</p>
                <ol>
                    <li>";
        // line 85
        echo Lang::get("txt_tourtnc08");
        echo "</li>
                    <li>";
        // line 86
        echo Lang::get("txt_tourtnc09");
        echo "</li>
                    <li>";
        // line 87
        echo Lang::get("txt_tourtnc10");
        echo "</li>
                    <li>";
        // line 88
        echo Lang::get("txt_tourtnc11");
        echo "</li>
                    <li>";
        // line 89
        echo Lang::get("txt_tourtnc12");
        echo "</li>
                </ol>
            </div>
            <div class=\"tabs-panel  is-active\" id=\"panel2\">
                <div class=\"row\">
                    <div class=\"medium-8 medium-offset-2 small-12 columns\">
                        ";
        // line 96
        echo "                        <p id=\"calendar-mandatory-info\" class=\"sub-desc\">";
        echo Lang::get("txt_tour10");
        echo "</p>
                        <div id='calendar' class=\"kejayan\"></div>
                        ";
        // line 99
        echo "                        <form role=\"form\" id=\"registration-form\" enctype=\"multipart/form-data\" action=\"\" name=\"registration-form\" method=\"post\" class=\"hide\">
                            <div class=\"row\">
                                <div class=\"small-3 columns\">
                                    <label for=\"middle-label\" class=\"text-right middle\">";
        // line 102
        echo Lang::get("txt_form_tour01");
        echo "</label>
                                </div>
                                <div class=\"small-1 columns\">:</div>
                                <div class=\"small-8 columns\">
                                    <input type=\"hidden\" name=\"factory\" value=\"kejayan\" />
                                    <input type=\"hidden\" name=\"visit_date_full\" id=\"visit-date-full\" value=\"\" />
                                    <input type=\"hidden\" name=\"visit_date\" id=\"visit-date\" value=\"\" />
                                    <input type=\"hidden\" name=\"visit_shift\" id=\"visit-shift\" value=\"\" />
                                    <span id=\"label-visit-date\"></span>
                                    <span id=\"label-visit-shift\"></span>
                                </div>
                            </div>
                            <div class=\"row\">
                                <div class=\"small-3 columns\">
                                    <label for=\"middle-label\" class=\"text-right middle\">";
        // line 116
        echo Lang::get("txt_form_tour02");
        echo "</label>
                                </div>
                                <div class=\"small-1 columns\">:</div>
                                <div class=\"small-8 columns\">
                                    Kejayan
                                </div>
                            </div>
                            <div class=\"row\">
                                <div class=\"small-3 columns\">
                                    <label for=\"middle-label\" class=\"text-right middle\">";
        // line 125
        echo Lang::get("txt_form_tour03");
        echo "</label>
                                </div>
                                <div class=\"small-1 columns\">:</div>
                                <div class=\"small-8 columns\">
                                    <input id=\"name\" name=\"name\" type=\"text\" placeholder=\"";
        // line 129
        echo Lang::get("txt_form_tour03");
        echo "\" required>
                                </div>
                            </div>
                            <div class=\"row\">
                                <div class=\"small-3 columns\">
                                    <label for=\"middle-label\" class=\"text-right middle\">";
        // line 134
        echo Lang::get("txt_form_tour04");
        echo "</label>
                                </div>
                                <div class=\"small-1 columns\">:</div>
                                <div class=\"small-8 columns\">
                                    <input id=\"institution\" name=\"institution\" type=\"text\" placeholder=\"";
        // line 138
        echo Lang::get("txt_form_tour04");
        echo "\" required>
                                </div>
                            </div>
                            <div class=\"row\">
                                <div class=\"small-3 columns\">
                                    <label for=\"middle-label\" class=\"text-right middle\">";
        // line 143
        echo Lang::get("txt_form_tour05");
        echo "</label>
                                </div>
                                <div class=\"small-1 columns\">:</div>
                                <div class=\"small-8 columns\">
                                    <input id=\"visitor\" name=\"visitor\" type=\"number\" placeholder=\"";
        // line 147
        echo Lang::get("txt_form_tour05_01");
        echo "\" min=\"20\" max=\"100\">
                                </div>
                            </div>
                            <div class=\"row\">
                                <div class=\"small-3 columns\">
                                    <label for=\"alamat\" class=\"text-right middle\">";
        // line 152
        echo Lang::get("txt_form_tour06");
        echo "</label>
                                </div>
                                <div class=\"small-1 columns\">:</div>
                                <div class=\"small-8 columns\">
                                    <textarea id=\"address\" name=\"address\" rows=\"2\" name=\"alamat\" class=\"large-12 columns\" placeholder=\"\"  minlength=\"20\" maxlength=\"150\" required></textarea>
                                </div>
                            </div>
                            <div class=\"row\">
                                <div class=\"small-3 columns\">
                                    <label for=\"alamat\" class=\"text-right middle\">";
        // line 161
        echo Lang::get("txt_form_tour07");
        echo "</label>
                                </div>
                                <div class=\"small-1 columns\">:</div>
                                <div class=\"small-8 columns\">
                                    <div class=\"small-6 columns no-padd\">
                                        <input id=\"landline\" name=\"landline\" type=\"tel\" placeholder=\"Tel\" required>
                                    </div>
                                    <div class=\"small-6 columns no-padd\">
                                        <input id=\"mobile\" name=\"mobile\" type=\"tel\" placeholder=\"Hp\" style=\"float: right\" required>
                                    </div>
                                </div>
                            </div>
                            <div class=\"row\">
                                <div class=\"small-3 columns\">
                                    <label for=\"middle-label\" class=\"text-right middle\">";
        // line 175
        echo Lang::get("txt_form_tour08");
        echo "</label>
                                </div>
                                <div class=\"small-1 columns\">:</div>
                                <div class=\"small-8 columns\">
                                    <input id=\"email\" name=\"email\" type=\"email\" placeholder=\"";
        // line 179
        echo Lang::get("txt_form_tour08");
        echo "\" required>
                                </div>
                            </div>
                            <div class=\"row\">
                                <div class=\"small-3 columns\">
                                    <label for=\"alamat\" class=\"text-right middle\">";
        // line 184
        echo Lang::get("txt_form_tour09");
        echo "</label>
                                </div>
                                <div class=\"small-1 columns\">:</div>
                                <div class=\"small-8 columns\">
                                    <textarea id=\"message\" rows=\"2\" name=\"message\" class=\"large-12 columns\" placeholder=\"";
        // line 188
        echo Lang::get("txt_form_tour09");
        echo "\" required></textarea>
                                </div>
                            </div>
                            <div class=\"row\">
                                <div class=\"medium-6 medium-offset-6 small-12 columns captcha-factory\">
                                    <div class=\"g-recaptcha position-captcha\" data-sitekey=\"6LcOtBwTAAAAAMr8p2x6uA0BsIscnHqIiqmqHc07\"></div>
                                </div>
                            </div>
                            <button id=\"btn-register\" onclick=\"\">SEND</button>
                            <button onclick=\"goBack()\">BACK</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div>
    ";
        // line 206
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["err_msg"]) ? $context["err_msg"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
            // line 207
            echo "        <div>";
            echo (isset($context["error"]) ? $context["error"] : null);
            echo "</div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 209
        echo "</div>
<div class=\"gallery-visit\">
    <div class=\"row custom-padd\">
        <div class=\"medium-3 small-3 columns no-padd\">
            <div class=\"title\">
                <p>GALLERY</p>
                <p>EXPLORION<br/>TOUR</p>
                <hr/>
            </div>
            <div class=\"see-gallery\">
                <a class=\"button\" href=\"";
        // line 219
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/gallery-detail\">CLICK HERE<br/>TO SEE ALL GALLERY</a>
            </div>
        </div>
        <div class=\"medium-9 small-9 columns\">
            <a href=\"";
        // line 223
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/gallery-detail\"><img src=\"";
        echo Uri::base();
        echo "/assets/img/factory-tour/gallery/gallery.jpg\"></a>
        </div>
    </div>
    <div class=\"show-for-small-only\">
        ";
        // line 227
        $this->displayBlock('back_to_top', $context, $blocks);
        // line 230
        echo "    </div>
</div>
";
        // line 232
        if ((isset($context["flag"]) ? $context["flag"] : null)) {
            // line 233
            echo "    <div id=\"factoryModal\" class=\"reveal\" data-reveal>
        <button class=\"close-reveal-modal\" data-close aria-label=\"Close modal\" type=\"button\">
            <span aria-hidden=\"true\">&times;</span>
        </button>
        <p>Terima kasih, data anda akan kami proses. Untuk jadwal kunjungan, akan kami informasikan lebih lanjut</p>
    </div>
";
        }
        // line 240
        echo "

";
    }

    // line 227
    public function block_back_to_top($context, array $blocks = array())
    {
        // line 228
        echo "            ";
        $this->displayParentBlock("back_to_top", $context, $blocks);
        echo "
        ";
    }

    // line 243
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 244
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
";
        // line 245
        echo Asset::js("jquery.maskedinput.js");
        echo "
";
        // line 246
        echo Asset::js("css3-animate-it.js");
        echo "
<script src='https://www.google.com/recaptcha/api.js'></script>
<script type=\"text/javascript\">
    function goBack() {
        \$(\"#registration-form\").addClass(\"hide\");
        \$(\"#calendar\").removeClass(\"hide\");
        \$(\"body, html\").animate({
            scrollTop: jQuery('#registration-form').offset().top - 300
        });
    }
    jQuery('#calendar').on('click', 'li.slot', function(){
        var shift = jQuery(this).data('shift'),
            date_selected = \$.fn.dp_calendar.date_selected,
            date_mysql_format = date_selected.getFullYear()+'-'+(date_selected.getMonth()*1+1)+'-'+date_selected.getDate(),
            date_display = \$.fn.dp_calendar.regional[\"lang_";
        // line 260
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "\"].dayNames[date_selected.getDay()]+', '+date_selected.getDate()+' '+\$.fn.dp_calendar.regional[\"lang_";
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "\"].monthNames[date_selected.getMonth()]+' '+date_selected.getFullYear(),
            chosen_date = \$.fn.dp_calendar.regional[\"lang_id\"].dayNames[date_selected.getDay()]+', '+date_selected.getDate()+' '+\$.fn.dp_calendar.regional[\"lang_id\"].monthNames[date_selected.getMonth()]+' '+date_selected.getFullYear(),
            label_shift='';
        jQuery('#visit-date').val(date_mysql_format);
        switch (date_selected.getDay()) {
            case 5:
                if (shift == 1) {
                    label_shift = '09.00 WIB - 11.00 WIB';
                } else if (shift == 2) {
                    label_shift = '";
        // line 269
        echo Lang::get("txt_tour09");
        echo "';
                } else if (shift == 3) {
                    label_shift = '13.00 WIB - 15.00 WIB';
                }
                break;
            default:
                if (shift == 1) {
                    label_shift = '09.00 WIB - 11.00 WIB';
                } else if (shift == 2) {
                    label_shift = '11.00 WIB - 13.00 WIB';
                } else if (shift == 3) {
                    label_shift = '13.00 WIB - 15.00 WIB';
                }
        }
        jQuery('#visit-shift').val(shift);
        jQuery('#visit-date-full').val(chosen_date);
        jQuery('#label-visit-date').text(date_display);
        jQuery('#label-visit-shift').text(' | '+label_shift);
        jQuery('#calendar').addClass('hide');
        jQuery('#calendar-mandatory-info').addClass('hide');
        jQuery('#registration-form').removeClass('hide');
        jQuery(\"body, html\").animate({ 
            scrollTop: jQuery('#registration-form').offset().top - 300
        }, 600);
    });

    var events_array = [];
    ";
        // line 296
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["factory_schedules"]) ? $context["factory_schedules"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["schedule"]) {
            // line 297
            echo "        events_array.push({
            startDate: new Date(";
            // line 298
            echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["schedule"]) ? $context["schedule"] : null), "visit_date"), "Y");
            echo ", ";
            echo (twig_date_format_filter($this->env, $this->getAttribute((isset($context["schedule"]) ? $context["schedule"] : null), "visit_date"), "m") - 1);
            echo ", ";
            echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["schedule"]) ? $context["schedule"] : null), "visit_date"), "d");
            echo "),
            endDate: new Date(";
            // line 299
            echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["schedule"]) ? $context["schedule"] : null), "visit_date"), "Y");
            echo ", ";
            echo (twig_date_format_filter($this->env, $this->getAttribute((isset($context["schedule"]) ? $context["schedule"] : null), "visit_date"), "m") - 1);
            echo ", ";
            echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["schedule"]) ? $context["schedule"] : null), "visit_date"), "d");
            echo "),
            title: \"";
            // line 300
            echo $this->getAttribute((isset($context["schedule"]) ? $context["schedule"] : null), "institution_name");
            echo "\",
            shift: ";
            // line 301
            echo $this->getAttribute((isset($context["schedule"]) ? $context["schedule"] : null), "shift");
            echo "
        });
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['schedule'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 304
        echo "
    jQuery(\"#calendar\").dp_calendar({
        events_array: events_array
    },'";
        // line 307
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "');

    ";
        // line 309
        if ((isset($context["flag"]) ? $context["flag"] : null)) {
            // line 310
            echo "    \$('#factoryModal').foundation('open');
    ";
        }
        // line 312
        echo "
    jQuery(function(\$){
        \$(\"#landline\").mask(\"(099)-999-9999?9\");
        \$(\"#mobile\").mask(\"(0899)-9999-999?9\");
    });

    ";
        // line 318
        $this->displayBlock('back_to_top_js', $context, $blocks);
        // line 321
        echo "</script>

";
    }

    // line 318
    public function block_back_to_top_js($context, array $blocks = array())
    {
        // line 319
        echo "    ";
        $this->displayParentBlock("back_to_top_js", $context, $blocks);
        echo "
    ";
    }

    public function getTemplateName()
    {
        return "factory_tour.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  615 => 319,  612 => 318,  606 => 321,  604 => 318,  596 => 312,  592 => 310,  590 => 309,  585 => 307,  580 => 304,  571 => 301,  567 => 300,  559 => 299,  551 => 298,  548 => 297,  544 => 296,  514 => 269,  500 => 260,  483 => 246,  479 => 245,  475 => 244,  472 => 243,  465 => 228,  462 => 227,  456 => 240,  447 => 233,  445 => 232,  441 => 230,  439 => 227,  429 => 223,  421 => 219,  409 => 209,  400 => 207,  396 => 206,  375 => 188,  368 => 184,  360 => 179,  353 => 175,  336 => 161,  324 => 152,  316 => 147,  309 => 143,  301 => 138,  294 => 134,  286 => 129,  279 => 125,  267 => 116,  250 => 102,  245 => 99,  239 => 96,  230 => 89,  226 => 88,  222 => 87,  218 => 86,  214 => 85,  209 => 83,  204 => 81,  200 => 80,  196 => 79,  192 => 78,  188 => 77,  184 => 76,  180 => 75,  172 => 70,  168 => 69,  152 => 56,  148 => 55,  144 => 54,  138 => 51,  131 => 47,  119 => 38,  115 => 37,  107 => 32,  103 => 31,  90 => 21,  86 => 19,  83 => 18,  77 => 16,  73 => 15,  69 => 14,  64 => 13,  61 => 12,  54 => 9,  50 => 8,  45 => 7,  41 => 5,  36 => 4,  33 => 3,);
    }
}
