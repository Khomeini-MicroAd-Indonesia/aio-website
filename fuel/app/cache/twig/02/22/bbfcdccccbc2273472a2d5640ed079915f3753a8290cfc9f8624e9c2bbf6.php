<?php

/* pages/views/template_frontend_new.twig */
class __TwigTemplate_0222bbfcdccccbc2273472a2d5640ed079915f3753a8290cfc9f8624e9c2bbf6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_head_js' => array($this, 'block_frontend_head_js'),
            'frontend_meta_twitter' => array($this, 'block_frontend_meta_twitter'),
            'frontend_meta_facebook' => array($this, 'block_frontend_meta_facebook'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html class=\"ie8\">
<head>

    <meta charset=\"UTF-8\">
    <title>";
        // line 6
        echo (isset($context["meta_title"]) ? $context["meta_title"] : null);
        echo "</title>
    <meta name=\"description\" content=\"";
        // line 7
        echo (isset($context["meta_desc"]) ? $context["meta_desc"] : null);
        echo "\">
    <meta name=\"viewport\" content=\"width=device-width\">
    <link rel=\"shortcut icon\" href=\"";
        // line 9
        echo Uri::base();
        echo "assets/css/images/logo_aio.png\" />
    <link rel=\"stylesheet\" href=\"";
        // line 10
        echo Uri::base();
        echo "/assets/fa-icon/css/font-awesome.min.css\"/>
    <link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800\">
    <link href=\"https://fonts.googleapis.com/css?family=Oswald:300,400,700\" rel=\"stylesheet\">
    ";
        // line 13
        $this->displayBlock('frontend_css', $context, $blocks);
        // line 18
        echo "    ";
        $this->displayBlock('frontend_head_js', $context, $blocks);
        // line 22
        echo "    ";
        $this->displayBlock('frontend_meta_twitter', $context, $blocks);
        // line 24
        echo "    ";
        $this->displayBlock('frontend_meta_facebook', $context, $blocks);
        // line 26
        echo "    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '";
        // line 32
        echo Config::get("config_basic.google_analytic_code");
        echo "', 'auto');
        ga('send', 'pageview');
    </script>
</head>
<body>

<div class=\"off-canvas-wrapper\">
    <div class=\"off-canvas-wrapper-inner\" data-off-canvas-wrapper>
        <div class=\"off-canvas position-right\" id=\"offCanvas\" data-off-canvas data-position=\"right\">
            <!-- Close button -->
            <button class=\"close-button\" aria-label=\"Close menu\" type=\"button\" data-close>
                <span aria-hidden=\"true\">&times;</span>
            </button>

            <form style=\"margin-top: 50px; padding: 20px\" action=\"";
        // line 46
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/search\">
                <div class=\"expanded row\">
                    <input name=\"keyword\" type=\"text\" placeholder=\"Search\">
                </div>
            </form>
            <!-- Menu -->
            <ul class=\"vertical dropdown menu\" data-drilldown>
                <li><a class=\"";
        // line 53
        echo (isset($context["active_home"]) ? $context["active_home"] : null);
        echo "\" href=\"";
        echo Uri::base();
        echo "\"><i class=\"fa fa-home\" style=\"font-size: 24px\" aria-hidden=\"true\"></i></a></li>
                <li>
                    <a class=\"";
        // line 55
        echo (isset($context["active_about"]) ? $context["active_about"] : null);
        echo "\" href=\"javascript:void(0);\">";
        echo Lang::get("menu_about");
        echo "</a>
                    <ul class=\"vertical menu\">
                        <li><a href=\"";
        // line 57
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/about-us-new\">";
        echo Lang::get("menu_about");
        echo "</a></li>
                        <li><a href=\"";
        // line 58
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/about-us-new/history\">";
        echo Lang::get("menu_history");
        echo "</a></li>
                    </ul>
                </li>
                <li>
                    <a href=\"javascript:void(0);\" class=\"";
        // line 62
        echo (isset($context["active_brand"]) ? $context["active_brand"] : null);
        echo "\">Brand</a>
                    <ul class=\"vertical menu\">
                        <li><a href=\"";
        // line 64
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/brand-new/pocari-sweat\">Pocari Sweat</a></li>
                        <li><a href=\"";
        // line 65
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/brand-new/soyjoy\">Soyjoy</a></li>
                        <li><a href=\"";
        // line 66
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/brand-new/ionessence\">Ionessence</a></li>
                    </ul>
                </li>
                <li><a href=\"";
        // line 69
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/csr/shcb\" class=\"";
        echo (isset($context["active_csr"]) ? $context["active_csr"] : null);
        echo "\">CSR</a></li>
                <li><a href=\"";
        // line 70
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/explorion-new\">Explorion Tour</a></li>
                <li>
                    <a href=\"javascript:void(0);\" class=\"";
        // line 72
        echo (isset($context["active_news"]) ? $context["active_news"] : null);
        echo "\" data-equalizer-watch=\"top-bar\">";
        echo Lang::get("menu_news");
        echo "</a>
                    <ul class=\"vertical menu\">
                        <li><a href=\"";
        // line 74
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/news\">";
        echo Lang::get("menu_news");
        echo "</a></li>
                        <li><a href=\"";
        // line 75
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/press\">";
        echo Lang::get("menu_press");
        echo "</a></li>
                    </ul>
                </li>
                <li>
                    <a href=\"javascript:void(0);\" class=\"";
        // line 79
        echo (isset($context["active_career"]) ? $context["active_career"] : null);
        echo "\">";
        echo Lang::get("menu_career");
        echo "</a>
                    <ul class=\"vertical menu\">
                        <li><a href=\"";
        // line 81
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/career-new\">";
        echo Lang::get("menu_join");
        echo "</a></li>
                        <li><a href=\"";
        // line 82
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/life-at-otsuka-new\">Life at Amerta Indah Otsuka</a></li>
                    </ul>
                </li>
                <li><a href=\"";
        // line 85
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/distributor-new\" class=\"";
        echo (isset($context["active_dist"]) ? $context["active_dist"] : null);
        echo "\">Distribution</a></li>
                <li><a href=\"";
        // line 86
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/faq\" data-equalizer-watch=\"top-bar\" class=\"";
        echo (isset($context["active_faq"]) ? $context["active_faq"] : null);
        echo "\">FAQ</a></li>
                <li><a href=\"";
        // line 87
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/contact-us\" data-equalizer-watch=\"top-bar\" class=\"";
        echo (isset($context["active_contact"]) ? $context["active_contact"] : null);
        echo "\">Contact Us</a></li>
            </ul>
        </div>
        <div class=\"off-canvas-content\" data-off-canvas-content>
            <div class=\"title-bar hide-for-large\">
                <div class=\"small-7 columns \"><span class=\"title-bar-title\"><a href=\"";
        // line 92
        echo Uri::base();
        echo "\"><img src=\"";
        echo Uri::base();
        echo "/assets/css/images/logo-otsuka.jpg\"/></a></span></div>
                <div class=\"small-5 columns\">
                    <ul class=\"menu\">
                        <li data-equalizer-watch=\"top-bar\">
                            <ul class=\"lang\">
                                ";
        // line 97
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["valid_lang"]) ? $context["valid_lang"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["lang_item"]) {
            // line 98
            echo "                                    ";
            if (((isset($context["current_lang"]) ? $context["current_lang"] : null) == $this->getAttribute((isset($context["lang_item"]) ? $context["lang_item"] : null), "class"))) {
                // line 99
                echo "                                        <li><a class=\"active\" href=\"";
                echo $this->getAttribute((isset($context["lang_item"]) ? $context["lang_item"] : null), "link");
                echo "\"><img src=\"";
                echo Uri::base();
                echo $this->getAttribute((isset($context["lang_item"]) ? $context["lang_item"] : null), "icon");
                echo "\"/></a></li>
                                    ";
            } else {
                // line 101
                echo "                                        <li><a href=\"";
                echo $this->getAttribute((isset($context["lang_item"]) ? $context["lang_item"] : null), "link");
                echo "\"><img src=\"";
                echo Uri::base();
                echo $this->getAttribute((isset($context["lang_item"]) ? $context["lang_item"] : null), "icon");
                echo "\"/></a></li>
                                    ";
            }
            // line 103
            echo "                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lang_item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 104
        echo "                            </ul>
                            <label>";
        // line 105
        echo Lang::get("menu_lang");
        echo "</label>
                        </li>
                    </ul>
                    <button class=\"menu-icon\" type=\"button\" data-open=\"offCanvas\"></button>
                </div>
                <div style=\"clear: both;\"></div>
            </div>
            <div class=\"top-bar show-for-large\" data-equalizer=\"top-bar\">
                <div class=\"top-bar-title\" data-equalizer-watch=\"top-bar\"><a href=\"";
        // line 113
        echo Uri::base();
        echo "\"><img src=\"";
        echo Uri::base();
        echo "/assets/css/images/logo-otsuka.jpg\" alt=\"Logo Otsuka\"/></a></div>
                <div class=\"top-bar-left\" data-equalizer-watch=\"top-bar\">
                    <ul class=\"dropdown menu\" data-dropdown-menu>
                        <li><a class=\"";
        // line 116
        echo (isset($context["active_home"]) ? $context["active_home"] : null);
        echo "\" href=\"";
        echo Uri::base();
        echo "\" data-equalizer-watch=\"top-bar\"><i class=\"fa fa-home\" style=\"font-size: 24px\" aria-hidden=\"true\"></i></a></li>
                        <li>
                            <a class=\"";
        // line 118
        echo (isset($context["active_about"]) ? $context["active_about"] : null);
        echo "\" href=\"javascript:void(0);\" data-equalizer-watch=\"top-bar\">";
        echo Lang::get("menu_about");
        echo "</a>
                            <ul class=\"menu\">
                                <li><a href=\"";
        // line 120
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/about-us-new\">";
        echo Lang::get("menu_about");
        echo "</a></li>
                                <li><a href=\"";
        // line 121
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/about-us-new/history\">";
        echo Lang::get("menu_history");
        echo "</a></li>
                            </ul>
                        </li>
                        <li>
                            <a class=\"";
        // line 125
        echo (isset($context["active_brand"]) ? $context["active_brand"] : null);
        echo "\" href=\"javascript:void(0);\" data-equalizer-watch=\"top-bar\">Brand</a>
                            <ul class=\"menu\">
                                <li><a href=\"";
        // line 127
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/brand-new/pocari-sweat\">Pocari Sweat</a></li>
                                <li><a href=\"";
        // line 128
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/brand-new/soyjoy\">Soyjoy</a></li>
                                <li><a href=\"";
        // line 129
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/brand-new/ionessence\">Ionessence</a></li>
                            </ul>
                        </li>
                        <li><a class=\"";
        // line 132
        echo (isset($context["active_csr"]) ? $context["active_csr"] : null);
        echo " \" href=\"";
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/csr/shcb\" data-equalizer-watch=\"top-bar\">Csr</a></li>
                        <li><a class=\"";
        // line 133
        echo (isset($context["active_explorion"]) ? $context["active_explorion"] : null);
        echo " \" href=\"";
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/explorion-new\" data-equalizer-watch=\"top-bar\">Explorion Tour</a></li>
                        <li>
                            <a class=\"";
        // line 135
        echo (isset($context["active_news"]) ? $context["active_news"] : null);
        echo " \" href=\"javascript:void(0);\" data-equalizer-watch=\"top-bar\">";
        echo Lang::get("menu_news");
        echo "</a>
                            <ul class=\"menu\">
                                <li><a href=\"";
        // line 137
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/news\">";
        echo Lang::get("menu_news");
        echo "</a></li>
                                <li><a href=\"";
        // line 138
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/press\">";
        echo Lang::get("menu_press");
        echo "</a></li>
                            </ul>
                        </li>
                        <li>
                            <a class=\"";
        // line 142
        echo (isset($context["active_career"]) ? $context["active_career"] : null);
        echo " \" href=\"javascript:void(0);\" data-equalizer-watch=\"top-bar\">";
        echo Lang::get("menu_career");
        echo "</a>
                            <ul class=\"menu\">
                                <li><a href=\"";
        // line 144
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/career-new\">";
        echo Lang::get("menu_join");
        echo "</a></li>
                                <li><a href=\"";
        // line 145
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/life-at-otsuka-new\">Life at Amerta Indah Otsuka</a></li>
                            </ul>
                        </li>
                        <li><a class=\"";
        // line 148
        echo (isset($context["active_dist"]) ? $context["active_dist"] : null);
        echo " \" href=\"";
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/distributor-new\" data-equalizer-watch=\"top-bar\">";
        echo Lang::get("menu_distribution");
        echo "</a></li>
                        <li><a class=\"";
        // line 149
        echo (isset($context["active_faq"]) ? $context["active_faq"] : null);
        echo " \" href=\"";
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/faq\" data-equalizer-watch=\"top-bar\">FAQ</a></li>
                        <li><a class=\"";
        // line 150
        echo (isset($context["active_contact"]) ? $context["active_contact"] : null);
        echo " \" href=\"";
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/contact-us\" data-equalizer-watch=\"top-bar\">";
        echo Lang::get("menu_contact");
        echo "</a></li>
                    </ul>
                </div>
                <div class=\"top-bar-right\" data-equalizer-watch=\"top-bar\">
                    <ul class=\"menu\">
                        <li data-equalizer-watch=\"top-bar\">
                            <ul class=\"lang\">
                                ";
        // line 157
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["valid_lang"]) ? $context["valid_lang"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["lang_item"]) {
            // line 158
            echo "                                    ";
            if (((isset($context["current_lang"]) ? $context["current_lang"] : null) == $this->getAttribute((isset($context["lang_item"]) ? $context["lang_item"] : null), "class"))) {
                // line 159
                echo "                                        <li><a class=\"active\" href=\"";
                echo $this->getAttribute((isset($context["lang_item"]) ? $context["lang_item"] : null), "link");
                echo "\"><img src=\"";
                echo Uri::base();
                echo $this->getAttribute((isset($context["lang_item"]) ? $context["lang_item"] : null), "icon");
                echo "\"/></a></li>
                                    ";
            } else {
                // line 160
                echo "    
                                        <li><a href=\"";
                // line 161
                echo $this->getAttribute((isset($context["lang_item"]) ? $context["lang_item"] : null), "link");
                echo "\"><img src=\"";
                echo Uri::base();
                echo $this->getAttribute((isset($context["lang_item"]) ? $context["lang_item"] : null), "icon");
                echo "\"/></a></li>
                                    ";
            }
            // line 163
            echo "                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lang_item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 164
        echo "                            </ul>
                            <label>";
        // line 165
        echo Lang::get("menu_lang");
        echo "</label>
                        </li>
                        <li class=\"search\" data-equalizer-watch=\"top-bar\">
                            <button class=\"button float-left\" type=\"button\" data-toggle=\"search\"><i class=\"fa fa-search\" aria-hidden=\"true\"></i></button>
                            <label>";
        // line 169
        echo Lang::get("menu_search");
        echo "</label>
                            <div class=\"dropdown-pane bottom\" id=\"search\" data-dropdown data-auto-focus=\"true\">
                                <form action=\"";
        // line 171
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/search\">
                                    <div class=\"expanded row\">
                                        <input name=\"keyword\" type=\"text\" placeholder=\"Search\">
                                    </div>
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            ";
        // line 181
        $this->displayBlock('frontend_content', $context, $blocks);
        // line 183
        echo "            <footer class=\"footer\" data-equalizer=\"footer\" data-equalize-on=\"large\">
                <div class=\"row expanded\">
                    <div class=\"large-3 large-push-5 columns social-media\" data-equalizer-watch=\"footer\">
                        <div class=\"row\">
                            <p>";
        // line 187
        echo Lang::get("foot_follow");
        echo "</p>
                            <ul class=\"menu\">
                                <li><a href=\"https://twitter.com/Otsuka_AIO\"><i class=\"fa fa-twitter\" aria-hidden=\"true\"></i></a></li>
                                <li><a href=\"https://www.facebook.com/OtsukaAIOCompany/\"><i class=\"fa fa-facebook\" aria-hidden=\"true\"></i></a></li>
                                <li><a href=\"https://www.linkedin.com/company/678286\"><i class=\"fa fa-linkedin\" aria-hidden=\"true\"></i></a></li>
                                <li><a href=\"https://www.instagram.com/otsuka_aio/\"><i class=\"fa fa-instagram\" aria-hidden=\"true\"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class=\"large-4 large-push-5 columns foot-link\" data-equalizer-watch=\"footer\">
                        <div class=\"row\">
                            <div class=\"menu-centered\">
                                <ul class=\"menu\">
                                    <li><a href=\"#\">";
        // line 200
        echo Lang::get("foot_pvp");
        echo "</a></li>
                                    <li>|</li>
                                    <li><a href=\"\">";
        // line 202
        echo Lang::get("foot_tnc");
        echo "</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class=\"large-5 large-pull-7 columns copyright\" data-equalizer-watch=\"footer\">
                        <div class=\"row\">
                            <p>Otsuka &copy; Copyright 2016. All Right Reserved.</p>
                            <p>Client Logos are copyright and trademark of the respective owners / companies.</p>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</div>

";
        // line 219
        $this->displayBlock('frontend_js', $context, $blocks);
        // line 233
        echo "</body>
</html>
";
    }

    // line 13
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 14
        echo "        ";
        echo Asset::css("custom/normalize.css");
        echo "
        <link rel=\"stylesheet\" href=\"";
        // line 15
        echo Uri::base();
        echo "/assets/foundation-6.2.3/css/foundation.min.css\">
        ";
        // line 16
        echo Asset::css("custom/style_new.css");
        echo "
    ";
    }

    // line 18
    public function block_frontend_head_js($context, array $blocks = array())
    {
        // line 19
        echo "        ";
        echo Asset::js("vendor/jquery.min.js");
        echo "
        ";
        // line 20
        echo Asset::js("modernizr.js");
        echo "
    ";
    }

    // line 22
    public function block_frontend_meta_twitter($context, array $blocks = array())
    {
        // line 23
        echo "    ";
    }

    // line 24
    public function block_frontend_meta_facebook($context, array $blocks = array())
    {
        // line 25
        echo "    ";
    }

    // line 181
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 182
        echo "            ";
    }

    // line 219
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 220
        echo "    <script src=\"";
        echo Uri::base();
        echo "/assets/foundation-6.2.3/js/vendor/foundation.min.js\"></script>
    <script src=\"";
        // line 221
        echo Uri::base();
        echo "/assets/foundation-6.2.3/js/app.js\"></script>
    <script type=\"text/javascript\">
        \$(\".cbp-hrmenu > li\").hover(
                function(){
                    \$(this).addClass(\"cbp-hropen\");
                },function(){
                    \$(this).removeClass(\"cbp-hropen\");
                }
        );

    </script>
";
    }

    public function getTemplateName()
    {
        return "pages/views/template_frontend_new.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  591 => 221,  586 => 220,  583 => 219,  579 => 182,  576 => 181,  572 => 25,  569 => 24,  565 => 23,  562 => 22,  556 => 20,  551 => 19,  548 => 18,  542 => 16,  538 => 15,  533 => 14,  530 => 13,  524 => 233,  522 => 219,  502 => 202,  497 => 200,  481 => 187,  475 => 183,  473 => 181,  460 => 171,  455 => 169,  448 => 165,  445 => 164,  439 => 163,  431 => 161,  428 => 160,  419 => 159,  416 => 158,  412 => 157,  398 => 150,  392 => 149,  384 => 148,  378 => 145,  372 => 144,  365 => 142,  356 => 138,  350 => 137,  343 => 135,  336 => 133,  330 => 132,  324 => 129,  320 => 128,  316 => 127,  311 => 125,  302 => 121,  296 => 120,  289 => 118,  282 => 116,  274 => 113,  263 => 105,  260 => 104,  254 => 103,  245 => 101,  236 => 99,  233 => 98,  229 => 97,  219 => 92,  209 => 87,  203 => 86,  197 => 85,  191 => 82,  185 => 81,  178 => 79,  169 => 75,  163 => 74,  156 => 72,  151 => 70,  145 => 69,  139 => 66,  135 => 65,  131 => 64,  126 => 62,  117 => 58,  111 => 57,  104 => 55,  97 => 53,  87 => 46,  70 => 32,  62 => 26,  59 => 24,  56 => 22,  53 => 18,  51 => 13,  45 => 10,  41 => 9,  36 => 7,  32 => 6,  25 => 1,);
    }
}
