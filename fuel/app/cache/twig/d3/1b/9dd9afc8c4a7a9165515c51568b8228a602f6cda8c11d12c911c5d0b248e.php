<?php

/* list.twig */
class __TwigTemplate_d31b9dd9afc8c4a7a9165515c51568b8228a602f6cda8c11d12c911c5d0b248e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("backend/template.twig");

        $this->blocks = array(
            'backend_css' => array($this, 'block_backend_css'),
            'backend_content_header' => array($this, 'block_backend_content_header'),
            'backend_content' => array($this, 'block_backend_content'),
            'backend_js' => array($this, 'block_backend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "backend/template.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_backend_css($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("backend_css", $context, $blocks);
        echo "
    <!-- dataTables css -->
    ";
        // line 6
        echo Asset::css("datatables/dataTables.bootstrap.css");
        echo "
";
    }

    // line 9
    public function block_backend_content_header($context, array $blocks = array())
    {
        // line 10
        echo "    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>";
        // line 13
        echo "            Product
            <small>List</small>
        </h1>
        ";
        // line 17
        echo "        <ol class=\"breadcrumb\">
            <li><a href=\"";
        // line 18
        echo Uri::base();
        echo "backend\">Home</a></li>
            <li class=\"active\">Product List</li>
        </ol>
    </section>
";
    }

    // line 24
    public function block_backend_content($context, array $blocks = array())
    {
        // line 25
        echo "
    ";
        // line 26
        if ((twig_length_filter($this->env, (isset($context["success_message"]) ? $context["success_message"] : null)) > 0)) {
            // line 27
            echo "        <div class=\"alert alert-success alert-dismissable\">
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
            ";
            // line 29
            echo (isset($context["success_message"]) ? $context["success_message"] : null);
            echo "
        </div>
    ";
        }
        // line 32
        echo "
    ";
        // line 33
        if ((twig_length_filter($this->env, (isset($context["error_message"]) ? $context["error_message"] : null)) > 0)) {
            // line 34
            echo "        <div class=\"alert alert-danger alert-dismissable\">
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
            ";
            // line 36
            echo (isset($context["error_message"]) ? $context["error_message"] : null);
            echo "
        </div>
    ";
        }
        // line 39
        echo "
    <div class=\"box box-solid\">
        <div class=\"box-body text-right\">
            <a href=\"";
        // line 42
        echo Uri::base();
        echo "backend/product/add\">
                <button class=\"btn btn-default\">Create</button>
            </a>
        </div>
    </div>
    <div class=\"row\">
        <div class=\"col-xs-12\">
            <div class=\"box\">
                <div class=\"box-body table-responsive\">
                    <table id=\"table-product\" class=\"table table-bordered table-striped\">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Status</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            ";
        // line 62
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["product_list"]) ? $context["product_list"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 63
            echo "                                <tr>
                                    <td>";
            // line 64
            echo $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "id");
            echo "</td>
                                    <td>";
            // line 65
            echo $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "name");
            echo "</td>
                                    <td>";
            // line 66
            echo $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "desc");
            echo "</td>
                                    <td>";
            // line 67
            echo $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "get_status_name", array(), "method");
            echo "</td>
                                    <td>
                                        <div class=\"btn-group\">
                                            <button type=\"button\" class=\"btn btn-default dropdown-toggle\" data-toggle=\"dropdown\">
                                                Action <span class=\"caret\"></span>
                                            </button>
                                            <ul class=\"dropdown-menu\" role=\"menu\">
                                                <li><a href=\"";
            // line 74
            echo Uri::base();
            echo "backend/product/edit/";
            echo $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "id");
            echo "\">Edit</a></li>
                                                <li><a class=\"btn-delete\" data-url-delete=\"";
            // line 75
            echo Uri::base();
            echo "backend/product/delete/";
            echo $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "id");
            echo "\" href=\"#\">Delete</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 81
        echo "                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
";
    }

    // line 89
    public function block_backend_js($context, array $blocks = array())
    {
        // line 90
        echo "    ";
        $this->displayParentBlock("backend_js", $context, $blocks);
        echo "
    <!-- DATA TABES SCRIPT -->
    ";
        // line 92
        echo Asset::js("plugins/datatables/jquery.dataTables.js");
        echo "
    ";
        // line 93
        echo Asset::js("plugins/datatables/dataTables.bootstrap.js");
        echo "
    <!-- custom script -->
    <script type=\"text/javascript\">
        \$(function () {
            \$(\"#table-product\").dataTable({
                \"aoColumns\": [
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    {\"bSearchable\": false, \"bSortable\": false},
                    {\"bSearchable\": false, \"bSortable\": false}
                ]
            });
        });
    </script>
    <!-- Dialog Confirmation Script -->
    ";
        // line 113
        echo Asset::js("backend-dialog.js");
        echo "
    <script type=\"text/javascript\">
        jQuery('#table-product').on('click', '.btn-delete', function (e) {
            e.preventDefault();
            var my = jQuery(this),
                    url_del = my.data('url-delete');
            var yes_callback = function (e) {
                e.preventDefault();
                jQuery(location).attr('href', url_del);
            }
            backend_dialog.show_dialog_confirm('Are you sure want to delete this?', yes_callback);
        });

    </script>
";
    }

    public function getTemplateName()
    {
        return "list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 113,  203 => 93,  199 => 92,  193 => 90,  190 => 89,  180 => 81,  166 => 75,  160 => 74,  150 => 67,  146 => 66,  142 => 65,  138 => 64,  135 => 63,  131 => 62,  108 => 42,  103 => 39,  97 => 36,  93 => 34,  91 => 33,  88 => 32,  82 => 29,  78 => 27,  76 => 26,  73 => 25,  70 => 24,  61 => 18,  58 => 17,  53 => 13,  49 => 10,  46 => 9,  40 => 6,  34 => 4,  31 => 3,);
    }
}
