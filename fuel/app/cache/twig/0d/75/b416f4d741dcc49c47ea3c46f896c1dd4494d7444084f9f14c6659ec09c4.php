<?php

/* press.twig */
class __TwigTemplate_0d75b416f4d741dcc49c47ea3c46f896c1dd4494d7444084f9f14c6659ec09c4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'about_menu_us' => array($this, 'block_about_menu_us'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'back_to_top' => array($this, 'block_back_to_top'),
            'frontend_js' => array($this, 'block_frontend_js'),
            'back_to_top_js' => array($this, 'block_back_to_top_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("frontend_css", $context, $blocks);
        echo "
    ";
        // line 5
        echo Asset::css("custom/press.css");
        echo "
";
    }

    // line 7
    public function block_about_menu_us($context, array $blocks = array())
    {
        echo ".test";
    }

    // line 8
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 9
        echo "    ";
        $this->env->loadTemplate("template_press_room.twig")->display($context);
        // line 10
        echo "    
    <div class=\"press-content\">
        <div class=\"padding-custom\"></div>
            <div class=\"row\">
                ";
        // line 14
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["press_list"]) ? $context["press_list"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["press_item"]) {
            // line 15
            echo "                <div class=\"columns press-item\">
                    <div class=\"large-3 columns press-item-photo\">
                        <img src=\"";
            // line 17
            echo (((Uri::base() . $this->getAttribute((isset($context["press_item"]) ? $context["press_item"] : null), "get_image_path", array(), "method")) . "media/press-room/thumbnail/") . $this->getAttribute((isset($context["press_item"]) ? $context["press_item"] : null), "image"));
            echo "\" alt=\"";
            echo $this->getAttribute((isset($context["press_item"]) ? $context["press_item"] : null), "title");
            echo "\" />
                    </div>
                    <div class=\"large-9 columns press-item-content\">
                        <span class=\"press-item-title\">";
            // line 20
            echo $this->getAttribute((isset($context["press_item"]) ? $context["press_item"] : null), "title");
            echo "</span><br/>
                        <span class=\"press-date\">Posted on ";
            // line 21
            echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["press_item"]) ? $context["press_item"] : null), "date"), "F d, Y");
            echo " - ";
            echo $this->getAttribute((isset($context["press_item"]) ? $context["press_item"] : null), "get_category_name", array(), "method");
            echo "</span><br/>
                        <div class=\"press-text\">";
            // line 22
            echo $this->getAttribute((isset($context["press_item"]) ? $context["press_item"] : null), "highlight");
            echo "</div>
                        <a href=\"";
            // line 23
            echo (((Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null)) . "/our-event/detail/") . $this->getAttribute((isset($context["press_item"]) ? $context["press_item"] : null), "slug"));
            echo "\"><div class=\"read-more\"><img src=\"";
            echo Uri::base();
            echo "assets/img/press/right.png\"/>&nbsp;Read More</div></a>
                    </div>
                </div>
                <hr/>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['press_item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "            </div>
        <div class=\"show-for-small-only\">
            ";
        // line 30
        $this->displayBlock('back_to_top', $context, $blocks);
        // line 33
        echo "        </div>
    </div>
    
";
    }

    // line 30
    public function block_back_to_top($context, array $blocks = array())
    {
        // line 31
        echo "                ";
        $this->displayParentBlock("back_to_top", $context, $blocks);
        echo "
            ";
    }

    // line 38
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 39
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    <script type=\"text/javascript\">
        ";
        // line 41
        $this->displayBlock('back_to_top_js', $context, $blocks);
        // line 44
        echo "    </script>
";
    }

    // line 41
    public function block_back_to_top_js($context, array $blocks = array())
    {
        // line 42
        echo "        ";
        $this->displayParentBlock("back_to_top_js", $context, $blocks);
        echo "
        ";
    }

    public function getTemplateName()
    {
        return "press.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  150 => 42,  147 => 41,  142 => 44,  140 => 41,  134 => 39,  131 => 38,  124 => 31,  121 => 30,  114 => 33,  112 => 30,  108 => 28,  95 => 23,  91 => 22,  85 => 21,  81 => 20,  73 => 17,  69 => 15,  65 => 14,  59 => 10,  56 => 9,  53 => 8,  47 => 7,  41 => 5,  36 => 4,  33 => 3,);
    }
}
