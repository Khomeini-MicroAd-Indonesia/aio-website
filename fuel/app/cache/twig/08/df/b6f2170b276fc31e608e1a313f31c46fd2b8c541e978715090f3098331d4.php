<?php

/* template_csr.twig */
class __TwigTemplate_08dfb6f2170b276fc31e608e1a313f31c46fd2b8c541e978715090f3098331d4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"csr\">
    <div class=\"banner-csr\">
        <div class=\"title-page\">
            <div class=\"title-csr\">
                CORPORATE<br/>
                SOCIAL<br/>
                RESPONSIBILITY
            </div>
            <hr/>
            <div class=\"csr-lead\">
                PT Amerta Indah Otsuka melalui yayasan <strong>Satu Hati</strong> mewujudkan kepedulian terhadap pendidikan dan lingkungan sebagai salah satu bentuk tanggung jawab sosial PT Amerta Indah Otsuka terhadap masyarakat dan alam.
            </div>
        </div>
        ";
        // line 14
        echo (isset($context["csr_banner"]) ? $context["csr_banner"] : null);
        echo "
    </div>
    ";
        // line 17
        echo "        ";
        // line 18
        echo "            ";
        // line 19
        echo "                ";
        // line 20
        echo "                    ";
        // line 21
        echo "                ";
        // line 22
        echo "                ";
        // line 23
        echo "                    ";
        // line 24
        echo "                ";
        // line 25
        echo "                ";
        // line 26
        echo "                    ";
        // line 27
        echo "                ";
        // line 28
        echo "                ";
        // line 29
        echo "                    ";
        // line 30
        echo "                ";
        // line 31
        echo "            ";
        // line 32
        echo "        ";
        // line 33
        echo "    ";
        // line 34
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "template_csr.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 34,  71 => 33,  69 => 32,  67 => 31,  65 => 30,  63 => 29,  61 => 28,  59 => 27,  55 => 25,  53 => 24,  49 => 22,  47 => 21,  43 => 19,  41 => 18,  19 => 1,  134 => 51,  131 => 50,  126 => 47,  123 => 26,  104 => 21,  100 => 20,  96 => 19,  92 => 18,  85 => 16,  79 => 14,  62 => 13,  57 => 26,  54 => 9,  51 => 23,  45 => 20,  39 => 17,  34 => 14,  31 => 3,);
    }
}
