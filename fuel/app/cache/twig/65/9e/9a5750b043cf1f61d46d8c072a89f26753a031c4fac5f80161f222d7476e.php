<?php

/* home_new.twig */
class __TwigTemplate_659e9a5750b043cf1f61d46d8c072a89f26753a031c4fac5f80161f222d7476e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend_new.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend_new.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("frontend_css", $context, $blocks);
        echo "
    ";
        // line 5
        echo Asset::css("custom/home_new.css");
        echo "
";
    }

    // line 7
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 8
        echo "    <div id=\"fb-root\"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = \"//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=387528301585839\";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <div class=\"expanded row homeslider\">
        <div class=\"orbit\" role=\"region\" aria-label=\"Homebanner\" data-orbit data-options=\"autoPlay: true;\">
            <ul class=\"orbit-container\">
            ";
        // line 19
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["banner_detail"]) ? $context["banner_detail"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["banner"]) {
            // line 20
            echo "                <li class=\"is-active orbit-slide\">
                    <img class=\"orbit-image\" src=\"";
            // line 21
            echo Uri::base();
            echo "media/homebanner/";
            echo $this->getAttribute((isset($context["banner"]) ? $context["banner"] : null), "image");
            echo "\" alt=\"";
            echo $this->getAttribute((isset($context["banner"]) ? $context["banner"] : null), "title");
            echo "\">
                    ";
            // line 22
            if (($this->getAttribute((isset($context["banner"]) ? $context["banner"] : null), "text_position") == "TOP")) {
                // line 23
                echo "                        ";
                if ((twig_length_filter($this->env, $this->getAttribute((isset($context["banner"]) ? $context["banner"] : null), "title")) > 0)) {
                    // line 24
                    echo "                        <div class=\"medium-6 medium-offset-3 columns end desc-top\">";
                    echo $this->getAttribute((isset($context["banner"]) ? $context["banner"] : null), "title");
                    echo "<span>";
                    echo $this->getAttribute((isset($context["banner"]) ? $context["banner"] : null), "highlight");
                    echo "</span></div>
                        ";
                }
                // line 26
                echo "                    ";
            } elseif (($this->getAttribute((isset($context["banner"]) ? $context["banner"] : null), "text_position") == "RIGHT")) {
                // line 27
                echo "                        ";
                if ((twig_length_filter($this->env, $this->getAttribute((isset($context["banner"]) ? $context["banner"] : null), "title")) > 0)) {
                    // line 28
                    echo "                            <div class=\"medium-6 medium-offset-6 columns end desc-right\">";
                    echo $this->getAttribute((isset($context["banner"]) ? $context["banner"] : null), "title");
                    echo "<span>";
                    echo $this->getAttribute((isset($context["banner"]) ? $context["banner"] : null), "highlight");
                    echo "</span></div>
                        ";
                }
                // line 30
                echo "                    ";
            }
            // line 31
            echo "                </li>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['banner'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "            </ul>
            <div class=\"orbit-bullets-container\">
            <nav class=\"orbit-bullets\">
                ";
        // line 36
        $context["arr_length"] = twig_length_filter($this->env, (isset($context["banner_detail"]) ? $context["banner_detail"] : null));
        // line 37
        echo "                    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(0, ((isset($context["arr_length"]) ? $context["arr_length"] : null) - 1)));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 38
            echo "                        <button class=\"";
            echo (($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "first")) ? ("is-active") : (""));
            echo "\" data-slide=\"";
            echo (isset($context["i"]) ? $context["i"] : null);
            echo "\"><span class=\"show-for-sr\">";
            echo (isset($context["i"]) ? $context["i"] : null);
            echo " slide details.</span></button>
                    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 40
        echo "            </nav>
            </div>
        </div>
    </div>
    <div class=\"expanded row custom-left latest-news\">
        <img class=\"medium-8 medium-offset-4 columns latest-bg\" src=\"";
        // line 45
        echo Uri::base();
        echo "/assets/img/home/new-bg.jpg\"/>
        <div class=\"title\">
            <h2>";
        // line 47
        echo Lang::get("home_latest");
        echo "</h2>
            <a href=\"";
        // line 48
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/news\">";
        echo Lang::get("home_news");
        echo " <img src=\"";
        echo Uri::base();
        echo "/assets/css/images/see-all.png\"></a>
        </div>
        <div class=\"medium-8 medium-offset-2 end columns custom-content\">
            <div class=\"row\">
            ";
        // line 52
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["press_detail"]) ? $context["press_detail"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["news"]) {
            // line 53
            echo "                    <div class=\"medium-12 small-12 small-nopadd columns each-wrapper\">
                        <a href=\"";
            // line 54
            echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
            echo "/news/detail/";
            echo $this->getAttribute((isset($context["news"]) ? $context["news"] : null), "slug");
            echo "\">
                            <div class=\"row medium-collapse each-news\" data-equalizer=\"news\" data-equalize-on=\"large\">
                                <div class=\"large-5 small-nopadd columns\" data-equalizer-watch=\"news\"><img src=\"";
            // line 56
            echo Uri::base();
            echo "media/press-room/";
            echo $this->getAttribute((isset($context["news"]) ? $context["news"] : null), "image");
            echo "\" alt=\"";
            echo $this->getAttribute((isset($context["news"]) ? $context["news"] : null), "title");
            echo "\"></div>
                                <div class=\"large-7 small-nopadd columns list-news\" data-equalizer-watch=\"news\">
                                    <div class=\"post-category\">
                                        ";
            // line 59
            if (((isset($context["current_lang"]) ? $context["current_lang"] : null) == "id")) {
                // line 60
                echo "                                            Berita ";
                echo $this->getAttribute((isset($context["news"]) ? $context["news"] : null), "category");
                echo "
                                        ";
            } else {
                // line 62
                echo "                                            ";
                echo $this->getAttribute((isset($context["news"]) ? $context["news"] : null), "category");
                echo " News
                                        ";
            }
            // line 64
            echo "                                    </div>
                                    <div class=\"columns post\">
                                        <p class=\"post-title\">";
            // line 66
            echo $this->getAttribute((isset($context["news"]) ? $context["news"] : null), "title");
            echo "</p>
                                        <p class=\"post-date\">Posted on ";
            // line 67
            echo $this->getAttribute((isset($context["news"]) ? $context["news"] : null), "date");
            echo "</p>
                                        <p class=\"post-detail\">";
            // line 68
            echo $this->getAttribute((isset($context["news"]) ? $context["news"] : null), "highlight");
            echo "</p>
                                    </div>
                                    <div class=\"columns post-link\">
                                        <a href=\"";
            // line 71
            echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
            echo "/news/detail/";
            echo $this->getAttribute((isset($context["news"]) ? $context["news"] : null), "slug");
            echo "\" class=\"button\">
                                            ";
            // line 72
            echo Lang::get("home_detail");
            echo " 
                                            <img src=\"";
            // line 73
            echo Uri::base();
            echo "assets/css/images/read-more.png\"/> 
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['news'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 80
        echo "    
            </div>
        </div>
    </div>
    <div class=\"expanded row custom-left life-at-otsuka\">
        <div class=\"title\">
            <h2>";
        // line 86
        echo Lang::get("title_lao");
        echo "</h2>
        </div>
        <div class=\"medium-8 medium-offset-2 end columns custom-content\" data-equalizer=\"life-at-otsuka\">
            <div class=\"row gallery\">
                <div class=\"large-6 columns\" data-equalizer-watch=\"life-at-otsuka\">
                    <p class=\"subtitle\">";
        // line 91
        echo Lang::get("home_story2");
        echo "</p>
                </div>
                <div class=\"large-6 columns\" data-equalizer-watch=\"life-at-otsuka\">
                    <div class=\"expanded row\">
                        <div class=\"medium-6 columns\">
                            <img src=\"";
        // line 96
        echo Uri::base();
        echo "/assets/css/images/ornamen-lao-2.jpg\" alt=\"life at otsuka gallery\"/>
                        </div>
                        <div class=\"medium-6 columns\">
                            <img src=\"";
        // line 99
        echo Uri::base();
        echo "/assets/css/images/ornamen-lao-1.jpg\" alt=\"life at otsuka gallery\"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"row story-at-otsuka\">
                <div class=\"row\">
                    <div class=\"medium-12 columns\">
                        <div class=\"row\">
                            <div class=\"orbit\" role=\"region\" aria-label=\"Favorite Space Pictures\" data-orbit data-options=\"autoPlay:false;\">
                                <ul class=\"orbit-container story-otsuka\" data-equalizer=\"story-at-otsuka\" data-equalize-on=\"large\">
                                ";
        // line 110
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["lao_detail"]) ? $context["lao_detail"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["lao"]) {
            // line 111
            echo "                                    <li class=\"";
            echo (($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "first")) ? ("is-active") : (""));
            echo " orbit-slide\">
                                        <div class=\"row\">
                                            <div class=\"large-3 large-push-9 columns nopadd otsuka-staff\" data-equalizer-watch=\"story-at-otsuka\">
                                                <img class=\"orbit-image\" src=\"";
            // line 114
            echo Uri::base();
            echo "media/lao-photo/";
            echo $this->getAttribute((isset($context["lao"]) ? $context["lao"] : null), "photo");
            echo "\" alt=\"";
            echo $this->getAttribute((isset($context["lao"]) ? $context["lao"] : null), "name");
            echo "\">
                                                <div class=\"columns story-profile\">
                                                    <p>";
            // line 116
            echo $this->getAttribute((isset($context["lao"]) ? $context["lao"] : null), "name");
            echo "</p>
                                                    <span>";
            // line 117
            echo $this->getAttribute((isset($context["lao"]) ? $context["lao"] : null), "job");
            echo ", ";
            echo $this->getAttribute((isset($context["lao"]) ? $context["lao"] : null), "location");
            echo "</span>
                                                </div>
                                            </div>
                                            <div class=\"large-9 large-pull-3 columns\" data-equalizer-watch=\"story-at-otsuka\">
                                                <p>";
            // line 121
            echo Lang::get("home_story");
            echo "</p>
                                                <img class=\"quote\" src=\"";
            // line 122
            echo Uri::base();
            echo "/assets/img/home/quote.jpg\"/>
                                                <p>";
            // line 123
            echo $this->getAttribute((isset($context["lao"]) ? $context["lao"] : null), "story");
            echo "</p>
                                                <div class=\"post-link\">
                                                    <div class=\"small-9 columns\">
                                                        <a href=\"#\">";
            // line 126
            echo Lang::get("home_detail_Story");
            echo "</a>
                                                    </div>
                                                    <div class=\"small-3 columns\">
                                                        <i class=\"fa fa-long-arrow-right\" aria-hidden=\"true\"></i>
                                                    </div>
                                                    <div style=\"clear: both;\"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lao'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 136
        echo "    
                                </ul>
                                <nav class=\"orbit-bullets\">
                                ";
        // line 139
        $context["arr_length"] = twig_length_filter($this->env, (isset($context["lao_detail"]) ? $context["lao_detail"] : null));
        // line 140
        echo "                                    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(0, ((isset($context["arr_length"]) ? $context["arr_length"] : null) - 1)));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 141
            echo "                                        <button class=\"";
            echo (($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "first")) ? ("is-active") : (""));
            echo "\" data-slide=\"";
            echo (isset($context["i"]) ? $context["i"] : null);
            echo "\"><span class=\"show-for-sr\">";
            echo (isset($context["i"]) ? $context["i"] : null);
            echo " slide details.</span></button>
                                    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 143
        echo "                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class=\"expanded row feeds-updates\">
        <div class=\"medium-8 medium-offset-2 end columns custom-content\">
            <h2>";
        // line 153
        echo Lang::get("title_feed");
        echo "</h2>
        </div>
        <div class=\"medium-8 medium-offset-2 end columns custom-content\">
            <div class=\"row\">
                <div class=\"medium-12 columns\">
                    <div class=\"large-6 columns feeds-wrap\" style=\"padding-left: 0;padding-right: 0\">
                        <div class=\"row each-feed\">
                            <div class=\"medium-6 large-6 columns nopadd fb-feed\" style=\"margin-bottom: 10px; overflow: hidden;\">
                                <div class=\"fb-page\" data-href=\"https://www.facebook.com/OtsukaAIOCompany/\" data-tabs=\"timeline\"  data-adapt-container-width=\"true\" data-small-header=\"false\" data-hide-cover=\"true\" data-show-facepile=\"false\" data-hide-cta=\"true\">
                                    <blockquote cite=\"https://www.facebook.com/OtsukaAIOCompany/\" class=\"fb-xfbml-parse-ignore\">
                                        <a href=\"https://www.facebook.com/OtsukaAIOCompany/\">PT Amerta Indah Otsuka</a>
                                    </blockquote>
                                </div>
                            </div>
                            <div class=\"medium-6 large-6 columns nopadd fb-image\" style=\"margin-bottom: 10px; \">
                                ";
        // line 173
        echo "                                <div class=\"fb-post\" data-href=\"https://www.facebook.com/OtsukaAIOCompany/posts/1502623416431813:0\" data-adapt-container-width=\"true\" data-height=\"261\" data-show-text=\"false\">
                                    <blockquote cite=\"https://www.facebook.com/OtsukaAIOCompany/posts/1502623416431813:0\" class=\"fb-xfbml-parse-ignore\">
                                        <p>Akhir tahun adalah saat yang tepat untuk menelusuri kembali hal apa saja yang sudah kita raih sepanjang tahun dan...</p>Posted by 
                                        <a href=\"https://www.facebook.com/OtsukaAIOCompany/\">PT Amerta Indah Otsuka</a> on&nbsp;
                                        <a href=\"https://www.facebook.com/OtsukaAIOCompany/posts/1502623416431813:0\">Friday, December 30, 2016</a>
                                    </blockquote>
                                </div>
                            </div>
                            <a class=\"twitter-grid\" data-chrome=\"nofooter\" data-limit=\"2\" data-partner=\"tweetdeck\" href=\"https://twitter.com/Otsuka_AIO/timelines/813948001219530753\"></a> 
                            <script async src=\"//platform.twitter.com/widgets.js\" charset=\"utf-8\"></script>
                        </div>
                    </div>
                    <div class=\"large-6 columns end instagram-wrapper\" style=\"padding-right: 0\">
                        <div class=\"instagram-feed\">
                            <i class=\"fa fa-instagram\" aria-hidden=\"true\"></i>
                            <div class=\"col-md-4 col-sm-6 col-xs-12 item_box\">
                                <div class=\"grid\">
                                    <figure class=\"effect-zoe\">
                                        <a href=\"";
        // line 191
        echo (isset($context["pic_link"]) ? $context["pic_link"] : null);
        echo "\" target=\"_blank\">
                                            <img class=\"img-responsive photo-thumb\" src=\"";
        // line 192
        echo (isset($context["pic_src"]) ? $context["pic_src"] : null);
        echo "\" alt=\"";
        echo (isset($context["pic_text"]) ? $context["pic_text"] : null);
        echo "\">
                                            <figcaption>
                                                <p class=\"icon-links\">
                                                    ";
        // line 195
        echo (isset($context["pic_text"]) ? $context["pic_text"] : null);
        echo "
                                                </p>
                                            </figcaption>
                                        </a>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

";
    }

    // line 210
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 211
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    <script>

        \$( window ).load(function() {
//        banner
            var banner = \$('.orbit').height();
            var orbitButton = \$('.orbit-bullets-container').height();
            var positionTop = ((banner - orbitButton) / 2);
            \$('.orbit-bullets-container').css('top', positionTop);
//            \$('.fb-page').css('width',\$('.instagram-wrapper').width());
//        latest news
//            var maxHeight = \$('.each-news > .medium-5').height();
//            var postLink = \$('.each-news > .medium-7 > .post-link').height();
//            \$('.each-news > .medium-7 > .post').css('height',maxHeight-postLink);
//            console.log(maxHeight);
//        update feed
            if(\$(window).width()>1024){
                var maxFeed = \$('.instagram-feed').height();
                var eachHeight = (maxFeed / 2);
                console.log(maxFeed);
                console.log(eachHeight);
                \$('.each-feed > .columns').css('height',eachHeight-19);
            }
            else if(\$(window).width() == 1024){
                \$('.fb-wrap').css('height',\$('.facebook-feed').height());
                \$('.fb-page').attr('data-width','431');
                \$('.fb-page').attr('data-height','431');
            }
            else if(\$(window).width()<641){
                
                \$('.story-at-otsuka .orbit .orbit-bullets').css('top',\$('.otsuka-staff').height()-40);
            }
            else if(\$(window).width()==640){

                \$('.fb-page').attr('data-width','640');
                \$('.fb-page').attr('data-height','431');
            }
            \$('.story-at-otsuka .orbit-bullets button').click(function(){
                \$(\"body, html\").animate({
                    scrollTop: jQuery('.otsuka-staff img').offset().top
                });
            });
        });

    
    </script>

";
    }

    public function getTemplateName()
    {
        return "home_new.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  524 => 211,  521 => 210,  502 => 195,  494 => 192,  490 => 191,  470 => 173,  452 => 153,  440 => 143,  419 => 141,  401 => 140,  399 => 139,  394 => 136,  369 => 126,  363 => 123,  359 => 122,  355 => 121,  346 => 117,  342 => 116,  333 => 114,  326 => 111,  309 => 110,  295 => 99,  289 => 96,  281 => 91,  273 => 86,  265 => 80,  251 => 73,  247 => 72,  241 => 71,  235 => 68,  231 => 67,  227 => 66,  223 => 64,  217 => 62,  211 => 60,  209 => 59,  199 => 56,  192 => 54,  189 => 53,  185 => 52,  174 => 48,  170 => 47,  165 => 45,  158 => 40,  137 => 38,  119 => 37,  117 => 36,  112 => 33,  105 => 31,  102 => 30,  94 => 28,  91 => 27,  88 => 26,  80 => 24,  77 => 23,  75 => 22,  67 => 21,  64 => 20,  60 => 19,  47 => 8,  44 => 7,  38 => 5,  33 => 4,  30 => 3,);
    }
}
