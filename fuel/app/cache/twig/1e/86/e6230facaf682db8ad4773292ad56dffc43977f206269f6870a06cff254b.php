<?php

/* backend/base.twig */
class __TwigTemplate_1e86e6230facaf682db8ad4773292ad56dffc43977f206269f6870a06cff254b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'backend_css' => array($this, 'block_backend_css'),
            'backend_content' => array($this, 'block_backend_content'),
            'backend_js' => array($this, 'block_backend_js'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html class=\"bg-black\">
    <head>
        <meta charset=\"UTF-8\">
\t\t<title>";
        // line 5
        echo (isset($context["meta_title"]) ? $context["meta_title"] : null);
        echo "</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
\t\t
        ";
        // line 8
        $this->displayBlock('backend_css', $context, $blocks);
        // line 25
        echo "    </head>
    <body class=\"";
        // line 26
        echo (isset($context["body_tag_class"]) ? $context["body_tag_class"] : null);
        echo "\">

        ";
        // line 28
        $this->displayBlock('backend_content', $context, $blocks);
        // line 29
        echo "
        ";
        // line 30
        $this->displayBlock('backend_js', $context, $blocks);
        // line 43
        echo "    </body>
</html>
";
    }

    // line 8
    public function block_backend_css($context, array $blocks = array())
    {
        // line 9
        echo "\t\t<!-- bootstrap 3.0.2 -->
\t\t";
        // line 10
        echo Asset::css("bootstrap.min.css");
        echo "
        
        <!-- font Awesome -->
\t\t";
        // line 13
        echo Asset::css("font-awesome.min.css");
        echo "
        
        <!-- Theme style -->
\t\t";
        // line 16
        echo Asset::css("AdminLTE.css");
        echo "
\t\t
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
          <script src=\"https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js\"></script>
        <![endif]-->
\t\t";
    }

    // line 28
    public function block_backend_content($context, array $blocks = array())
    {
    }

    // line 30
    public function block_backend_js($context, array $blocks = array())
    {
        // line 31
        echo "\t\t<!-- jQuery 2.0.2 -->
\t\t<!-- <script src=\"http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js\"></script> -->
\t\t
\t\t<!-- jQuery 1.11.1 -->
        ";
        // line 35
        echo Asset::js("jquery-1.11.1.min.js");
        echo "
\t\t
\t\t<!-- Bootstrap -->
\t\t";
        // line 38
        echo Asset::js("bootstrap.min.js");
        echo "
        
        <!-- AdminLTE App -->
        ";
        // line 41
        echo Asset::js("AdminLTE/app.js");
        echo "
\t\t";
    }

    public function getTemplateName()
    {
        return "backend/base.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  114 => 41,  108 => 38,  102 => 35,  96 => 31,  93 => 30,  88 => 28,  75 => 16,  69 => 13,  63 => 10,  60 => 9,  57 => 8,  51 => 43,  49 => 30,  44 => 28,  39 => 26,  22 => 1,  46 => 29,  40 => 8,  36 => 25,  34 => 8,  31 => 4,  28 => 5,);
    }
}
