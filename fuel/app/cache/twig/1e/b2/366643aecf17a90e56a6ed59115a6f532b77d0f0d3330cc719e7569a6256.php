<?php

/* template_csr_new.twig */
class __TwigTemplate_1eb2366643aecf17a90e56a6ed59115a6f532b77d0f0d3330cc719e7569a6256 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"columns no-padding banner animatedParent\">
    <img src=\"";
        // line 2
        echo Uri::base();
        echo "/media/csr/otsuka-csr.jpg\">
    <div class=\"medium-4 small-8 columns banner-desc animated fadeInUp\">
        <p class=\"title\">Corporate<br/>social<br/>responsibility</p>
        <div style=\"border-bottom: 1px solid #bdbdbd; margin: 10px auto\"></div>
        <p class=\"desc\">";
        // line 6
        echo Lang::get("txt_csr");
        echo "</p>
    </div>
</div>
<div class=\"row\">
    <div class=\"columns devider\">
        <img src=\"";
        // line 11
        echo Uri::base();
        echo "/assets/img/csr-new/icon-devider.jpg\">
    </div>
    <div class=\"columns csr-menu\">
        <div class=\"medium-4 small-4 columns each-menu ";
        // line 14
        echo (isset($context["active_cerdaskan_bangsa"]) ? $context["active_cerdaskan_bangsa"] : null);
        echo "\">
            <a href=\"";
        // line 15
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/csr/cerdaskan-bangsa\">
                <img src=\"";
        // line 16
        echo Uri::base();
        echo "/assets/img/csr-new/icon-satu-hati-cerdaskan-bangsa.jpg\"/>
            </a>
            <div class=\"hr\"></div>
        </div>
        <div class=\"medium-4 small-4 columns each-menu ";
        // line 20
        echo (isset($context["active_peduli_lingkungan"]) ? $context["active_peduli_lingkungan"] : null);
        echo "\">
            <a href=\"";
        // line 21
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/csr/peduli-lingkungan\">
                <img src=\"";
        // line 22
        echo Uri::base();
        echo "/assets/img/csr-new/icon-satu-hati-peduli-lingkungan.jpg\"/>
            </a>
            <div class=\"hr\"></div>
        </div>
        <div class=\"medium-4 small-4 columns each-menu ";
        // line 26
        echo (isset($context["active_sehatkan_bangsa"]) ? $context["active_sehatkan_bangsa"] : null);
        echo "\">
            <a href=\"";
        // line 27
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/csr/sehatkan-bangsa\">
                <img src=\"";
        // line 28
        echo Uri::base();
        echo "/assets/img/csr-new/icon-satu-hati-sehatkan-bangsa.jpg\"/>
            </a>
            <div class=\"hr\"></div>
        </div>
    </div>
    <div class=\"columns csr-title\">
        <div style=\"border-bottom: 1px solid #ced2d6\"></div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "template_csr_new.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  84 => 28,  79 => 27,  68 => 22,  59 => 20,  52 => 16,  47 => 15,  43 => 14,  37 => 11,  29 => 6,  22 => 2,  19 => 1,  134 => 47,  131 => 46,  126 => 49,  124 => 46,  119 => 44,  114 => 43,  111 => 42,  104 => 36,  101 => 35,  95 => 38,  93 => 35,  75 => 26,  65 => 12,  63 => 21,  60 => 10,  57 => 9,  51 => 8,  45 => 6,  41 => 5,  36 => 4,  33 => 3,);
    }
}
