<?php

/* press_detail.twig */
class __TwigTemplate_15c9f81960a0010c8a2d643ca40a9cd95d02ff0bbbe1a56dfa8c464afbb43c16 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_meta_twitter' => array($this, 'block_frontend_meta_twitter'),
            'frontend_meta_facebook' => array($this, 'block_frontend_meta_facebook'),
            'about_menu_us' => array($this, 'block_about_menu_us'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'back_to_top' => array($this, 'block_back_to_top'),
            'frontend_js' => array($this, 'block_frontend_js'),
            'back_to_top_js' => array($this, 'block_back_to_top_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 5
        echo "    ";
        $this->displayParentBlock("frontend_css", $context, $blocks);
        echo "
    ";
        // line 6
        echo Asset::css("custom/press.css");
        echo "
";
    }

    // line 8
    public function block_frontend_meta_twitter($context, array $blocks = array())
    {
        // line 9
        echo "    <meta name=\"twitter:card\" content=\"summary\">
    <meta name=\"twitter:title\" content=\"";
        // line 10
        echo (isset($context["meta_title"]) ? $context["meta_title"] : null);
        echo "\" />
    <meta name=\"twitter:description\" content=\"";
        // line 11
        echo (isset($context["meta_desc"]) ? $context["meta_desc"] : null);
        echo "\" />
    <meta name=\"twitter:url\" content=\"";
        // line 12
        echo Uri::current();
        echo "\" />
    <meta name=\"twitter:image\" content=\"";
        // line 13
        echo ((Uri::base() . $this->getAttribute((isset($context["detail_data"]) ? $context["detail_data"] : null), "get_image_path", array(), "method")) . $this->getAttribute((isset($context["detail_data"]) ? $context["detail_data"] : null), "image"));
        echo "\" />
";
    }

    // line 15
    public function block_frontend_meta_facebook($context, array $blocks = array())
    {
        // line 16
        echo "    <meta property=\"og:url\"           content=\"";
        echo Uri::current();
        echo "\" />
    <meta property=\"og:type\"          content=\"website\" />
    <meta property=\"og:title\"         content=\"";
        // line 18
        echo (isset($context["meta_title"]) ? $context["meta_title"] : null);
        echo "\" />
    <meta property=\"og:description\"   content=\"";
        // line 19
        echo (isset($context["meta_desc"]) ? $context["meta_desc"] : null);
        echo "\" />
    <meta property=\"og:image\"         content=\"";
        // line 20
        echo ((Uri::base() . $this->getAttribute((isset($context["detail_data"]) ? $context["detail_data"] : null), "get_image_path", array(), "method")) . $this->getAttribute((isset($context["detail_data"]) ? $context["detail_data"] : null), "image"));
        echo "\" />
";
    }

    // line 22
    public function block_about_menu_us($context, array $blocks = array())
    {
        echo ".test";
    }

    // line 23
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 24
        echo "    <div class=\"press-detail\">
        <div class=\"row\">
            <div class=\"large-12 columns\">
                <h4 class=\"title\">";
        // line 27
        echo $this->getAttribute((isset($context["detail_data"]) ? $context["detail_data"] : null), "title");
        echo "</h4>
                <div class=\"posted\">
                    Posted on ";
        // line 29
        echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["detail_data"]) ? $context["detail_data"] : null), "date"), "F d, Y");
        echo "
                </div>
                <div class=\"row\">
                    <div class=\"large-10 large-centered columns banner\">
                        <img src=\"";
        // line 33
        echo ((Uri::base() . $this->getAttribute((isset($context["detail_data"]) ? $context["detail_data"] : null), "get_image_path", array(), "method")) . $this->getAttribute((isset($context["detail_data"]) ? $context["detail_data"] : null), "image"));
        echo "\" alt=\"";
        echo $this->getAttribute((isset($context["detail_data"]) ? $context["detail_data"] : null), "title");
        echo "\" />
                    </div>
                </div>
                <div class=\"text-detail\">
                    ";
        // line 37
        echo $this->getAttribute((isset($context["detail_data"]) ? $context["detail_data"] : null), "content");
        echo "
                </div>
                <div class=\"text-share\">
                    Share this article...<br>
                    <div class=\"row\">
                        <div class=\"large-2 large-centered columns\">
                            <div class=\"small-6 columns\">
                                <a href=\"https://twitter.com/share\" class=\"twitter-share-button\" data-url=\"";
        // line 44
        echo Uri::current();
        echo "\" data-text=\"";
        echo (isset($context["meta_title"]) ? $context["meta_title"] : null);
        echo " - ";
        echo (isset($context["meta_desc"]) ? $context["meta_desc"] : null);
        echo "\" target=\"_blank\" data-popup=\"true\"><img src=\"";
        echo Uri::base();
        echo "assets/img/press/twitter.png\"/> </a>
                                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                            </div>
                            <div class=\"small-6 columns\">
                                <a target=\"_blank\" href=\"http://www.facebook.com/sharer/sharer.php?s=100&amp;p[url]=";
        // line 48
        echo Uri::current();
        echo "&amp;p[images][0]=";
        echo ((Uri::base() . $this->getAttribute((isset($context["detail_data"]) ? $context["detail_data"] : null), "get_image_path", array(), "method")) . $this->getAttribute((isset($context["detail_data"]) ? $context["detail_data"] : null), "image"));
        echo "&amp;p[title]=";
        echo (isset($context["meta_title"]) ? $context["meta_title"] : null);
        echo "&amp;p[summary]=";
        echo (isset($context["meta_desc"]) ? $context["meta_desc"] : null);
        echo "\"><img src=\"";
        echo Uri::base();
        echo "assets/img/press/fb.png\"></a>
                                ";
        // line 60
        echo "                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"show-for-small-only\">
            ";
        // line 67
        $this->displayBlock('back_to_top', $context, $blocks);
        // line 70
        echo "        </div>
    </div>
";
    }

    // line 67
    public function block_back_to_top($context, array $blocks = array())
    {
        // line 68
        echo "                ";
        $this->displayParentBlock("back_to_top", $context, $blocks);
        echo "
            ";
    }

    // line 74
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 75
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    <script type=\"text/javascript\">
        ";
        // line 77
        $this->displayBlock('back_to_top_js', $context, $blocks);
        // line 80
        echo "    </script>
";
    }

    // line 77
    public function block_back_to_top_js($context, array $blocks = array())
    {
        // line 78
        echo "        ";
        $this->displayParentBlock("back_to_top_js", $context, $blocks);
        echo "
        ";
    }

    public function getTemplateName()
    {
        return "press_detail.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  212 => 78,  209 => 77,  204 => 80,  202 => 77,  196 => 75,  193 => 74,  186 => 68,  183 => 67,  177 => 70,  175 => 67,  166 => 60,  154 => 48,  141 => 44,  131 => 37,  122 => 33,  115 => 29,  110 => 27,  105 => 24,  102 => 23,  96 => 22,  90 => 20,  86 => 19,  82 => 18,  76 => 16,  73 => 15,  67 => 13,  63 => 12,  59 => 11,  55 => 10,  52 => 9,  49 => 8,  43 => 6,  38 => 5,  35 => 4,);
    }
}
