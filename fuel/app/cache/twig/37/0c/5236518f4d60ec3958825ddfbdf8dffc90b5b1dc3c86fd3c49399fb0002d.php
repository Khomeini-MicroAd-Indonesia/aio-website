<?php

/* pages/views/template_frontend.twig */
class __TwigTemplate_370c5236518f4d60ec3958825ddfbdf8dffc90b5b1dc3c86fd3c49399fb0002d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_head_js' => array($this, 'block_frontend_head_js'),
            'frontend_meta_twitter' => array($this, 'block_frontend_meta_twitter'),
            'frontend_meta_facebook' => array($this, 'block_frontend_meta_facebook'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'back_to_top' => array($this, 'block_back_to_top'),
            'frontend_js' => array($this, 'block_frontend_js'),
            'back_to_top_js' => array($this, 'block_back_to_top_js'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html class=\"ie8\">
<head>
    <meta charset=\"UTF-8\">
    <title>";
        // line 5
        echo (isset($context["meta_title"]) ? $context["meta_title"] : null);
        echo "</title>
    <meta name=\"description\" content=\"";
        // line 6
        echo (isset($context["meta_desc"]) ? $context["meta_desc"] : null);
        echo "\">
    <meta name=\"viewport\" content=\"width=device-width\">
    <link rel=\"shortcut icon\" href=\"";
        // line 8
        echo Uri::base();
        echo "assets/css/images/logo_aio.png\" />
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
    ";
        // line 10
        $this->displayBlock('frontend_css', $context, $blocks);
        // line 17
        echo "    ";
        $this->displayBlock('frontend_head_js', $context, $blocks);
        // line 21
        echo "    ";
        $this->displayBlock('frontend_meta_twitter', $context, $blocks);
        // line 23
        echo "    ";
        $this->displayBlock('frontend_meta_facebook', $context, $blocks);
        // line 25
        echo "    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '";
        // line 31
        echo Config::get("config_basic.google_analytic_code");
        echo "', 'auto');
        ga('send', 'pageview');
    </script>
</head>
<body>
<div class=\"hide-for-small-only\" data-sticky-container>
    <div class=\"sticky is-stuck\">
        <div class=\"header\">
            <div class=\"top-header\" data-equalizer=\"top-item\">
                <div class=\"large-7 small-7 columns\" data-equalizer-watch=\"top-item\">
                    <div data-equalizer=\"left-top\">
                        <div class=\"small-3 columns\" data-equalizer-watch=\"left-top\">
                            <a href=\"";
        // line 43
        echo Uri::base();
        echo "\"><img class=\"main-logo\" src=\"";
        echo Uri::base();
        echo "/assets/css/images/logo-otsuka.png\"/></a>
                        </div>
                        <div class=\"small-2 columns\" data-equalizer-watch=\"left-top\">
                            <ul class=\"vertical medium-horizontal menu top-menu\">
                                <li>
                                    <ul class=\"dropdown menu lang\" style=\"width: 66px!important;\" data-dropdown-menu>
                                        <li>
                                            <a href=\"javascript:\">";
        // line 50
        echo $this->getAttribute($this->getAttribute((isset($context["valid_lang"]) ? $context["valid_lang"] : null), (isset($context["current_lang"]) ? $context["current_lang"] : null), array(), "array"), "label");
        echo "&nbsp;&nbsp;&nbsp;&nbsp;</a>
                                            <ul class=\"menu\">
                                                ";
        // line 52
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["valid_lang"]) ? $context["valid_lang"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["main_menu"]) {
            if ((!$this->getAttribute((isset($context["main_menu"]) ? $context["main_menu"] : null), "active"))) {
                // line 53
                echo "                                                    <li><a href=\"";
                echo $this->getAttribute((isset($context["main_menu"]) ? $context["main_menu"] : null), "link");
                echo "\">";
                echo $this->getAttribute((isset($context["main_menu"]) ? $context["main_menu"] : null), "label");
                echo "&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                                                ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['main_menu'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class=\"large-5 small-5 columns\" data-equalizer-watch=\"top-item\">
                    <ul class=\"vertical medium-horizontal menu top-menu\">
                        <li><a href=\"https://www.facebook.com/OtsukaAIOCompany/\" target=\"_blank\"><img src=\"";
        // line 66
        echo Uri::base();
        echo "assets/css/images/fb_icon.png\"/></a></li>
                        <li><a href=\"https://twitter.com/Otsuka_AIO\" target=\"_blank\"><img src=\"";
        // line 67
        echo Uri::base();
        echo "assets/css/images/twit_icon.png\"/></a></li>
                        <li><a href=\"https://www.linkedin.com/company/pt-amerta-indah-otsuka\" target=\"_blank\"><img src=\"";
        // line 68
        echo Uri::base();
        echo "/assets/css/images/in_icon.png\"/></a></li>
                        <li><a href=\"https://www.instagram.com/otsuka_aio/\" target=\"_blank\"><img src=\"";
        // line 69
        echo Uri::base();
        echo "/assets/css/images/insta-icon.png\"></a> </li>
                        <li><a href=\"";
        // line 70
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "contact-us\">Contact&nbsp;&&nbsp;Address</a></li>
                        <li>
                            <form class=\"navbar-form hidden-for-small-only\" role=\"search\" action=\"";
        // line 72
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/search\" id=\"form-search-product\">
                                <div>
                                    <div class=\"row collapse\">
                                        <input class=\"search\" name=\"keyword\" type=\"text\" placeholder=\"Search\" id=\"txt-search-keyword\">
                                    </div>
                                </div>
                            </form>
                        </li>
                    </ul>
                </div>
                <div style=\"clear: both;\"></div>
            </div>
            <div class=\"bottom-header\" data-equalizer=\"bottom-item\">
                <div class=\"large-7 small-7 columns\" data-equalizer-watch=\"bottom-item\">
                    <ul class=\"dropdown menu bottom-menu\" data-dropdown-menu>
                        <li><a href=\"";
        // line 87
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/home\"><img src=\"";
        echo Uri::base();
        echo "/assets/css/images/icon-home.jpg\" class=\"menu-home\"> </a> </li>
                        <li>
                            ABOUT
                            <ul class=\"menu\">
                                <li><a href=\"";
        // line 91
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us\">ABOUT US</a></li>
                                <li><a href=\"";
        // line 92
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/awards-and-certificates\">AWARDS & CERTIFICATES</a></li>
                                <li><a href=\"";
        // line 93
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/history\">HISTORY</a></li>
                            </ul>
                        </li>
                        <li>
                            BRAND
                            <ul class=\"menu\">
                                <li><a href=\"";
        // line 99
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/brand/pocari-sweat\">POCARI SWEAT</a></li>
                                <li><a href=\"";
        // line 100
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/brand/soyjoy\">SOYJOY</a></li>
                                <li><a href=\"";
        // line 101
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/brand/ionessence\">IONESSENCE</a></li>
                            </ul>
                        </li>
                        <li>
                            CSR
                            <ul class=\"menu\">
                                <li><a href=\"";
        // line 107
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/csr/cerdaskan-bangsa\">SATU HATI CERDASKAN BANGSA</a></li>
                                <li><a href=\"";
        // line 108
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/csr/peduli-lingkungan\">SATU HATI PEDULI LINGKUNGAN</a></li>
                                <li><a href=\"";
        // line 109
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/csr/sehatkan-bangsa\">SATU HATI SEHATKAN BANGSA</a></li>
                            </ul>
                        </li>
                        <li>
                            EXPLORION TOUR
                            <ul class=\"menu\">
                                <li><a href=\"";
        // line 115
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/explorion-advanture\">EXPLORION ADVENTURE</a></li>
                                <li><a href=\"";
        // line 116
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/factory-tour\">FACTORY TOUR</a> </li>
                            </ul>
                        <li><a href=\"";
        // line 118
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/our-event\">PRESS ROOM</a></li>
                        <li><a href=\"";
        // line 119
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/career\">CAREER</a></li>
                        <li><a href=\"";
        // line 120
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/distribution\">DISTRIBUTION</a></li>
                        <li><a href=\"";
        // line 121
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/faq\">FAQ</a></li>
                    </ul>
                </div>
                <div class=\"large-5 small-5 columns\" data-equalizer-watch=\"bottom-item\">
                    Otsuka-people creating new products<br/>
                    for better health worldwide
                </div>
                <div style=\"clear: both;\"></div>
            </div>
        </div>
    </div>
</div>
<div class=\"off-canvas-wrapper\">
    <div class=\"off-canvas-wrapper-inner\" data-off-canvas-wrapper>
        <div class=\"off-canvas position-left\" id=\"offCanvas\" data-off-canvas>
            <div class=\"logo-mobile\">
                <a href=\"";
        // line 137
        echo Uri::base();
        echo "\">
                    <img src=\"";
        // line 138
        echo Uri::base();
        echo "/assets/css/images/logo-otsuka.jpg\"/>
                </a>
            </div>
            <!-- Close button -->
            <button class=\"close-button\" aria-label=\"Close menu\" type=\"button\" data-close>
                <span aria-hidden=\"true\">&times;</span>
            </button>
            <ul class=\"menu medium-horizontal\">
                <li><a href=\"https://www.facebook.com/OtsukaAIOCompany/\" target=\"_blank\"><img src=\"";
        // line 146
        echo Uri::base();
        echo "assets/css/images/fb_icon.png\"/></a></li>
                <li><a href=\"https://twitter.com/Otsuka_AIO\" target=\"_blank\"><img src=\"";
        // line 147
        echo Uri::base();
        echo "assets/css/images/twit_icon.png\"/></a></li>
                <li><a href=\"https://www.linkedin.com/company/pt-amerta-indah-otsuka\" target=\"_blank\"><img src=\"";
        // line 148
        echo Uri::base();
        echo "/assets/css/images/in_icon.png\"/></a></li>
                <li><a href=\"https://www.instagram.com/otsuka_aio/\" target=\"_blank\"><img src=\"";
        // line 149
        echo Uri::base();
        echo "/assets/css/images/insta-icon.png\"></a> </li>
                <li><a href=\"";
        // line 150
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "contact-us\">Contact&nbsp;&&nbsp;Address</a></li>
                <li>
                    <ul class=\"dropdown menu lang\" style=\"width: 66px!important;\" data-dropdown-menu>
                        <li>
                            <a href=\"javascript:\">";
        // line 154
        echo $this->getAttribute($this->getAttribute((isset($context["valid_lang"]) ? $context["valid_lang"] : null), (isset($context["current_lang"]) ? $context["current_lang"] : null), array(), "array"), "label");
        echo "&nbsp;&nbsp;&nbsp;&nbsp;</a>
                            <ul class=\"menu\">
                                ";
        // line 156
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["valid_lang"]) ? $context["valid_lang"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["main_menu"]) {
            if ((!$this->getAttribute((isset($context["main_menu"]) ? $context["main_menu"] : null), "active"))) {
                // line 157
                echo "                                    <li><a href=\"";
                echo $this->getAttribute((isset($context["main_menu"]) ? $context["main_menu"] : null), "link");
                echo "\">";
                echo $this->getAttribute((isset($context["main_menu"]) ? $context["main_menu"] : null), "label");
                echo "&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                                ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['main_menu'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 159
        echo "                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class=\"vertical menu\" data-drilldown>
                <li class=\"dropdown\">
                    <a href=\"";
        // line 166
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us\">ABOUT</a>
                    <ul class=\"vertical menu nested\">
                        <li><a href=\"";
        // line 168
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us\">ABOUT US</a></li>
                        <li><a href=\"";
        // line 169
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/awards-and-certificates\">AWARDS & CERTIFICATES</a></li>
                        <li><a href=\"";
        // line 170
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/history\">HISTORY</a></li>
                    </ul>
                </li>
                <li class=\"dropdown\">
                    <a href=\"";
        // line 174
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/brand\">BRAND</a>
                    <ul class=\"vertical menu nested\">
                        <li><a href=\"";
        // line 176
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/brand/pocari-sweat\">POCARI SWEAT</a></li>
                        <li><a href=\"";
        // line 177
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/brand/soyjoy\">SOYJOY</a></li>
                        <li><a href=\"";
        // line 178
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/brand/ionessence\">IONESSENCE</a></li>
                    </ul>
                </li>
                <li>
                    <a href=\"";
        // line 182
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/csr\">CSR</a>
                    <ul class=\"vertical menu nested\">
                        <li><a href=\"";
        // line 184
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/csr/cerdaskan-bangsa\">SATU HATI CERDASKAN BANGSA</a></li>
                        <li><a href=\"";
        // line 185
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/csr/peduli-lingkungan\">SATU HATI PEDULI LINGKUNGAN</a></li>
                        <li><a href=\"";
        // line 186
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/csr/sehatkan-bangsa\">SATU HATI SEHATKAN BANGSA</a></li>
                    </ul>
                </li>
                <li>
                    <a href=\"";
        // line 190
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/explorion-advanture\">EXPLORION TOUR</a>
                    <ul class=\"vertical menu nested\">
                        <li><a href=\"";
        // line 192
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/explorion-advanture\">EXPLORION ADVENTURE</a></li>
                        <li><a href=\"";
        // line 193
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/factory-tour\">FACTORY TOUR</a> </li>
                    </ul>
                <li>
                
                <li><a href=\"";
        // line 197
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/our-event\">PRESS ROOM</a></li>
                <li><a href=\"";
        // line 198
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/career\">CAREER</a></li>
                <li><a href=\"";
        // line 199
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/distribution\">DISTRIBUTION</a></li>
                <li><a href=\"";
        // line 200
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/faq\">FAQ</a></li>
            </ul>
            <div style=\"height: 20px;\"></div>

        </div>
        <div class=\"off-canvas-content\" data-off-canvas-content>
            <div class=\"title-bar show-for-small-only\">
                <div class=\"title-bar-left\">
                    <button class=\"menu-icon\" type=\"button\" data-open=\"offCanvas\"></button>
                    <span class=\"title-bar-title\"><a href=\"";
        // line 209
        echo Uri::base();
        echo "\">Otsuka</a></span>
                </div>
                <form class=\"navbar-form hidden-for-small-only\" role=\"search\" action=\"";
        // line 211
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/search\" id=\"form-search-product\">
                    <div>
                        <div class=\"row collapse\">
                            <input class=\"search\" name=\"keyword\" type=\"text\" placeholder=\"Search\" id=\"txt-search-keyword\">
                        </div>
                    </div>
                </form>
            </div>
            ";
        // line 220
        echo "            <div class=\"custom-content\">
                ";
        // line 221
        $this->displayBlock('frontend_content', $context, $blocks);
        // line 229
        echo "            </div>
            ";
        // line 231
        echo "            <div data-equalizer=\"footer\" data-equalize-on=\"medium\">
                <div class=\"large-9 columns footer\" data-equalizer-watch=\"footer\">
                    PT Amerta Indah Otsuka &copy; Copyright ";
        // line 233
        echo twig_date_format_filter($this->env, "now", "Y");
        echo ". All rights reserved.<br/>
                    <span> Client Logos are copyright and trademark of the respective owners / companies.</span>
                </div>
                <div class=\"large-3 columns footer\" data-equalizer-watch=\"footer\">
                    <a href=\"";
        // line 237
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/policy/\">Privacy Policy</a> |
                    <a href=\"";
        // line 238
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/term-of-use\">Term Of Use</a>
                </div>
            </div>
        </div>
    </div>
</div>

";
        // line 245
        $this->displayBlock('frontend_js', $context, $blocks);
        // line 272
        echo "</body>
</html>

";
    }

    // line 10
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 11
        echo "        ";
        echo Asset::css("custom/normalize.css");
        echo "
        ";
        // line 12
        echo Asset::css("custom/foundation.css");
        echo "
        ";
        // line 13
        echo Asset::css("custom/motion-ui.css");
        echo "
        ";
        // line 14
        echo Asset::css("custom/foundation-icons/foundation-icons.css");
        echo "
        ";
        // line 15
        echo Asset::css("custom/style.css");
        echo "
    ";
    }

    // line 17
    public function block_frontend_head_js($context, array $blocks = array())
    {
        // line 18
        echo "        ";
        echo Asset::js("vendor/jquery.min.js");
        echo "
        ";
        // line 19
        echo Asset::js("modernizr.js");
        echo "
    ";
    }

    // line 21
    public function block_frontend_meta_twitter($context, array $blocks = array())
    {
        // line 22
        echo "    ";
    }

    // line 23
    public function block_frontend_meta_facebook($context, array $blocks = array())
    {
        // line 24
        echo "    ";
    }

    // line 221
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 222
        echo "                    ";
        $this->displayBlock('back_to_top', $context, $blocks);
        // line 228
        echo "                ";
    }

    // line 222
    public function block_back_to_top($context, array $blocks = array())
    {
        // line 223
        echo "                        <div class=\"scroll-top-wrapper \">
                        <span class=\"scroll-top-inner\">
                        </span>
                        </div>
                    ";
    }

    // line 245
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 246
        echo "    ";
        echo Asset::js("motion-ui.js");
        echo "
    ";
        // line 247
        echo Asset::js("foundation.js");
        echo "
    ";
        // line 248
        echo Asset::js("app.js");
        echo "
    <script type=\"text/javascript\">
        ";
        // line 250
        $this->displayBlock('back_to_top_js', $context, $blocks);
        // line 270
        echo "    </script>
";
    }

    // line 250
    public function block_back_to_top_js($context, array $blocks = array())
    {
        // line 251
        echo "            \$(function(){
                \$(document).on( 'scroll', function(){
                    if (\$(window).scrollTop() > 200) {
                        \$('.scroll-top-wrapper').addClass('show');
                    } else {
                        \$('.scroll-top-wrapper').removeClass('show');
                    }
                });
                \$('.scroll-top-wrapper').on('click', scrollToTop);
            });

            function scrollToTop() {
                verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
                element = \$('body');
                offset = element.offset();
                offsetTop = offset.top;
                \$('html, body').animate({scrollTop: offsetTop}, 500, 'linear');
            }
        ";
    }

    public function getTemplateName()
    {
        return "pages/views/template_frontend.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  633 => 251,  630 => 250,  623 => 250,  618 => 248,  614 => 247,  609 => 246,  606 => 245,  598 => 223,  595 => 222,  591 => 228,  585 => 221,  581 => 24,  578 => 23,  574 => 22,  571 => 21,  565 => 19,  560 => 18,  557 => 17,  547 => 14,  543 => 13,  539 => 12,  524 => 272,  522 => 245,  506 => 237,  499 => 233,  495 => 231,  492 => 229,  490 => 221,  476 => 211,  471 => 209,  458 => 200,  453 => 199,  448 => 198,  443 => 197,  435 => 193,  430 => 192,  424 => 190,  416 => 186,  411 => 185,  406 => 184,  400 => 182,  392 => 178,  387 => 177,  382 => 176,  376 => 174,  368 => 170,  363 => 169,  358 => 168,  352 => 166,  343 => 159,  331 => 157,  326 => 156,  321 => 154,  310 => 149,  306 => 148,  302 => 147,  298 => 146,  283 => 137,  263 => 121,  253 => 119,  248 => 118,  242 => 116,  227 => 109,  222 => 108,  217 => 107,  207 => 101,  202 => 100,  197 => 99,  187 => 93,  177 => 91,  167 => 87,  149 => 72,  144 => 70,  136 => 68,  132 => 67,  115 => 55,  103 => 53,  98 => 52,  93 => 50,  81 => 43,  58 => 25,  55 => 23,  52 => 21,  49 => 17,  47 => 10,  42 => 8,  37 => 6,  33 => 5,  27 => 1,  709 => 332,  706 => 331,  700 => 334,  698 => 331,  669 => 305,  661 => 299,  644 => 284,  640 => 283,  635 => 282,  632 => 281,  625 => 270,  622 => 262,  603 => 265,  601 => 262,  593 => 257,  588 => 222,  584 => 254,  577 => 252,  568 => 247,  556 => 241,  553 => 240,  551 => 15,  546 => 237,  540 => 234,  534 => 11,  531 => 10,  527 => 227,  517 => 220,  511 => 238,  505 => 211,  497 => 208,  489 => 206,  487 => 220,  482 => 204,  479 => 203,  475 => 202,  465 => 195,  450 => 185,  445 => 183,  441 => 182,  433 => 177,  427 => 174,  418 => 167,  410 => 165,  405 => 164,  401 => 163,  393 => 157,  384 => 150,  379 => 149,  372 => 145,  367 => 144,  360 => 140,  355 => 139,  334 => 121,  324 => 114,  314 => 150,  311 => 108,  297 => 107,  287 => 138,  284 => 104,  267 => 103,  262 => 101,  258 => 120,  246 => 91,  237 => 115,  233 => 85,  221 => 83,  216 => 81,  213 => 80,  209 => 79,  194 => 66,  182 => 92,  176 => 61,  172 => 60,  168 => 59,  161 => 55,  157 => 54,  152 => 51,  148 => 50,  140 => 69,  128 => 66,  124 => 37,  120 => 36,  113 => 34,  110 => 33,  106 => 32,  99 => 27,  92 => 25,  88 => 24,  84 => 22,  73 => 19,  70 => 18,  66 => 31,  57 => 10,  54 => 9,  48 => 7,  44 => 6,  40 => 5,  35 => 4,  32 => 3,);
    }
}
