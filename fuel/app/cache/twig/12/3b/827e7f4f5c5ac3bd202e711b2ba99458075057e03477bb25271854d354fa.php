<?php

/* csr.twig */
class __TwigTemplate_123b827e7f4f5c5ac3bd202e711b2ba99458075057e03477bb25271854d354fa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'about_menu_us' => array($this, 'block_about_menu_us'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("frontend_css", $context, $blocks);
        echo "
    ";
        // line 5
        echo Asset::css("custom/csr.css");
        echo "
";
    }

    // line 7
    public function block_about_menu_us($context, array $blocks = array())
    {
        echo ".test";
    }

    // line 8
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 9
        echo "    ";
        $this->env->loadTemplate("template_csr.twig")->display($context);
        // line 10
        echo "    <div class=\"csr-content\">
        <div class=\"padding-custom\"></div>
            <div class=\"row\" data-equalizer=\"csr-activity-list\">
                ";
        // line 13
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["activities"]) ? $context["activities"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["activity"]) {
            // line 14
            echo "                <div class=\"large-6 small-12 columns csr-padding ";
            echo (($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "last")) ? ("end") : (""));
            echo "\">
                    <div class=\"csr-item\" data-equalizer-watch=\"csr-activity-list\">
                        <img src=\"";
            // line 16
            echo (((Uri::base() . $this->getAttribute((isset($context["activity"]) ? $context["activity"] : null), "get_image_path", array(), "method")) . "media/csr/thumbnail/") . $this->getAttribute((isset($context["activity"]) ? $context["activity"] : null), "image"));
            echo "\" alt=\"";
            echo $this->getAttribute((isset($context["activity"]) ? $context["activity"] : null), "title");
            echo "\" /><br/>
                        <div class=\"csr-content-item\">
                            <span class=\"csr-date\">";
            // line 18
            echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["activity"]) ? $context["activity"] : null), "date"), "d F Y");
            echo "</span><br/>
                            <span class=\"csr-item-title\">";
            // line 19
            echo $this->getAttribute((isset($context["activity"]) ? $context["activity"] : null), "title");
            echo "</span><br/>
                            <div class=\"csr-text\">";
            // line 20
            echo $this->getAttribute((isset($context["activity"]) ? $context["activity"] : null), "highlight");
            echo "</div>
                            <div class=\"read-more\"><a href=\"";
            // line 21
            echo (((Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null)) . "/csr/detail/") . $this->getAttribute((isset($context["activity"]) ? $context["activity"] : null), "slug"));
            echo "\">Read more..</a></div>
                        </div>
                    </div>
                </div>
                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['activity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "            </div>
            ";
        // line 47
        echo "    </div>
";
    }

    // line 50
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 51
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "csr.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  134 => 51,  131 => 50,  126 => 47,  123 => 26,  104 => 21,  100 => 20,  96 => 19,  92 => 18,  85 => 16,  79 => 14,  62 => 13,  57 => 10,  54 => 9,  51 => 8,  45 => 7,  39 => 5,  34 => 4,  31 => 3,);
    }
}
