<?php

/* ionessence.twig */
class __TwigTemplate_8c71863b2b6d4ba102e89def227eeff0011fc50760063dd699fa5b18121ff58d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        echo Asset::css("custom/animations.css");
        echo "
    ";
        // line 5
        $this->displayParentBlock("frontend_css", $context, $blocks);
        echo "
    ";
        // line 6
        echo Asset::css("custom/product.css");
        echo "
";
    }

    // line 8
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 9
        echo "    <div class=\"ionessense columns\">
        <div class=\"columns banner text-center\">
            <img data-interchange=\"[";
        // line 11
        echo Uri::base();
        echo "/assets/img/product/banner-ionessense-small.png, small], [";
        echo Uri::base();
        echo "/assets/img/product/banner-ionessense-medium.png, medium], [";
        echo Uri::base();
        echo "/assets/img/product/banner-ionessense-new.png, large]\">
            <div class=\"columns sub-menu-product\" data-equalizer=\"menu-ionessense\">
                <div class=\"medium-2 medium-offset-3 small-4 columns\">
                    <a href=\"";
        // line 14
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/brand/pocari-sweat\">
                        <div class=\"columns pocari-sweat-color\" data-equalizer-watch=\"menu-ionessense\">
                            <img src=\"";
        // line 16
        echo Uri::base();
        echo "assets/img/product/icon-brand-pocarisweat.png\"/>
                        </div>
                    </a>
                </div>
                <div class=\"medium-2 small-4 columns\">
                    <a href=\"";
        // line 21
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/brand/soyjoy\">
                        <div class=\"columns soyjoy-color\" data-equalizer-watch=\"menu-ionessense\">
                            <img src=\"";
        // line 23
        echo Uri::base();
        echo "assets/img/product/icon-brand-soyjoy.png\"/>
                        </div>
                    </a>
                </div>
                <div class=\"medium-2 small-4 end columns\">
                    <a href=\"";
        // line 28
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/brand/ionessence\">
                        <div class=\"columns ionesence-color\" data-equalizer-watch=\"menu-ionessense\">
                            <img src=\"";
        // line 30
        echo Uri::base();
        echo "assets/img/product/icon-brand-ionessence.png\"/>
                            <span class=\"active-menu\"></span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"columns product-detail\" data-equalizer=\"product-detail\">
                <div class=\"medium-6 columns\">
                    <div class=\"columns product-desc animatedParent\" data-sequence=\"300\" data-equalizer-watch=\"product-detail\">
                        <div class=\"title  animated fadeInUp\" data-id=\"1\">
                            <p>IONESSENCE</p>
                            <div style=\"width: 110px; height: 3px;background: #ffffff;margin-top: 23px;margin-bottom: 15px\"></div>
                        </div>
                        <p class=\"animated fadeInUp\" data-id=\"2\">";
        // line 45
        echo Lang::get("txt_ionessence02");
        echo "</p>
                    </div>
                </div>
                <div class=\"medium-6 columns animatedParent\">
                    <div class=\"columns product-desc-img animated fadeInUp delay-250\">
                        <img src=\"";
        // line 50
        echo Uri::base();
        echo "/assets/img/product/ionessence.jpg\" data-equalizer-watch=\"product-detail\">
                    </div>
                </div>
            </div>
            <div class=\"columns\">
            ";
        // line 55
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["campaign_ionessence"]) ? $context["campaign_ionessence"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["detail"]) {
            // line 56
            echo "                <div class=\"columns\">
                    <div class=\"product-variant animatedParent\">
                        <a href=\"";
            // line 58
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "link");
            echo "\"><img class=\"animated fadeInUp\" data-interchange=\"[";
            echo Uri::base();
            echo "media/campaign/";
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "image_s");
            echo ", small], [";
            echo Uri::base();
            echo "media/campaign/";
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "image_m");
            echo ", medium], [";
            echo Uri::base();
            echo "media/campaign/";
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "image");
            echo ", large]\"></a>
                    </div>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['detail'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 62
        echo "            </div>
            <div class=\"columns\">
                <div class=\"columns\">
                    <div class=\"columns product-link\">
                        <div class=\"product-button\">
                            <a href=\"https://ionessence.co.id/\" target=\"_blank\" class=\"button\">GO TO <br/>PRODUCT WEBSITE</a>
                            <div style=\"border-bottom: 4px solid #ffffff ; width: 60px\"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
";
    }

    // line 77
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 78
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    ";
        // line 79
        echo Asset::js("css3-animate-it.js");
        echo "
";
    }

    public function getTemplateName()
    {
        return "ionessence.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  189 => 79,  184 => 78,  181 => 77,  163 => 62,  141 => 58,  137 => 56,  133 => 55,  125 => 50,  117 => 45,  99 => 30,  93 => 28,  85 => 23,  79 => 21,  71 => 16,  65 => 14,  55 => 11,  51 => 9,  48 => 8,  42 => 6,  38 => 5,  33 => 4,  30 => 3,);
    }
}
