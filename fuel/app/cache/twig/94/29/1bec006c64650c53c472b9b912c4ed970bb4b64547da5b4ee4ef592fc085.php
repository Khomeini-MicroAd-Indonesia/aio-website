<?php

/* factory.twig */
class __TwigTemplate_94291bec006c64650c53c472b9b912c4ed970bb4b64547da5b4ee4ef592fc085 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_head_js' => array($this, 'block_frontend_head_js'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'back_to_top' => array($this, 'block_back_to_top'),
            'frontend_js' => array($this, 'block_frontend_js'),
            'back_to_top_js' => array($this, 'block_back_to_top_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        echo Asset::css("custom/animations.css");
        echo "
    ";
        // line 5
        $this->displayParentBlock("frontend_css", $context, $blocks);
        echo "
    ";
        // line 6
        echo Asset::css("custom/factory.css");
        echo "
";
    }

    // line 8
    public function block_frontend_head_js($context, array $blocks = array())
    {
        // line 9
        echo "     ";
        $this->displayParentBlock("frontend_head_js", $context, $blocks);
        echo "
 ";
    }

    // line 11
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 12
        echo "    <div class=\"factory\">
        <div class=\"banner animatedParent animateOnce\">
            <img src=\"";
        // line 14
        echo Uri::base();
        echo "/assets/img/factory-tour/factory-banner.jpg\"/>
            <div class=\"banner-desc animated fadeInUpShort\">
                <p>FACTORY<br/>TOUR</p>
                <hr/>
            </div>
        </div>
        <div class=\"row factory-detail\" data-equalizer=\"factory-detail\">
            <div class=\"medium-4 columns\" data-equalizer-watch=\"factory-detail\">
                <div class=\"medium-10 end columns\">
                ";
        // line 23
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["factory_sideimages"]) ? $context["factory_sideimages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["sideimage"]) {
            // line 24
            echo "                    <div class=\"medium-12 small-4 columns factory-gallery\"><img src=\"";
            echo Uri::base();
            echo "media/factorytour/";
            echo $this->getAttribute((isset($context["sideimage"]) ? $context["sideimage"] : null), "filename");
            echo "\"> </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sideimage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "                </div>
            </div>
            <div class=\"medium-8 columns\" style=\"padding-right: 1.875rem;\">
                <div class=\"desc\" data-equalizer-watch=\"factory-detail\">
                ";
        // line 30
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["factory_data"]) ? $context["factory_data"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["data"]) {
            // line 31
            echo "                    <p>";
            echo $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "title");
            echo "</p>
                    <p>";
            // line 32
            echo $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "content");
            echo "</p>
                    <div class=\"youtube\">
                        ";
            // line 34
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["factory_data"]) ? $context["factory_data"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["link"]) {
                // line 35
                echo "                            ";
                if (($this->getAttribute((isset($context["link"]) ? $context["link"] : null), "link") == "")) {
                    // line 36
                    echo "                                <img src=\"";
                    echo Uri::base();
                    echo "/media/press-room/grand-launching-explorion.jpg\"/>
                            ";
                } else {
                    // line 38
                    echo "                                <iframe width=\"770\" height=\"433\" src=\"";
                    echo $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "link");
                    echo "\" frameborder=\"0\" allowfullscreen></iframe>
                            ";
                }
                // line 40
                echo "                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['link'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 41
            echo "                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['data'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "                </div>
            </div>
        </div>
        <div class=\"row explorion custom-padd animatedParent animateOnce\" data-sequence=\"500\">
        ";
        // line 47
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["explorion"]) ? $context["explorion"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["detail"]) {
            echo " 
            <div class=\"medium-6 columns animated fadeInUp\" data-id=\"1\">
                <img src=\"";
            // line 49
            echo Uri::base();
            echo "media/explorion/";
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "image");
            echo "\">
            </div>
            <div class=\"medium-6 columns animated fadeInUp\" data-id=\"2\">

                <div class=\"title\">
                    <p>EXPLORION<br/>
                    <span>";
            // line 55
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "title");
            echo "</span></p>
                </div>
                <div style=\"border-bottom: 1px solid #657079; margin-bottom: 20px\"></div>
                <div class=\"desc\">
                    <p>";
            // line 59
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "content");
            echo "</p>
                </div>
                <a href=\"";
            // line 61
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "link");
            echo "\" class=\"button\" target=\"_blank\">";
            echo Lang::get("txt_tour04");
            echo "</a>
            
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['detail'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 65
        echo "        </div>
        <div class=\"row visiting-form custom-padd\">
            <div  class=\"title\">
                <hr/>
                <p>VISITING</p>
                <p>FORM</p>
            </div>
            <div class=\"visiting-detail\">
                <ul class=\"tabs\" data-tabs id=\"example-tabs\">
                    <li class=\"tabs-title\"><a href=\"#panel1\" aria-selected=\"true\">";
        // line 74
        echo Lang::get("txt_tour01");
        echo "</a></li>
                    <li class=\"tabs-title  is-active\"><a href=\"#panel2\">";
        // line 75
        echo Lang::get("txt_tour02");
        echo "</a></li>
                </ul>
                <div class=\"tabs-content\" data-tabs-content=\"example-tabs\">
                    <div class=\"tabs-panel\" id=\"panel1\">
                        <ol>
                            <li>";
        // line 80
        echo Lang::get("txt_tourtnc01");
        echo "</li>
                            <li>";
        // line 81
        echo Lang::get("txt_tourtnc02");
        echo "</li>
                            <li>";
        // line 82
        echo Lang::get("txt_tourtnc03");
        echo "</li>
                            <li>";
        // line 83
        echo Lang::get("txt_tourtnc04");
        echo "</li>
                            <li>";
        // line 84
        echo Lang::get("txt_tourtnc05");
        echo "</li>
                            <li>";
        // line 85
        echo Lang::get("txt_tourtnc06");
        echo "</li>
                            <li>";
        // line 86
        echo Lang::get("txt_tourtnc07");
        echo "</li>
                        </ol>
                        <p style=\"color: #ffffff;background: #01b2e5;display: inline-block;padding: 10px 20px 10px 60px;margin-left: -27px\">Pada saat berkunjung, pengunjung di mohon untuk:</p>
                        <ol>
                            <li>";
        // line 90
        echo Lang::get("txt_tourtnc08");
        echo "</li>
                            <li>";
        // line 91
        echo Lang::get("txt_tourtnc09");
        echo "</li>
                            <li>";
        // line 92
        echo Lang::get("txt_tourtnc10");
        echo "</li>
                            <li>";
        // line 93
        echo Lang::get("txt_tourtnc11");
        echo "</li>
                            <li>";
        // line 94
        echo Lang::get("txt_tourtnc12");
        echo "</li>
                        </ol>
                    </div>
                    <div class=\"tabs-panel  is-active\" id=\"panel2\">
                        <div class=\"row\">
                            <div class=\"medium-8 small-12 columns\">
                                <form role=\"form\" id=\"registration-form\" enctype=\"multipart/form-data\" action=\"\" name=\"registration-form\" method=\"post\">
                                    <div class=\"row\">
                                        <div class=\"small-3 columns\">
                                            <label for=\"middle-label\" class=\"text-right middle\">Kunjungan ke</label>
                                        </div>
                                        <div class=\"small-1 columns\">:</div>
                                        <div class=\"small-8 columns\">
                                            <input type=\"radio\" class=\"factory-place\" data-id=\"sukabumi\" name=\"factory\" value=\"Sukabumi\" id=\"pabrik-sukabumi\"><label for=\"kunjungan-pabrik-sukabumi\">";
        // line 107
        echo Lang::get("txt_tour06");
        echo "</label>
                                            <input type=\"radio\" class=\"factory-place\" data-id=\"kejayaan\" name=\"factory\" value=\"Kejayan\" id=\"pabrik-kejayaan\"><label for=\"kunjungan-pabrik-kejayaan\">";
        // line 108
        echo Lang::get("txt_tour05");
        echo "</label>
                                        </div>
                                    </div>
                                    <div class=\"row\">
                                        <div class=\"small-3 columns\">
                                            <label for=\"middle-label\" class=\"text-right middle\">Nama Lengkap</label>
                                        </div>
                                        <div class=\"small-1 columns\">:</div>
                                        <div class=\"small-8 columns\">
                                            <input id=\"name\" name=\"name\" type=\"text\" placeholder=\"Nama Lengkap\" required>
                                        </div>
                                    </div>
                                    <div class=\"row\">
                                        <div class=\"small-3 columns\">
                                            <label for=\"middle-label\" class=\"text-right middle\">Nama Institusi</label>
                                        </div>
                                        <div class=\"small-1 columns\">:</div>
                                        <div class=\"small-8 columns\">
                                            <input id=\"institution\" name=\"institution\" type=\"text\" placeholder=\"Nama Institusi\" required>
                                        </div>
                                    </div>
                                    <div class=\"row\">
                                        <div class=\"small-3 columns\">
                                            <label for=\"middle-label\" class=\"text-right middle\">Jumlah Peserta</label>
                                        </div>
                                        <div class=\"small-1 columns\">:</div>
                                        <div class=\"small-8 columns\">
                                            <input id=\"visitor\" name=\"visitor\" type=\"number\" placeholder=\"Jumlah peserta 20-100\" min=\"20\" max=\"100\">
                                        </div>
                                    </div>
                                    <div class=\"row\">
                                        <div class=\"small-3 columns\">
                                            <label for=\"alamat\" class=\"text-right middle\">Alamat</label>
                                        </div>
                                        <div class=\"small-1 columns\">:</div>
                                        <div class=\"small-8 columns\">
                                            <textarea id=\"address\" name=\"address\" rows=\"2\" name=\"alamat\" class=\"large-12 columns\" placeholder=\"\"  minlength=\"20\" maxlength=\"150\" required></textarea>
                                        </div>
                                    </div>
                                    <div class=\"row\">
                                        <div class=\"small-3 columns\">
                                            <label for=\"alamat\" class=\"text-right middle\">No Telephone & Hp</label>
                                        </div>
                                        <div class=\"small-1 columns\">:</div>
                                        <div class=\"small-8 columns\">
                                            <div class=\"small-6 columns no-padd\">
                                                <input id=\"landline\" name=\"landline\" type=\"tel\" placeholder=\"Tel\" required>
                                            </div>
                                            <div class=\"small-6 columns no-padd\">
                                                <input id=\"mobile\" name=\"mobile\" type=\"tel\" placeholder=\"Hp\" style=\"float: right\" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class=\"row\">
                                        <div class=\"small-3 columns\">
                                            <label for=\"middle-label\" class=\"text-right middle\">Email</label>
                                        </div>
                                        <div class=\"small-1 columns\">:</div>
                                        <div class=\"small-8 columns\">
                                            <input id=\"email\" name=\"email\" type=\"email\" placeholder=\"Email\" required>
                                        </div>
                                    </div>
                                    <div class=\"row\">
                                        <div class=\"small-3 columns\">
                                            <label for=\"alamat\" class=\"text-right middle\">Pesan & Keterangan</label>
                                        </div>
                                        <div class=\"small-1 columns\">:</div>
                                        <div class=\"small-8 columns\">
                                            <textarea id=\"message\" rows=\"2\" name=\"message\" class=\"large-12 columns\" placeholder=\"Pesan & Keterangan\" required></textarea>
                                        </div>
                                    </div>
                                    <div class=\"row\">
                                        <div class=\"medium-6 medium-offset-6 small-12 columns captcha-factory\">
                                            <div class=\"g-recaptcha position-captcha\" data-sitekey=\"6LcOtBwTAAAAAMr8p2x6uA0BsIscnHqIiqmqHc07\"></div>
                                        </div>
                                    </div>
                                    <button id=\"btn-register\" onclick=\"\">SEND</button>
                                </form>
                            </div>
                            <div class=\"medium-4 columns\">
                                <div id=\"sukabumi\" class=\"map\">
                                    <p>";
        // line 189
        echo Lang::get("txt_tour06");
        echo "</p>
                                    <iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3962.0277160903825!2d106.78864881436583!3d-6.766474868048572!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69ccfa13a625f7%3A0x1ee561a0b6d96208!2sAmerta+Indah+Otsuka!5e0!3m2!1sen!2sid!4v1457007187123\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>
                                </div>
                                <div id=\"kejayaan\" class=\"map\">
                                    <p>";
        // line 193
        echo Lang::get("txt_tour05");
        echo "</p>
                                    <iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3953.834484992937!2d112.84214261437559!3d-7.700904578404511!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7cfb1eb92f31d%3A0xe519b17f6dd13a46!2sPT+Amerta+Indah+Otsuka(+Pocari+Sweat!5e0!3m2!1sen!2sid!4v1457006929076\" width=\"400\" height=\"300\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            ";
        // line 203
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["err_msg"]) ? $context["err_msg"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
            // line 204
            echo "                <div>";
            echo (isset($context["error"]) ? $context["error"] : null);
            echo "</div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 206
        echo "        </div>
        <div class=\"gallery-visit\">
            <div class=\"row custom-padd\">
                <div class=\"medium-3 small-3 columns no-padd\">
                    <div class=\"title\">
                        <p>GALLERY</p>
                        <p>FACTORY<br/>TOUR</p>
                        <hr/>
                    </div>
                    <div class=\"see-gallery\">
                        <a class=\"button\" href=\"";
        // line 216
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/gallery-detail\">CLICK HERE<br/>TO SEE ALL GALLERY</a>
                    </div>
                </div>
                <div class=\"medium-9 small-9 columns\">
                    <a href=\"";
        // line 220
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/gallery-detail\"><img src=\"";
        echo Uri::base();
        echo "/assets/img/factory-tour/gallery/gallery.jpg\"></a>
                </div>
        </div>
        <div class=\"show-for-small-only\">
            ";
        // line 224
        $this->displayBlock('back_to_top', $context, $blocks);
        // line 227
        echo "        </div>
    </div>
        ";
        // line 229
        if ((isset($context["flag"]) ? $context["flag"] : null)) {
            // line 230
            echo "            <div id=\"factoryModal\" class=\"reveal\" data-reveal>
                <button class=\"close-reveal-modal\" data-close aria-label=\"Close modal\" type=\"button\">
                    <span aria-hidden=\"true\">&times;</span>
                </button>
                <p>Terima kasih, data anda akan kami proses. Untuk jadwal kunjungan, akan kami informasikan lebih lanjut</p>
            </div>
        ";
        }
        // line 237
        echo "
                    
";
    }

    // line 224
    public function block_back_to_top($context, array $blocks = array())
    {
        // line 225
        echo "                ";
        $this->displayParentBlock("back_to_top", $context, $blocks);
        echo "
            ";
    }

    // line 240
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 241
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    ";
        // line 242
        echo Asset::js("jquery.maskedinput.js");
        echo "
    ";
        // line 243
        echo Asset::js("css3-animate-it.js");
        echo "
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script type=\"text/javascript\">

        ";
        // line 247
        if ((isset($context["flag"]) ? $context["flag"] : null)) {
            // line 248
            echo "        \$('#factoryModal').foundation('open');
        ";
        }
        // line 250
        echo "            
        jQuery(function(\$){
            \$(\"#landline\").mask(\"(099)-999-9999?9\");
            \$(\"#mobile\").mask(\"(0899)-9999-999?9\");
        });

    \$('.factory-place').change(function (event) {
        var id = \$(this).data('id');
        \$('#' + id).removeClass('map').siblings().addClass('map');
    });

        ";
        // line 261
        $this->displayBlock('back_to_top_js', $context, $blocks);
        // line 264
        echo "    </script>
    
";
    }

    // line 261
    public function block_back_to_top_js($context, array $blocks = array())
    {
        // line 262
        echo "        ";
        $this->displayParentBlock("back_to_top_js", $context, $blocks);
        echo "
        ";
    }

    public function getTemplateName()
    {
        return "factory.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  514 => 262,  511 => 261,  505 => 264,  503 => 261,  490 => 250,  486 => 248,  484 => 247,  477 => 243,  473 => 242,  468 => 241,  465 => 240,  458 => 225,  455 => 224,  449 => 237,  440 => 230,  438 => 229,  434 => 227,  432 => 224,  422 => 220,  414 => 216,  402 => 206,  393 => 204,  389 => 203,  376 => 193,  369 => 189,  285 => 108,  281 => 107,  265 => 94,  261 => 93,  257 => 92,  253 => 91,  249 => 90,  242 => 86,  238 => 85,  234 => 84,  230 => 83,  226 => 82,  222 => 81,  218 => 80,  210 => 75,  206 => 74,  195 => 65,  183 => 61,  178 => 59,  171 => 55,  160 => 49,  153 => 47,  147 => 43,  140 => 41,  134 => 40,  128 => 38,  122 => 36,  119 => 35,  115 => 34,  110 => 32,  105 => 31,  101 => 30,  95 => 26,  84 => 24,  80 => 23,  68 => 14,  64 => 12,  61 => 11,  54 => 9,  51 => 8,  45 => 6,  41 => 5,  36 => 4,  33 => 3,);
    }
}
