<?php

/* search.twig */
class __TwigTemplate_c88c3f147f6ec9c88c326ebdb832f499d2988e263f6daa6a29d85885ae289768 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend_new.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend_new.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("frontend_css", $context, $blocks);
        echo "
    ";
        // line 5
        echo Asset::css("custom/search.css");
        echo "
";
    }

    // line 7
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 8
        echo "    <div class=\"search-content\">
        <div class=\"row\">
            <div class=\"columns title\">
                <p>SEARCH RESULT</p>
                <div style=\"width: 100%; border-bottom: 2px solid #ced2d6; margin-bottom: 10px\"></div>
            </div>
            <div class=\"columns search-bar\">
                ";
        // line 15
        if ((((twig_length_filter($this->env, (isset($context["search_result_activity"]) ? $context["search_result_activity"] : null)) > 0) && (twig_length_filter($this->env, (isset($context["search_result_press"]) ? $context["search_result_press"] : null)) > 0)) && (twig_length_filter($this->env, (isset($context["search_result_career"]) ? $context["search_result_career"] : null)) > 0))) {
            // line 16
            echo "                    <div class=\"medium-7 small-12 columns\">
                    ";
            // line 17
            $context["size"] = ((twig_length_filter($this->env, (isset($context["search_result_activity"]) ? $context["search_result_activity"] : null)) + twig_length_filter($this->env, (isset($context["search_result_press"]) ? $context["search_result_press"] : null))) + twig_length_filter($this->env, (isset($context["search_result_career"]) ? $context["search_result_career"] : null)));
            // line 18
            echo "                        <a href=\"#\"> About <b>";
            echo (isset($context["size"]) ? $context["size"] : null);
            echo "</b> results for <b>'";
            echo (isset($context["keyword"]) ? $context["keyword"] : null);
            echo "'</b></a>
                    </div>
                ";
        } elseif (((twig_length_filter($this->env, (isset($context["search_result_activity"]) ? $context["search_result_activity"] : null)) > 0) && (twig_length_filter($this->env, (isset($context["search_result_career"]) ? $context["search_result_career"] : null)) > 0))) {
            // line 21
            echo "                    <div class=\"medium-7 small-12 columns\">
                    ";
            // line 22
            $context["size"] = (twig_length_filter($this->env, (isset($context["search_result_activity"]) ? $context["search_result_activity"] : null)) + twig_length_filter($this->env, (isset($context["search_result_career"]) ? $context["search_result_career"] : null)));
            // line 23
            echo "                        <a href=\"#\"> About <b>";
            echo (isset($context["size"]) ? $context["size"] : null);
            echo "</b> results for <b>'";
            echo (isset($context["keyword"]) ? $context["keyword"] : null);
            echo "'</b></a>
                    </div>
                ";
        } elseif (((twig_length_filter($this->env, (isset($context["search_result_activity"]) ? $context["search_result_activity"] : null)) > 0) && (twig_length_filter($this->env, (isset($context["search_result_press"]) ? $context["search_result_press"] : null)) > 0))) {
            // line 26
            echo "                    <div class=\"medium-7 small-12 columns\">
                    ";
            // line 27
            $context["size"] = (twig_length_filter($this->env, (isset($context["search_result_activity"]) ? $context["search_result_activity"] : null)) + twig_length_filter($this->env, (isset($context["search_result_press"]) ? $context["search_result_press"] : null)));
            // line 28
            echo "                        <a href=\"#\"> About <b>";
            echo (isset($context["size"]) ? $context["size"] : null);
            echo "</b> results for <b>'";
            echo (isset($context["keyword"]) ? $context["keyword"] : null);
            echo "'</b></a>
                    </div>
                ";
        } elseif ((twig_length_filter($this->env, (isset($context["search_result_career"]) ? $context["search_result_career"] : null)) > 0)) {
            // line 31
            echo "                    <div class=\"medium-7 small-12 columns\">
                    ";
            // line 32
            $context["size"] = twig_length_filter($this->env, (isset($context["search_result_career"]) ? $context["search_result_career"] : null));
            // line 33
            echo "                        <a href=\"#\"> About <b>";
            echo (isset($context["size"]) ? $context["size"] : null);
            echo "</b> results for <b>'";
            echo (isset($context["keyword"]) ? $context["keyword"] : null);
            echo "'</b></a>
                    </div>
                ";
        } elseif ((twig_length_filter($this->env, (isset($context["search_result_press"]) ? $context["search_result_press"] : null)) > 0)) {
            // line 36
            echo "                    <div class=\"medium-7 small-12 columns\">
                    ";
            // line 37
            $context["size"] = twig_length_filter($this->env, (isset($context["search_result_press"]) ? $context["search_result_press"] : null));
            // line 38
            echo "                        <a href=\"#\"> About <b>";
            echo (isset($context["size"]) ? $context["size"] : null);
            echo "</b> results for <b>'";
            echo (isset($context["keyword"]) ? $context["keyword"] : null);
            echo "'</b></a>
                    </div>
                ";
        } elseif (((isset($context["keyword"]) ? $context["keyword"] : null) == "")) {
            // line 41
            echo "                    <div class=\"medium-7 small-12 columns\">
                        <a href=\"#\">The search keyword is either reset or not set!</a>
                    </div>
                ";
        }
        // line 45
        echo "                
                <div class=\"medium-5 small-12 columns refine\">
                    <form role=\"search\" >
                        <div class=\"row\">
                            <input name=\"keyword\" type=\"text\" placeholder=\"Refine Search\" >
                            ";
        // line 51
        echo "                        </div>
                    </form>
                </div>
            </div>
            <div class=\"columns each no-left-padd\">
            ";
        // line 56
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["search_result_activity"]) ? $context["search_result_activity"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["res"]) {
            // line 57
            echo "                <a href=\"";
            echo (((Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null)) . "/csr/") . $this->getAttribute((isset($context["res"]) ? $context["res"] : null), "slug"));
            echo "\">
                    <div class=\"columns title\">
                        <p>";
            // line 59
            echo $this->getAttribute((isset($context["res"]) ? $context["res"] : null), "title");
            echo "</p>
                    </div>
                    <div class=\"medium-2 small-12 columns no-left-padd\">
                        <img src=\"";
            // line 62
            echo Uri::base();
            echo "media/csr/";
            echo $this->getAttribute((isset($context["res"]) ? $context["res"] : null), "image");
            echo "\">
                    </div>
                    <div class=\"medium-10 small-12 columns\">
                        <div class=\"desc\">
                            <p>";
            // line 66
            echo $this->getAttribute((isset($context["res"]) ? $context["res"] : null), "highlight");
            echo "</p>
                        </div>
                    </div>
                </a>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['res'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 71
        echo "            </div>
            <div class=\"columns each no-left-padd\">
            ";
        // line 73
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["search_result_press"]) ? $context["search_result_press"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["res"]) {
            // line 74
            echo "                
                <a href=\"";
            // line 75
            echo (((Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null)) . "/our-event/detail/") . $this->getAttribute((isset($context["res"]) ? $context["res"] : null), "slug"));
            echo "\">
                    <div class=\"columns title\">
                        <p>";
            // line 77
            echo $this->getAttribute((isset($context["res"]) ? $context["res"] : null), "title");
            echo "</p>
                    </div>
                    <div class=\"medium-2 small-12 columns no-left-padd\">
                        <img src=\"";
            // line 80
            echo Uri::base();
            echo "media/press-room/";
            echo $this->getAttribute((isset($context["res"]) ? $context["res"] : null), "image");
            echo "\">
                    </div>
                    <div class=\"medium-10 small-12 columns\">
                        <div class=\"desc\">
                            <p>";
            // line 84
            echo $this->getAttribute((isset($context["res"]) ? $context["res"] : null), "highlight");
            echo "</p>
                        </div>
                    </div>
                </a>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['res'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 89
        echo "            </div>
            <div class=\"columns each no-left-padd\">
            ";
        // line 91
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["search_result_career"]) ? $context["search_result_career"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["res"]) {
            // line 92
            echo "                <a href=\"";
            echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
            echo "/career#list-of-vacancy\">
                    <div class=\"columns title\">
                        <p>";
            // line 94
            echo $this->getAttribute((isset($context["res"]) ? $context["res"] : null), "name");
            echo " - ";
            echo $this->getAttribute((isset($context["res"]) ? $context["res"] : null), "level");
            echo " (";
            echo $this->getAttribute((isset($context["res"]) ? $context["res"] : null), "location");
            echo ")</p>
                    </div>
                    ";
            // line 99
            echo "                    <div class=\"medium-10 small-12 columns\">
                        <div class=\"desc\">
                            <p>";
            // line 101
            echo $this->getAttribute((isset($context["res"]) ? $context["res"] : null), "req");
            echo "</p>
                        </div>
                    </div>
                </a>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['res'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 106
        echo "            </div>
            <div class=\"columns title\">
                <div style=\"width: 100%;border-bottom: 2px solid #ced2d6;margin-bottom: 50px\"></div>
            </div>
        </div>
    </div>
";
    }

    // line 114
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 115
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "search.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  277 => 115,  274 => 114,  264 => 106,  253 => 101,  249 => 99,  240 => 94,  234 => 92,  230 => 91,  226 => 89,  215 => 84,  206 => 80,  200 => 77,  195 => 75,  192 => 74,  188 => 73,  184 => 71,  173 => 66,  164 => 62,  158 => 59,  152 => 57,  148 => 56,  141 => 51,  134 => 45,  128 => 41,  119 => 38,  117 => 37,  114 => 36,  105 => 33,  103 => 32,  100 => 31,  91 => 28,  89 => 27,  86 => 26,  77 => 23,  75 => 22,  72 => 21,  63 => 18,  61 => 17,  58 => 16,  56 => 15,  47 => 8,  44 => 7,  38 => 5,  33 => 4,  30 => 3,);
    }
}
