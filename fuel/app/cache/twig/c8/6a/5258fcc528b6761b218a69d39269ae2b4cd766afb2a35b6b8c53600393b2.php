<?php

/* factory_tour_new.twig */
class __TwigTemplate_c86a5258fcc528b6761b218a69d39269ae2b4cd766afb2a35b6b8c53600393b2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend_new.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend_new.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("frontend_css", $context, $blocks);
        echo "
    ";
        // line 5
        echo Asset::css("custom/dp_calendar.css");
        echo "
    ";
        // line 6
        echo Asset::css("custom/factory_new.css");
        echo "
";
    }

    // line 8
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 9
        echo "    <div class=\"expanded row custom-left factory-tour\">
        <div class=\"title\">
            <h2>EXPLORION tour</h2>
        </div>
        <div class=\"background\">
            <img src=\"";
        // line 14
        echo Uri::base();
        echo "/assets/css/images/banner-explorion.jpg\" alt=\"\" title=\"\"/>
        </div>
    </div>
    <div class=\"expanded row principal\">
        <div class=\"large-6 large-centered\">
            <div class=\"sub-title\">
                ";
        // line 20
        echo Lang::get("explorion_sub_title");
        echo "
            </div>
            <div class=\"detail\">
               ";
        // line 23
        echo Lang::get("explorion_detail");
        echo "
            </div>
        </div>
    </div>
    <div class=\"expanded row explorion\">
    ";
        // line 28
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["factory_explorion"]) ? $context["factory_explorion"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["explorion"]) {
            // line 29
            echo "        <div class=\"explorion-adventure\">
            <div class=\"explorion-item\">
                <div class=\"top-content\">
                    <div class=\"large-8 large-centered\">
                        <div class=\"title\">
                            ";
            // line 34
            echo twig_upper_filter($this->env, $this->getAttribute((isset($context["explorion"]) ? $context["explorion"] : null), "title"));
            echo "
                        </div>
                        <div class=\"sub-title\">
                            ";
            // line 37
            echo $this->getAttribute((isset($context["explorion"]) ? $context["explorion"] : null), "headline");
            echo "
                        </div>
                    </div>
                </div>
                <div class=\"middle-content\">
                    <div>
                        <img src=\"";
            // line 43
            echo Uri::base();
            echo "media/factorytour/";
            echo $this->getAttribute((isset($context["explorion"]) ? $context["explorion"] : null), "image");
            echo "\" alt=\"\" title=\"";
            echo $this->getAttribute((isset($context["explorion"]) ? $context["explorion"] : null), "title");
            echo "\"/>
                    </div>
                    <div class=\"explorion-button\">
                        <a href=\"#calendar-wrapper\" id=\"source-";
            // line 46
            echo $this->getAttribute((isset($context["explorion"]) ? $context["explorion"] : null), "id");
            echo "\" class=\"button\">";
            echo Lang::get("explorion_reg_tour");
            echo " <span><img src=\"";
            echo Uri::base();
            echo "/assets/css/images/explorion-arrow.png\" alt=\"\" title=\"\"/></span></a>
                    </div>
                </div>
                <div class=\"bottom-content\">
                    <div class=\"large-8 large-centered\">
                        <div class=\"large-6 columns\">
                            <div class=\"detail\">
                                ";
            // line 53
            echo $this->getAttribute((isset($context["explorion"]) ? $context["explorion"] : null), "content1");
            echo "
                            </div>
                        </div>
                        <div class=\"large-6 columns\">
                            <div class=\"detail\">
                                ";
            // line 58
            echo $this->getAttribute((isset($context["explorion"]) ? $context["explorion"] : null), "content2");
            echo "
                            </div>
                        </div>
                        <div style=\"clear: both;\"></div>
                    </div>
                </div>
            </div>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['explorion'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 67
        echo "    </div>
    <div class=\"expanded row calendar\" id=\"calendar-wrapper\">
        <div id=\"visiting-form\">
            <div class=\"large-6 large-centered\">
                <div id=\"form-title\">";
        // line 71
        echo Lang::get("explorion_form");
        echo "</div>
                <div class=\"menu-centered\">
                    <ul class=\"menu tabs tabs-explore\" data-tabs id=\"explorion-menu\">
                        <li class=\"tabs-title is-active\" id=\"tnc-tab-label\"><a href=\"#calendar-item\" aria-selected=\"true\">";
        // line 74
        echo Lang::get("explorion_reg");
        echo "</a></li>
                        <li class=\"tabs-title\" id=\"reg-tab-label\"><a href=\"#terms-explorion\">";
        // line 75
        echo Lang::get("explorion_tnc");
        echo "</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class=\"tabs-content\" data-tabs-content=\"explorion-menu\">
            <div class=\"tabs-panel\" id=\"terms-explorion\">
                <div class=\"large-6 large-centered calendar-terms\">
                    <ol>
                        <li>Factory tour terbuka untuk sekolah, universitas, ataupun institusi lainnya, pengunjung minimum kelas 3 sekolah dasar.</li>
                        <li>Pengunjung sehat, tidak terkena penyakit dan suhu tubuh tidak melebihi 37.50C ketika berkunjung.</li>
                        <li>Proses produksi sewaktu-waktu dapat berhenti maka proses produksi tidak dapat diperlihatkan secara langsung namun hanya diperlihatkan melalui video.</li>
                        <li>Sabtu/Minggu/Hari libur nasional tidak menerima kunjungan.</li>
                        <li>Kapasitas pengunjung min. 20 orang max. 100 orang.</li>
                        <li>Pengunjung berpakaian rapi dan sopan dan tidak menggunakan sandal.</li>
                        <li>Ketersediaan tanggal dan jam kunjungan yang telah Anda ajukan sebelumnya, akan kami konfirmasikan terlebih dahulu. Apabila jam dan tanggal tidak tersedia, Anda bisa mengubah tanggal dan jam tersebut untuk menyesuaikan.</li>
                    </ol>
                    <div class=\"terms-title\">
                        <strong>Pada saat berkunjung, pengunjung di mohon untuk:</strong>
                    </div>
                    <ol>
                        <li>Tidak merokok di dalam lingkungan pabrik.</li>
                        <li>Tidak membuang sampah di sembarangan tempat.</li>
                        <li>Tidak mengambil foto/video di koridor proses produksi.</li>
                        <li>Mengikuti petunjuk pemandu selama kunjungan demi menjamin keselamatan pengunjung.</li>
                        <li>Tidak membawa turun dari bus makanan dan minuman ke dalam lingkungan pabrik.</li>
                    </ol>
                </div>
            </div>
            <div class=\"tabs-panel is-active\" id=\"calendar-item\">
                <div class=\"large-6 large-centered calendar-wrapper\">
                    <ul class=\"tabs tabs-register\" data-tabs id=\"example-tabs\">
                        <li class=\"tabs-title is-active\" id=\"sukabumi-menu\"><a href=\"#sukabumi\" aria-selected=\"true\">Sukabumi</a></li>
                        <li class=\"tabs-title\" id=\"kejayan-menu\"><a href=\"#kejayan\">Kejayan</a></li>
                    </ul>
                    <div class=\"tabs-content\" data-tabs-content=\"example-tabs\">
                        <div class=\"tabs-panel\" id=\"kejayan\">
                            <div id='calendar' class=\"calendar_kejayan\"></div>
                            <form role=\"form\" id=\"registration-form\" enctype=\"multipart/form-data\" action=\"\" name=\"registration-form2\" method=\"post\" class=\"hide\">
                                <div class=\"row\">
                                    <div class=\"small-12 large-3 columns\" data-equalizer-watch=\"form-sukabumi\">
                                        <label for=\"middle-label\" class=\"text-left middle\">";
        // line 116
        echo Lang::get("txt_form_tour01");
        echo "</label>
                                    </div>
                                    <div class=\"small-12 large-9 columns view-data\" data-equalizer-watch=\"form-sukabumi\">
                                        <input type=\"hidden\" name=\"factory\" value=\"kejayan\" />
                                        <input type=\"hidden\" name=\"visit_date_full\" id=\"visit-date-full\" value=\"\" />
                                        <input type=\"hidden\" name=\"visit_date\" id=\"visit-date\" value=\"\" />
                                        <input type=\"hidden\" name=\"visit_shift\" id=\"visit-shift\" value=\"\" />
                                        <label id=\"label-visit-date\" class=\"middle\"></label>
                                        <label id=\"label-visit-shift\"></label>
                                    </div>
                                    <div style=\"clear: both;\"></div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"large-3 columns\" data-equalizer-watch=\"form-sukabumi\">
                                        <label for=\"middle-label\" class=\"text-right middle\">";
        // line 130
        echo Lang::get("txt_form_tour02");
        echo "</label>
                                    </div>
                                    <div class=\"large-9 columns view-data\" data-equalizer-watch=\"form-sukabumi\">
                                        <label class=\"middle\">Kejayan</label>
                                    </div>
                                    <div style=\"clear: both;\"></div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"large-3 columns\" data-equalizer-watch=\"form-sukabumi\">
                                        <label for=\"middle-label\" class=\"text-right middle\">";
        // line 139
        echo Lang::get("txt_form_tour03");
        echo "</label>
                                    </div>
                                    <div class=\"large-9 columns\" data-equalizer-watch=\"form-sukabumi\">
                                        <input id=\"name\" name=\"name\" type=\"text\" placeholder=\"";
        // line 142
        echo Lang::get("txt_form_tour03");
        echo "\" required>
                                    </div>
                                    <div style=\"clear: both;\"></div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"large-3 columns\" data-equalizer-watch=\"form-sukabumi\">
                                        <label for=\"middle-label\" class=\"text-right middle\">";
        // line 148
        echo Lang::get("txt_form_tour04");
        echo "</label>
                                    </div>
                                    <div class=\"large-9 columns\" data-equalizer-watch=\"form-sukabumi\">
                                        <input id=\"institution\" name=\"institution\" type=\"text\" placeholder=\"";
        // line 151
        echo Lang::get("txt_form_tour04");
        echo "\" required>
                                    </div>
                                    <div style=\"clear: both;\"></div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"large-3 columns\" data-equalizer-watch=\"form-sukabumi\">
                                        <label for=\"middle-label\" class=\"text-right middle\">";
        // line 157
        echo Lang::get("txt_form_tour05");
        echo "</label>
                                    </div>
                                    <div class=\"large-9 columns\" data-equalizer-watch=\"form-sukabumi\">
                                        <input id=\"visitor\" name=\"visitor\" type=\"number\" placeholder=\"";
        // line 160
        echo Lang::get("txt_form_tour05_01");
        echo "\" min=\"20\" max=\"100\">
                                    </div>
                                    <div style=\"clear: both;\"></div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"large-3 columns address-wrapper\" data-equalizer-watch=\"form-sukabumi\">
                                        <label for=\"alamat\" class=\"text-right middle\">";
        // line 166
        echo Lang::get("txt_form_tour06");
        echo "</label>
                                    </div>
                                    <div class=\"large-9 columns\" data-equalizer-watch=\"form-sukabumi\">
                                        <textarea id=\"address\" name=\"address\" rows=\"2\" name=\"alamat\" class=\"large-12 columns\" placeholder=\"\"  minlength=\"20\" maxlength=\"150\" required></textarea>
                                    </div>
                                    <div style=\"clear: both;\"></div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"large-3 columns\" data-equalizer-watch=\"form-sukabumi\">
                                        <label for=\"alamat\" class=\"text-right middle\">";
        // line 175
        echo Lang::get("txt_form_tour07");
        echo "</label>
                                    </div>
                                    <div class=\"large-9 columns\" data-equalizer-watch=\"form-sukabumi\">
                                        <div class=\"small-6 columns no-padd\" style=\"padding-left: 0\">
                                            <input id=\"landline\" name=\"landline\" type=\"tel\" placeholder=\"Tel\" required>
                                        </div>
                                        <div class=\"small-6 columns no-padd\" style=\"padding: 0\">
                                            <input id=\"mobile\" name=\"mobile\" type=\"tel\" placeholder=\"Hp\" style=\"float: right\" required>
                                        </div>
                                        <div style=\"clear: both;\"></div>
                                    </div>
                                    <div style=\"clear: both;\"></div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"large-3 columns\" data-equalizer-watch=\"form-sukabumi\">
                                        <label for=\"middle-label\" class=\"text-right middle\">";
        // line 190
        echo Lang::get("txt_form_tour08");
        echo "</label>
                                    </div>
                                    <div class=\"large-9 columns\" data-equalizer-watch=\"form-sukabumi\">
                                        <input id=\"email\" name=\"email\" type=\"email\" pattern=\"[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}\$\" placeholder=\"";
        // line 193
        echo Lang::get("txt_form_tour08");
        echo "\" required>
                                    </div>
                                    <div style=\"clear: both;\"></div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"large-3 columns address-wrapper\" data-equalizer-watch=\"form-sukabumi\">
                                        <label for=\"alamat\" class=\"text-right middle\">";
        // line 199
        echo Lang::get("txt_form_tour09");
        echo "</label>
                                    </div>
                                    <div class=\"large-9 columns\" data-equalizer-watch=\"form-sukabumi\">
                                        <textarea id=\"message\" rows=\"2\" name=\"message\" class=\"large-12 columns\" placeholder=\"";
        // line 202
        echo Lang::get("txt_form_tour09");
        echo "\" required></textarea>
                                    </div>
                                    <div style=\"clear: both;\"></div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"medium-6 medium-offset-6 small-12 columns captcha-factory\">
                                        <div class=\"g-recaptcha position-captcha\" data-sitekey=\"6LcOtBwTAAAAAMr8p2x6uA0BsIscnHqIiqmqHc07\"></div>
                                    </div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"large-3 columns\">
                                        <label>&nbsp;</label>
                                    </div>
                                    <div class=\"large-9 columns form-wrap\">
                                        <div class=\"form-button\">
                                            <button class=\"button back-button\" onclick=\"goBack()\"><span><img src=\"";
        // line 217
        echo Uri::base();
        echo "/assets/css/images/explorion-arrow.png\" alt=\"\" title=\"\"/></span>";
        echo Lang::get("explorion_schedule2");
        echo " </button>
                                        </div>
                                        <div class=\"form-button\">
                                            <button id=\"btn-register\" onclick=\" \" class=\"button\">";
        // line 220
        echo Lang::get("explorion_sbmt_form");
        echo " <span><img src=\"";
        echo Uri::base();
        echo "/assets/css/images/explorion-arrow.png\" alt=\"\" title=\"\"/></span></button>
                                        </div>
                                    </div>
                                    <div style=\"clear: both;\"></div>
                                </div>
                            </form>
                        </div>
                        <div class=\"tabs-panel is-active\" id=\"sukabumi\" >
                            <div id='calendar2' class=\"calendar_sukabumi\"></div>
                            <form role=\"form\" id=\"registration-form2\" enctype=\"multipart/form-data\" action=\"\" name=\"registration-form2\" method=\"post\" class=\"hide\">
                                <div class=\"row\">
                                    <div class=\"small-12 large-3 columns\" data-equalizer-watch=\"form-sukabumi\">
                                        <label for=\"middle-label\" class=\"text-left middle\">";
        // line 232
        echo Lang::get("txt_form_tour01");
        echo "</label>
                                    </div>
                                    <div class=\"small-12 large-9 columns view-data\" data-equalizer-watch=\"form-sukabumi\">
                                        <input type=\"hidden\" name=\"factory\" value=\"sukabumi\" />
                                        <input type=\"hidden\" name=\"visit_date_full\" id=\"visit-date-full-sukabumi\" value=\"\" />
                                        <input type=\"hidden\" name=\"visit_date\" id=\"visit-date-sukabumi\" value=\"\" />
                                        <input type=\"hidden\" name=\"visit_shift\" id=\"visit-shift-sukabumi\" value=\"\" />
                                        <label id=\"label-visit-date-sukabumi\" class=\"middle\"></label>
                                        <label id=\"label-visit-shift-sukabumi\"></label>
                                    </div>
                                    <div style=\"clear: both;\"></div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"small-12 large-3 columns\" data-equalizer-watch=\"form-sukabumi\">
                                        <label for=\"middle-label\" class=\"text-right middle\">";
        // line 246
        echo Lang::get("txt_form_tour02");
        echo "</label>
                                    </div>
                                    <div class=\"small-12 large-9 columns view-data\" data-equalizer-watch=\"form-sukabumi\">
                                        <label class=\"middle\">Sukabumi</label>
                                    </div>
                                    <div style=\"clear: both;\"></div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"small-12 large-3 columns\" data-equalizer-watch=\"form-sukabumi\">
                                        <label for=\"middle-label\" class=\"text-right middle\">";
        // line 255
        echo Lang::get("txt_form_tour03");
        echo "</label>
                                    </div>
                                    <div class=\"small-12 large-9 columns\" data-equalizer-watch=\"form-sukabumi\">
                                        <input id=\"name-sukabumi\" name=\"name\" type=\"text\" placeholder=\"";
        // line 258
        echo Lang::get("txt_form_tour03");
        echo "\" required>
                                    </div>
                                    <div style=\"clear: both;\"></div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"small-12 large-3 columns\" data-equalizer-watch=\"form-sukabumi\">
                                        <label for=\"middle-label\" class=\"text-right middle\">";
        // line 264
        echo Lang::get("txt_form_tour04");
        echo "</label>
                                    </div>
                                    <div class=\"small-12 large-9 columns\" data-equalizer-watch=\"form-sukabumi\">
                                        <input id=\"institution-sukabumi\" name=\"institution\" type=\"text\" placeholder=\"";
        // line 267
        echo Lang::get("txt_form_tour04");
        echo "\" required>
                                    </div>
                                    <div style=\"clear: both;\"></div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"small-12 large-3 columns\" data-equalizer-watch=\"form-sukabumi\">
                                        <label for=\"middle-label\" class=\"text-right middle\">";
        // line 273
        echo Lang::get("txt_form_tour05");
        echo "</label>
                                    </div>
                                    <div class=\"small-12 large-9 columns\" data-equalizer-watch=\"form-sukabumi\">
                                        <input id=\"visitor-sukabumi\" name=\"visitor\" type=\"number\" placeholder=\"";
        // line 276
        echo Lang::get("txt_form_tour05_01");
        echo "\" min=\"20\" max=\"100\">
                                    </div>
                                    <div style=\"clear: both;\"></div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"small-12 large-3 columns address-wrapper\" data-equalizer-watch=\"form-sukabumi\">
                                        <label for=\"alamat\" class=\"text-right middle\">";
        // line 282
        echo Lang::get("txt_form_tour06");
        echo "</label>
                                    </div>
                                    <div class=\"small-12 large-9 columns\" data-equalizer-watch=\"form-sukabumi\">
                                        <textarea id=\"address-sukabumi\" name=\"address\" rows=\"2\" name=\"alamat\" class=\"large-12 columns\" placeholder=\"\"  minlength=\"20\" maxlength=\"150\" required></textarea>
                                    </div>
                                    <div style=\"clear: both;\"></div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"small-12 large-3 columns\" data-equalizer-watch=\"form-sukabumi\">
                                        <label for=\"alamat\" class=\"text-right middle\">";
        // line 291
        echo Lang::get("txt_form_tour07");
        echo "</label>
                                    </div>
                                    <div class=\"small-12 large-9 columns\" data-equalizer-watch=\"form-sukabumi\">
                                        <div class=\"small-6 columns no-padd\" style=\"padding-left: 0\">
                                            <input id=\"landline-sukabumi\" name=\"landline\" type=\"tel\" placeholder=\"Tel\" required>
                                        </div>
                                        <div class=\"small-6 columns no-padd\" style=\"padding: 0\">
                                            <input id=\"mobile-sukabumi\" name=\"mobile\" type=\"tel\" placeholder=\"Hp\" style=\"float: right\" required>
                                        </div>
                                        <div style=\"clear: both;\"></div>
                                    </div>
                                    <div style=\"clear: both;\"></div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"small-12 large-3 columns\" data-equalizer-watch=\"form-sukabumi\">
                                        <label for=\"middle-label\" class=\"text-right middle\">";
        // line 306
        echo Lang::get("txt_form_tour08");
        echo "</label>
                                    </div>
                                    <div class=\"small-12 large-9 columns\" data-equalizer-watch=\"form-sukabumi\">
                                        <input id=\"email-sukabumi\" name=\"email\" type=\"email\" pattern=\"[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}\$\" placeholder=\"";
        // line 309
        echo Lang::get("txt_form_tour08");
        echo "\" required>
                                    </div>
                                    <div style=\"clear: both;\"></div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"small-12 large-3 columns address-wrapper\" data-equalizer-watch=\"form-sukabumi\">
                                        <label for=\"alamat\" class=\"text-right middle\">";
        // line 315
        echo Lang::get("txt_form_tour09");
        echo "</label>
                                    </div>
                                    <div class=\"small-12 large-9 columns\" data-equalizer-watch=\"form-sukabumi\">
                                        <textarea id=\"message-sukabumi\" rows=\"2\" name=\"message\" class=\"large-12 columns\" placeholder=\"";
        // line 318
        echo Lang::get("txt_form_tour09");
        echo "\" required></textarea>
                                    </div>
                                    <div style=\"clear: both;\"></div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"medium-6 medium-offset-6 small-12 columns captcha-factory\">
                                        <div class=\"g-recaptcha position-captcha\" data-sitekey=\"6LcOtBwTAAAAAMr8p2x6uA0BsIscnHqIiqmqHc07\"></div>
                                    </div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"large-3 columns\">
                                        <label>&nbsp;</label>
                                    </div>
                                    <div class=\"large-9 columns form-wrap\">
                                        <div class=\"form-button\">
                                            <button class=\"button back-button\" onclick=\"goBack2()\"><span><img src=\"";
        // line 333
        echo Uri::base();
        echo "/assets/css/images/explorion-arrow.png\" alt=\"\" title=\"\"/></span>";
        echo Lang::get("explorion_schedule2");
        echo " </button>
                                        </div>
                                        <div class=\"form-button\">
                                            <button id=\"btn-register-sukabumi\" onclick=\" \" class=\"button\">";
        // line 336
        echo Lang::get("explorion_sbmt_form");
        echo " <span><img src=\"";
        echo Uri::base();
        echo "/assets/css/images/explorion-arrow.png\" alt=\"\" title=\"\"/></span></button>
                                        </div>
                                    </div>
                                    <div style=\"clear: both;\"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class=\"expanded row gallery\">
        <div class=\"large-4 columns\">
            <div class=\"explain\">
                <div class=\"large-9 large-offset-3 columns\">
                    <div class=\"sub-title\">
                        ";
        // line 353
        echo Lang::get("galeri_title");
        echo "
                    </div>
                    <div class=\"explaination\">
                        ";
        // line 356
        echo Lang::get("galeri_detail");
        echo "
                    </div>
                </div>
            </div>
        </div>
        <div class=\"large-7 columns\">
            <div class=\"explorion-gallery\" data-equalizer=\"gallery\">
                ";
        // line 363
        $context["n"] = 1;
        // line 364
        echo "                ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 7));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 365
            echo "                    ";
            if (((isset($context["i"]) ? $context["i"] : null) % 2 == 1)) {
                // line 366
                echo "                        <div class=\"small-6 medium-3 columns no-padding odd-item\" data-equalizer-watch=\"gallery\">
                            <a href=\"#\">
                                <div class=\"gallery-item\" style=\"background: url('";
                // line 368
                echo Uri::base();
                echo "media/gallery/";
                echo $this->getAttribute($this->getAttribute((isset($context["latest_album"]) ? $context["latest_album"] : null), (isset($context["i"]) ? $context["i"] : null), array(), "array"), "filename");
                echo "') center no-repeat; background-size: 150%;\">
                                        ";
                // line 370
                echo "                                </div>
                            </a>
                        </div>
                    ";
            } else {
                // line 374
                echo "                        ";
                $context["n"] = ((isset($context["n"]) ? $context["n"] : null) + 1);
                // line 375
                echo "                        <div class=\"small-6 medium-3 columns no-padding\" data-equalizer-watch=\"gallery\">
                            <div class=\"gallery-item\">
                                <i class=\"fa fa-instagram\" aria-hidden=\"true\"></i>
                                <a href=\"#\">
                                    <img src=\"";
                // line 379
                echo $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["pic_obj"]) ? $context["pic_obj"] : null), ((isset($context["i"]) ? $context["i"] : null) - (isset($context["n"]) ? $context["n"] : null)), array(), "array"), "images", array(), "array"), "standard_resolution", array(), "array"), "url", array(), "array");
                echo "\" alt=\"";
                echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["pic_obj"]) ? $context["pic_obj"] : null), ((isset($context["i"]) ? $context["i"] : null) - (isset($context["n"]) ? $context["n"] : null)), array(), "array"), "caption", array(), "array"), "text", array(), "array");
                echo "\"/>
                                </a>
                            </div>
                        </div>
                    ";
            }
            // line 384
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 385
        echo "                <div class=\" small-6 medium-3 columns no-padding\">
                    <div class=\"gallery-item\">
                        <a href=\"#\">
                            <img src=\"";
        // line 388
        echo Uri::base();
        echo "/assets/css/images/full-gallery-";
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo ".jpg\" alt=\"\" title=\"\"/>
                        </a>
                    </div>
                </div>
                <div style=\"clear: both;\"></div>
            </div>
        </div>
        <div style=\"clear: both;\"></div>
    </div>

";
    }

    // line 399
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 400
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    ";
        // line 401
        echo Asset::js("jquery.maskedinput.js");
        echo "
    ";
        // line 402
        echo Asset::js("css3-animate-it.js");
        echo "
    ";
        // line 403
        echo Asset::js("jquery-ui-1.12.0.js");
        echo "
    ";
        // line 404
        echo Asset::js("date.js");
        echo "
    <script>
        \$jadwal = \$(\"<div class='calendar_title'>";
        // line 406
        echo Lang::get("explorion_schedule");
        echo "</div>\");
        \$note = \$(\"<div class='medium-12 small-12 columns' style='text-align: left; font-size: 12px; color: #8191ae;'>\" + \"<p>";
        // line 407
        echo Lang::get("explorion_note");
        echo " :</p>\" + \"</div>\");
    </script>
    ";
        // line 409
        echo Asset::js("jquery.dp_calendar_new.js");
        echo "
    <script>
        function goBack() {
            \$(\"#registration-form\").addClass(\"hide\");
            \$(\"#calendar\").removeClass(\"hide\");
            \$(\".tabs-register\").removeClass(\"hide\");
            \$(\".tabs-explore\").removeClass(\"hide\");
            \$(\"body, html\").animate({
                scrollTop: jQuery('#registration-form').offset().top - 300
            });
        }

        function goBack2() {
            \$(\"#registration-form2\").addClass(\"hide\");
            \$(\"#calendar2\").removeClass(\"hide\");
            \$(\".tabs-register\").removeClass(\"hide\");
            \$(\".tabs-explore\").removeClass(\"hide\");
            \$(\"body, html\").animate({
                scrollTop: jQuery('#registration-form2').offset().top - 300
            });
        }

        \$('#source-2').click(function(){
            \$(\"#kejayan\").addClass(\"is-active\");
            \$(\"#sukabumi\").removeClass(\"is-active\");
            \$(\"#kejayan-menu\").addClass(\"is-active\");
            \$(\"#sukabumi-menu\").removeClass(\"is-active\");
            \$(\"#sukabumi-label\").attr(\"aria-selected\",false);
            \$(\"#kejayan-label\").attr(\"aria-selected\",true);
            \$(\"body, html\").animate({
                scrollTop: jQuery('#kejayan').offset().top - 300
            });
        });

        \$('#source-1').click(function(){
            \$(\"#kejayan\").removeClass(\"is-active\");
            \$(\"#sukabumi\").addClass(\"is-active\");
            \$(\"#kejayan-menu\").removeClass(\"is-active\");
            \$(\"#sukabumi-menu\").addClass(\"is-active\");
            \$(\"#kejayan-label\").attr(\"aria-selected\",false);
            \$(\"#sukabumi-label\").attr(\"aria-selected\",true);
            \$(\"body, html\").animate({
                scrollTop: jQuery('#sukabumi').offset().top - 300
            });
        });

        \$( window ).load(function() {
            jQuery('#calendar').on('click', 'li.slot', function(){
                var shift = jQuery(this).data('shift'),
                        date_selected = \$.fn.dp_calendar.date_selected,
                        date_mysql_format = date_selected.getFullYear()+'-'+(date_selected.getMonth()*1+1)+'-'+date_selected.getDate(),
                        date_display = \$.fn.dp_calendar.regional[\"lang_";
        // line 460
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "\"].dayNames[date_selected.getDay()]+', '+date_selected.getDate()+' '+\$.fn.dp_calendar.regional[\"lang_";
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "\"].monthNames[date_selected.getMonth()]+' '+date_selected.getFullYear(),
                        chosen_date = \$.fn.dp_calendar.regional[\"lang_id\"].dayNames[date_selected.getDay()]+', '+date_selected.getDate()+' '+\$.fn.dp_calendar.regional[\"lang_id\"].monthNames[date_selected.getMonth()]+' '+date_selected.getFullYear(),
                        label_shift='';
                jQuery('#visit-date').val(date_mysql_format);
                switch (date_selected.getDay()) {
                    case 5:
                        if (shift == 1) {
                            label_shift = '09.00 WIB - 11.00 WIB';
                        } else if (shift == 2) {
                            label_shift = '";
        // line 469
        echo Lang::get("txt_tour09");
        echo "';
                        } else if (shift == 3) {
                            label_shift = '13.00 WIB - 15.00 WIB';
                        }
                        break;
                    default:
                        if (shift == 1) {
                            label_shift = '09.00 WIB - 11.00 WIB';
                        } else if (shift == 2) {
                            label_shift = '11.00 WIB - 13.00 WIB';
                        } else if (shift == 3) {
                            label_shift = '13.00 WIB - 15.00 WIB';
                        }
                }
                jQuery('#visit-shift').val(shift);
                jQuery('#visit-date-full').val(chosen_date);
                jQuery('#label-visit-date').text(date_display);
                jQuery('#label-visit-shift').html(' <span>|</span> '+label_shift);
                jQuery('#calendar').addClass('hide');
                jQuery('#calendar-mandatory-info').addClass('hide');
                \$(\".tabs-register\").addClass(\"hide\");
                \$(\".tabs-explore\").addClass(\"hide\");
                jQuery('#registration-form').removeClass('hide');
                jQuery(\"body, html\").animate({
                    scrollTop: jQuery('#registration-form').offset().top - 300
                }, 600);
            });

            var events_array = [];
            ";
        // line 498
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["factory_schedules"]) ? $context["factory_schedules"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["schedule"]) {
            // line 499
            echo "            events_array.push({
                startDate: new Date(";
            // line 500
            echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["schedule"]) ? $context["schedule"] : null), "visit_date"), "Y");
            echo ", ";
            echo (twig_date_format_filter($this->env, $this->getAttribute((isset($context["schedule"]) ? $context["schedule"] : null), "visit_date"), "m") - 1);
            echo ", ";
            echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["schedule"]) ? $context["schedule"] : null), "visit_date"), "d");
            echo "),
                endDate: new Date(";
            // line 501
            echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["schedule"]) ? $context["schedule"] : null), "visit_date"), "Y");
            echo ", ";
            echo (twig_date_format_filter($this->env, $this->getAttribute((isset($context["schedule"]) ? $context["schedule"] : null), "visit_date"), "m") - 1);
            echo ", ";
            echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["schedule"]) ? $context["schedule"] : null), "visit_date"), "d");
            echo "),
                title: \"";
            // line 502
            echo $this->getAttribute((isset($context["schedule"]) ? $context["schedule"] : null), "institution_name");
            echo "\",
                shift: ";
            // line 503
            echo $this->getAttribute((isset($context["schedule"]) ? $context["schedule"] : null), "shift");
            echo "
            });
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['schedule'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 506
        echo "
            jQuery(\"#calendar\").dp_calendar({
                events_array: events_array
            },'";
        // line 509
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "');

            jQuery('#calendar2').on('click', 'li.slot', function(){
                var shift = jQuery(this).data('shift'),
                        date_selected = \$.fn.dp_calendar.date_selected,
                        date_mysql_format = date_selected.getFullYear()+'-'+(date_selected.getMonth()*1+1)+'-'+date_selected.getDate(),
                        date_display = \$.fn.dp_calendar.regional[\"lang_";
        // line 515
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "\"].dayNames[date_selected.getDay()]+', '+date_selected.getDate()+' '+\$.fn.dp_calendar.regional[\"lang_";
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "\"].monthNames[date_selected.getMonth()]+' '+date_selected.getFullYear(),
                        chosen_date = \$.fn.dp_calendar.regional[\"lang_id\"].dayNames[date_selected.getDay()]+', '+date_selected.getDate()+' '+\$.fn.dp_calendar.regional[\"lang_id\"].monthNames[date_selected.getMonth()]+' '+date_selected.getFullYear(),
                        label_shift='';
                jQuery('#visit-date-sukabumi').val(date_mysql_format);
                switch (date_selected.getDay()) {
                    case 5:
                        if (shift == 1) {
                            label_shift = '08.30 WIB - 09.00 WIB';
                        } else if (shift == 2) {
                            label_shift = '";
        // line 524
        echo Lang::get("txt_tour09");
        echo "';
                        } else if (shift == 3) {
                            label_shift = '13.00 WIB - 14.00 WIB';
                        }
                        break;
                    default:
                        if (shift == 1) {
                            label_shift = '08.30 WIB - 09.00 WIB';
                        } else if (shift == 2) {
                            label_shift = '10.00 WIB - 11.00 WIB';
                        } else if (shift == 3) {
                            label_shift = '13.00 WIB - 14.00 WIB';
                        }
                }
                jQuery('#visit-shift-sukabumi').val(shift);
                jQuery('#visit-date-full-sukabumi').val(chosen_date);
                jQuery('#label-visit-date-sukabumi').text(date_display);
                jQuery('#label-visit-shift-sukabumi').html(' <span>|</span> '+label_shift);
                jQuery('#calendar2').addClass('hide');
                jQuery('#calendar-mandatory-info').addClass('hide');
                \$(\".tabs-register\").addClass(\"hide\");
                \$(\".tabs-explore\").addClass(\"hide\");
                jQuery('#registration-form2').removeClass('hide');
                jQuery(\"body, html\").animate({
                    scrollTop: jQuery('#registration-form2').offset().top - 300
                }, 600);
            });

            var events_array_sukabumi = [];
            ";
        // line 553
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["explore_schedules"]) ? $context["explore_schedules"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["schedule"]) {
            // line 554
            echo "            events_array_sukabumi.push({
                startDate: new Date(";
            // line 555
            echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["schedule"]) ? $context["schedule"] : null), "visit_date"), "Y");
            echo ", ";
            echo (twig_date_format_filter($this->env, $this->getAttribute((isset($context["schedule"]) ? $context["schedule"] : null), "visit_date"), "m") - 1);
            echo ", ";
            echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["schedule"]) ? $context["schedule"] : null), "visit_date"), "d");
            echo "),
                endDate: new Date(";
            // line 556
            echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["schedule"]) ? $context["schedule"] : null), "visit_date"), "Y");
            echo ", ";
            echo (twig_date_format_filter($this->env, $this->getAttribute((isset($context["schedule"]) ? $context["schedule"] : null), "visit_date"), "m") - 1);
            echo ", ";
            echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["schedule"]) ? $context["schedule"] : null), "visit_date"), "d");
            echo "),
                title: \"";
            // line 557
            echo $this->getAttribute((isset($context["schedule"]) ? $context["schedule"] : null), "institution_name");
            echo "\",
                shift: ";
            // line 558
            echo $this->getAttribute((isset($context["schedule"]) ? $context["schedule"] : null), "shift");
            echo "
            });
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['schedule'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 561
        echo "

            jQuery(\"#calendar2\").dp_calendar({
                events_array: events_array_sukabumi
            },'";
        // line 565
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "');


            if(\$(window).width()>1023){
                var test = jQuery('.calendar_sukabumi').height();
                jQuery('.dp_calendar .large-3').css('height',test);
            }

        });
        
        jQuery(function(\$){
            \$(\"#landline\").mask(\"(099)-999-9999?9\");
            \$(\"#mobile\").mask(\"(0899)-9999-999?9\");
            \$(\"#landline-sukabumi\").mask(\"(099)-999-9999?9\");
            \$(\"#mobile-sukabumi\").mask(\"(0899)-9999-999?9\");
        });
    </script>

";
    }

    public function getTemplateName()
    {
        return "factory_tour_new.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  916 => 565,  910 => 561,  901 => 558,  897 => 557,  889 => 556,  881 => 555,  878 => 554,  874 => 553,  842 => 524,  828 => 515,  819 => 509,  814 => 506,  805 => 503,  801 => 502,  793 => 501,  785 => 500,  782 => 499,  778 => 498,  746 => 469,  732 => 460,  678 => 409,  673 => 407,  669 => 406,  664 => 404,  660 => 403,  656 => 402,  652 => 401,  647 => 400,  644 => 399,  627 => 388,  622 => 385,  616 => 384,  606 => 379,  600 => 375,  597 => 374,  591 => 370,  585 => 368,  581 => 366,  578 => 365,  573 => 364,  571 => 363,  561 => 356,  555 => 353,  533 => 336,  525 => 333,  507 => 318,  501 => 315,  492 => 309,  486 => 306,  468 => 291,  456 => 282,  447 => 276,  441 => 273,  432 => 267,  426 => 264,  417 => 258,  411 => 255,  399 => 246,  382 => 232,  365 => 220,  357 => 217,  339 => 202,  333 => 199,  324 => 193,  318 => 190,  300 => 175,  288 => 166,  279 => 160,  273 => 157,  264 => 151,  258 => 148,  249 => 142,  243 => 139,  231 => 130,  214 => 116,  170 => 75,  166 => 74,  160 => 71,  154 => 67,  139 => 58,  131 => 53,  117 => 46,  107 => 43,  98 => 37,  92 => 34,  85 => 29,  81 => 28,  73 => 23,  67 => 20,  58 => 14,  51 => 9,  48 => 8,  42 => 6,  38 => 5,  33 => 4,  30 => 3,);
    }
}
