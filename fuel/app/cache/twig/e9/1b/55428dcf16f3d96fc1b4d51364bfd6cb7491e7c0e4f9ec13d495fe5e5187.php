<?php

/* list_history.twig */
class __TwigTemplate_e91b55428dcf16f3d96fc1b4d51364bfd6cb7491e7c0e4f9ec13d495fe5e5187 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("backend/template.twig");

        $this->blocks = array(
            'backend_css' => array($this, 'block_backend_css'),
            'backend_content_header' => array($this, 'block_backend_content_header'),
            'backend_content' => array($this, 'block_backend_content'),
            'backend_js' => array($this, 'block_backend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "backend/template.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_backend_css($context, array $blocks = array())
    {
        // line 4
        echo "\t";
        $this->displayParentBlock("backend_css", $context, $blocks);
        echo "
\t<!-- dataTables css -->
\t";
        // line 6
        echo Asset::css("datatables/dataTables.bootstrap.css");
        echo "
";
    }

    // line 9
    public function block_backend_content_header($context, array $blocks = array())
    {
        // line 10
        echo "<!-- Content Header (Page header) -->
<section class=\"content-header\">
\t<h1>";
        // line 13
        echo "\t\tFactory Tour
\t\t<small>History List</small>
\t</h1>
\t";
        // line 17
        echo "\t<ol class=\"breadcrumb\">
\t\t<li><a href=\"";
        // line 18
        echo Uri::base();
        echo "backend\">Home</a></li>
\t\t<li><a href=\"";
        // line 19
        echo Uri::base();
        echo "backend/factorytour\">Factory Tour</a></li>
\t\t<li class=\"active\">History List</li>
\t</ol>
</section>
";
    }

    // line 25
    public function block_backend_content($context, array $blocks = array())
    {
        // line 26
        echo "
";
        // line 27
        if ((twig_length_filter($this->env, (isset($context["success_message"]) ? $context["success_message"] : null)) > 0)) {
            // line 28
            echo "<div class=\"alert alert-success alert-dismissable\">
\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
\t";
            // line 30
            echo (isset($context["success_message"]) ? $context["success_message"] : null);
            echo "
</div>
";
        }
        // line 33
        echo "
";
        // line 34
        if ((twig_length_filter($this->env, (isset($context["error_message"]) ? $context["error_message"] : null)) > 0)) {
            // line 35
            echo "<div class=\"alert alert-danger alert-dismissable\">
\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
\t";
            // line 37
            echo (isset($context["error_message"]) ? $context["error_message"] : null);
            echo "
</div>
";
        }
        // line 40
        echo "
\t<div class=\"row\">
\t\t<div class=\"col-xs-12\">
\t\t\t<div class=\"box\">
\t\t\t\t<div class=\"box-body table-responsive\">
\t\t\t\t\t<table id=\"table-factorytour-histories\" class=\"table table-bordered table-striped\">
\t\t\t\t\t<thead>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<th>Factory</th>
\t\t\t\t\t\t\t<th>Date</th>
\t\t\t\t\t\t\t<th>Shift</th>
                                                        <th>Name</th>
                                                        <th>Institution</th>
                                                        <th>Visitor</th>
                                                        <th>Address</th>
                                                        <th>Landline</th>
                                                        <th>MObile</th>
                                                        <th>Email</th>
                                                        <th>Message</th>
                                                        <th>Register At</th>
\t\t\t\t\t\t</tr>
\t\t\t\t\t</thead>
\t\t\t\t\t<tbody>
\t\t\t\t\t\t";
        // line 63
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["history_list"]) ? $context["history_list"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["history"]) {
            // line 64
            echo "\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td>";
            // line 65
            echo $this->getAttribute((isset($context["history"]) ? $context["history"] : null), "get_factory_name", array(), "method");
            echo "</td>
\t\t\t\t\t\t\t<td>";
            // line 66
            echo $this->getAttribute((isset($context["history"]) ? $context["history"] : null), "visit_date");
            echo "</td>
\t\t\t\t\t\t\t<td>";
            // line 67
            echo $this->getAttribute((isset($context["history"]) ? $context["history"] : null), "visit_shift");
            echo "</td>
                                                        <td>";
            // line 68
            echo $this->getAttribute((isset($context["history"]) ? $context["history"] : null), "name");
            echo "</td>
                                                        <td>";
            // line 69
            echo $this->getAttribute((isset($context["history"]) ? $context["history"] : null), "institution");
            echo "</td>
                                                        <td>";
            // line 70
            echo $this->getAttribute((isset($context["history"]) ? $context["history"] : null), "visitor");
            echo "</td>
                                                        <td>";
            // line 71
            echo $this->getAttribute((isset($context["history"]) ? $context["history"] : null), "address");
            echo "</td>
                                                        <td>";
            // line 72
            echo $this->getAttribute((isset($context["history"]) ? $context["history"] : null), "landline");
            echo "</td>
                                                        <td>";
            // line 73
            echo $this->getAttribute((isset($context["history"]) ? $context["history"] : null), "mobile");
            echo "</td>
                                                        <td>";
            // line 74
            echo $this->getAttribute((isset($context["history"]) ? $context["history"] : null), "email");
            echo "</td>
                                                        <td>";
            // line 75
            echo $this->getAttribute((isset($context["history"]) ? $context["history"] : null), "message");
            echo "</td>
                                                        <td>";
            // line 76
            echo $this->getAttribute((isset($context["history"]) ? $context["history"] : null), "register_at");
            echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['history'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 79
        echo "\t\t\t\t\t</tbody>
\t\t\t\t\t</table>
\t\t\t\t</div><!-- /.box-body -->
\t\t\t</div><!-- /.box -->
\t\t</div>
\t</div>
";
    }

    // line 87
    public function block_backend_js($context, array $blocks = array())
    {
        // line 88
        echo "\t";
        $this->displayParentBlock("backend_js", $context, $blocks);
        echo "
\t<!-- DATA TABES SCRIPT -->
\t";
        // line 90
        echo Asset::js("plugins/datatables/jquery.dataTables.js");
        echo "
\t";
        // line 91
        echo Asset::js("plugins/datatables/dataTables.bootstrap.js");
        echo "
\t<!-- custom script -->
\t<script type=\"text/javascript\">
\t\t\$(function() {
\t\t\t\$(\"#table-factorytour-histories\").dataTable( {
\t\t\t\t\"aoColumns\": [ 
\t\t\t\t\tnull,
\t\t\t\t\tnull,
\t\t\t\t\tnull,
\t\t\t\t\tnull,
\t\t\t\t\t{ \"bSearchable\": false, \"bSortable\": false },
\t\t\t\t\t{ \"bSearchable\": false, \"bSortable\": false }
\t\t\t\t]
\t\t\t} );
\t\t} );
\t</script>
\t<!-- Dialog Confirmation Script -->
\t";
        // line 108
        echo Asset::js("backend-dialog.js");
        echo "
\t<script type=\"text/javascript\">
\t\tjQuery('#table-factorytour-histories').on('click', '.btn-delete', function(e){
\t\t\te.preventDefault();
\t\t\tvar my = jQuery(this),
\t\t\t\turl_del = my.data('url-delete');
\t\t\tvar yes_callback = function(e){
\t\t\t\te.preventDefault();
\t\t\t\tjQuery(location).attr('href', url_del);
\t\t\t}
\t\t\tbackend_dialog.show_dialog_confirm('Are you sure want to delete this?', yes_callback);
\t\t});
        jQuery('#table-factorytour-histories').on('click', '.btn-rearrange', function(e){
\t\t\te.preventDefault();
\t\t\tvar my = jQuery(this),
\t\t\t\turl_del = my.data('url-rearrange');
\t\t\tvar yes_callback = function(e){
\t\t\t\te.preventDefault();
\t\t\t\tjQuery(location).attr('href', url_del);
\t\t\t}
\t\t\tbackend_dialog.show_dialog_confirm('Are you sure want to rearrange from this?', yes_callback);
\t\t});
\t</script>
";
    }

    public function getTemplateName()
    {
        return "list_history.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  235 => 108,  215 => 91,  211 => 90,  205 => 88,  202 => 87,  192 => 79,  183 => 76,  179 => 75,  175 => 74,  171 => 73,  167 => 72,  163 => 71,  159 => 70,  155 => 69,  151 => 68,  147 => 67,  143 => 66,  139 => 65,  136 => 64,  132 => 63,  107 => 40,  101 => 37,  97 => 35,  95 => 34,  92 => 33,  86 => 30,  82 => 28,  80 => 27,  77 => 26,  74 => 25,  65 => 19,  61 => 18,  58 => 17,  53 => 13,  49 => 10,  46 => 9,  40 => 6,  34 => 4,  31 => 3,);
    }
}
