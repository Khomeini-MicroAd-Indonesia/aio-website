<?php

/* template_about_us.twig */
class __TwigTemplate_62c85f23a777abc77a5d99863b5aa9c7122e90322d41b5e4adcb78f53dae1067 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"about-us\">
    <div class=\"banner-about\">
        ";
        // line 3
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["banner_detail"]) ? $context["banner_detail"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["banner"]) {
            // line 4
            echo "            <img class=\"banner\" data-interchange=\"[";
            echo Uri::base();
            echo "media/about/";
            echo $this->getAttribute((isset($context["banner"]) ? $context["banner"] : null), "image_s");
            echo ", small], [";
            echo Uri::base();
            echo "media/about/";
            echo $this->getAttribute((isset($context["banner"]) ? $context["banner"] : null), "image_m");
            echo ", medium], [";
            echo Uri::base();
            echo "/media/about/";
            echo $this->getAttribute((isset($context["banner"]) ? $context["banner"] : null), "image");
            echo ", large]\">
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['banner'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 6
        echo "    </div>
    <div class=\"about-headline\">
        <div class=\"row\">
            <h1>Otsuka-People creating new products for better health worldwide</h1>
            <div style=\"height: 20px;\"></div>
            <div class=\"highlight-line\">
                <img src=\"";
        // line 12
        echo Uri::base();
        echo "/assets/css/images/highlights-line.png\"/>
            </div>
        </div>
    </div>
    <div class=\"about-menu\">
        <div class=\"row\" data-equalizer=\"about-menu\">
            <div class=\"small-4 columns sub-menu ";
        // line 18
        echo (isset($context["active_about"]) ? $context["active_about"] : null);
        echo "\" data-equalizer-watch=\"about-menu\">
                ";
        // line 20
        echo "                <a href=\"";
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us\">About Us</a>
                <div class=\"hr\"></div>
            </div>
            <div class=\"small-4 columns sub-menu ";
        // line 23
        echo (isset($context["active_award"]) ? $context["active_award"] : null);
        echo "\" data-equalizer-watch=\"about-menu\">
                <a href=\"";
        // line 24
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/awards-and-certificates\">AWARDS & CERTIFICATES</a>
                <div class=\"hr\"></div>
            </div>
            <div class=\"small-4 columns sub-menu ";
        // line 27
        echo (isset($context["active_history"]) ? $context["active_history"] : null);
        echo "\" data-equalizer-watch=\"about-menu\">
                <a href=\"";
        // line 28
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/history\">HISTORY</a>
                <div class=\"hr\"></div>
            </div>
            <div style=\"clear: both;\"></div>
        </div>
    </div>
    <div style=\"height: 20px;\"></div>
</div>";
    }

    public function getTemplateName()
    {
        return "template_about_us.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 28,  86 => 27,  79 => 24,  67 => 20,  63 => 18,  54 => 12,  27 => 4,  23 => 3,  19 => 1,  183 => 63,  180 => 62,  175 => 65,  173 => 62,  167 => 60,  164 => 59,  157 => 52,  154 => 51,  136 => 54,  134 => 51,  127 => 47,  120 => 43,  114 => 40,  107 => 36,  94 => 26,  87 => 22,  75 => 23,  70 => 10,  52 => 9,  49 => 8,  46 => 6,  40 => 5,  35 => 4,  32 => 3,);
    }
}
