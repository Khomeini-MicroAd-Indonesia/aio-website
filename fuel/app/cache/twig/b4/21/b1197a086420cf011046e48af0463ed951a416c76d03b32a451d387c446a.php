<?php

/* pocari_sweat.twig */
class __TwigTemplate_b421b1197a086420cf011046e48af0463ed951a416c76d03b32a451d387c446a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'back_to_top' => array($this, 'block_back_to_top'),
            'frontend_js' => array($this, 'block_frontend_js'),
            'back_to_top_js' => array($this, 'block_back_to_top_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        echo Asset::css("custom/animations.css");
        echo "
    ";
        // line 5
        $this->displayParentBlock("frontend_css", $context, $blocks);
        echo "
    ";
        // line 6
        echo Asset::css("custom/product.css");
        echo "
";
    }

    // line 8
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 9
        echo "    <div class=\"pocari columns\">
        <div class=\"columns sub-menu-product\" data-equalizer=\"menu-ionessense\">
            <div class=\"medium-2 medium-offset-3 small-4 columns\">
                <a href=\"";
        // line 12
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/brand/pocari-sweat\">
                    <div class=\"columns pocari-sweat-color\" data-equalizer-watch=\"menu-ionessense\">
                        <img src=\"";
        // line 14
        echo Uri::base();
        echo "assets/img/product/icon-brand-pocarisweat.png\">
                        <span class=\"active-menu\"></span>
                    </div>
                </a>
            </div>
            <div class=\"medium-2 small-4 columns\">
                <a href=\"";
        // line 20
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/brand/soyjoy\">
                    <div class=\"columns soyjoy-color\" data-equalizer-watch=\"menu-ionessense\">
                        <img src=\"";
        // line 22
        echo Uri::base();
        echo "assets/img/product/icon-brand-soyjoy.png\">
                    </div>
                </a>
            </div>
            <div class=\"medium-2 small-4 end columns\">
                <a href=\"";
        // line 27
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/brand/ionessence\">
                    <div class=\"columns ionesence-color\" data-equalizer-watch=\"menu-ionessense\">
                        <img src=\"";
        // line 29
        echo Uri::base();
        echo "assets/img/product/icon-brand-ionessence.png\">
                    </div>
                </a>
            </div>
        </div>
        <div class=\"columns banner text-center\">
            <img data-interchange=\"[";
        // line 35
        echo Uri::base();
        echo "/assets/img/product/banner-pocari-sweat-small-new.png, small], [";
        echo Uri::base();
        echo "/assets/img/product/banner-pocari-sweat-medium-new.png, medium], [";
        echo Uri::base();
        echo "/assets/img/product/banner-pocari-sweat-new.png, large]\">
        </div>
        <div class=\"row\">
            <div class=\"columns product-detail\" data-equalizer=\"product-detail\">
                <div class=\"medium-6 small-12 columns\">
                    <div class=\"columns product-desc animatedParent\" data-sequence=\"300\" data-equalizer-watch=\"product-detail\">
                        <div class=\"animated fadeInUp\" data-id=\"1\">
                            <div class=\"title\">
                                <p>Pocari Sweat</p>
                                <div style=\"width: 110px; height: 3px;background: #ffffff;margin-top: 23px;margin-bottom: 15px\"></div>
                            </div>
                            <p class=\"animated fadeInUp\" data-id=\"2\">";
        // line 46
        echo Lang::get("txt_pocari02");
        echo "</p>
                        </div>
                    </div>
                </div>
                <div class=\"medium-6 small-12 columns animatedParent\">
                    ";
        // line 51
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["campaign_pocari"]) ? $context["campaign_pocari"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["detail"]) {
            // line 52
            echo "                    <div class=\"columns product-desc-img  animated fadeInUp delay-250\">
                        <a href=\"";
            // line 53
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "link");
            echo "\" target=\"_blank\"><img class=\"\" src=\"";
            echo Uri::base();
            echo "media/campaign/";
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "image");
            echo "\" data-equalizer-watch=\"product-detail\"></a>
                    </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['detail'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 56
        echo "                </div>
            </div>
            <div class=\"columns\">
                <div class=\"columns\">
                    <div class=\"product-variant columns\">
                        <div class=\"medium-6 columns variant-display\">
                            <div class=\"small-up-7 columns animatedParent\" data-sequence=\"200\" data-equalizer=\"product\">
                                <div class=\"small-2 columns animated fadeInUp\" data-id=\"1\"><img src=\"";
        // line 63
        echo Uri::base();
        echo "/assets/img/product/variant/pocari-variant-sachet-15g-new-right.png\"></div>
                                <div class=\"small-2 columns animated fadeInUp\" data-id=\"2\"><img src=\"";
        // line 64
        echo Uri::base();
        echo "/assets/img/product/variant/sachet-37gr.png\"></div>
                                <div class=\"small-2 columns animated fadeInUp\" data-id=\"3\"><img src=\"";
        // line 65
        echo Uri::base();
        echo "/assets/img/product/variant/pocari-variant-kaleng-330ml-new.png\"></div>
                                <div class=\"small-2 columns animated fadeInUp\" data-id=\"4\"><img src=\"";
        // line 66
        echo Uri::base();
        echo "/assets/img/product/variant/pocari-variant-350ml-new.png\"> </div>
                                <div class=\"small-2 columns animated fadeInUp\" data-id=\"5\"><img src=\"";
        // line 67
        echo Uri::base();
        echo "/assets/img/product/variant/pocari-variant-500ml-new.png\"> </div>
                                <div class=\"small-2 columns animated fadeInUp\" data-id=\"6\"><img src=\"";
        // line 68
        echo Uri::base();
        echo "/assets/img/product/variant/pocari-variant-900ml-new.png\"> </div>
                                <div class=\"small-2 columns animated fadeInUp\" data-id=\"7\"><img src=\"";
        // line 69
        echo Uri::base();
        echo "/assets/img/product/variant/pocari-variant-2L.png\"> </div>
                            </div>
                        </div>
                        <div class=\"medium-6 columns\">
                            <div class=\"columns\">
                                <p class=\"variant-title\">";
        // line 74
        echo Lang::get("txt_pocari03");
        echo "</p>
                                <div class=\"line\"></div>
                                <ul class=\"variant-name medium-6 end columns animatedParent\" data-sequence=\"100\">
                                    <li class=\"animated fadeInUp\" data-id=\"1\">Sachet 15 gr</li>
                                    <li class=\"animated fadeInUp\" data-id=\"2\">Sachet 37 gr</li>
                                    <li class=\"animated fadeInUp\" data-id=\"3\">Kaleng 330 ml</li>
                                    <li class=\"animated fadeInUp\" data-id=\"4\">PET 350 ml</li>
                                    <li class=\"animated fadeInUp\" data-id=\"5\">PET 500 ml</li>
                                    <li class=\"animated fadeInUp\" data-id=\"6\">PET 900 ml</li>
                                    <li class=\"animated fadeInUp\" data-id=\"7\">PET 2 L</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"columns\">
                <div class=\"columns\">
                    <div class=\"columns product-link\">
                        <div class=\"product-button\">
                            <a href=\"http://www.pocarisweat.co.id/\" target=\"_blank\" class=\"button\">GO TO <br/>PRODUCT WEBSITE</a>
                            <div style=\"border-bottom: 4px solid #ffffff ; width: 60px\"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"show-for-small-only\">
            ";
        // line 102
        $this->displayBlock('back_to_top', $context, $blocks);
        // line 105
        echo "        </div>
    </div>
";
    }

    // line 102
    public function block_back_to_top($context, array $blocks = array())
    {
        // line 103
        echo "                ";
        $this->displayParentBlock("back_to_top", $context, $blocks);
        echo "
            ";
    }

    // line 108
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 109
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    ";
        // line 110
        echo Asset::js("css3-animate-it.js");
        echo "
    <script type=\"text/javascript\">
        ";
        // line 112
        $this->displayBlock('back_to_top_js', $context, $blocks);
        // line 115
        echo "    </script>
";
    }

    // line 112
    public function block_back_to_top_js($context, array $blocks = array())
    {
        // line 113
        echo "        ";
        $this->displayParentBlock("back_to_top_js", $context, $blocks);
        echo "
        ";
    }

    public function getTemplateName()
    {
        return "pocari_sweat.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  261 => 113,  258 => 112,  253 => 115,  251 => 112,  246 => 110,  241 => 109,  238 => 108,  231 => 103,  228 => 102,  222 => 105,  220 => 102,  189 => 74,  181 => 69,  177 => 68,  173 => 67,  169 => 66,  165 => 65,  161 => 64,  157 => 63,  148 => 56,  135 => 53,  132 => 52,  128 => 51,  120 => 46,  102 => 35,  93 => 29,  87 => 27,  79 => 22,  73 => 20,  64 => 14,  58 => 12,  53 => 9,  50 => 8,  44 => 6,  40 => 5,  35 => 4,  32 => 3,);
    }
}
