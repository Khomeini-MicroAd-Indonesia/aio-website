<?php

/* awards_and_certificates.twig */
class __TwigTemplate_6b5521e4a59b49ad846d1ed41fd9283ee530a96980302395fad331bb34be2a3d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("frontend_css", $context, $blocks);
        echo "
    ";
        // line 6
        echo "    ";
        echo Asset::css("custom/about.css");
        echo "
";
    }

    // line 8
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 9
        echo "    ";
        $this->env->loadTemplate("template_about_us.twig")->display($context);
        // line 10
        echo "    <div class=\"awards-and-certificates\">
        <div class=\"description\">
            <div class=\"row\">
                <div class=\"large-6 medium-6 columns\">
                    <p>PT. Amerta Indah Otsuka berkomitmen untuk memberikan yang terbaik pada konsumen dan masyarakat sesuai dengan misinya untuk menjadi perusahaan yang brilian, dengan memberikan kontribusi yang signifikan dan terpercaya bagi konsumen serta masyarakat.                </p>
                </div>
                <div class=\"large-6 medium-6 columns\">
                    <p>Komitmen tersebut diwujudkan dengan kegiatan operasional yang menjunjung tinggi kualitas dan standarisasi baik lokal maupun internasional yang telah diakui dunia. Komitmen dan kerja keras PT. Amerta Indah Otsuka telah diakui berbagai pihak melalui berbagai sertifikat sebagai berikut:</p>
                </div>
            </div>
        </div>
        <div class=\"large-12 medium-12 columns no-padd\">
            <h1 class=\"title\">AWARDS</h1>
        </div>
        <div class=\"large-12 medium-12 columns description\">
            <div class=\"large-8 large-centered medium-8 medium-centered columns\">
                <div class=\"orbit\" role=\"region\" aria-label=\"Favorite Space Pictures\" data-orbit data-options=\"animInFromLeft:fade-in; animInFromRight:fade-in; animOutToLeft:fade-out; animOutToRight:fade-out;\">

                    <ul class=\"orbit-container\">
                        <button class=\"orbit-previous\" aria-label=\"previous\"><span class=\"show-for-sr\">Previous Slide</span><i class=\"fi-arrow-left small\"></i></button>
                        <button class=\"orbit-next\" aria-label=\"next\"><span class=\"show-for-sr\">Next Slide</span><i class=\"fi-arrow-right small\"></i></button>
                        ";
        // line 31
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["award_detail"]) ? $context["award_detail"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["award"]) {
            // line 32
            echo "                            <li class=\"is-active orbit-slide ";
            echo $this->getAttribute((isset($context["award"]) ? $context["award"] : null), "id");
            echo "\">
                                <div class=\"banner\">
                                    <img src=\"";
            // line 34
            echo Uri::base();
            echo "media/awardcertificate/";
            echo $this->getAttribute((isset($context["award"]) ? $context["award"] : null), "image");
            echo "\">
                                </div>
                                <div class=\"desc\">
                                    <p>";
            // line 37
            echo $this->getAttribute((isset($context["award"]) ? $context["award"] : null), "desc");
            echo "</p>
                                </div>
                            </li>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['award'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 41
        echo "                    </ul>

                    <nav class=\"orbit-bullets\">
                        ";
        // line 44
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["award_detail"]) ? $context["award_detail"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["award"]) {
            // line 45
            echo "                            <button class=\"is-active\" data-slide=\"";
            echo $this->getAttribute((isset($context["award"]) ? $context["award"] : null), "id");
            echo "\"><span class=\"show-for-sr\">First slide details.</span><span class=\"show-for-sr\">Current Slide</span></button>
                            ";
            // line 49
            echo "                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['award'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 50
        echo "                    </nav>

                </div>
            </div>
        </div>
        <div class=\"large-12 medium-12 columns no-padd\">
            <h1 class=\"title\">CERTIFICATES</h1>
        </div>
        <div class=\"large-12 medium-12 columns description\">
            <div class=\"large-8 large-centered medium-8 medium-centered columns\">
                <div class=\"orbit\" role=\"region\" aria-label=\"Favorite Space Pictures\" data-orbit data-options=\"animInFromLeft:fade-in; animInFromRight:fade-in; animOutToLeft:fade-out; animOutToRight:fade-out;\">

                    <ul class=\"orbit-container\">
                        <button class=\"orbit-previous\" aria-label=\"previous\"><span class=\"show-for-sr\">Previous Slide</span><i class=\"fi-arrow-left small\"></i></button>
                        <button class=\"orbit-next\" aria-label=\"next\"><span class=\"show-for-sr\">Next Slide</span><i class=\"fi-arrow-right small\"></i></button>
                        ";
        // line 65
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["certificate_detail"]) ? $context["certificate_detail"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["certificate"]) {
            // line 66
            echo "                            <li class=\"is-active orbit-slide ";
            echo $this->getAttribute((isset($context["certificate"]) ? $context["certificate"] : null), "id");
            echo "\">
                                <div class=\"banner\">
                                    <img src=\"";
            // line 68
            echo Uri::base();
            echo "media/awardcertificate/";
            echo $this->getAttribute((isset($context["certificate"]) ? $context["certificate"] : null), "image");
            echo "\">
                                </div>
                                <div class=\"desc\">
                                    <p>";
            // line 71
            echo $this->getAttribute((isset($context["certificate"]) ? $context["certificate"] : null), "desc");
            echo "</p>
                                </div>
                            </li>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['certificate'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 75
        echo "                    </ul>

                    <nav class=\"orbit-bullets\">
                        ";
        // line 78
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["certificate_detail"]) ? $context["certificate_detail"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["certificate"]) {
            // line 79
            echo "                            <button class=\"is-active\" data-slide=\"";
            echo $this->getAttribute((isset($context["certificate"]) ? $context["certificate"] : null), "id");
            echo "\"><span class=\"show-for-sr\">First slide details.</span><span class=\"show-for-sr\">Current Slide</span></button>
                            ";
            // line 83
            echo "                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['certificate'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 84
        echo "                    </nav>

                </div>
            </div>
        </div>
    </div>
";
    }

    // line 92
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 93
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    ";
    }

    public function getTemplateName()
    {
        return "awards_and_certificates.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  200 => 93,  197 => 92,  187 => 84,  181 => 83,  176 => 79,  172 => 78,  167 => 75,  157 => 71,  149 => 68,  143 => 66,  139 => 65,  122 => 50,  116 => 49,  111 => 45,  107 => 44,  102 => 41,  92 => 37,  84 => 34,  78 => 32,  74 => 31,  51 => 10,  48 => 9,  45 => 8,  38 => 6,  33 => 4,  30 => 3,);
    }
}
