<?php

/* contact_us.twig */
class __TwigTemplate_7b7bd21286200a0fe4210fdb4fb6b233f211ef2f11061101443658fa0e4e703e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'frontend_content' => array($this, 'block_frontend_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('frontend_content', $context, $blocks);
    }

    public function block_frontend_content($context, array $blocks = array())
    {
        // line 2
        echo "    <html>
    <body>
    <div>
        <p>Kepada Customer Care PT Amerta Indah Otsuka,</p>
        <p>Berikut data yang masuk ke form Contact Us: </p>
        <p>Nama : ";
        // line 7
        echo (isset($context["name"]) ? $context["name"] : null);
        echo "</p>
        <p>Email : ";
        // line 8
        echo (isset($context["email"]) ? $context["email"] : null);
        echo "</p>
        <p>Phone : ";
        // line 9
        echo (isset($context["phone"]) ? $context["phone"] : null);
        echo "</p>
        <p>Fax : ";
        // line 10
        echo (isset($context["fax"]) ? $context["fax"] : null);
        echo "</p>
        <p>Message : ";
        // line 11
        echo (isset($context["msg"]) ? $context["msg"] : null);
        echo "</p>
    </div>
    </body>
    </html>
";
    }

    public function getTemplateName()
    {
        return "contact_us.twig";
    }

    public function getDebugInfo()
    {
        return array (  49 => 11,  45 => 10,  41 => 9,  37 => 8,  33 => 7,  26 => 2,  20 => 1,);
    }
}
