<?php

/* contact_us.twig */
class __TwigTemplate_0c457853dfb607b268d926e1dbb4dd7c7906246873e45f7126467ddae6d973fa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend_new.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'back_to_top' => array($this, 'block_back_to_top'),
            'frontend_js' => array($this, 'block_frontend_js'),
            'back_to_top_js' => array($this, 'block_back_to_top_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend_new.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("frontend_css", $context, $blocks);
        echo "
    ";
        // line 5
        echo Asset::css("custom/contact_us.css");
        echo "
";
    }

    // line 7
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 8
        echo "    <div class=\"columns contact-us\">
        <div class=\"medium-12 columns columns-contact\">
            <div class=\"medium-7 columns\">
                <form role=\"form\" id=\"contact-form\" enctype=\"multipart/form-data\" action=\"\" name=\"contact-form\" method=\"POST\" class=\"contact-form\">
                    <div class=\"row\">
                        <div class=\"medium-12 columns\">
                            <p class=\"font-email\">Email Us!</p>
                        </div>
                        <div class=\"medium-6 columns\">
                            <input id=\"name\" type=\"text\" placeholder=\"Name*\" name=\"name\" required>
                        </div>
                        <div class=\"medium-6 columns\">
                            <input id=\"email\" type=\"email\" placeholder=\"Email*\" name=\"email\" required>
                        </div>
                        <div class=\"medium-6 columns\">
                            <input id=\"phone\" type=\"text\" placeholder=\"Phone number\" name=\"phone\" required>
                        </div>
                        <div class=\"medium-6 columns\">
                            <input id=\"fax\" type=\"text\" placeholder=\"Fax\" name=\"fax\" required>
                        </div>
                        <div class=\"medium-12 columns\">
                            <textarea id=\"msg\" name=\"msg\" placeholder=\"Your message*\" rows=\"15\" required></textarea>
                        </div>
                        <div class=\"row\">
                            <div class=\"medium-12 small-12 columns captcha\">
                                <div class=\"g-recaptcha position-captcha\" data-sitekey=\"6LcOtBwTAAAAAMr8p2x6uA0BsIscnHqIiqmqHc07\"></div>
                            </div>
                        </div>
                        <div class=\"fill columns\" style=\"text-align: right\">
                            <button onclick=\"contact_us();\" class=\"send\" type=\"send\">SEND</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class=\"medium-5 columns\">
                <div class=\"contact-info\">
                    <p>CONTACT INFORMATION</p>
                    <div class=\"info\" style=\"font-size: 14px\">
                        <p>HEAD OFFICE</p>
                        <p>Pondok Indah Office Tower 5th Floor. Jl. Sultan Iskandar Muda Kav. V-TA South Jakarta, DKI Jakarta 12310</p>
                        <p class=\"no-space\">CUSTOMER CARE:</p>
                        <p>0800-1- OTSUKA (687852)</p>
                        <p class=\"no-space\">TELEPHONES:</p>
                        <p>+62 21 7697475</p>
                        <p class=\"no-space\">FAX:</p>
                        <p>+62 21 7697472</p>
                        <hr/>
                        <p>SUKABUMI OFFICE</p>
                        <p>Jl. Raya Siliwangi Km 28, Desa Kutajaya, Kecamatan Cicurug, Kab Sukabumi 43559</p>
                        <p>Phone : +62 – 266 – 733700</p>
                        <p>Fax : +62 – 266 – 733699</p>
                        <hr/>
                        <p>KEJAYAN FACTORY</p>
                        <p>Jl. Raya Pasuruan – Malang Km 11, Desa Pacarkeling, Kecamatan Kejayan, Kab Pasuruan 67172</p>
                        <p>Phone : +62 – 343 – 414200</p>
                        <p>Fax : +62 – 343 – 414201</p>
                        <hr/>
                        <p>GOOGLE MAPS</p>
                        <iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.0174245797575!2d106.77974331442877!3d-6.261434663043442!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f1a87085269f%3A0x55a6c33715ae5552!2sWisma+Pondok+Indah+2%2C+Jl.+Sultan+Iskandar+Muda!5e0!3m2!1sid!2sid!4v1454998911855\" width=\"100%\" height=\"250\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            <div style=\"clear: both;\"></div>
        </div>
        <div style=\"clear: both;\"></div>
        <div class=\"show-for-small-only\">
            ";
        // line 74
        $this->displayBlock('back_to_top', $context, $blocks);
        // line 77
        echo "        </div>
    </div>
    <div style=\"clear: both;\"></div>
    <div class=\"reveal\" id=\"exampleModal1\" data-reveal>
        <p>Terima kasih atas saran dan kritik</p>
        <button class=\"close-button\" data-close aria-label=\"Close modal\" type=\"button\">
            <span aria-hidden=\"true\">&times;</span>
        </button>
    </div>
";
    }

    // line 74
    public function block_back_to_top($context, array $blocks = array())
    {
        // line 75
        echo "                ";
        $this->displayParentBlock("back_to_top", $context, $blocks);
        echo "
            ";
    }

    // line 87
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 88
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    ";
        // line 89
        echo Asset::js("jquery.maskedinput.js");
        echo "
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script type=\"text/javascript\">
        jQuery(function(\$){
            \$(\"#phone\").mask(\"(099)-999-9999?99\");
            \$(\"#fax\").mask(\"(099)-999-9999?9\");
        });
        
        function contact_us(){
            
            var name = document.getElementById('name').value;
            var email = document.getElementById('email').value;
            var phone = document.getElementById('phone').value;
            var fax = document.getElementById('fax').value;
            var message = document.getElementById('msg').value;
            //alert(fax);
            
                
            if(message.length > 0 ){
                    
                \$.ajax({
                    type: \"POST\",
                    url: \"";
        // line 111
        echo Uri::base();
        echo "contact?email=\"+email,
                    data: {
                        'name': name,
                        'email': email,
                        'phone': phone,
                        'fax': fax,
                        'msg': message
                    },
                    success: function(){
                        document.getElementById('email').value = '';
                    },
                    error: function(){
                        event.preventDefault();
                    }
                });
                \$('#contact-form').submit();
                \$('#exampleModal1').foundation('open');

            }
                
        }
        ";
        // line 132
        $this->displayBlock('back_to_top_js', $context, $blocks);
        // line 135
        echo "    </script>
";
    }

    // line 132
    public function block_back_to_top_js($context, array $blocks = array())
    {
        // line 133
        echo "        ";
        $this->displayParentBlock("back_to_top_js", $context, $blocks);
        echo "
        ";
    }

    public function getTemplateName()
    {
        return "contact_us.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  209 => 133,  206 => 132,  201 => 135,  199 => 132,  175 => 111,  150 => 89,  145 => 88,  142 => 87,  135 => 75,  132 => 74,  119 => 77,  117 => 74,  49 => 8,  46 => 7,  40 => 5,  35 => 4,  32 => 3,);
    }
}
