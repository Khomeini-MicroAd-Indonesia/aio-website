<?php

/* email_responder.twig */
class __TwigTemplate_fbcafbacbb8af58dee7b8833b0c3729200c37a9cf12a6e2b550161f2c95fba58 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'frontend_content' => array($this, 'block_frontend_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('frontend_content', $context, $blocks);
    }

    public function block_frontend_content($context, array $blocks = array())
    {
        // line 2
        echo "    Dear ";
        echo (isset($context["name"]) ? $context["name"] : null);
        echo ", <br/><br/>
    Terima kasih atas pertanyaan, saran dan kritik yang Anda sampaikan.<br/>
    Tim Customer Care kami akan menindaklajuti permintaan Anda dengan segera.<br/><br/>
    Salam,<br/><br/><br/>
    Tim Customer Care PT Amerta Indah Otsuka
";
    }

    public function getTemplateName()
    {
        return "email_responder.twig";
    }

    public function getDebugInfo()
    {
        return array (  26 => 2,  20 => 1,);
    }
}
