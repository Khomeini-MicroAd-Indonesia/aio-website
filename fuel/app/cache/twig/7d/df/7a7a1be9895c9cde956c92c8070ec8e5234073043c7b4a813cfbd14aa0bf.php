<?php

/* list.twig */
class __TwigTemplate_7ddf7a7a1be9895c9cde956c92c8070ec8e5234073043c7b4a813cfbd14aa0bf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("backend/template.twig");

        $this->blocks = array(
            'backend_css' => array($this, 'block_backend_css'),
            'backend_content_header' => array($this, 'block_backend_content_header'),
            'backend_content' => array($this, 'block_backend_content'),
            'backend_js' => array($this, 'block_backend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "backend/template.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_backend_css($context, array $blocks = array())
    {
        // line 4
        echo "\t";
        $this->displayParentBlock("backend_css", $context, $blocks);
        echo "
\t<!-- dataTables css -->
\t";
        // line 6
        echo Asset::css("datatables/dataTables.bootstrap.css");
        echo "
";
    }

    // line 9
    public function block_backend_content_header($context, array $blocks = array())
    {
        // line 10
        echo "<!-- Content Header (Page header) -->
<section class=\"content-header\">
\t<h1>";
        // line 13
        echo "\t\tCareer
\t\t<small>List</small>
\t</h1>
\t";
        // line 17
        echo "\t<ol class=\"breadcrumb\">
\t\t<li><a href=\"";
        // line 18
        echo Uri::base();
        echo "backend\">Home</a></li>
\t\t<li class=\"active\">Career List</li>
\t</ol>
</section>
";
    }

    // line 24
    public function block_backend_content($context, array $blocks = array())
    {
        // line 25
        echo "
";
        // line 26
        if ((twig_length_filter($this->env, (isset($context["success_message"]) ? $context["success_message"] : null)) > 0)) {
            // line 27
            echo "<div class=\"alert alert-success alert-dismissable\">
\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
\t";
            // line 29
            echo (isset($context["success_message"]) ? $context["success_message"] : null);
            echo "
</div>
";
        }
        // line 32
        echo "
";
        // line 33
        if ((twig_length_filter($this->env, (isset($context["error_message"]) ? $context["error_message"] : null)) > 0)) {
            // line 34
            echo "<div class=\"alert alert-danger alert-dismissable\">
\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
\t";
            // line 36
            echo (isset($context["error_message"]) ? $context["error_message"] : null);
            echo "
</div>
";
        }
        // line 39
        echo "
\t<div class=\"box box-solid\">
\t\t<div class=\"box-body text-right\">
\t\t\t<a href=\"";
        // line 42
        echo Uri::base();
        echo "backend/career/add\">
\t\t\t\t<button class=\"btn btn-default\">Create</button>
\t\t\t</a>
\t\t</div>
\t</div>
\t<div class=\"row\">
\t\t<div class=\"col-xs-12\">
\t\t\t<div class=\"box\">
\t\t\t\t<div class=\"box-body table-responsive\">
\t\t\t\t\t<table id=\"table-career-careers\" class=\"table table-bordered table-striped\">
\t\t\t\t\t<thead>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<th>Career ID</th>
\t\t\t\t\t\t\t<th>Career Name</th>
                                                        <th>Career Req</th>
\t\t\t\t\t\t\t<th>Status</th>
\t\t\t\t\t\t\t<th>&nbsp;</th>
\t\t\t\t\t\t</tr>
\t\t\t\t\t</thead>
\t\t\t\t\t<tbody>
\t\t\t\t\t\t";
        // line 62
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["career_list"]) ? $context["career_list"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["career"]) {
            // line 63
            echo "\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td>";
            // line 64
            echo $this->getAttribute((isset($context["career"]) ? $context["career"] : null), "id");
            echo "</td>
\t\t\t\t\t\t\t<td>";
            // line 65
            echo $this->getAttribute((isset($context["career"]) ? $context["career"] : null), "name");
            echo "</td>
                                                        <td>";
            // line 66
            echo $this->getAttribute((isset($context["career"]) ? $context["career"] : null), "req_id");
            echo "</td>
\t\t\t\t\t\t\t<td>";
            // line 67
            echo $this->getAttribute((isset($context["career"]) ? $context["career"] : null), "get_status_name", array(), "method");
            echo "</td>
\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t<div class=\"btn-group\">
\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-default dropdown-toggle\" data-toggle=\"dropdown\">
\t\t\t\t\t\t\t\t\t\tAction <span class=\"caret\"></span>
\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t<ul class=\"dropdown-menu\" role=\"menu\">
                                                                            <li><a href=\"";
            // line 74
            echo Uri::base();
            echo "backend/career/edit/";
            echo $this->getAttribute((isset($context["career"]) ? $context["career"] : null), "id");
            echo "\">Edit</a></li>
                                                                            <li><a class=\"btn-delete\" data-url-delete=\"";
            // line 75
            echo Uri::base();
            echo "backend/career/delete/";
            echo $this->getAttribute((isset($context["career"]) ? $context["career"] : null), "id");
            echo "\" href=\"#\">Delete</a></li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['career'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 81
        echo "\t\t\t\t\t</tbody>
\t\t\t\t\t</table>
\t\t\t\t</div><!-- /.box-body -->
\t\t\t</div><!-- /.box -->
\t\t</div>
\t</div>
";
    }

    // line 89
    public function block_backend_js($context, array $blocks = array())
    {
        // line 90
        echo "\t";
        $this->displayParentBlock("backend_js", $context, $blocks);
        echo "
\t<!-- DATA TABES SCRIPT -->
\t";
        // line 92
        echo Asset::js("plugins/datatables/jquery.dataTables.js");
        echo "
\t";
        // line 93
        echo Asset::js("plugins/datatables/dataTables.bootstrap.js");
        echo "
\t<!-- custom script -->
\t<script type=\"text/javascript\">
\t\t\$(function() {
\t\t\t\$(\"#table-career-careers\").dataTable( {
\t\t\t\t\"aoColumns\": [ 
\t\t\t\t\tnull,
\t\t\t\t\tnull,
\t\t\t\t\tnull,
\t\t\t\t\t{ \"bSearchable\": false, \"bSortable\": false }
\t\t\t\t]
\t\t\t} );
\t\t} );
\t</script>
\t<!-- Dialog Confirmation Script -->
\t";
        // line 108
        echo Asset::js("backend-dialog.js");
        echo "
\t<script type=\"text/javascript\">
\t\tjQuery('#table-career-careers').on('click', '.btn-delete', function(e){
\t\t\te.preventDefault();
\t\t\tvar my = jQuery(this),
\t\t\t\turl_del = my.data('url-delete');
\t\t\tvar yes_callback = function(e){
\t\t\t\te.preventDefault();
\t\t\t\tjQuery(location).attr('href', url_del);
\t\t\t}
\t\t\tbackend_dialog.show_dialog_confirm('Are you sure want to delete this?', yes_callback);
\t\t});
        
\t</script>
";
    }

    public function getTemplateName()
    {
        return "list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  221 => 108,  203 => 93,  199 => 92,  193 => 90,  190 => 89,  180 => 81,  166 => 75,  160 => 74,  150 => 67,  146 => 66,  142 => 65,  138 => 64,  135 => 63,  131 => 62,  108 => 42,  103 => 39,  97 => 36,  93 => 34,  91 => 33,  88 => 32,  82 => 29,  78 => 27,  76 => 26,  73 => 25,  70 => 24,  61 => 18,  58 => 17,  53 => 13,  49 => 10,  46 => 9,  40 => 6,  34 => 4,  31 => 3,);
    }
}
