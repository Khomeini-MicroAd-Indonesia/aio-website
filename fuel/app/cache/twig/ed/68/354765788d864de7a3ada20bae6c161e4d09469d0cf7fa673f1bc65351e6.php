<?php

/* about_new.twig */
class __TwigTemplate_ed68354765788d864de7a3ada20bae6c161e4d09469d0cf7fa673f1bc65351e6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend_new.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend_new.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("frontend_css", $context, $blocks);
        echo "
    ";
        // line 5
        echo Asset::css("custom/about_new.css");
        echo "
";
    }

    // line 7
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 8
        echo "    <div class=\"expanded row custom-left about-us\">
        <div class=\"title\">
            <h2>";
        // line 10
        echo Lang::get("about_title");
        echo "</h2>
        </div>
        <div class=\"medium-8 medium-offset-2 end columns custom-content\">
            <div class=\"row\">
                <div class=\"medium-4 medium-centered columns logo-title\">
                    <img src=\"";
        // line 15
        echo Uri::base();
        echo "/assets/css/images/logo-otsuka-large.png\" alt=\"about otsuka\"/>
                </div>
            </div>
        </div>
    </div>
    <div class=\"expanded row custom-left about\">
        <div class=\"medium-8 medium-offset-2 end columns custom-content\">
            <p class=\"text-center title-section\">Otsuka-people creating new products for better health worldwide</p>
            <div class=\"columns about-desc\">
                ";
        // line 24
        echo Lang::get("about_desc");
        echo "
            </div>
        </div>
    </div>
    <div class=\"expanded row custom-left direktur\">
        <div class=\"medium-8 medium-offset-2 end columns custom-content\" data-equalizer=\"direktur\" data-equalize-on=\"large\">
        ";
        // line 30
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["president_msg"]) ? $context["president_msg"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["pres"]) {
            // line 31
            echo "            <p class=\"text-center title-section\">";
            echo Lang::get("about_msg");
            echo "</p>
            <div class=\"large-6 columns\" data-equalizer-watch=\"direktur\">
                <img src=\"";
            // line 33
            echo Uri::base();
            echo "media/president/";
            echo $this->getAttribute((isset($context["pres"]) ? $context["pres"] : null), "img");
            echo "\" alt=\"presiden otsuka\"/>
                ";
            // line 35
            echo "            </div>
            <div class=\"large-6 columns presiden-quote\" data-equalizer-watch=\"direktur\" style=\"position: relative;background: #FFFFFF\">
                <div>
                    <div class=\"quote-content\">
                        <div class=\"content-detail hide-for-large\">
                            ";
            // line 40
            echo $this->getAttribute((isset($context["pres"]) ? $context["pres"] : null), "msg");
            echo "
                            <span>-";
            // line 41
            echo $this->getAttribute((isset($context["pres"]) ? $context["pres"] : null), "name");
            echo ", ";
            echo $this->getAttribute((isset($context["pres"]) ? $context["pres"] : null), "position");
            echo " </span>
                        </div>
                        <img class=\"show-for-large\" src=\"";
            // line 43
            echo Uri::base();
            echo "/assets/css/images/president-speech-";
            echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
            echo ".png\" alt=\"\" title=\"\"/>
                    </div>
                </div>
                ";
            // line 47
            echo "            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pres'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "        </div>
    </div>
    <div class=\"expanded row custom-left visi-misi\" data-equalizer=\"visi\" data-equalize-on=\"large\">
        <div class=\"medium-8 medium-offset-2 end columns custom-content\">
            <div class=\"medium-12 columns\">
                <p class=\"title-section\" style=\"display:none;\">";
        // line 54
        echo Lang::get("about_visi");
        echo "</p>
            </div>
            <div class=\"large-4 large-push-8 columns visi-ornamen\"  data-equalizer-watch=\"visi\">
                <div class=\"ornamen-wrapper\">
                    <div class=\"ornamen-vision\">
                        <img src=\"";
        // line 59
        echo Uri::base();
        echo "/assets/css/images/visi-misi.jpg\" alt=\"visi misi otsuka\"/>
                    </div>
                </div>
            </div>
            <div class=\"large-8 large-pull-4 columns\" data-equalizer-watch=\"visi\">
            ";
        // line 64
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["about_detail"]) ? $context["about_detail"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["about"]) {
            // line 65
            echo "                <p style=\"display:none;\">
                <!-- ";
            // line 66
            echo $this->getAttribute((isset($context["about"]) ? $context["about"] : null), "obj");
            echo " -->
                </p>
                <div class=\"visi\">
                    <p>";
            // line 69
            echo Lang::get("txt_aboutus03");
            echo "</p>
                    <p>";
            // line 70
            echo $this->getAttribute((isset($context["about"]) ? $context["about"] : null), "highlight");
            echo "</p>
                </div>
                <div class=\"misi\">
                    <p>";
            // line 73
            echo Lang::get("txt_aboutus04");
            echo "</p>
                    ";
            // line 81
            echo "                    ";
            echo $this->getAttribute((isset($context["about"]) ? $context["about"] : null), "detail");
            echo "
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['about'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 84
        echo "            </div>
        </div>
    </div>
    <div class=\"expanded row custom-left konsep-product\">
        <div class=\"medium-8 medium-offset-2 end columns custom-content\">

            <div class=\"medium-12 columns\">
                <p class=\"text-center title-section\">";
        // line 91
        echo Lang::get("about_product");
        echo "</p>
            </div>
            <div class=\"medium-12 columns\">
                <div class=\"medium-up-3 small-up-3\">
                    ";
        // line 95
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["product_concept"]) ? $context["product_concept"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["prodconcept"]) {
            // line 96
            echo "                    <div class=\"columns nopadd\">
                        <img src=\"";
            // line 97
            echo Uri::base();
            echo "media/productconcept/";
            echo $this->getAttribute((isset($context["prodconcept"]) ? $context["prodconcept"] : null), "img");
            echo "\" alt=\"\"/>
                    </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['prodconcept'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 100
        echo "                </div>
            </div>

            <div class=\"medium-12 columns\">
                <p class=\"subtitle\">";
        // line 104
        echo Lang::get("txt_nutrition");
        echo "</p>
                <p class=\"konsep-desc\">";
        // line 105
        echo Lang::get("txt_nutrition_desc");
        echo "</p>
            </div>

        </div>
    </div>
    <div class=\"expanded row custom-left penghargaan\">
        <div class=\"medium-8 medium-offset-2 end columns custom-content\">
            <div class=\"medium-12 columns\">
                <p class=\"text-center title-section\">";
        // line 113
        echo Lang::get("txt_about_awards");
        echo "</p>
            </div>
            <div class=\"medium-12 columns penghargaan-desc\">
                ";
        // line 116
        echo Lang::get("txt_about_awards_desc");
        echo "
            </div>
            <div class=\"medium-12 columns\">
                <div class=\"medium-8 medium-offset-2 end orbit\" role=\"achievement\"  data-orbit data-infinite-wrap=\"false\" data-auto-play=\"false\">

                    <button class=\"orbit-previous\"><span class=\"show-for-sr\">Previous Slide</span><img src=\"";
        // line 121
        echo Uri::base();
        echo "/assets/css/images/arrow-achievement.png\"/></button>
                    <button class=\"orbit-next\"><span class=\"show-for-sr\">Next Slide</span><img src=\"";
        // line 122
        echo Uri::base();
        echo "/assets/css/images/arrow-achievement.png\"/></button>
                    <ul class=\"orbit-container\">
                    ";
        // line 124
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["award_certificate"]) ? $context["award_certificate"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["award"]) {
            // line 125
            echo "                        <li class=\"is-active orbit-slide\">
                            <div class=\"title-orbit\">";
            // line 126
            echo $this->getAttribute((isset($context["award"]) ? $context["award"] : null), "title");
            echo "</div>
                            <img class=\"ornamen-orbit\" src=\"";
            // line 127
            echo Uri::base();
            echo "/assets/css/images/shape-orbit.jpg\"/>
                            <div class=\"image-award\">
                                <div class=\"ia-wrap\">
                                    <img class=\"orbit-image\" src=\"";
            // line 130
            echo Uri::base();
            echo "media/awardcertificate/";
            echo $this->getAttribute((isset($context["award"]) ? $context["award"] : null), "img");
            echo "\" alt=\"";
            echo $this->getAttribute((isset($context["award"]) ? $context["award"] : null), "name");
            echo "\" title=\"";
            echo $this->getAttribute((isset($context["award"]) ? $context["award"] : null), "name");
            echo "\">
                                </div>
                            </div>
                            <figcaption class=\"orbit-caption\">";
            // line 133
            echo $this->getAttribute((isset($context["award"]) ? $context["award"] : null), "desc");
            echo "</figcaption>
                        </li>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['award'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 136
        echo "                    </ul>
                </div>
            </div>
        </div>
    </div>


";
    }

    // line 144
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 145
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    <script>
        \$( window ).load(function() {
        });
    </script>

";
    }

    public function getTemplateName()
    {
        return "about_new.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  319 => 145,  316 => 144,  305 => 136,  296 => 133,  284 => 130,  278 => 127,  274 => 126,  271 => 125,  267 => 124,  262 => 122,  258 => 121,  250 => 116,  244 => 113,  233 => 105,  229 => 104,  223 => 100,  212 => 97,  209 => 96,  205 => 95,  198 => 91,  189 => 84,  179 => 81,  175 => 73,  169 => 70,  165 => 69,  159 => 66,  156 => 65,  152 => 64,  144 => 59,  136 => 54,  129 => 49,  122 => 47,  114 => 43,  107 => 41,  103 => 40,  96 => 35,  90 => 33,  84 => 31,  80 => 30,  71 => 24,  59 => 15,  51 => 10,  47 => 8,  44 => 7,  38 => 5,  33 => 4,  30 => 3,);
    }
}
