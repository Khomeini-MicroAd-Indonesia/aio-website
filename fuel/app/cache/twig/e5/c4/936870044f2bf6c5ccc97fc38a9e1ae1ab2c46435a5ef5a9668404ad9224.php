<?php

/* about.twig */
class __TwigTemplate_e5c4936870044f2bf6c5ccc97fc38a9e1ae1ab2c46435a5ef5a9668404ad9224 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'back_to_top' => array($this, 'block_back_to_top'),
            'frontend_js' => array($this, 'block_frontend_js'),
            'back_to_top_js' => array($this, 'block_back_to_top_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("frontend_css", $context, $blocks);
        echo "
    ";
        // line 5
        echo Asset::css("custom/about.css");
        echo "
";
    }

    // line 7
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 8
        echo "    ";
        $this->env->loadTemplate("template_about_us.twig")->display($context);
        // line 9
        echo "    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["about_detail"]) ? $context["about_detail"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["detail"]) {
            // line 10
            echo "        <div class=\"about-content\">
            <div class=\"about-logo\">
                <div class=\"row\">
                    <img src=\"";
            // line 13
            echo Uri::base();
            echo "/assets/img/about-us/logo-about.jpg\"/>
                </div>
            </div>
            <div class=\"row\">
                <div style=\"border-bottom: 1px solid #bdbdbd\"></div>
            </div>
            <div style=\"height: 40px;\"></div>
            <div class=\"about-item\">
                <div class=\"row\" data-equalizer=\"hal\">
                    <h5>";
            // line 22
            echo Lang::get("txt_aboutus02");
            echo "</h5>
                    <hr/>
                    <div class=\"small-12 columns\" data-equalizer-watch=\"hal\">
                        <p>
                            ";
            // line 26
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "meaning");
            echo "
                        </p>
                    </div>
                    <div style=\"clear: both;\"></div>
                </div>
            </div>
        </div>
        <div class=\"columns about-visi\">
            <div class=\"row detail\">
               <div class=\"title\">
                   <p>";
            // line 36
            echo Lang::get("txt_aboutus03");
            echo "</p>
                   <div style=\"border-bottom: 6px solid #fff;width: 96px;margin-bottom: 26px;\"></div>
               </div>
               <div class=\"desc\">
                   ";
            // line 40
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "highlight");
            echo "
               </div>
                <div class=\"title\">
                    <p>";
            // line 43
            echo Lang::get("txt_aboutus04");
            echo "</p>
                    <div style=\"border-bottom: 6px solid #fff;width: 96px;margin-bottom: 26px;\"></div>
                </div>
                <div class=\"desc\">
                    ";
            // line 47
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "detail");
            echo "
                </div>
            </div>
            <div class=\"show-for-small-only\">
                ";
            // line 51
            $this->displayBlock('back_to_top', $context, $blocks);
            // line 54
            echo "            </div>
        </div>
    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['detail'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    // line 51
    public function block_back_to_top($context, array $blocks = array())
    {
        // line 52
        echo "                    ";
        $this->displayParentBlock("back_to_top", $context, $blocks);
        echo "
                ";
    }

    // line 59
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 60
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    <script type=\"text/javascript\">
    ";
        // line 62
        $this->displayBlock('back_to_top_js', $context, $blocks);
        // line 65
        echo "    </script>
";
    }

    // line 62
    public function block_back_to_top_js($context, array $blocks = array())
    {
        // line 63
        echo "        ";
        $this->displayParentBlock("back_to_top_js", $context, $blocks);
        echo "
    ";
    }

    public function getTemplateName()
    {
        return "about.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  183 => 63,  180 => 62,  175 => 65,  173 => 62,  167 => 60,  164 => 59,  157 => 52,  154 => 51,  136 => 54,  134 => 51,  127 => 47,  120 => 43,  114 => 40,  107 => 36,  94 => 26,  87 => 22,  75 => 13,  70 => 10,  52 => 9,  49 => 8,  46 => 7,  40 => 5,  35 => 4,  32 => 3,);
    }
}
