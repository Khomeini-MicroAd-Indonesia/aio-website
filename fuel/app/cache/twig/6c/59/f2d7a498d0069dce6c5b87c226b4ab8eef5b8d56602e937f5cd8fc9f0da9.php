<?php

/* csr_sehatkan_bangsa.twig */
class __TwigTemplate_6c59f2d7a498d0069dce6c5b87c226b4ab8eef5b8d56602e937f5cd8fc9f0da9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'about_menu_us' => array($this, 'block_about_menu_us'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'back_to_top' => array($this, 'block_back_to_top'),
            'frontend_js' => array($this, 'block_frontend_js'),
            'back_to_top_js' => array($this, 'block_back_to_top_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("frontend_css", $context, $blocks);
        echo "
    ";
        // line 5
        echo Asset::css("custom/csr_new.css");
        echo "
";
    }

    // line 7
    public function block_about_menu_us($context, array $blocks = array())
    {
        echo ".test";
    }

    // line 8
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 9
        echo "    <div class=\"csr-content\">
        ";
        // line 10
        $this->env->loadTemplate("template_csr_new.twig")->display($context);
        // line 11
        echo "
        <div class=\"row\">
            <div class=\"columns csr-description\">
            <p class=\"title\">SATU HATI SEHATKAN BANGSA</p>
            <div style=\"border-bottom: 1px solid #ced2d6\"></div>
            <div class=\"columns csr-description\">
                <div class=\"sub-title\">
                    <p>PT Amerta Indah Otsuka berkomitmen untuk mengembangkan program – program guna menciptakan kesehatan masyarakat yang lebih baik.</p>
                </div>
                <div class=\"banner-csr\"><img src=\"";
        // line 20
        echo Uri::base();
        echo "/assets/img/csr-new/demam-berdarah.png\"/></div>
            </div>
            <div class=\"columns\">
                <div class=\"desc-text\">
                    <div class=\"sub-title-program\">
                        <p>";
        // line 25
        echo Lang::get("txt_csr05");
        echo "</p>
                    </div>
                    <p>";
        // line 27
        echo Lang::get("txt_csr06");
        echo "</p>
                    <div class=\"sub-title-program\">
                        <p>";
        // line 29
        echo Lang::get("txt_csr07");
        echo "</p>
                    </div>
                    <p>";
        // line 31
        echo Lang::get("txt_csr08");
        echo "</p>
                </div>
            </div>
        </div>
        <div class=\"show-for-small-only\">
            ";
        // line 36
        $this->displayBlock('back_to_top', $context, $blocks);
        // line 39
        echo "        </div>
    </div>
";
    }

    // line 36
    public function block_back_to_top($context, array $blocks = array())
    {
        // line 37
        echo "                ";
        $this->displayParentBlock("back_to_top", $context, $blocks);
        echo "
            ";
    }

    // line 43
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 44
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    <script type=\"text/javascript\">
        ";
        // line 46
        $this->displayBlock('back_to_top_js', $context, $blocks);
        // line 49
        echo "    </script>
";
    }

    // line 46
    public function block_back_to_top_js($context, array $blocks = array())
    {
        // line 47
        echo "        ";
        $this->displayParentBlock("back_to_top_js", $context, $blocks);
        echo "
        ";
    }

    public function getTemplateName()
    {
        return "csr_sehatkan_bangsa.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  140 => 47,  137 => 46,  132 => 49,  130 => 46,  124 => 44,  121 => 43,  114 => 37,  111 => 36,  105 => 39,  103 => 36,  95 => 31,  90 => 29,  85 => 27,  80 => 25,  72 => 20,  61 => 11,  59 => 10,  56 => 9,  53 => 8,  47 => 7,  41 => 5,  36 => 4,  33 => 3,);
    }
}
