<?php

/* distributor.twig */
class __TwigTemplate_7221fd3ef4722f4330b4fd69deade64505e7c6c62e538fefbc41ba9cfb5de7ee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'back_to_top' => array($this, 'block_back_to_top'),
            'frontend_js' => array($this, 'block_frontend_js'),
            'back_to_top_js' => array($this, 'block_back_to_top_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        echo Asset::css("custom/animations.css");
        echo "
    ";
        // line 5
        $this->displayParentBlock("frontend_css", $context, $blocks);
        echo "
    ";
        // line 6
        echo Asset::css("custom/distribution.css");
        echo "
";
    }

    // line 8
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 9
        echo "    <div class=\"columns distribution\">
        <div class=\"banner\">
            <img data-interchange=\"[";
        // line 11
        echo Uri::base();
        echo "/assets/img/distribution/distribution-map-s.jpg, small], [";
        echo Uri::base();
        echo "/assets/img/distribution/distribution-map-m.jpg, medium], [";
        echo Uri::base();
        echo "/assets/img/distribution/distribution-map.jpg, large]\">
            <div style=\"width: 100%; height: 110px;background: #3e5172;margin-bottom: 50px\"></div>
        </div>
        <div class=\"row address-list\" data-equalizer=\"address-list\">
            <p class=\"title\">DISTRIBUTION<br/>MAP</p>
            <div style=\"width: 194px;border-bottom: 6px solid #4d5a64;margin-bottom: 60px\"></div>
            <div class=\"row address-detail\" id=\"address-detail\">
                <div class=\"medium-3 small-12 columns provinsi\">
                    <select id=\"province-list\" class=\"medium-12 small-12 columns list-provinsi\">
                        ";
        // line 20
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["provinces"]) ? $context["provinces"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["prov"]) {
            // line 21
            echo "                            <option value=\"";
            echo $this->getAttribute((isset($context["prov"]) ? $context["prov"] : null), "id");
            echo "\">";
            echo $this->getAttribute((isset($context["prov"]) ? $context["prov"] : null), "name");
            echo "</option>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['prov'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "                    </select>
                    <select id=\"city-list\" class=\"medium-12 small-12 columns list-city-name\"></select>
                    <input type=\"checkbox\" id=\"type-branch\" value=\"branch\" checked hidden> ";
        // line 26
        echo "                    <input type=\"checkbox\" id=\"type-distributor\" value=\"distributor\" checked hidden> ";
        // line 27
        echo "                </div>
                <div class=\"medium-4 small-12 columns address\" id=\"city\" data-equalizer-watch=\"address-list\">
                <div class=\"small-12 columns list-address\">
                    <div class=\"columns title\">BRANCH</div>
                    <div class=\"columns list-branch\" id=\"branch\"></div>
                </div>
                </div>
                <div class=\"medium-4 end columns address\" id=\"city\" data-equalizer-watch=\"address-list\">
                    <div class=\"small-12 columns list-address\">
                        <div class=\"columns title\">DISTRIBUTOR</div>
                        <div class=\"columns list-distributor\" id=\"distributor\"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"show-for-small-only\">
            ";
        // line 43
        $this->displayBlock('back_to_top', $context, $blocks);
        // line 46
        echo "        </div>
    </div>
";
    }

    // line 43
    public function block_back_to_top($context, array $blocks = array())
    {
        // line 44
        echo "                ";
        $this->displayParentBlock("back_to_top", $context, $blocks);
        echo "
            ";
    }

    // line 49
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 50
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    ";
        // line 51
        echo Asset::js("css3-animate-it.js");
        echo "
    <script type=\"text/javascript\">
        var data_dist;
        
        function render_dist_city_list(idx) {
            var list_branch = \$('.list-branch');
            var list_distributor = \$('.list-distributor');
            list_branch.html('');
            list_distributor.html('');
            var _is_checked_branch = \$('#type-branch').prop('checked');
            var _is_checked_distributor = \$('#type-distributor').prop('checked');
            
            var counter = data_dist.length;
            var mark = 0;
            
            for(var n = 0; n < counter; n++ ){
                var check = data_dist[n].distributors;
                if(check){
                    console.log(data_dist[idx].distributors);
                    
                    for (var idy=0; idy<data_dist[idx].distributors.length; idy++) {
                        
                        var curr_dist_type = data_dist[n].distributors[idy].type.toLowerCase();
                        if((curr_dist_type == \"branch\" && _is_checked_branch)){
                            var branch_name = \$('<div />');
                            var data = data_dist[n].distributors[idy];
                            branch_name.addClass('columns branch-name');
                            branch_name.attr('data-dist-idx', idx);
                            branch_name.attr('data-dist-idy', idy);
                            branch_name.html('<b>'+data.name+'</b>'+'<hr/>'+data.address+'<br/>');
                            branch_name.appendTo(list_branch);
                        }

                        if((curr_dist_type == \"distributor\" && _is_checked_distributor)){
                            var distributor_name = \$('<div />');
                            var data = data_dist[n].distributors[idy];
                            distributor_name.addClass('columns distributor-name');
                            distributor_name.attr('data-dist-idx', idx);
                            distributor_name.attr('data-dist-idy', idy);
                            distributor_name.html('<b>'+data.name+'</b>'+'<hr/>'+data.address+'<br/>');
                            distributor_name.appendTo(list_distributor);
                        }

                    }

                    
                }else{ mark++; }
                
            }
            
            console.log(mark);
        }
        function check_distribution_city(idx) {
            var retval = false;
            var _is_checked_branch = \$('#type-branch').prop('checked');
            var _is_checked_distributor = \$('#type-distributor').prop('checked');
            for (var idy=0; idy<data_dist[idx].distributors.length; idy++) {
                var curr_dist_type = data_dist[idx].distributors[idy].type.toLowerCase();
                if((curr_dist_type == \"branch\" && _is_checked_branch) || (curr_dist_type == \"distributor\" && _is_checked_distributor)){
                    retval = true;
                }
            }
            return retval;
        }
        function render_dist_city() {
            var list_city = \$('#city-list');
            list_city.html('');
            \$('.dist-address-detail').html('');//\$('.list-branch').html('');
            for (var idx=0;idx<data_dist.length;idx++){
                if (typeof data_dist[idx].distributors !== 'undefined') {
                    if (check_distribution_city(idx)) {
                        var new_opt = \$('<option />');
                        new_opt.attr('value', idx).text(data_dist[idx].name);
                        list_city.append(new_opt);
                    }
                }
            }
            \$('#city-list').trigger('change');
            //new Foundation.Equalizer(\$('.address-list'));
        }
        \$('.distribution').on('change', '#province-list', function(){
            var id = parseFloat(\$(this).val());
            if (isNaN(id)) { id = 0; }
            \$.ajax({
                method: \"POST\",
                url: \"";
        // line 136
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/distribution/get_city\",
                data: {
                        id_province: id
                      }
            }).done(function( res ) {
                data_dist = \$.parseJSON(res);
                render_dist_city();
            });
        });
        \$('.distribution').on('change', '#city-list', function(){
            var idx = parseFloat(\$(this).val());
            if (isNaN(idx)) { idx = 0; }
            render_dist_city_list(idx);
        });
        \$('.distribution').on('change', '#type-branch', function(){
            render_dist_city();
        });
        \$('.distribution').on('change', '#type-distributor', function(){
            render_dist_city();
        });
        \$('.distribution').on('click', '.branch-name', function(){
            var idx = \$(this).data('dist-idx');
            var idy = \$(this).data('dist-idy');
            \$('a', '.branch-name').removeClass('is-active');
            \$('a', this).addClass('is-active');
            var data = data_dist[idx].distributors[idy];
            \$('.list-branch').html(data.address);//\$('.dist-address-detail').html(data.address);
            new Foundation.Equalizer(\$('.address-list'));
        });
        \$('.distribution').on('click', '.distributor-name', function(){
            var idx = \$(this).data('dist-idx');
            var idy = \$(this).data('dist-idy');
            \$('a', '.distributor-name').removeClass('is-active');
            \$('a', this).addClass('is-active');
            var data = data_dist[idx].distributors[idy];
            \$('.list-distributor').html(data.address);//\$('.dist-address-detail').html(data.address);
            //new Foundation.Equalizer(\$('.address-list'));
        });
        \$('#province-list').trigger('change');
        ";
        // line 175
        $this->displayBlock('back_to_top_js', $context, $blocks);
        // line 178
        echo "    </script>
";
    }

    // line 175
    public function block_back_to_top_js($context, array $blocks = array())
    {
        // line 176
        echo "        ";
        $this->displayParentBlock("back_to_top_js", $context, $blocks);
        echo "
        ";
    }

    public function getTemplateName()
    {
        return "distributor.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  278 => 176,  275 => 175,  270 => 178,  268 => 175,  226 => 136,  138 => 51,  133 => 50,  130 => 49,  123 => 44,  120 => 43,  114 => 46,  112 => 43,  94 => 27,  92 => 26,  88 => 23,  77 => 21,  73 => 20,  57 => 11,  53 => 9,  50 => 8,  44 => 6,  40 => 5,  35 => 4,  32 => 3,);
    }
}
