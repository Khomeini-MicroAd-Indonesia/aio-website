<?php

/* data_report_factorytour.twig */
class __TwigTemplate_b73e0f53eb3c2f04753088d77fedc811af1f5372363b347fe4d5aac3caee4a45 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("backend/template.twig");

        $this->blocks = array(
            'backend_css' => array($this, 'block_backend_css'),
            'backend_content_header' => array($this, 'block_backend_content_header'),
            'backend_content' => array($this, 'block_backend_content'),
            'backend_js' => array($this, 'block_backend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "backend/template.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_backend_css($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("backend_css", $context, $blocks);
        echo "
    <!-- dataTables css -->
    ";
        // line 6
        echo Asset::css("daterangepicker/daterangepicker-bs3.css");
        echo "
    <style>
        .report{
            padding-top: 50px;
        }
    </style>
";
    }

    // line 14
    public function block_backend_content_header($context, array $blocks = array())
    {
        // line 15
        echo "    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>";
        // line 18
        echo "            Data Factory Tour
            <small>Data Report</small>
        </h1>
        ";
        // line 22
        echo "        <ol class=\"breadcrumb\">
            <li><a href=\"";
        // line 23
        echo Uri::base();
        echo "backend\">Home</a></li>
            <li>Data Factory Tour</li>
            <li class=\"active\">Data Report</li>
        </ol>
    </section>
";
    }

    // line 30
    public function block_backend_content($context, array $blocks = array())
    {
        // line 31
        echo "
    ";
        // line 32
        if ((twig_length_filter($this->env, (isset($context["success_message"]) ? $context["success_message"] : null)) > 0)) {
            // line 33
            echo "        <div class=\"alert alert-success alert-dismissable\">
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
            ";
            // line 35
            echo (isset($context["success_message"]) ? $context["success_message"] : null);
            echo "
        </div>
    ";
        }
        // line 38
        echo "
    ";
        // line 39
        if ((twig_length_filter($this->env, (isset($context["error_message"]) ? $context["error_message"] : null)) > 0)) {
            // line 40
            echo "        <div class=\"alert alert-danger alert-dismissable\">
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
            ";
            // line 42
            echo (isset($context["error_message"]) ? $context["error_message"] : null);
            echo "
        </div>
    ";
        }
        // line 45
        echo "
    <form class=\"form-horizontal\" accept-charset=\"utf-8\" method=\"post\" action=\"\" role=\"form\" name=\"frm_data_report\" id=\"frm-data-report\">
        <div class=\"form-group\">
            <label for=\"data-from-to\" class=\"col-sm-2 control-label\">
                From - To
            </label>
            <div class=\"col-sm-10\">
                <div class=\"input-group\">
                    <div class=\"input-group-addon\">
                        <i class=\"fa fa-calendar\"></i>
                    </div>
                    <input type=\"text\" name=\"data_from_to\" id=\"data-from-to\" class=\"form-control pull-right\" required />
                </div>
            </div>
        </div>
        <div class=\"form-group\">
            <div class=\"col-sm-offset-2 col-sm-3\">
                <button type=\"button\" class=\"btn btn-primary\" id=\"get-registrant\">Get Data</button>
            </div>
\t\t</div>
    </form>
";
    }

    // line 68
    public function block_backend_js($context, array $blocks = array())
    {
        // line 69
        echo "    ";
        $this->displayParentBlock("backend_js", $context, $blocks);
        echo "
    <!-- DATA TABES SCRIPT -->
    ";
        // line 71
        echo Asset::js("plugins/daterangepicker/daterangepicker.js");
        echo "
    <!-- page script -->
    <script type=\"text/javascript\">
        \$(function() {
            \$('#data-from-to').daterangepicker();
            \$('#get-registrant').click(function(){
                \$('#frm-data-report').submit();
            });
        } );
    </script>
";
    }

    public function getTemplateName()
    {
        return "data_report_factorytour.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  143 => 71,  137 => 69,  134 => 68,  109 => 45,  103 => 42,  99 => 40,  97 => 39,  94 => 38,  88 => 35,  84 => 33,  82 => 32,  79 => 31,  76 => 30,  66 => 23,  63 => 22,  58 => 18,  54 => 15,  51 => 14,  40 => 6,  34 => 4,  31 => 3,);
    }
}
