<?php

/* template_faq.twig */
class __TwigTemplate_142ac554576c9c8d91e24a4a9e20931e155f324fe52b2ca1c709e6003da0f4e3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"row faq\">
    <div class=\"title custom-padd\">
        <h2>FAQ</h2>
    </div>
    <div class=\"content\">
        <div class=\"large-3 columns submenu\">
            <div class=\"pocari-sweat\" data-magellan>
                <div class=\"sub-title\"><a href=\"";
        // line 8
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/faq\">POCARI</a></div>
                <div class=\"list\"><a href=\"";
        // line 9
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/faq#General\">Umum</a></div>
                <div class=\"list\"><a href=\"";
        // line 10
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/faq#Health\">Kesehatan</a></div>
                <div class=\"list\"><a href=\"";
        // line 11
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/faq#Product\">Produk</a></div>
            </div>
            <div class=\"soyjoy\">
                <div class=\"sub-title\"><a href=\"";
        // line 14
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/faq-soyjoy\">SOYJOY</a></div>
                <div class=\"list\"><a href=\"";
        // line 15
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/faq-soyjoy#General\">Umum</a></div>
                <div class=\"list\"><a href=\"";
        // line 16
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/faq-soyjoy#Health\">Kesehatan</a></div>
                <div class=\"list\"><a href=\"";
        // line 17
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/faq-soyjoy#Product\">Produk</a></div>
            </div>
            <div class=\"ionesence\">
                <div class=\"sub-title\"><a href=\"";
        // line 20
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/faq-ionessence\">IONESSENCE</a></div>
                <div class=\"list\"><a href=\"";
        // line 21
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/faq-ionessence#General\">Umum</a></div>
                <div class=\"list\"><a href=\"";
        // line 22
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/faq-ionessence#Health\">Kesehatan</a></div>
                <div class=\"list\"><a href=\"";
        // line 23
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/faq-ionessence#Product\">Produk</a></div>
            </div>
        </div>";
    }

    public function getTemplateName()
    {
        return "template_faq.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 23,  82 => 22,  77 => 21,  72 => 20,  65 => 17,  60 => 16,  55 => 15,  50 => 14,  43 => 11,  38 => 10,  33 => 9,  28 => 8,  19 => 1,  148 => 30,  145 => 29,  140 => 32,  138 => 29,  132 => 27,  129 => 26,  125 => 24,  122 => 23,  115 => 21,  97 => 17,  90 => 15,  85 => 14,  68 => 13,  58 => 11,  54 => 10,  51 => 9,  48 => 8,  45 => 7,  39 => 5,  34 => 4,  31 => 3,);
    }
}
