<?php

/* gallery_detail.twig */
class __TwigTemplate_ec316a3614fe86235a70bfa67aa86a59953990e68707c035b06da12ecdaaa401 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_meta_twitter' => array($this, 'block_frontend_meta_twitter'),
            'frontend_meta_facebook' => array($this, 'block_frontend_meta_facebook'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        echo Asset::css("custom/animations.css");
        echo "
    ";
        // line 5
        $this->displayParentBlock("frontend_css", $context, $blocks);
        echo "
    ";
        // line 6
        echo Asset::css("custom/jquery.bxslider.css");
        echo "
    ";
        // line 7
        echo Asset::css("custom/gallery.css");
        echo "
";
    }

    // line 9
    public function block_frontend_meta_twitter($context, array $blocks = array())
    {
        // line 10
        echo "    <meta name=\"twitter:card\" content=\"summary\">
    <meta name=\"twitter:title\" content=\"Gallery Factory Tour Life At Otsuka\" />
    <meta name=\"twitter:description\" content=\"Bergabunglah dengan Otsuka dan bawa perubahan yang lebih baik untuk kesehatan di seluruh dunia.\" />
    <meta name=\"twitter:url\" content=\"http://athena.microad.co.id/aio.co.id/id/gallery-detail\" />
    <meta name=\"twitter:image\" content=\"";
        // line 14
        echo Uri::base();
        echo "/assets/img/gallery/banner-gallery.jpg\" />
";
    }

    // line 17
    public function block_frontend_meta_facebook($context, array $blocks = array())
    {
        // line 18
        echo "        <meta property=\"og:url\"           content=\"http://athena.microad.co.id/aio.co.id/id/gallery-detail\" />
        <meta property=\"og:type\"          content=\"website\" />
        <meta property=\"og:title\"         content=\"Gallery Factory Tour Life At Otsuka\" />
        <meta property=\"og:description\"   content=\"Bergabunglah dengan Otsuka dan bawa perubahan yang lebih baik untuk kesehatan di seluruh dunia.\" />
        <meta property=\"og:image\"         content=\"";
        // line 22
        echo Uri::base();
        echo "/assets/img/gallery/banner-gallery.jpg\" />
    ";
    }

    // line 24
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 25
        echo "    <div class=\"columns gallery-detail\">
        <div class=\"columns banner\">
            ";
        // line 27
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["banners"]) ? $context["banners"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["banner"]) {
            // line 28
            echo "            <div class=\"medium-7 small-12 columns animatedParent\">
                <div class=\"medium-9 medium-offset-3 columns animated fadeInLeft\">
                    <div class=\"columns banner-title\">
                        <p>";
            // line 31
            echo $this->getAttribute((isset($context["banner"]) ? $context["banner"] : null), "title");
            echo "</p>
                        <div style=\"width: 80%;border-bottom: 2px solid #bdbdbd\"></div>
                        <img class=\"icon-banner\" src=\"";
            // line 33
            echo Uri::base();
            echo "/assets/img/gallery/icon-scrolldown.png\">
                    </div>
                </div>
            </div>
            <div class=\"medium-5 small-12 columns\">
                <img src=\"";
            // line 38
            echo Uri::base();
            echo "/media/gallery/";
            echo $this->getAttribute((isset($context["banner"]) ? $context["banner"] : null), "banner");
            echo "\">
            </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['banner'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 41
        echo "        </div>
        <div class=\"columns testimonial\">
            <div class=\"row animatedParent\">
                <p class=\"title animated fadeInLeft\">Testimonial</p>
                <div style=\"width: 100px;border-bottom: 5px solid #000;margin-bottom: 30px\"></div>

                <div class=\"columns gallery-testimonial-slide\">
                    ";
        // line 48
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["testimonies"]) ? $context["testimonies"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["testimony"]) {
            // line 49
            echo "                    <div class=\"columns slide\">
                        <div class=\"columns gallery-content\">
                            <div class=\"small-3 columns gallery-pic\">
                                <img src=\"";
            // line 52
            echo Uri::base();
            echo "/media/gallery/";
            echo $this->getAttribute((isset($context["testimony"]) ? $context["testimony"] : null), "image");
            echo "\">
                            </div>
                            <div class=\"small-9 columns gallery-desc\">
                                <div class=\"title\">
                                    <p>";
            // line 56
            echo $this->getAttribute((isset($context["testimony"]) ? $context["testimony"] : null), "name");
            echo "</p>
                                </div>
                                <div class=\"from\">
                                    <p>";
            // line 59
            echo $this->getAttribute((isset($context["testimony"]) ? $context["testimony"] : null), "institution");
            echo "</p>
                                </div>
                                <div class=\"comment\">
                                    ";
            // line 62
            echo $this->getAttribute((isset($context["testimony"]) ? $context["testimony"] : null), "testimony");
            echo "
                                </div>
                            </div>
                        </div>
                    </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['testimony'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 68
        echo "                </div>
                <div style=\"text-align: center\">
                    <img style=\"width: 100%\" src=\"";
        // line 70
        echo Uri::base();
        echo "/assets/img/gallery/devider.jpg\">
                </div>
            </div>
        </div>
        <div class=\"columns testimonial gallery-group\">
            <div class=\"row animatedParent\">
                <p class=\"title animated fadeInUp\">Gallery</p>
                <div style=\"width: 100px;border-bottom: 5px solid #000;margin-bottom: 30px\"></div>

                <div class=\"columns gallery-from-slide\">
                    ";
        // line 80
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["albums"]) ? $context["albums"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["album"]) {
            // line 81
            echo "                    <div class=\"columns slide\">
                        <div class=\"columns gallery-content\">
                            ";
            // line 83
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["images"]) ? $context["images"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                // line 84
                echo "                                ";
                if (($this->getAttribute((isset($context["album"]) ? $context["album"] : null), "id") == $this->getAttribute((isset($context["image"]) ? $context["image"] : null), "id_album"))) {
                    // line 85
                    echo "                                <img src=\"";
                    echo Uri::base();
                    echo "/media/gallery/";
                    echo $this->getAttribute((isset($context["image"]) ? $context["image"] : null), "filename");
                    echo "\">
                                ";
                }
                // line 87
                echo "                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 88
            echo "                            <div class=\"title\">
                                <p>";
            // line 89
            echo $this->getAttribute((isset($context["album"]) ? $context["album"] : null), "name");
            echo "</p>
                            </div>
                        </div>
                    </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['album'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 94
        echo "                </div>
            </div>
        </div>
        <div class=\"columns gallery-view\">
            <div class=\"row\" data-equalizer=\"gallery-view-title\">
            ";
        // line 99
        $context["id_album"] = "";
        // line 100
        echo "            ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["albums"]) ? $context["albums"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["album"]) {
            // line 101
            echo "                ";
            if ($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "first")) {
                // line 102
                echo "                    ";
                $context["id_album"] = $this->getAttribute((isset($context["album"]) ? $context["album"] : null), "id");
                // line 103
                echo "                <div class=\"medium-6 columns animatedParent\" data-equalizer-watch=\"gallery-view-title\">
                    <div style=\"width: 110px; border-bottom: 1px solid #000;margin-top: 20px\"></div>
                    <div class=\"title animated fadeInUp\">
                        <p>";
                // line 106
                echo $this->getAttribute((isset($context["album"]) ? $context["album"] : null), "name");
                echo "</p>
                    </div>
                </div>
                ";
            }
            // line 110
            echo "            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['album'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 111
        echo "                <div class=\"medium-6 columns\" style=\"position: relative\" data-equalizer-watch=\"gallery-view-title\">
                    <div class=\"columns\" style=\"position: absolute;bottom: 40px;\">
                        <div class=\"medium-2 columns facebook-share\">
                            <div id=\"fb-root\"></div>
                            <script>(function(d, s, id) {
                                    var js, fjs = d.getElementsByTagName(s)[0];
                                    if (d.getElementById(id)) return;
                                    js = d.createElement(s); js.id = id;
                                    js.src = \"//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5\";
                                    fjs.parentNode.insertBefore(js, fjs);
                                }(document, 'script', 'facebook-jssdk'));</script>
                            <div class=\"fb-share-button\" data-href=\"http://athena.microad.co.id/aio.co.id/id/gallery-detail\" data-layout=\"button_count\"></div>
                        </div>
                        <div class=\"medium-2 end columns twitter-share\">
                            <a href=\"https://twitter.com/share\" class=\"twitter-share-button\" data-url=\"http://athena.microad.co.id/aio.co.id/id/gallery-detail\">Tweet</a>
                            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"columns gallery-view-detail\">
            <div class=\"row\">
                <div class=\"medium-8 small-12 columns display-gallery\">
                    <ul class=\"columns gallery-from-detail\">
                    ";
        // line 136
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["image_list"]) ? $context["image_list"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
            // line 137
            echo "                        ";
            if (($this->getAttribute((isset($context["image"]) ? $context["image"] : null), "id_album") == (isset($context["id_album"]) ? $context["id_album"] : null))) {
                // line 138
                echo "                        <li class=\"columns\"><img src=\"";
                echo Uri::base();
                echo "/media/gallery/";
                echo $this->getAttribute((isset($context["image"]) ? $context["image"] : null), "filename");
                echo "\" /></li>
                        ";
            }
            // line 140
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 141
        echo "                    </ul>
                </div>
                <div class=\"medium-4 small-6 end columns\">
                    <div class=\"columns\" id=\"bx-pager\">
                    ";
        // line 145
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["image_list"]) ? $context["image_list"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["image"]) {
            // line 146
            echo "                        ";
            if (($this->getAttribute((isset($context["image"]) ? $context["image"] : null), "id_album") == (isset($context["id_album"]) ? $context["id_album"] : null))) {
                // line 147
                echo "                        <a data-slide-index=\"0\" href=\"\"><img class=\"medium-6 small-6 columns\" src=\"";
                echo Uri::base();
                echo "/media/gallery/";
                echo $this->getAttribute((isset($context["image"]) ? $context["image"] : null), "filename");
                echo "\" /></a>
                        ";
            }
            // line 149
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['image'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 150
        echo "                    </div>
                </div>
            </div>
        </div>
        <div class=\"columns gallery-comment\">
            <div class=\"row\">
                <div class=\"columns\">
                    <div class=\"columns detail animatedParent\"  data-sequence='500'>
                        <div class=\"columns animated fadeInUp\" data-id='1'>
                            <div style=\"width: 155px;border-bottom: 1px solid #000000;margin-top: 60px;margin-bottom: 30px\"></div>
                            <p class=\"title\" style=\"margin-bottom: 30px\">COMMENTS</p>
                        </div>
                        <div class=\"columns\">
                        <div class=\"medium-5 columns textarea\">
                            <form role=\"form\" id=\"comment-form\" enctype=\"multipart/form-data\" action=\"\" name=\"comment-form\" method=\"post\" >
                                <div class=\"row\">
                                    <div class=\"medium-10 columns\">
                                        <input id=\"name\" name=\"name\" type=\"text\" placeholder=\"Nama Lengkap\">
                                        <textarea id=\"message\" name=\"message\" rows=\"8\" placeholder=\"Message\"></textarea>
                                        <button style=\"color: #2f92d9;font-size: 12px;font-weight: bold;float: right;padding: 10px 0px 10px 10px\">SEND</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class=\"medium-7 columns\">
                            ";
        // line 175
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["comments"]) ? $context["comments"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["comment"]) {
            // line 176
            echo "                            <div class=\"columns comment-each animated fadeInUp\" data-id='2'>
                                <div class=\"medium-12 columns comment-name\">
                                    <p>";
            // line 178
            echo $this->getAttribute((isset($context["comment"]) ? $context["comment"] : null), "name");
            echo " <span>| ";
            echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["comment"]) ? $context["comment"] : null), "created_at"), "d F, Y H:i");
            echo "</span></p>
                                    <div class=\"comment-detail\">
                                        <p>";
            // line 180
            echo $this->getAttribute((isset($context["comment"]) ? $context["comment"] : null), "comment");
            echo "</p>
                                    </div>
                                </div>
                            </div>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['comment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 185
        echo "                            <!--<div class=\"columns comment-each animated fadeInUp\" data-id='3'>
                                <div class=\"medium-2 medium-offset-3 columns comment-pic second\">
                                    <img src=\"";
        // line 187
        echo Uri::base();
        echo "/assets/img/gallery/comment-thumb-2.png\">
                                </div>
                                <div class=\"medium-7 columns comment-name\">
                                    <p>Tiara Ulfa <span>| 13 july 2015, 20:05 / reply</span></p>
                                    <div class=\"comment-detail\">
                                        <p>Lorem ipsum dolor sit amet, consectetur elit, sed do eiusmod tempor incididunt et dolore...</p>
                                    </div>
                                </div>
                            </div>-->
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
";
    }

    // line 204
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 205
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    ";
        // line 206
        echo Asset::js("css3-animate-it.js");
        echo "
    ";
        // line 207
        echo Asset::js("jquery.bxslider.min.js");
        echo "
    <script type=\"text/javascript\">
        \$(document).ready(function(){
            \$('.gallery-testimonial-slide').bxSlider({
                slideWidth: 400,
                minSlides: 2,
                maxSlides: 3,
                moveSlides: 1,
                slideMargin: 0,
                pager: false,
                infiniteLoop: false,
                hideControlOnEnd: true
            });
            \$('.gallery-from-slide').bxSlider({
                slideWidth: 400,
                minSlides: 2,
                maxSlides: 3,
                moveSlides: 1,
                slideMargin: 10,
                pager: false,
                infiniteLoop: false,
                hideControlOnEnd: true
            });
            \$('.gallery-from-detail').bxSlider({
                pagerCustom: '#bx-pager'
            });
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "gallery_detail.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  459 => 207,  455 => 206,  450 => 205,  447 => 204,  426 => 187,  422 => 185,  411 => 180,  404 => 178,  400 => 176,  396 => 175,  369 => 150,  363 => 149,  355 => 147,  352 => 146,  348 => 145,  342 => 141,  336 => 140,  328 => 138,  325 => 137,  321 => 136,  294 => 111,  280 => 110,  273 => 106,  268 => 103,  265 => 102,  262 => 101,  244 => 100,  242 => 99,  235 => 94,  224 => 89,  221 => 88,  215 => 87,  207 => 85,  204 => 84,  200 => 83,  196 => 81,  192 => 80,  179 => 70,  175 => 68,  163 => 62,  157 => 59,  151 => 56,  142 => 52,  137 => 49,  133 => 48,  124 => 41,  113 => 38,  105 => 33,  100 => 31,  95 => 28,  91 => 27,  87 => 25,  84 => 24,  78 => 22,  72 => 18,  69 => 17,  63 => 14,  57 => 10,  54 => 9,  48 => 7,  44 => 6,  40 => 5,  35 => 4,  32 => 3,);
    }
}
