<?php

/* template_press_room.twig */
class __TwigTemplate_ecdb6d611f91650d38ec4d9c8ae98f893bfc807362b35cb1b84227d9cd277f7f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'about_menu_us' => array($this, 'block_about_menu_us'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"press-room\">
    <div class=\"banner-press-room\">
        ";
        // line 3
        echo (isset($context["press_room_banner"]) ? $context["press_room_banner"] : null);
        echo "
        <div class=\"banner-desc animated fadeInUpShort\">
            <p> NEWS &amp;<br/>
                PRESS RELEASE</p>
            <hr/>
        </div>
    </div>
    <div class=\"padding-custom\"></div>
    <div class=\"press-cat\">
        <div class=\"row\" data-equalizer=\"press-menu\">
            <div class=\"small-4 columns sub-menu ";
        // line 13
        echo (isset($context["active_corporate"]) ? $context["active_corporate"] : null);
        echo "\" data-equalizer-watch=\"press-menu\">
                <div class=\"";
        // line 14
        $this->displayBlock('about_menu_us', $context, $blocks);
        echo "\"></div>
                <a href=\"";
        // line 15
        echo ((Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null)) . "/our-event");
        echo "\">CORPORATE NEWS</a>
                <div class=\"hr\"></div>
            </div>
            <div class=\"small-4 columns sub-menu ";
        // line 18
        echo (isset($context["active_brand"]) ? $context["active_brand"] : null);
        echo "\" data-equalizer-watch=\"press-menu\">
                <a href=\"";
        // line 19
        echo ((Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null)) . "/brand-news");
        echo "\">BRAND NEWS</a>
                <div class=\"hr\"></div>
            </div>
            <div class=\"small-4 columns sub-menu ";
        // line 22
        echo (isset($context["active_press"]) ? $context["active_press"] : null);
        echo "\" data-equalizer-watch=\"press-menu\">
                <a href=\"";
        // line 23
        echo ((Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null)) . "/press-release");
        echo "\">PRESS RELEASE</a>
                <div class=\"hr\"></div>
            </div>
            <div class=\"clear\"></div>
        </div>
    </div>
    <div class=\"padding-custom\"></div>
</div>";
    }

    // line 14
    public function block_about_menu_us($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "template_press_room.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 14,  61 => 22,  55 => 19,  51 => 18,  45 => 15,  37 => 13,  24 => 3,  20 => 1,  150 => 42,  147 => 41,  142 => 44,  140 => 41,  134 => 39,  131 => 38,  124 => 31,  121 => 30,  114 => 33,  112 => 30,  108 => 28,  95 => 23,  91 => 22,  85 => 21,  81 => 20,  73 => 17,  69 => 15,  65 => 23,  59 => 10,  56 => 9,  53 => 8,  47 => 7,  41 => 14,  36 => 4,  33 => 3,);
    }
}
