<?php

/* soyjoy.twig */
class __TwigTemplate_f7d7303dbe4e383de6b30b76afbb911cbc96a78d8e8597f478e95934d801c126 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        echo Asset::css("custom/animations.css");
        echo "
    ";
        // line 5
        $this->displayParentBlock("frontend_css", $context, $blocks);
        echo "
    ";
        // line 6
        echo Asset::css("custom/product.css");
        echo "
";
    }

    // line 8
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 9
        echo "    <div class=\"soyjoy columns\">
        <div class=\"columns sub-menu-product\" data-equalizer=\"menu-ionessense\">
            <div class=\"medium-2 medium-offset-3 small-4 columns\">
                <a href=\"";
        // line 12
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/brand/pocari-sweat\"  data-equalizer-watch=\"menu-ionessense\">
                <div class=\"columns pocari-sweat-color\">
                        <img src=\"";
        // line 14
        echo Uri::base();
        echo "assets/img/product/icon-brand-pocarisweat.png\">
                </div>
                </a>
            </div>
            <div class=\"medium-2 small-4 columns\">
                <a href=\"";
        // line 19
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/brand/soyjoy\" data-equalizer-watch=\"menu-ionessense\">
                    <div class=\"columns soyjoy-color\">
                            <img src=\"";
        // line 21
        echo Uri::base();
        echo "assets/img/product/icon-brand-soyjoy.png\">
                        <span class=\"active-menu\"></span>
                    </div>
                </a>
            </div>
            <div class=\"medium-2 small-4 end columns\">
                <a href=\"";
        // line 27
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/brand/ionessence\" data-equalizer-watch=\"menu-ionessense\">
                    <div class=\"columns ionesence-color\">
                            <img src=\"";
        // line 29
        echo Uri::base();
        echo "assets/img/product/icon-brand-ionessence.png\">
                    </div>
                </a>
            </div>
        </div>
        <div class=\"columns banner text-center\">
            <img data-interchange=\"[";
        // line 35
        echo Uri::base();
        echo "/assets/img/product/banner-soyjoy-small.png, small], [";
        echo Uri::base();
        echo "/assets/img/product/banner-soyjoy-medium.png, medium], [";
        echo Uri::base();
        echo "/assets/img/product/banner-soyjoy.png, large]\">
        </div>
        <div class=\"row\">
            <div class=\"columns product-detail\" data-equalizer=\"product-detail\">
                <div class=\"medium-6 columns\">
                    <div class=\"columns product-desc animatedParent\" data-equalizer-watch=\"product-detail\">
                        <p class=\"animated fadeInUp\"><b>SOYJOY</b> ";
        // line 41
        echo Lang::get("txt_soyjoy01");
        echo "</p>
                    </div>
                </div>
                <div class=\"medium-6 columns animatedParent\">
                ";
        // line 45
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["campaign_soyjoy"]) ? $context["campaign_soyjoy"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["detail"]) {
            // line 46
            echo "                    <div class=\"columns product-desc-img animated fadeInUp delay-250\">
                        <a href=\"";
            // line 47
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "link");
            echo "\" target=\"_blank\"><img src=\"";
            echo Uri::base();
            echo "media/campaign/";
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "image");
            echo "\" data-equalizer-watch=\"product-detail\"></a>
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['detail'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 50
        echo "                </div>
            </div>
            <div class=\"columns\">
                <div class=\"columns\">
                <div class=\"product-variant columns\">
                    <div class=\"medium-6 columns variant-display animatedParent\" data-sequence=\"200\">
                        <div class=\"columns\">
                            <div class=\"small-2 columns animated fadeInUpShort\" data-id=\"1\"><img src=\"";
        // line 57
        echo Uri::base();
        echo "/assets/img/product/variant/soyjoy-variant-stroberry.png\"></div>
                            <div class=\"small-2 columns animated fadeInUpShort\" data-id=\"2\"><img src=\"";
        // line 58
        echo Uri::base();
        echo "/assets/img/product/variant/soyjoy-variant-peanut.png\"></div>
                            <div class=\"small-2 columns animated fadeInUpShort\" data-id=\"3\"><img src=\"";
        // line 59
        echo Uri::base();
        echo "/assets/img/product/variant/soyjoy-variant-raisin-almond.png\"> </div>
                            <div class=\"small-2 columns animated fadeInUpShort\" data-id=\"4\"><img src=\"";
        // line 60
        echo Uri::base();
        echo "/assets/img/product/variant/soyjoy-variant-banana.png\"> </div>
                            <div class=\"small-2 columns animated fadeInUpShort\" data-id=\"5\"><img src=\"";
        // line 61
        echo Uri::base();
        echo "/assets/img/product/variant/soyjoy-variant-hawthorn-berry.png\"> </div>
                            <div class=\"small-2 columns animated fadeInUpShort\" data-id=\"6\"><img src=\"";
        // line 62
        echo Uri::base();
        echo "/assets/img/product/variant/soyjoy-almond-and-chocolate.png\"> </div>
                        </div>
                    </div>
                    <div class=\"medium-6 columns\">
                        <div class=\"columns\">
                            <p class=\"variant-title\">";
        // line 67
        echo Lang::get("txt_soyjoy02");
        echo "</p>
                            <div class=\"line\"></div>
                            <ul class=\"variant-name medium-6 end columns animatedParent\" data-sequence=\"100\">
                                <li class=\"animated fadeInUp\" data-id=\"1\">Strawberry</li>
                                <li class=\"animated fadeInUp\" data-id=\"2\">Peanut</li>
                                <li class=\"animated fadeInUp\" data-id=\"3\">Raisin Almond</li>
                                <li class=\"animated fadeInUp\" data-id=\"4\">Banana</li>
                                <li class=\"animated fadeInUp\" data-id=\"5\">Hawthorn Berry</li>
                                <li class=\"animated fadeInUp\" data-id=\"6\">Almond & Chocolate</li>
                            </ul>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <div class=\"columns\">
                <div class=\"columns\">
                    <div class=\"columns product-link\">
                        <div class=\"product-button\">
                            <a href=\"http://www.soyjoy.co.id/home\" target=\"_blank\" class=\"button\">GO TO <br/>PRODUCT WEBSITE</a>
                            <div style=\"border-bottom: 4px solid #ffffff ; width: 60px\"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
";
    }

    // line 95
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 96
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    ";
        // line 97
        echo Asset::js("css3-animate-it.js");
        echo "
";
    }

    public function getTemplateName()
    {
        return "soyjoy.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  217 => 97,  212 => 96,  209 => 95,  177 => 67,  169 => 62,  165 => 61,  161 => 60,  157 => 59,  153 => 58,  149 => 57,  140 => 50,  127 => 47,  124 => 46,  120 => 45,  113 => 41,  100 => 35,  91 => 29,  85 => 27,  76 => 21,  70 => 19,  62 => 14,  56 => 12,  51 => 9,  48 => 8,  42 => 6,  38 => 5,  33 => 4,  30 => 3,);
    }
}
