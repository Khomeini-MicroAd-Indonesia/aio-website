<?php

/* history.twig */
class __TwigTemplate_882a9c53f17dc5b65c76d05fa2d474db0abb7d5a3d3c4e9c00283ca2df250365 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'menu_about_awards' => array($this, 'block_menu_about_awards'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'back_to_top' => array($this, 'block_back_to_top'),
            'frontend_js' => array($this, 'block_frontend_js'),
            'back_to_top_js' => array($this, 'block_back_to_top_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        echo Asset::css("custom/animations.css");
        echo "
    ";
        // line 5
        $this->displayParentBlock("frontend_css", $context, $blocks);
        echo "
    ";
        // line 6
        echo Asset::css("custom/about.css");
        echo "
";
    }

    // line 8
    public function block_menu_about_awards($context, array $blocks = array())
    {
        echo "active";
    }

    // line 9
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 10
        echo "    ";
        $this->env->loadTemplate("template_about_us.twig")->display($context);
        // line 11
        echo "    <div class=\"history\">
        <div class=\"row\">
            <div class=\"main-title\">
                <p>";
        // line 14
        echo Lang::get("txt_aboutus07");
        echo "</p>
                <hr/>
            </div>

            <ul class=\"history-list animatedParent animateOnce\" data-sequence='500'>
                ";
        // line 19
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["history_detail"]) ? $context["history_detail"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["history"]) {
            // line 20
            echo "                    ";
            $context["additional_class"] = "";
            // line 21
            echo "                    ";
            if ((($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "length") % 2) == 0)) {
                // line 22
                echo "                        ";
                if (((($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index") + 1) == $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "length")) || $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "last"))) {
                    // line 23
                    echo "                            ";
                    $context["additional_class"] = "end";
                    // line 24
                    echo "                        ";
                }
                // line 25
                echo "                    ";
            } else {
                // line 26
                echo "                        ";
                if ($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "last")) {
                    // line 27
                    echo "                            ";
                    $context["additional_class"] = "end";
                    // line 28
                    echo "                        ";
                }
                // line 29
                echo "                    ";
            }
            // line 30
            echo "                    <li class=\"medium-6 small-12 columns animated fadeInUp ";
            echo (isset($context["additional_class"]) ? $context["additional_class"] : null);
            echo "\" data-id=\"";
            echo $this->getAttribute((isset($context["history"]) ? $context["history"] : null), "seq");
            echo "\">
                        <div class=\"medium-3 small-12 columns history-year\"><p style=\"color: #1a8c8d\">";
            // line 31
            echo $this->getAttribute((isset($context["history"]) ? $context["history"] : null), "year");
            echo "<br><span>";
            echo $this->getAttribute((isset($context["history"]) ? $context["history"] : null), "month");
            echo "</span></p></div>
                        <div class=\"medium-9 small-12 columns history-content\">
                            <div class=\"small-5 columns\">
                                <img src=\"";
            // line 34
            echo Uri::base();
            echo "media/history/";
            echo $this->getAttribute((isset($context["history"]) ? $context["history"] : null), "image");
            echo "\">
                            </div>
                            <div class=\"small-7 columns\">
                                <div class=\"title\">
                                    <p>";
            // line 38
            echo $this->getAttribute((isset($context["history"]) ? $context["history"] : null), "title");
            echo "</p>
                                </div>
                                <p>";
            // line 40
            echo $this->getAttribute((isset($context["history"]) ? $context["history"] : null), "description");
            echo "</p>
                            </div>
                        </div>
                    </li>
                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['history'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "            </ul>
        </div>
        ";
        // line 47
        $this->displayBlock('back_to_top', $context, $blocks);
        // line 50
        echo "    </div>
";
    }

    // line 47
    public function block_back_to_top($context, array $blocks = array())
    {
        // line 48
        echo "            ";
        $this->displayParentBlock("back_to_top", $context, $blocks);
        echo "
        ";
    }

    // line 53
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 54
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    ";
        // line 55
        echo Asset::js("css3-animate-it.js");
        echo "
    <script type=\"text/javascript\">
        ";
        // line 57
        $this->displayBlock('back_to_top_js', $context, $blocks);
        // line 60
        echo "    </script>
";
    }

    // line 57
    public function block_back_to_top_js($context, array $blocks = array())
    {
        // line 58
        echo "            ";
        $this->displayParentBlock("back_to_top_js", $context, $blocks);
        echo "
        ";
    }

    public function getTemplateName()
    {
        return "history.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  215 => 58,  212 => 57,  207 => 60,  205 => 57,  200 => 55,  195 => 54,  192 => 53,  185 => 48,  182 => 47,  177 => 50,  175 => 47,  171 => 45,  152 => 40,  147 => 38,  138 => 34,  130 => 31,  123 => 30,  120 => 29,  117 => 28,  114 => 27,  111 => 26,  108 => 25,  105 => 24,  102 => 23,  99 => 22,  96 => 21,  93 => 20,  76 => 19,  68 => 14,  63 => 11,  60 => 10,  57 => 9,  51 => 8,  45 => 6,  41 => 5,  36 => 4,  33 => 3,);
    }
}
