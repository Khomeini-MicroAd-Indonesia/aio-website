<?php

/* lifeatotsuka.twig */
class __TwigTemplate_880d7b2867c3ed7bb484945aef9544bae6403b5626b9a0ab71f61dbc28b245c3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        echo Asset::css("custom/animations.css");
        echo "
    ";
        // line 5
        $this->displayParentBlock("frontend_css", $context, $blocks);
        echo "
    <link href=\"";
        // line 6
        echo Uri::base();
        echo "assets/css/custom/lifeatotsuka.css\" rel=\"stylesheet\">
";
    }

    // line 9
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 10
        echo "    <div class=\"lifeatotsuka animatedParent\">
        <div class=\"banner-lifeatotsuka\">
            <div class=\"title-page animated fadeInUp\">
                <div>
                    LIFE<br/>
                    AT OTSUKA
                    <div class=\"line-desc\" style=\"\"></div>
                </div>
            </div>
            ";
        // line 19
        echo (isset($context["lao_banner"]) ? $context["lao_banner"] : null);
        echo "
        </div>
        <div class=\"row\">
            <div class=\"lifeatotsuka-desc\">
                <div class=\"large-12 medium-12 small-12 columns\" >
                    <div class=\"top-border-desc\"></div>
                    <div class=\"title\">
                        ";
        // line 26
        echo (isset($context["page_content"]) ? $context["page_content"] : null);
        echo "
                    </div>
                    <img class=\"line-colour\" src=\"";
        // line 28
        echo Uri::base();
        echo "/assets/img/lifeatotsuka/line-lifeatotsuka.jpg\">
                </div>
            </div>
        </div>
        <br><br>

        <div id=\"wrapper-story-list\">
        ";
        // line 35
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["stories"]) ? $context["stories"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["story_item"]) {
            // line 36
            echo "        <div class=\"row line-hr\" ";
            echo (($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "first")) ? ("id=\"story-clone-base\"") : (""));
            echo ">
            <div class=\"lifeatotsuka-testimony-list\">
                <div class=\"large-12 medium-12 small-12 columns\">
                    <div class=\"large-3  medium-3  small-12 columns\" style=\"padding-right: 0\">
                        <div class=\"wrapper-img-testimony\">
                            <div class=\"img-testimony\">
                                <img class=\"img-testimony-lifeatotsuka\" src=\"";
            // line 42
            echo (Uri::base() . $this->getAttribute((isset($context["story_item"]) ? $context["story_item"] : null), "get_photo_path", array(), "method"));
            echo "/";
            echo $this->getAttribute((isset($context["story_item"]) ? $context["story_item"] : null), "photo");
            echo "\" alt=\"";
            echo $this->getAttribute((isset($context["story_item"]) ? $context["story_item"] : null), "name");
            echo "\">
                            </div>
                            <div class=\"text-testimony\">
                                <p class=\"name-testimony\">
                                    ";
            // line 46
            echo $this->getAttribute((isset($context["story_item"]) ? $context["story_item"] : null), "name");
            echo ", ";
            echo $this->getAttribute((isset($context["story_item"]) ? $context["story_item"] : null), "age");
            echo " Tahun
                                </p>
                                <p class=\"address-testimony\">
                                    ";
            // line 49
            echo $this->getAttribute((isset($context["story_item"]) ? $context["story_item"] : null), "job");
            echo ", ";
            echo $this->getAttribute((isset($context["story_item"]) ? $context["story_item"] : null), "location");
            echo "
                                </p>
                            </div>
                        </div>
                    </div>
                    ";
            // line 54
            if (((isset($context["current_lang"]) ? $context["current_lang"] : null) == "en")) {
                // line 55
                echo "                    <div class=\"large-9 medium-9  small-12 columns lifeatotsuka-wrapper-testimony\">
                        <div class=\"large-4  medium-5  small-12 columns\">
                            <p class=\"lead-content\">";
                // line 57
                echo $this->getAttribute((isset($context["story_item"]) ? $context["story_item"] : null), "story_lead_en");
                echo "</p>
                        </div>
                        <div class=\"large-8  medium-7  small-12 columns\">
                            <div class=\"lao-content\">";
                // line 60
                echo $this->getAttribute((isset($context["story_item"]) ? $context["story_item"] : null), "story_en");
                echo "</div>
                            <p class=\"hour\"></p>
                        </div>
                    </div>
                    ";
            }
            // line 65
            echo "                    ";
            if (((isset($context["current_lang"]) ? $context["current_lang"] : null) == "id")) {
                // line 66
                echo "                    <div class=\"large-9 medium-9  small-12 columns lifeatotsuka-wrapper-testimony\">
                        <div class=\"large-4  medium-5  small-12 columns\">
                            <p class=\"lead-content\">";
                // line 68
                echo $this->getAttribute((isset($context["story_item"]) ? $context["story_item"] : null), "story_lead_id");
                echo "</p>
                        </div>
                        <div class=\"large-8  medium-7  small-12 columns\">
                            <div class=\"lao-content\">";
                // line 71
                echo $this->getAttribute((isset($context["story_item"]) ? $context["story_item"] : null), "story_id");
                echo "</div>
                            <p class=\"hour\"></p>
                        </div>
                    </div>    
                    ";
            }
            // line 76
            echo "                </div>
            </div>
        </div>
        <hr/>
        <br>
        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['story_item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 82
        echo "        </div>

        <div class=\"row\">
            <div id=\"btn-loadmore\" class=\"lifeatotsuka-box-blue\" style=\"cursor: pointer\">
                <div class=\"large-12 medium-12 small-12 columns colour-lifeatotsuka-load\">
                    <p class=\"title lifeatotsuka-load\">
                        LOAD
                        <br>
                        MORE
                        <br>
                        STORIES
                        </p></a>
                </div>
            </div>
        </div>
        <br>
    </div>
";
    }

    // line 100
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 101
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    ";
        // line 102
        echo Asset::js("css3-animate-it.js");
        echo "
    <script type=\"text/javascript\">
        var curr_page = 1;
        var total_page = ";
        // line 105
        echo (isset($context["story_page"]) ? $context["story_page"] : null);
        echo ";

        \$(\"#btn-loadmore\").click(function(){
            loadmore();
        });

        function loadmore()
        {
            \$.ajax({
                type: 'post',
                url: \"";
        // line 115
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/lifeatotsuka/list\",
                data: {
                    next_offset: curr_page
                },
                success: function (response) {
                    var data = \$.parseJSON(response);
                    // render story HTML
                    for (idx=0; idx<data.length; idx++) {
                        var new_obj = \$('#story-clone-base').clone();
                        new_obj.removeAttr('id');
                        var new_obj_img = new_obj.find('img.img-testimony-lifeatotsuka');
                        var new_obj_name = new_obj.find('p.name-testimony');
                        var new_obj_addr = new_obj.find('p.address-testimony');
                        var new_obj_lead = new_obj.find('p.lead-content');
                        var new_obj_content = new_obj.find('div.lao-content');
                        new_obj.attr('src', '";
        // line 130
        echo Uri::base();
        echo "media/...').attr('alt', data[idx].name);
                        new_obj_name.html(data[idx].name+', '+data[idx].age+' Tahun');
                        new_obj_addr.html(data[idx].job+', '+data[idx].location);
                        new_obj_lead.html(data[idx].story_lead);
                        new_obj_content.html(data[idx].story);
                        \$('#wrapper-story-list').append(new_obj);
                    }
                    curr_page++;
                    if (curr_page >= total_page) {
                        \$('#btn-loadmore').remove();
                    }
                }
            });
        }
    </script>
";
    }

    public function getTemplateName()
    {
        return "lifeatotsuka.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  269 => 130,  251 => 115,  238 => 105,  232 => 102,  227 => 101,  224 => 100,  203 => 82,  184 => 76,  176 => 71,  170 => 68,  166 => 66,  163 => 65,  155 => 60,  149 => 57,  145 => 55,  143 => 54,  133 => 49,  125 => 46,  114 => 42,  104 => 36,  87 => 35,  77 => 28,  72 => 26,  62 => 19,  51 => 10,  48 => 9,  42 => 6,  38 => 5,  33 => 4,  30 => 3,);
    }
}
