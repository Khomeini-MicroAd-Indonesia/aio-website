<?php

/* career.twig */
class __TwigTemplate_fac52aeb413f36c0eabd67b6ecd2a965296c5af3587e91cb0f171bab7453358a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_meta_twitter' => array($this, 'block_frontend_meta_twitter'),
            'frontend_meta_facebook' => array($this, 'block_frontend_meta_facebook'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'back_to_top' => array($this, 'block_back_to_top'),
            'frontend_js' => array($this, 'block_frontend_js'),
            'back_to_top_js' => array($this, 'block_back_to_top_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        echo Asset::css("custom/animations.css");
        echo "
    ";
        // line 5
        $this->displayParentBlock("frontend_css", $context, $blocks);
        echo "
    ";
        // line 6
        echo Asset::css("custom/tinyscrollbar.css");
        echo "
    ";
        // line 7
        echo Asset::css("custom/career.css");
        echo "
";
    }

    // line 9
    public function block_frontend_meta_twitter($context, array $blocks = array())
    {
        // line 10
        echo "       <meta name=\"twitter:card\" content=\"summary\">
       <meta property=\"og:title\" content=\"Life At Otsuka\" />
       <meta property=\"og:description\" content=\"Bergabunglah dengan Otsuka dan bawa perubahan yang lebih baik untuk kesehatan di seluruh dunia.\" />
       <meta property=\"og:url\" content=\"http://athena.microad.co.id/aio.co.id/id/career\" />
       <meta property=\"og:image\" content=\"";
        // line 14
        echo Uri::base();
        echo "assets/img/career/otsuka_career_02.jpg\" />
   ";
    }

    // line 17
    public function block_frontend_meta_facebook($context, array $blocks = array())
    {
        // line 18
        echo "        <meta property=\"og:url\"           content=\"http://athena.microad.co.id/aio.co.id/id/career\" />
        <meta property=\"og:type\"          content=\"website\" />
        <meta property=\"og:title\"         content=\"Life At Otsuka\" />
        <meta property=\"og:description\"   content=\"Bergabunglah dengan Otsuka dan bawa perubahan yang lebih baik untuk kesehatan di seluruh dunia.\" />
        <meta property=\"og:image\"         content=\"";
        // line 22
        echo Uri::base();
        echo "assets/img/career/otsuka_career_02.jpg\" />
    ";
    }

    // line 24
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 25
        echo "    <div class=\"career\">
        <div class=\"small-12 columns banner\">
            ";
        // line 27
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["career_content"]) ? $context["career_content"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["career"]) {
            // line 28
            echo "                <a href=\"";
            echo Uri::base();
            echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
            echo "/lifeatotsuka\"><img src=\"";
            echo Uri::base();
            echo "media/career/";
            echo $this->getAttribute((isset($context["career"]) ? $context["career"] : null), "image");
            echo "\"></a>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['career'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "        </div>
        <div class=\"row\">
            <div class=\"columns socmed\">
                <div class=\"large-2 small-6 small-offset-3 large-centered columns\">
                    <div class=\"small-6 columns\">
                        <div id=\"fb-root\"></div>
                        <script>(function(d, s, id) {
                                var js, fjs = d.getElementsByTagName(s)[0];
                                if (d.getElementById(id)) return;
                                js = d.createElement(s); js.id = id;
                                js.src = \"//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5\";
                                fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));</script>
                        <div class=\"fb-share-button\" data-href=\"http://athena.microad.co.id/aio.co.id/id/career\" data-layout=\"button_count\"></div>
                    </div>
                    <div class=\"small-6 columns\">
                        <a href=\"https://twitter.com/share\" class=\"twitter-share-button\" data-dnt=\"true\">Tweet</a>
                        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                    </div>
                </div>
                <hr class=\"large-11 large-centered columns\"/>
            </div>
            <div class=\"columns career-description\">
                <div class=\"large-6 columns\">
                    <div class=\"large-12 columns\">
                        <div class=\"list-desc\">
                            <div class=\"title\"><p>";
        // line 56
        echo Lang::get("txt_career01");
        echo "</p></div>
                            <div class=\"detail\"><p>";
        // line 57
        echo Lang::get("txt_career02");
        echo "</p></div>
                        </div>
                        <div class=\"list-desc\">
                            <div class=\"title\"><p>";
        // line 60
        echo Lang::get("txt_career03");
        echo "</p></div>
                            <div class=\"detail\"><p>";
        // line 61
        echo Lang::get("txt_career04");
        echo "</p></div>
                        </div>
                    </div>
                </div>
                <div class=\"large-6 columns\">
                    <div class=\"large-12 columns\">
                        <div class=\"list-desc\">
                            <div class=\"title\"><p>";
        // line 68
        echo Lang::get("txt_career05");
        echo "</p></div>
                            <div class=\"detail\"><p>";
        // line 69
        echo Lang::get("txt_career06");
        echo "</p></div>
                        </div>
                        <div class=\"list-desc\">
                            <div class=\"title\"><p>";
        // line 72
        echo Lang::get("txt_career07");
        echo "</p></div>
                            <div class=\"detail\"><p>";
        // line 73
        echo Lang::get("txt_career08");
        echo "</p></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"columns vacancy-and-apply animatedParent\">
            <div class=\"row\">
                <div class=\"columns\">
                    <div class=\"vertical menu\" data-magellan>
                        <div class=\"large-6 medium-6 small-6 columns text-center left\">
                            <a href=\"#list-of-vacancy\"><p>VACANCY</p><div class=\"icon-bottom\"></div></a>
                        </div>
                        <div class=\"large-6 medium-6 small-6 columns text-center right\">
                            <a href=\"#applying-form\"><p>APPLYING FORM</p><div class=\"icon-bottom\"></div></a>
                        </div>
                    </div>
                </div>
            </div>
            <div style=\"height: 35px;\"></div>
            <div class=\"banner animated fadeInUp\">
                <img src=\"";
        // line 94
        echo Uri::base();
        echo "/assets/img/career/banner-career-vacancy.jpg\">
            </div>
            <div class=\"row\">
                <hr/>
                <div style=\"height: 40px\"></div>
                <div class=\"sections\">
                    <section id=\"list-of-vacancy\" data-magellan-target=\"list-of-vacancy\">
                        <div class=\"columns list-of-vacancy\">
                            <div class=\"large-3 columns left animatedParent\">
                                <hr class=\"large-4 end columns\" style=\"margin: 0 auto 10px;border-color: #000!important;\"/>
                                <div class=\"columns title animated fadeInUp\">
                                    <p>LIST OF <br/><span>VACANCY</span></p>
                                </div>
                            </div>
                            <div class=\"large-9 columns right\">
                                <div class=\"desc\"><p>";
        // line 109
        echo Lang::get("txt_career09");
        echo "</p></div>
                                <div class=\"desc-2\"><p>";
        // line 110
        echo Lang::get("txt_career10");
        echo "</p></div>
                                <div class=\"large-12 columns\">
                                    <div id=\"scrollbar1\" class=\"hide-for-small-only\">
                                        <div class=\"scrollbar\"><div class=\"track\"><div class=\"thumb\"><div class=\"end\"></div></div></div></div>
                                        <div class=\"viewport\">
                                            <div class=\"large-10 medium-10 small-10 columns overview\">
                                                <ul class=\"accordion list-of-vacancy\" data-accordion data-options=\"allowAllClosed: true\">
                                                    ";
        // line 117
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["career_detail"]) ? $context["career_detail"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["career"]) {
            // line 118
            echo "                                                        <li class=\"accordion-item ";
            echo $this->getAttribute((isset($context["career"]) ? $context["career"] : null), "id");
            echo "\" data-accordion-item>
                                                            <a class=\"accordion-title\">
                                                                <div class=\"title-vacancy\">
                                                                    <p>";
            // line 121
            echo $this->getAttribute((isset($context["career"]) ? $context["career"] : null), "name");
            echo " - <span>";
            echo $this->getAttribute((isset($context["career"]) ? $context["career"] : null), "level");
            echo ".</span></p>
                                                                </div>
                                                                <div class=\"sub-title-vacancy\">
                                                                    <p>";
            // line 124
            echo Lang::get("txt_career11");
            echo ": ";
            echo $this->getAttribute((isset($context["career"]) ? $context["career"] : null), "location");
            echo " - <span>";
            echo Lang::get("txt_career12");
            echo ": ";
            echo $this->getAttribute((isset($context["career"]) ? $context["career"] : null), "date");
            echo "</span></p>
                                                                </div>
                                                            </a>
                                                            <div class=\"accordion-content\" data-tab-content>
                                                                <div class=\"coumns\">
                                                                    ";
            // line 129
            echo $this->getAttribute((isset($context["career"]) ? $context["career"] : null), "req");
            echo "
                                                                </div>
                                                                <div class=\"columns\" style=\"text-align: right\">
                                                                    <a href=\"#applying-form\" class=\"btn-apply\" data-job-position=\"";
            // line 132
            echo $this->getAttribute((isset($context["career"]) ? $context["career"] : null), "name");
            echo "\">APPLY</a>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['career'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 137
        echo "                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div id=\"scrollbar2\" class=\"show-for-small-only\">
                                        <div class=\"large-10 medium-10 small-10 columns overview\">
                                            <ul class=\"accordion list-of-vacancy\" data-accordion data-options=\"allowAllClosed: true\">
                                                ";
        // line 144
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["career_detail"]) ? $context["career_detail"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["career"]) {
            // line 145
            echo "                                                    <li class=\"accordion-item ";
            echo $this->getAttribute((isset($context["career"]) ? $context["career"] : null), "id");
            echo "\" data-accordion-item>
                                                        <a class=\"accordion-title\">
                                                            <div class=\"title-vacancy\">
                                                                <p>";
            // line 148
            echo $this->getAttribute((isset($context["career"]) ? $context["career"] : null), "name");
            echo " - <span>";
            echo $this->getAttribute((isset($context["career"]) ? $context["career"] : null), "level");
            echo ".</span></p>
                                                            </div>
                                                            <div class=\"sub-title-vacancy\">
                                                                <p>";
            // line 151
            echo Lang::get("txt_career11");
            echo ": ";
            echo $this->getAttribute((isset($context["career"]) ? $context["career"] : null), "location");
            echo " - <span>";
            echo Lang::get("txt_career12");
            echo ": ";
            echo $this->getAttribute((isset($context["career"]) ? $context["career"] : null), "date");
            echo "</span></p>
                                                            </div>
                                                        </a>
                                                        <div class=\"accordion-content\" data-tab-content>
                                                            <div class=\"coumns\">
                                                            ";
            // line 156
            echo $this->getAttribute((isset($context["career"]) ? $context["career"] : null), "req");
            echo "
                                                            </div>
                                                            <div class=\"columns\" style=\"text-align: right\">
                                                                <a href=\"#applying-form\" class=\"btn-apply\" data-job-position=\"";
            // line 159
            echo $this->getAttribute((isset($context["career"]) ? $context["career"] : null), "name");
            echo "\">APPLY</a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['career'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 164
        echo "                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <div style=\"clear:both;height: 70px;\"></div>
                    <hr style=\"border-width: 0px 0px 2px;\"/>
                    <div style=\"clear:both;height: 70px;\"></div>
                    <section id=\"applying-form\" data-magellan-target=\"applying-form\">
                        <div class=\"columns applying-form\">
                            <div class=\"large-3 columns left animatedParent\">
                                <div class=\"columns title animated fadeInUp\">
                                    <p>APPLYING <br/><span>FORM</span></p>
                                </div>
                                <hr class=\"large-4 end columns\" style=\"margin: 0 auto 10px;border-color: #000!important;\"/>
                            </div>
                            <div class=\"large-9 small-12 columns right\"style=\"background-color: #1c8db2\">

                                ";
        // line 184
        if (((isset($context["flag"]) ? $context["flag"] : null) == true)) {
            // line 185
            echo "                                    ";
            $this->env->loadTemplate("popup.twig")->display($context);
            // line 186
            echo "                                ";
        } else {
            // line 187
            echo "                                <form role=\"form\" id=\"applying-form\" enctype=\"multipart/form-data\" action=\"\" name=\"applying-form\" method=\"POST\" class=\"large-12 columns\">
                                    <div class=\"large-10 small-12 end columns\">
                                        <div class=\"fill columns\">
                                            <div class=\"large-2 large-offset-1 medium-2 medium-offset-1 small-3 columns\">
                                                <label>Posisi</label>
                                            </div>
                                            <div class=\"large-1 medium-1 small-1 columns\"><label>:</label></div>
                                            <div class=\"large-8 medium-8 end small-8 columns\">
                                                <select name=\"application_position\" id=\"job-title\" class=\"large-12 columns\">
                                                ";
            // line 196
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["career_detail"]) ? $context["career_detail"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["career"]) {
                // line 197
                echo "                                                    <option value=\"";
                echo $this->getAttribute((isset($context["career"]) ? $context["career"] : null), "name");
                echo "\">";
                echo $this->getAttribute((isset($context["career"]) ? $context["career"] : null), "name");
                echo "</option>
                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['career'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 199
            echo "                                                </select>
                                            </div>
                                        </div>
                                        <div class=\"fill columns\">
                                            <div class=\"large-2 large-offset-1 medium-2 medium-offset-1 small-3 columns\">
                                                <label>Nama Lengkap</label>
                                            </div>
                                            <div class=\"large-1 medium-1 small-1 columns\"><label>:</label></div>
                                            <div class=\"large-8 medium-8 end small-8 columns\">
                                                <input name=\"application_name\" class=\"large-12 columns\" required>
                                            </div>
                                        </div>
                                        <div class=\"fill columns\">
                                            <div class=\"large-2 large-offset-1 medium-2 medium-offset-1 small-3 columns\">
                                                <label>Tempat Lahir</label>
                                            </div>
                                            <div class=\"large-1 medium-1 small-1 columns\"><label>:</label></div>
                                            <div class=\"large-8 medium-8 end small-8 columns\">
                                                <input name=\"application_place\" class=\"large-12 columns\" required>
                                            </div>
                                        </div>
                                        <div class=\"fill columns\">
                                            <div class=\"large-2 large-offset-1 medium-2 medium-offset-1 small-3 columns\">
                                                <label>Tanggal Lahir</label>
                                            </div>
                                            <div class=\"large-1 medium-1 small-1 columns\"><label>:</label></div>
                                            <div class=\"large-8 medium-8 small-8 end columns\">
                                                <div class=\"large-12 columns\">
                                                    <input name=\"application_date\" class=\"large-12 columns\" placeholder=\"dd/mm/yyyy\" required/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class=\"fill columns\">
                                            <div class=\"large-2 large-offset-1 medium-2 medium-offset-1 small-3 columns\">
                                                <label>Jenis Kelamin</label>
                                            </div>
                                            <div class=\"large-1 medium-1 small-1 columns\"><label>:</label></div>
                                            <div class=\"large-8 medium-8 small-8 end columns\">
                                                <fieldset class=\"large-12 columns\">
                                                    <input name=\"application_sex\" type=\"radio\" name=\"jenis-kelamin\" value=\"laki-laki\" id=\"jk-laki-laki\" required checked><label for=\"jk-laki-laki\">Laki-laki</label>
                                                    <input name=\"application_sex\" type=\"radio\" name=\"jenis-kelamin\" value=\"Perempuan\" id=\"jk-perempuan\" required><label for=\"jk-perempuan\">Perempuan</label>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class=\"fill columns\">
                                            <div class=\"large-2 large-offset-1 medium-2 medium-offset-1 small-3 columns\">
                                                <label>Status</label>
                                            </div>
                                            <div class=\"large-1 medium-1 small-1 columns\"><label>:</label></div>
                                            <div class=\"large-8 medium-8 small-8 end columns\">
                                                <fieldset class=\"large-12 columns\">
                                                    <input name=\"application_status\" type=\"radio\" value=\"belum-menikah\" id=\"st-belum-menikah\" required checked><label for=\"st-belum-menikah\">Belum Menikah</label>
                                                    <input name=\"application_status\" type=\"radio\" value=\"menikah\" id=\"st-menikah\" required><label for=\"st-menikah\">Menikah</label>
                                                    <input name=\"application_status\" type=\"radio\" value=\"cerai\" id=\"st-cerai\" required><label for=\"st-cerai\">Janda/Duda</label>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class=\"fill columns\">
                                            <div class=\"large-2 large-offset-1 medium-2 medium-offset-1 small-3 columns\">
                                                <label>Alamat</label>
                                            </div>
                                            <div class=\"large-1 medium-1 small-1 columns\"><label>:</label></div>
                                            <div class=\"large-8 medium-8 small-8 end columns\">
                                                <textarea name=\"application_address\" class=\"large-12 columns\" placeholder=\"\" required></textarea>
                                            </div>
                                        </div>
                                        <div class=\"fill columns\">
                                            <div class=\"large-2 large-offset-1 medium-2 medium-offset-1 small-3 columns\">
                                                <label>Kota</label>
                                            </div>
                                            <div class=\"large-1 medium-1 small-1 columns\"><label>:</label></div>
                                            <div class=\"large-8 medium-8 small-8 end columns\">
                                                <input name=\"application_city\" class=\"large-12 columns\" required>
                                            </div>
                                        </div>
                                        <div class=\"fill columns\">
                                            <div class=\"large-2 large-offset-1 medium-2 medium-offset-1 small-3 columns\">
                                                <label>Negara</label>
                                            </div>
                                            <div class=\"large-1 medium-1 small-1 columns\"><label>:</label></div>
                                            <div class=\"large-8 medium-8 small-8 end columns\">
                                                <select name=\"application_country\" class=\"large-12 columns\">
                                                    <option value=\"Indonesia\">Indonesia</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class=\"fill columns\">
                                            <div class=\"large-2 large-offset-1 medium-2 medium-offset-1 small-3 columns\">
                                                <label>No Handphone</label>
                                            </div>
                                            <div class=\"large-1 medium-1 small-1 columns\"><label>:</label></div>
                                            <div class=\"large-8 medium-8 small-8 end columns\">
                                                <input name=\"application_mobile\" type=\"tel\" id=\"mobile\" class=\"large-12 columns\" required>
                                            </div>
                                        </div>
                                        <div class=\"fill columns\">
                                            <div class=\"large-2 large-offset-1 medium-2 medium-offset-1 small-3 columns\">
                                                <label>Email</label>
                                            </div>
                                            <div class=\"large-1 medium-1 small-1 columns\"><label>:</label></div>
                                            <div class=\"large-8 medium-8 small-8 end columns\">
                                                <input name=\"application_email\" type=\"email\" class=\"large-12 columns\" placeholder=\"yourname@yourdomain.com\" required>
                                            </div>
                                        </div>
                                        <div class=\"fill large-6 large-offset-4 medium-6 medium-offset-4 small-12 end columns\">
                                            <div class=\"large-5 medium-5 small-5 columns\">
                                                <label>Upload File / Document</label>
                                            </div>
                                            <div class=\"large-7 end columns\" style=\"margin-bottom: -14px;\">
                                                <div class=\"large-12 columns custom-upload\">
                                                    <input type=\"file\" name=\"cv_file\" accept=\"application/pdf, application/msword\" required/>

                                                    <div class=\"fake-file\">
                                                        <input disabled=\"disabled\" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div style=\"font-size:10px; padding-top:-10px;color: #ffffff\"><i>File type: .doc, .docx, .pdf (max 2 MB)</i></div>
                                            <div class=\"fill columns\">
                                                <div class=\"medium-6 small-12 columns\">
                                                    &nbsp;
                                                </div>
                                                <div class=\"medium-6 end small-12 columns\">
                                                    <div class=\"g-recaptcha\" style=\"position: relative;\" data-sitekey=\"6LcOtBwTAAAAAMr8p2x6uA0BsIscnHqIiqmqHc07\"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class=\"fill columns\" style=\"text-align: right\">
                                            <button class=\"send\" type=\"send\">SEND</button>
                                        </div>
                                    </div>
                                </form>
                                ";
        }
        // line 332
        echo "                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <div class=\"show-for-small-only\">
            ";
        // line 339
        $this->displayBlock('back_to_top', $context, $blocks);
        // line 342
        echo "        </div>
    </div>
";
    }

    // line 339
    public function block_back_to_top($context, array $blocks = array())
    {
        // line 340
        echo "                ";
        $this->displayParentBlock("back_to_top", $context, $blocks);
        echo "
            ";
    }

    // line 345
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 346
        echo "    <script src='https://www.google.com/recaptcha/api.js'></script>
    ";
        // line 347
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    ";
        // line 348
        echo Asset::js("css3-animate-it.js");
        echo "
    ";
        // line 349
        echo Asset::js("jquery.maskedinput.js");
        echo "
    ";
        // line 350
        echo Asset::js("jquery.tinyscrollbar.js");
        echo "
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script type=\"text/javascript\">

        jQuery(function(\$){
            \$(\"#date\").mask(\"99/99/9999\", {placeholder: \"dd/mm/yyyy\"});
            \$(\"#mobile\").mask(\"(0899)-9999-999?9\");
        });


        \$(document).ready(function()
        {
            \$('#scrollbar1').tinyscrollbar({ trackSize:170 , thumbSize:27 });
            var \$scrollbar1 = \$(\"#scrollbar1\");
            \$scrollbar1.tinyscrollbar();
            var \$func_scrollbar_update = function(){
                var scrollbar1 = \$scrollbar1.data(\"plugin_tinyscrollbar\");
                scrollbar1.update();
            }
            \$('[data-accordion]').on('down.zf.accordion', function() {
                setTimeout(\$func_scrollbar_update, 501);
            });
            \$('[data-accordion]').on('up.zf.accordion', function() {
                setTimeout(\$func_scrollbar_update, 501);

            });
        });

        \$('.custom-upload input[type=file]').change(function(){
            \$(this).next().find('input').val(\$(this).val());
        });
//        popup
        \$('form').submit(function(){
            \$('#submitModal').foundation('reveal', 'open');
        });
//        end popup
        
        \$( \".btn-apply\" ).click(function() {
            var currpos = \$(this).data('job-position');
            document.getElementById(\"job-title\").value = currpos;
        });

        ";
        // line 392
        $this->displayBlock('back_to_top_js', $context, $blocks);
        // line 395
        echo "    </script>
";
    }

    // line 392
    public function block_back_to_top_js($context, array $blocks = array())
    {
        // line 393
        echo "        ";
        $this->displayParentBlock("back_to_top_js", $context, $blocks);
        echo "
        ";
    }

    public function getTemplateName()
    {
        return "career.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  633 => 393,  630 => 392,  625 => 395,  623 => 392,  578 => 350,  574 => 349,  570 => 348,  566 => 347,  563 => 346,  560 => 345,  553 => 340,  550 => 339,  544 => 342,  542 => 339,  533 => 332,  398 => 199,  387 => 197,  383 => 196,  372 => 187,  369 => 186,  366 => 185,  364 => 184,  342 => 164,  331 => 159,  325 => 156,  311 => 151,  303 => 148,  296 => 145,  292 => 144,  283 => 137,  272 => 132,  266 => 129,  252 => 124,  244 => 121,  237 => 118,  233 => 117,  223 => 110,  219 => 109,  201 => 94,  177 => 73,  173 => 72,  167 => 69,  163 => 68,  153 => 61,  149 => 60,  143 => 57,  139 => 56,  111 => 30,  97 => 28,  93 => 27,  89 => 25,  86 => 24,  80 => 22,  74 => 18,  71 => 17,  65 => 14,  59 => 10,  56 => 9,  50 => 7,  46 => 6,  42 => 5,  37 => 4,  34 => 3,);
    }
}
