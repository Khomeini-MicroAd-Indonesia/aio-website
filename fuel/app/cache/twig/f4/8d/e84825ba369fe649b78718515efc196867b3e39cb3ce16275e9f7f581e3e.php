<?php

/* faq.twig */
class __TwigTemplate_f48de84825ba369fe649b78718515efc196867b3e39cb3ce16275e9f7f581e3e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend_new.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
            'back_to_top_js' => array($this, 'block_back_to_top_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend_new.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("frontend_css", $context, $blocks);
        echo "
    ";
        // line 5
        echo Asset::css("custom/faq.css");
        echo "
";
    }

    // line 7
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 8
        echo "    ";
        $this->env->loadTemplate("template_faq.twig")->display($context);
        // line 9
        echo "    
    <div class=\"large-9 columns qa\">
    ";
        // line 11
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["faq_pocarisweat"]) ? $context["faq_pocarisweat"] : null));
        foreach ($context['_seq'] as $context["category"] => $context["detail"]) {
            // line 12
            echo "        <a name=\"";
            echo (isset($context["category"]) ? $context["category"] : null);
            echo "\"><h5 id=\"pocarisweat\"  data-magellan-target=\"";
            echo (isset($context["category"]) ? $context["category"] : null);
            echo "\">";
            echo (isset($context["category"]) ? $context["category"] : null);
            echo "</h5></a>
        <ul class=\"accordion\" data-accordion data-options=\"allowAllClosed: true\">
            ";
            // line 14
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["detail"]) ? $context["detail"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["faq"]) {
                // line 15
                echo "            <li class=\"accordion-item ";
                echo (($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "first")) ? ("is-active") : (""));
                echo "\" data-accordion-item>
                <a href=\"#\" class=\"accordion-title\">";
                // line 16
                echo $this->getAttribute((isset($context["faq"]) ? $context["faq"] : null), "seq");
                echo ". ";
                echo $this->getAttribute((isset($context["faq"]) ? $context["faq"] : null), "title");
                echo "</a>
                <div class=\"accordion-content\" data-tab-content>
                    ";
                // line 18
                echo $this->getAttribute((isset($context["faq"]) ? $context["faq"] : null), "answer");
                echo "
                </div>
            </li>
            ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['faq'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 22
            echo "        </ul>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['category'], $context['detail'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "    
    </div>
    ";
        // line 25
        $this->env->loadTemplate("template_faq_bottom.twig")->display($context);
    }

    // line 27
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 28
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    <script>
        ";
        // line 30
        $this->displayBlock('back_to_top_js', $context, $blocks);
        // line 33
        echo "    </script>
";
    }

    // line 30
    public function block_back_to_top_js($context, array $blocks = array())
    {
        // line 31
        echo "        ";
        $this->displayParentBlock("back_to_top_js", $context, $blocks);
        echo "
        ";
    }

    public function getTemplateName()
    {
        return "faq.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  150 => 31,  147 => 30,  142 => 33,  140 => 30,  134 => 28,  131 => 27,  127 => 25,  123 => 23,  116 => 22,  98 => 18,  91 => 16,  86 => 15,  69 => 14,  59 => 12,  55 => 11,  51 => 9,  48 => 8,  45 => 7,  39 => 5,  34 => 4,  31 => 3,);
    }
}
