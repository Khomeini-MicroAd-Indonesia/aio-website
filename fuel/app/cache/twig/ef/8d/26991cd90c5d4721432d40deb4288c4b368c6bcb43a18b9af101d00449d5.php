<?php

/* jquery_image_uploader.twig */
class __TwigTemplate_ef8d26991cd90c5d4721432d40deb4288c4b368c6bcb43a18b9af101d00449d5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("backend/template.twig");

        $this->blocks = array(
            'backend_css' => array($this, 'block_backend_css'),
            'backend_content_header' => array($this, 'block_backend_content_header'),
            'backend_content' => array($this, 'block_backend_content'),
            'backend_js' => array($this, 'block_backend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "backend/template.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_backend_css($context, array $blocks = array())
    {
        // line 4
        echo "\t";
        $this->displayParentBlock("backend_css", $context, $blocks);
        echo "
\t
\t<!-- Bootstrap styles -->
\t<link rel=\"stylesheet\" href=\"//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css\">
\t
\t<!-- blueimp Gallery styles -->
\t<link rel=\"stylesheet\" href=\"//blueimp.github.io/Gallery/css/blueimp-gallery.min.css\">
\t
\t<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
\t";
        // line 13
        echo Asset::css("jquery.fileupload.css");
        echo "
\t";
        // line 14
        echo Asset::css("jquery.fileupload-ui.css");
        echo "
\t
\t<!-- CSS adjustments for browsers with JavaScript disabled -->
\t<noscript>";
        // line 17
        echo Asset::css("jquery.fileupload-noscript.css");
        echo "</noscript>
\t<noscript>";
        // line 18
        echo Asset::css("jquery.fileupload-ui-noscript.css");
        echo "</noscript>
";
    }

    // line 21
    public function block_backend_content_header($context, array $blocks = array())
    {
        // line 22
        echo "<!-- Content Header (Page header) -->
<section class=\"content-header\">
\t<h1>";
        // line 25
        echo "\t\tMedia Uploader
\t\t<small>";
        // line 26
        echo (isset($context["media_uploader_subtitle"]) ? $context["media_uploader_subtitle"] : null);
        echo "</small>
\t</h1>
\t";
        // line 29
        echo "\t<ol class=\"breadcrumb\">
\t\t<li><a href=\"";
        // line 30
        echo Uri::base();
        echo "backend\">Home</a></li>
\t\t<li>Media Uploader</li>
\t\t<li class=\"active\">";
        // line 32
        echo (isset($context["media_uploader_subtitle"]) ? $context["media_uploader_subtitle"] : null);
        echo "</li>
\t</ol>
</section>
";
    }

    // line 37
    public function block_backend_content($context, array $blocks = array())
    {
        // line 38
        echo "
";
        // line 39
        if ((twig_length_filter($this->env, (isset($context["success_message"]) ? $context["success_message"] : null)) > 0)) {
            // line 40
            echo "<div class=\"alert alert-success alert-dismissable\">
\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
\t";
            // line 42
            echo (isset($context["success_message"]) ? $context["success_message"] : null);
            echo "
</div>
";
        }
        // line 45
        echo "
";
        // line 46
        if ((twig_length_filter($this->env, (isset($context["error_message"]) ? $context["error_message"] : null)) > 0)) {
            // line 47
            echo "<div class=\"alert alert-danger alert-dismissable\">
\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
\t";
            // line 49
            echo (isset($context["error_message"]) ? $context["error_message"] : null);
            echo "
</div>
";
        }
        // line 52
        echo "
\t<div class=\"box box-solid\">
\t\t<div class=\"box-header\">
\t\t\t<i class=\"fa fa-text-width\"></i>
\t\t\t<h3 class=\"box-title\">Information</h3>
\t\t</div><!-- /.box-header -->
\t\t<div class=\"box-body\">
\t\t\t";
        // line 59
        echo (isset($context["information_text"]) ? $context["information_text"] : null);
        echo "
\t\t</div><!-- /.box-body -->
\t</div>
\t
\t<!-- The file upload form used as target for the file upload widget -->
\t<form id=\"fileupload\" action=\"";
        // line 64
        echo (isset($context["form_action_url"]) ? $context["form_action_url"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\">
\t\t<!-- Redirect browsers with JavaScript disabled to the origin page -->
\t\t<noscript><input type=\"hidden\" name=\"redirect\" value=\"";
        // line 66
        echo Uri::base();
        echo "backend\"></noscript>
\t\t
\t\t<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
\t\t<div class=\"row fileupload-buttonbar\">
\t\t\t<div class=\"col-lg-7\">
\t\t\t\t<!-- The fileinput-button span is used to style the file input field as button -->
\t\t\t\t<span class=\"btn btn-success fileinput-button\">
\t\t\t\t\t<i class=\"glyphicon glyphicon-plus\"></i>
\t\t\t\t\t<span>Add files...</span>
\t\t\t\t\t<input type=\"file\" name=\"files[]\" multiple>
\t\t\t\t</span>
\t\t\t\t<button type=\"submit\" class=\"btn btn-primary start\">
\t\t\t\t\t<i class=\"glyphicon glyphicon-upload\"></i>
\t\t\t\t\t<span>Start upload</span>
\t\t\t\t</button>
\t\t\t\t<button type=\"reset\" class=\"btn btn-warning cancel\">
\t\t\t\t\t<i class=\"glyphicon glyphicon-ban-circle\"></i>
\t\t\t\t\t<span>Cancel upload</span>
\t\t\t\t</button>
\t\t\t\t<button type=\"button\" class=\"btn btn-danger delete\">
\t\t\t\t\t<i class=\"glyphicon glyphicon-trash\"></i>
\t\t\t\t\t<span>Delete</span>
\t\t\t\t</button>
\t\t\t\t<!-- The global file processing state -->
\t\t\t\t<span class=\"fileupload-process\"></span>
\t\t\t</div>
\t\t\t<!-- The global progress state -->
\t\t\t<div class=\"col-lg-5 fileupload-progress fade\">
\t\t\t\t<!-- The global progress bar -->
\t\t\t\t<div class=\"progress progress-striped active\" role=\"progressbar\" aria-valuemin=\"0\" aria-valuemax=\"100\">
\t\t\t\t\t<div class=\"progress-bar progress-bar-success\" style=\"width:0%;\"></div>
\t\t\t\t</div>
\t\t\t\t<!-- The extended global progress state -->
\t\t\t\t<div class=\"progress-extended\">&nbsp;</div>
\t\t\t</div>
\t\t</div>
\t\t<!-- The table listing the files available for upload/download -->
\t\t<table role=\"presentation\" class=\"table table-striped\"><tbody class=\"files\"></tbody></table>
\t</form>
\t</div>
\t<!-- The blueimp Gallery widget -->
\t<div id=\"blueimp-gallery\" class=\"blueimp-gallery blueimp-gallery-controls\" data-filter=\":even\">
\t\t<div class=\"slides\"></div>
\t\t<h3 class=\"title\"></h3>
\t\t<a class=\"prev\">‹</a>
\t\t<a class=\"next\">›</a>
\t\t<a class=\"close\">×</a>
\t\t<a class=\"play-pause\"></a>
\t\t<ol class=\"indicator\"></ol>
\t</div>
";
    }

    // line 118
    public function block_backend_js($context, array $blocks = array())
    {
        // line 119
        echo "\t";
        $this->displayParentBlock("backend_js", $context, $blocks);
        echo "
\t
\t<!-- DATA TABES SCRIPT -->
\t";
        // line 122
        echo Asset::js("plugins/datatables/jquery.dataTables.js");
        echo "
\t";
        // line 123
        echo Asset::js("plugins/datatables/dataTables.bootstrap.js");
        echo "
\t
\t<!-- The template to display files available for upload -->
\t<script id=\"template-upload\" type=\"text/x-tmpl\">
\t\t";
        // line 157
        echo "
\t\t{% for (var i=0, file; file=o.files[i]; i++) { %}
\t\t\t<tr class=\"template-upload fade\">
\t\t\t\t<td>
\t\t\t\t\t<span class=\"preview\"></span>
\t\t\t\t</td>
\t\t\t\t<td>
\t\t\t\t\t<p class=\"name\">{%=file.name%}</p>
\t\t\t\t\t<strong class=\"error text-danger\"></strong>
\t\t\t\t</td>
\t\t\t\t<td>
\t\t\t\t\t<p class=\"size\">Processing...</p>
\t\t\t\t\t<div class=\"progress progress-striped active\" role=\"progressbar\" aria-valuemin=\"0\" aria-valuemax=\"100\" aria-valuenow=\"0\"><div class=\"progress-bar progress-bar-success\" style=\"width:0%;\"></div></div>
\t\t\t\t</td>
\t\t\t\t<td>
\t\t\t\t\t{% if (!i && !o.options.autoUpload) { %}
\t\t\t\t\t\t<button class=\"btn btn-primary start\" disabled>
\t\t\t\t\t\t\t<i class=\"glyphicon glyphicon-upload\"></i>
\t\t\t\t\t\t\t<span>Start</span>
\t\t\t\t\t\t</button>
\t\t\t\t\t{% } %}
\t\t\t\t\t{% if (!i) { %}
\t\t\t\t\t\t<button class=\"btn btn-warning cancel\">
\t\t\t\t\t\t\t<i class=\"glyphicon glyphicon-ban-circle\"></i>
\t\t\t\t\t\t\t<span>Cancel</span>
\t\t\t\t\t\t</button>
\t\t\t\t\t{% } %}
\t\t\t\t</td>
\t\t\t</tr>
\t\t{% } %}
\t\t";
        echo "
\t</script>
\t
\t<!-- The template to display files available for download -->
\t<script id=\"template-download\" type=\"text/x-tmpl\">
\t\t";
        // line 203
        echo "
\t\t{% for (var i=0, file; file=o.files[i]; i++) { %}
\t\t\t<tr class=\"template-download fade\">
\t\t\t\t<td>
\t\t\t\t\t<span class=\"preview\">
\t\t\t\t\t\t{% if (file.thumbnailUrl) { %}
\t\t\t\t\t\t\t<a href=\"{%=file.url%}\" title=\"{%=file.name%}\" download=\"{%=file.name%}\" data-gallery><img src=\"{%=file.thumbnailUrl%}\"></a>
\t\t\t\t\t\t{% } %}
\t\t\t\t\t</span>
\t\t\t\t</td>
\t\t\t\t<td>
\t\t\t\t\t<p class=\"name\">
\t\t\t\t\t\t{% if (file.url) { %}
\t\t\t\t\t\t\t<a href=\"{%=file.url%}\" title=\"{%=file.name%}\" download=\"{%=file.name%}\" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
\t\t\t\t\t\t{% } else { %}
\t\t\t\t\t\t\t<span>{%=file.name%}</span>
\t\t\t\t\t\t{% } %}
\t\t\t\t\t</p>
\t\t\t\t\t{% if (file.error) { %}
\t\t\t\t\t\t<div><span class=\"label label-danger\">Error</span> {%=file.error%}</div>
\t\t\t\t\t{% } %}
\t\t\t\t</td>
\t\t\t\t<td>
\t\t\t\t\t<span class=\"size\">{%=o.formatFileSize(file.size)%}</span>
\t\t\t\t</td>
\t\t\t\t<td>
\t\t\t\t\t{% if (file.deleteUrl) { %}
\t\t\t\t\t\t<button class=\"btn btn-danger delete\" data-type=\"{%=file.deleteType%}\" data-url=\"{%=file.deleteUrl%}\"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{\"withCredentials\":true}'{% } %}>
\t\t\t\t\t\t\t<i class=\"glyphicon glyphicon-trash\"></i>
\t\t\t\t\t\t\t<span>Delete</span>
\t\t\t\t\t\t</button>
\t\t\t\t\t\t<input type=\"checkbox\" name=\"delete\" value=\"1\" class=\"toggle\">
\t\t\t\t\t{% } else { %}
\t\t\t\t\t\t<button class=\"btn btn-warning cancel\">
\t\t\t\t\t\t\t<i class=\"glyphicon glyphicon-ban-circle\"></i>
\t\t\t\t\t\t\t<span>Cancel</span>
\t\t\t\t\t\t</button>
\t\t\t\t\t{% } %}
\t\t\t\t</td>
\t\t\t</tr>
\t\t{% } %}
\t\t";
        echo "
\t</script>
\t
\t<!-- The jQuery UI -->
\t";
        // line 207
        echo Asset::js("jquery-ui-1.11.0.min.js");
        echo "
\t
\t<!-- The Templates plugin is included to render the upload/download listings -->
\t";
        // line 210
        echo Asset::js("blueimp/tmpl.min.js");
        echo "
\t
\t<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
\t";
        // line 213
        echo Asset::js("blueimp/load-image.all.min.js");
        echo "
\t
\t<!-- The Canvas to Blob plugin is included for image resizing functionality -->
\t";
        // line 216
        echo Asset::js("blueimp/canvas-to-blob.min.js");
        echo "
\t
\t<!-- blueimp Gallery script -->
\t";
        // line 219
        echo Asset::js("blueimp/jquery.blueimp-gallery.min.js");
        echo "
\t
\t<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
\t";
        // line 222
        echo Asset::js("jquery-fileupload/jquery.iframe-transport.js");
        echo "
\t
\t<!-- The basic File Upload plugin -->
\t";
        // line 225
        echo Asset::js("jquery-fileupload/jquery.fileupload.js");
        echo "
\t
\t<!-- The File Upload processing plugin -->
\t";
        // line 228
        echo Asset::js("jquery-fileupload/jquery.fileupload-process.js");
        echo "
\t
\t<!-- The File Upload image preview & resize plugin -->
\t";
        // line 231
        echo Asset::js("jquery-fileupload/jquery.fileupload-image.js");
        echo "
\t
\t<!-- The File Upload audio preview plugin -->
\t";
        // line 234
        echo Asset::js("jquery-fileupload/jquery.fileupload-audio.js");
        echo "
\t
\t<!-- The File Upload video preview plugin -->
\t";
        // line 237
        echo Asset::js("jquery-fileupload/jquery.fileupload-video.js");
        echo "
\t
\t<!-- The File Upload validation plugin -->
\t";
        // line 240
        echo Asset::js("jquery-fileupload/jquery.fileupload-validate.js");
        echo "
\t
\t<!-- The File Upload user interface plugin -->
\t";
        // line 243
        echo Asset::js("jquery-fileupload/jquery.fileupload-ui.js");
        echo "
\t
\t<!-- The main application script -->
\t<script type=\"text/javascript\">
\t\t\$(function () {
\t\t\t'use strict';

\t\t\t// Initialize the jQuery File Upload widget:
\t\t\t\$('#fileupload').fileupload({
\t\t\t\t// Uncomment the following to send cross-domain cookies:
\t\t\t\t//xhrFields: {withCredentials: true},
\t\t\t\turl: \$('#fileupload').attr('action')
\t\t\t});

\t\t\t// Enable iframe cross-domain access via redirect option:
\t\t\t\$('#fileupload').fileupload(
\t\t\t\t'option',
\t\t\t\t'redirect',
\t\t\t\twindow.location.href.replace(
\t\t\t\t\t/\\/[^\\/]*\$/,
\t\t\t\t\t'/cors/result.html?%s'
\t\t\t\t)
\t\t\t);

\t\t\t// Load existing files:
\t\t\t\$('#fileupload').addClass('fileupload-processing');
\t\t\t\$.ajax({
\t\t\t\t// Uncomment the following to send cross-domain cookies:
\t\t\t\t//xhrFields: {withCredentials: true},
\t\t\t\turl: \$('#fileupload').fileupload('option', 'url'),
\t\t\t\tdataType: 'json',
\t\t\t\tcontext: \$('#fileupload')[0]
\t\t\t}).always(function () {
\t\t\t\t\$(this).removeClass('fileupload-processing');
\t\t\t}).done(function (result) {
\t\t\t\t\$(this).fileupload('option', 'done')
\t\t\t\t\t.call(this, \$.Event('done'), {result: result});
\t\t\t});
\t\t});
\t</script>
\t
\t<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
\t<!--[if (gte IE 8)&(lt IE 10)]>
\t";
        // line 286
        echo Asset::js("jquery-fileupload/cors/jquery.xdr-transport.js");
        echo "
\t<![endif]-->
";
    }

    public function getTemplateName()
    {
        return "jquery_image_uploader.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  433 => 286,  387 => 243,  381 => 240,  375 => 237,  369 => 234,  363 => 231,  357 => 228,  351 => 225,  345 => 222,  339 => 219,  333 => 216,  327 => 213,  321 => 210,  315 => 207,  267 => 203,  229 => 157,  222 => 123,  218 => 122,  211 => 119,  208 => 118,  153 => 66,  148 => 64,  140 => 59,  131 => 52,  125 => 49,  121 => 47,  119 => 46,  116 => 45,  110 => 42,  106 => 40,  104 => 39,  101 => 38,  98 => 37,  90 => 32,  85 => 30,  82 => 29,  77 => 26,  74 => 25,  70 => 22,  67 => 21,  61 => 18,  57 => 17,  51 => 14,  47 => 13,  34 => 4,  31 => 3,);
    }
}
