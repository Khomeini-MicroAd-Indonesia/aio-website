<?php

/* career_new.twig */
class __TwigTemplate_ef584c8004ef742cb5fe1843691ebac4150b358f443eea82169dee0b465d117f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend_new.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_meta_facebook' => array($this, 'block_frontend_meta_facebook'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend_new.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("frontend_css", $context, $blocks);
        echo "
    ";
        // line 5
        echo Asset::css("custom/career_new.css");
        echo "
";
    }

    // line 7
    public function block_frontend_meta_facebook($context, array $blocks = array())
    {
        // line 8
        echo "<meta property=\"og:url\"                content=\"http://aio.co.id/id/home\" />
<meta property=\"og:type\"               content=\"article\" />
<meta property=\"og:title\"              content=\"Karir di Otsuka\" />
<meta property=\"og:description\"        content=\"Lowongan Kerja di Otsuka\" />
<meta property=\"og:image\"              content=\"\" />
";
    }

    // line 14
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 15
        echo "    <div id=\"fb-root\"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = \"//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=215125842246444\";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
    <div class=\"expanded row custom-left career\">
        <div class=\"row banner\">
            <div class=\"medium-7 columns end nopadd\">
                <img src=\"";
        // line 26
        echo Uri::base();
        echo "/assets/css/images/career_banner.jpg\"/>
            </div>
            <div class=\"medium-5 medium-offset-7 columns nopadd banner-desc\">
                <div class=\"medium-10 columns end\">";
        // line 29
        echo Lang::get("career_title");
        echo "</div>
            </div>
            <div class=\"medium-12 columns title\">
                <h2>";
        // line 32
        echo Lang::get("career_join");
        echo "</h2>
            </div>
        </div>
        <div class=\"medium-8 medium-offset-2 end columns nopadd custom-content form-section\">
            <div class=\"row\">
                <div class=\"medium-12 columns small-nopadd\">
                    <div class=\"columns title-section\">
                        ";
        // line 39
        echo Lang::get("career_desc");
        echo "
                    </div>
                </div>
                <div class=\"daftar-lowongan\">
                    <div class=\"row collapse lowongan-detail\" data-equalizer=\"lowongan\" data-equalize-on=\"medium\">
                        <div class=\"medium-3 columns tabs-header\">
                            <ul class=\"tabs vertical\" id=\"example-vert-tabs\" data-tabs data-equalizer-watch=\"lowongan\">
                                <div class=\"daftar-title\">";
        // line 46
        echo Lang::get("career_list");
        echo "</div>
                            ";
        // line 47
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["career_detail"]) ? $context["career_detail"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["career"]) {
            // line 48
            echo "                                <li class=\"tabs-title ";
            echo (($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "first")) ? ("is-active") : (""));
            echo "\"><a href=\"#panel";
            echo $this->getAttribute((isset($context["career"]) ? $context["career"] : null), "id");
            echo "v\" aria-selected=\"true\">";
            echo $this->getAttribute((isset($context["career"]) ? $context["career"] : null), "name");
            echo "</a></li>
                            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['career'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 50
        echo "                                <div class=\"medium-12 columns sosmed-wrapper\">
                                    <ul class=\"sosmed\">
                                        <li>";
        // line 52
        echo Lang::get("career_share");
        echo " :</li>
                                        <li><div class=\"share-button-fb btn btn-success clearfix\" data-href=\"";
        // line 53
        echo Uri::base();
        echo "\"><span class=\"share-facebook\"><img src=\"";
        echo Uri::base();
        echo "/assets/css/images/fb_icon.png\"/></span></div></li>
                                        <li><div><span class=\"share-linkedin\"><a className=\"fa fa-linkedin share-base share-linked-in spacing-left-5\" onclick=\"window.open('https://www.linkedin.com/cws/share?url=";
        // line 54
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/career-new')\"><img src=\"";
        echo Uri::base();
        echo "/assets/css/images/in_icon.png\"/></a></span></div></li>
                                        
                                    </ul>
                                </span>
                                </div>
                            </ul>
                        </div>
                        <div class=\"medium-9 columns tabs-desc\" data-equalizer-watch=\"lowongan\">
                            <div class=\"tabs-content vertical\" data-tabs-content=\"example-vert-tabs\">
                            ";
        // line 63
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["career_detail"]) ? $context["career_detail"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["detail"]) {
            // line 64
            echo "                                <div class=\"tabs-panel ";
            echo (($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "first")) ? ("is-active") : (""));
            echo "\" id=\"panel";
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "id");
            echo "v\">
                                    <div class=\"career-title\">
                                        <div class=\"medium-4 large-6 columns\">
                                            <p>";
            // line 67
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "name");
            echo "</p>
                                            <p>";
            // line 68
            echo Lang::get("career_location");
            echo " : ";
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "location");
            echo "</p>
                                        </div>
                                        <div class=\"medium-8 large-6 columns\">
                                            <p>";
            // line 71
            echo Lang::get("career_time");
            echo " : ";
            echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "date"), "jS F Y");
            echo "</p>
                                        </div>
                                        <div style=\"clear: both\"></div>
                                    </div>
                                    <div class=\"medium-12 columns career-req\">
                                        <span>Requirements : </span>
                                        ";
            // line 84
            echo "                                        ";
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "req");
            echo "
                                       
                                    </div>
                                    <div style=\"clear: both;\"></div>
                                </div>
                            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['detail'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 90
        echo "                                <div class=\"career-button\">
                                    <a href=\"#applying-form\" class=\"button\">";
        // line 91
        echo Lang::get("career_proceed");
        echo " <img src=\"";
        echo Uri::base();
        echo "/assets/css/images/fill-form.png\"/></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class=\"expanded row formulir\">
        <div class=\"medium-8 medium-offset-2 end columns nopadd custom-content form-section\">
            <div class=\"row\">
                <div class=\"daftar-lowongan\">
                    <div class=\"row collapse\" data-equalizer=\"formulir\" data-equalize-on=\"medium\">
                        <div class=\"medium-3 columns formulir-title\" data-equalizer-watch=\"formulir\">
                            <div class=\"daftar-title\">";
        // line 106
        echo Lang::get("career_form");
        echo "</div>
                            <img src=\"";
        // line 107
        echo Uri::base();
        echo "/assets/css/images/ornamen-form.png\"/>
                        </div>
                        <div class=\"medium-9 columns form-desc\" data-equalizer-watch=\"formulir\">
                            <form role=\"form\" id=\"applying-form\" enctype=\"multipart/form-data\" action=\"\" name=\"applying-form\" method=\"POST\" class=\"large-12 columns\">
                                <div data-abide-error class=\"alert callout\" style=\"display: none;\">
                                    <p><i class=\"fi-alert\"></i> There are some errors in your form.</p>
                                </div>
                                <div class=\"medium-12 columns\">
                                    <div class=\"small-12 columns\">
                                        <label>
                                            <div class=\"medium-2 columns\">
                                                ";
        // line 118
        echo Lang::get("career_position");
        echo "
                                            </div>
                                            <div class=\"medium-10 columns\">
                                                <select name=\"application_position\" id=\"posisi\" required>
                                                ";
        // line 122
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["career_detail"]) ? $context["career_detail"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["career"]) {
            // line 123
            echo "                                                    <option value=\"";
            echo $this->getAttribute((isset($context["career"]) ? $context["career"] : null), "name");
            echo "\">";
            echo $this->getAttribute((isset($context["career"]) ? $context["career"] : null), "name");
            echo "</option>
                                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['career'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 125
        echo "                                                    <i class=\"fa fa-angle-down\" aria-hidden=\"true\"></i>
                                                </select>
                                                    <span class=\"form-error\">
                                                      it's required.
                                                    </span>
                                            </div>
                                        </label>
                                    </div>
                                    <div class=\"small-12 columns\">
                                        <label>
                                            <div class=\"medium-2 columns\">
                                                ";
        // line 136
        echo Lang::get("career_fullname");
        echo "
                                            </div>
                                            <div class=\"medium-10 columns\">
                                                <input name=\"application_name\" type=\"text\" placeholder=\"";
        // line 139
        echo Lang::get("career_name");
        echo "\" aria-describedby=\"exampleHelpText\" required>
                                            <span class=\"form-error\">
                                              it's required.
                                            </span>
                                            </div>
                                        </label>
                                    </div>
                                    <div class=\"small-12 columns\">
                                        <label>
                                            <div class=\"medium-2 columns\">
                                                ";
        // line 149
        echo Lang::get("career_pob");
        echo " 
                                            </div>
                                            <div class=\"medium-10 columns\">
                                                <input name=\"application_place\" type=\"text\" placeholder=\"";
        // line 152
        echo Lang::get("career_pob2");
        echo "\" aria-describedby=\"exampleHelpText\" required>
                                            <span class=\"form-error\">
                                              it's required.
                                            </span>
                                            </div>
                                        </label>
                                    </div>
                                    <div class=\"small-12 columns\">
                                        <label>
                                            <div class=\"medium-2 columns\">
                                                ";
        // line 162
        echo Lang::get("career_dob");
        echo " 
                                            </div>
                                            <div class=\"medium-10 columns\">
                                                <input id=\"date\" name=\"application_date\" type=\"text\" placeholder=\"dd/mm/yyyy\" aria-describedby=\"exampleHelpText\" required>
                                            <span class=\"form-error\">
                                              it's required.
                                            </span>
                                            </div>
                                        </label>
                                    </div>
                                    <div class=\"small-12 columns\">
                                        <label>
                                            <div class=\"medium-2 columns\">
                                                ";
        // line 175
        echo Lang::get("career_gender");
        echo " 
                                            </div>
                                            <div class=\"medium-10 columns\">
                                                <select name=\"application_sex\" required>
                                                    <option value=\"laki-laki\">";
        // line 179
        echo Lang::get("career_gd_male");
        echo " </option>
                                                    <option value=\"perempuan\">";
        // line 180
        echo Lang::get("career_gd_female");
        echo "</option>
                                                </select>
                                                    <span class=\"form-error\">
                                                      it's required.
                                                    </span>
                                            </div>
                                        </label>
                                    </div>
                                    <div class=\"small-12 columns\">
                                        <label>
                                            <div class=\"medium-2 columns\">
                                                Status 
                                            </div>
                                            <div class=\"medium-10 columns\">
                                                <select name=\"application_status\" required>
                                                    <option value=\"belum-menikah\">";
        // line 195
        echo Lang::get("career_st_single");
        echo "</option>
                                                    <option value=\"menikah\">";
        // line 196
        echo Lang::get("career_st_married");
        echo "</option>
                                                    <option value=\"cerai\">";
        // line 197
        echo Lang::get("career_st_widower");
        echo "</option>
                                                </select>
                                                <span class=\"form-error\">
                                                  it's required.
                                                </span>
                                            </div>
                                        </label>
                                    </div>
                                    <div class=\"small-12 columns\">
                                        <label>
                                            <div class=\"medium-2 columns\">
                                                ";
        // line 208
        echo Lang::get("career_address");
        echo " 
                                            </div>
                                            <div class=\"medium-10 columns\">
                                                <textarea name=\"application_address\" aria-describedby=\"exampleHelpText\" required></textarea>
                                            <span class=\"form-error\">
                                              it's required.
                                            </span>
                                            </div>
                                        </label>
                                    </div>
                                    <div class=\"small-12 columns\">
                                        <label>
                                            <div class=\"medium-2 columns\">
                                                ";
        // line 221
        echo Lang::get("career_city");
        echo " 
                                            </div>
                                            <div class=\"medium-10 columns\">
                                                <input name=\"application_city\" type=\"text\" placeholder=\"kota\" aria-describedby=\"exampleHelpText\" required>
                                            <span class=\"form-error\">
                                              it's required.
                                            </span>
                                            </div>
                                        </label>
                                    </div>
                                    <div class=\"small-12 columns\">
                                        <label>
                                            <div class=\"medium-2 columns\">
                                                ";
        // line 234
        echo Lang::get("career_country");
        echo " 
                                            </div>
                                            <div class=\"medium-10 columns\">
                                                <select name=\"application_country\" required>
                                                    <option value=\"Indonesia\">Indonesia</option>
                                                </select>
                                            <span class=\"form-error\">
                                              it's required.
                                            </span>
                                            </div>
                                        </label>
                                    </div>
                                    <div class=\"small-12 columns\">
                                        <label>
                                            <div class=\"medium-2 columns\">
                                                ";
        // line 249
        echo Lang::get("career_hp");
        echo "
                                            </div>
                                            <div class=\"medium-10 columns\">
                                                <input name=\"application_mobile\" type=\"tel\" id=\"mobile\" placeholder=\"(08XX)-XXXX-XXXX\" required>
                                            <span class=\"form-error\">
                                              it's required.
                                            </span>
                                            </div>
                                        </label>
                                    </div>
                                    <div class=\"small-12 columns\">
                                        <label>
                                            <div class=\"medium-2 columns\">
                                                Email 
                                            </div>
                                            <div class=\"medium-10 columns\">
                                                <input name=\"application_email\" type=\"email\" pattern=\"[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}\$\" placeholder=\"yourname@yourdomain.com\" required>
                                            <span class=\"form-error\">
                                              it's required.
                                            </span>
                                            </div>
                                        </label>
                                    </div>
                                    <div class=\"small-12 columns\">
                                        <div class=\"medium-10 medium-offset-2 no-padding columns upload-file\" style=\"position: relative\">
                                            <div class=\"large-6 columns no-padding\">
                                                ";
        // line 276
        echo "                                                <input class=\"inputfile inputfile-6\" id=\"file-7\" type=\"file\" class=\"upload\" name=\"cv_file\" accept=\"application/pdf, application/msword\" data-max-size=\"2097152\" required/>
                                                <label for=\"file-7\"><span id=\"filedrag\">";
        // line 277
        echo Lang::get("career_upload");
        echo " </span> <strong><img src=\"";
        echo Uri::base();
        echo "/assets/css/images/icon-upload.png\"/> </strong></label>
                                                <br/><span>";
        // line 278
        echo Lang::get("career_file");
        echo ": .doc, .docx, .pdf (max 2MB)</span>
                                                ";
        // line 279
        if (((isset($context["status"]) ? $context["status"] : null) == "yes")) {
            // line 280
            echo "                                                <div>";
            echo (isset($context["error_message"]) ? $context["error_message"] : null);
            echo "</div>
                                                ";
        }
        // line 282
        echo "                                            </div>
                                            <div class=\"large-6 columns captcha\">
                                                <div class=\"g-recaptcha position-captcha\" data-callback=\"recaptchaCallback\" data-sitekey=\"6LcOtBwTAAAAAMr8p2x6uA0BsIscnHqIiqmqHc07\"></div>
                                                <input type=\"hidden\" name=\"recaptcha\" data-rule-recaptcha=\"true\">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=\"medium-6 columns\">

                                </div>
                                <div class=\"career-button\">
                                    <button id=\"submitBtn\" class=\"button\" type=\"submit\" disabled value=\"Submit\">
                                        ";
        // line 295
        echo Lang::get("txt_kirim");
        echo " <span><img src=\"";
        echo Uri::base();
        echo "/assets/css/images/read-more.png\"></span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
";
    }

    // line 306
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 307
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    ";
        // line 308
        echo Asset::js("jquery.maskedinput.js");
        echo "
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src=\"//platform.linkedin.com/in.js\" type=\"text/javascript\"> lang: en_US</script>
    <script type=\"IN/Share\" data-url=\"";
        // line 311
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/career-new\"></script>
    <script>
        ";
        // line 314
        echo "        function recaptchaCallback() {
            \$('#submitBtn').removeAttr('disabled');
        };

        \$(\".tabs-title a\").click(function () {
            //this is change select value 1
            \$('#posisi').val(\$(this).text()).trigger('change');
        });

//        var placeholder = 'File Upload Here.';
//        \$('.fake-file input').attr('value', placeholder);
//
//        \$('.fake-file input').focus(function(){
//            if(\$(this).val() === placeholder){
//                \$(this).attr('value', '');
//            }
//        });
//
//        \$('.fake-file input').blur(function(){
//            if(\$(this).val() ===''){
//                \$(this).attr('value', placeholder);
//            }
//        });

        (function() {

            // getElementById
            function \$id(id) {
                return document.getElementById(id);
            }


            // output information
            function Output(msg) {
                \$('#filedrag').html('');
                var m = \$id(\"filedrag\");
                m.innerHTML = msg + m.innerHTML;
            }


            // file drag hover
            function FileDragHover(e) {
                e.stopPropagation();
                e.preventDefault();
                e.target.className = (e.type == \"dragover\" ? \"hover\" : \"\");
            }


            // file selection
            function FileSelectHandler(e) {

                // cancel event and hover styling
                FileDragHover(e);

                // fetch FileList object
                var files = e.target.files || e.dataTransfer.files;

                // process all File objects
                for (var i = 0, f; f = files[i]; i++) {
                    ParseFile(f);
                }

            }
            // output file information
            function ParseFile(file) {

                Output(
                        file.name
                );

            }


            // initialize
            function Init() {

                var fileselect = \$id(\"fileselect\"),
                        filedrag = \$id(\"filedrag\");
//                        submitbutton = \$id(\"submitbutton\");

                // file select
//                fileselect.addEventListener(\"change\", FileSelectHandler, false);

                // is XHR2 available?
                var xhr = new XMLHttpRequest();
                if (xhr.upload) {

                    // file drop
                    filedrag.addEventListener(\"dragover\", FileDragHover, false);
                    filedrag.addEventListener(\"dragleave\", FileDragHover, false);
                    filedrag.addEventListener(\"drop\", FileSelectHandler, false);
//                    filedrag.style.display = \"block\";

                    // remove submit button
//                    submitbutton.style.display = \"none\";
                }

            }

            // call initialization file
            if (window.File && window.FileList && window.FileReader) {
                Init();
            }


        })();

        ( function ( document, window, index )
        {
            var inputs = document.querySelectorAll( '.inputfile' );
            Array.prototype.forEach.call( inputs, function( input )
            {
                var label\t = input.nextElementSibling,
                        labelVal = label.innerHTML;

                input.addEventListener( 'change', function( e )
                {
                    var fileName = '';
                    if( this.files && this.files.length > 1 )
                        fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                    else
                        fileName = e.target.value.split( '\\\\' ).pop();

                    if( fileName )
                        label.querySelector( 'span' ).innerHTML = fileName;
                    else
                        label.innerHTML = labelVal;
                });

                // Firefox bug fix
                input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
                input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
            });
        }( document, window, 0 ));


        //facebook
        \$('.share-button-fb').click(function(e){
            e.preventDefault();
            FB.ui(
                    {
                        method: 'feed',
                        link: \$(this).data('href')
                    });
        });
        jQuery(function(\$){
            \$(\"#date\").mask(\"99/99/9999\", {placeholder: \"dd/mm/yyyy\"});
            \$(\"#mobile\").mask(\"(0899)-9999-999?9\");
        });
//
//        \$('.custom-upload input[type=file]').change(function(){
//            \$(this).next().find('input').val(\$(this).val());
//        });

        \$( window ).resize(function() {
//            banner

            if(\$(window).width() > 1023)
            {
                var banner = \$('.banner').height();
                var bannerDesc = \$('.banner-desc').height();
                var paddBanner = (banner - bannerDesc) / 2;
//            daftar lowongan
                var lowongan = \$('.tabs-header').height();
                \$('.tabs-desc > .tabs-content').css('height',lowongan);
//            formulir
                var formulir = \$('.form-desc').height();
                \$('.form-desc > form').css('height',formulir);
//            \$('.custom-upload input[type=file]').change(function(){
//                \$(this).next().find('input').val(\$(this).val());
//            });

//            \$('.fake-file input').css('height',\$('.fileUpload span img').height());
//            document.getElementById(\"uploadBtn\").onchange = function () {
//                document.getElementById(\"uploadFile\").value = this.value;
//            };
            }

            if (screen.width > 640) {
                \$('.banner-desc').css('padding-top', paddBanner);
            } else {
                \$('.banner-desc').css('padding-top', (banner - bannerDesc - 15));
            }
        });
        \$(window).trigger('resize');
    </script>

";
    }

    public function getTemplateName()
    {
        return "career_new.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  577 => 314,  572 => 311,  566 => 308,  561 => 307,  558 => 306,  541 => 295,  526 => 282,  520 => 280,  518 => 279,  514 => 278,  508 => 277,  505 => 276,  476 => 249,  458 => 234,  442 => 221,  426 => 208,  412 => 197,  408 => 196,  404 => 195,  386 => 180,  382 => 179,  375 => 175,  359 => 162,  346 => 152,  340 => 149,  327 => 139,  321 => 136,  308 => 125,  297 => 123,  293 => 122,  286 => 118,  272 => 107,  268 => 106,  248 => 91,  245 => 90,  224 => 84,  213 => 71,  205 => 68,  201 => 67,  192 => 64,  175 => 63,  161 => 54,  155 => 53,  151 => 52,  147 => 50,  126 => 48,  109 => 47,  105 => 46,  95 => 39,  85 => 32,  79 => 29,  73 => 26,  60 => 15,  57 => 14,  48 => 8,  45 => 7,  39 => 5,  34 => 4,  31 => 3,);
    }
}
