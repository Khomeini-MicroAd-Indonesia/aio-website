<?php

/* template_faq_bottom.twig */
class __TwigTemplate_d7fc82be946376307816358e38416caf144f08fc8e1acb312a2fab9ddcba3f85 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "        <div class=\"scroll-top-wrapper \">
            <span class=\"scroll-top-inner\">
            </span>
        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "template_faq_bottom.twig";
    }

    public function getDebugInfo()
    {
        return array (  38 => 14,  28 => 8,  19 => 1,  161 => 100,  158 => 99,  153 => 102,  151 => 99,  145 => 97,  142 => 96,  138 => 94,  51 => 9,  48 => 20,  45 => 7,  39 => 5,  34 => 4,  31 => 3,);
    }
}
