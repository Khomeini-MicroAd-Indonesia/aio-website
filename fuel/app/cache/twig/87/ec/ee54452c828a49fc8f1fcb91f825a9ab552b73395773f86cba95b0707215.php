<?php

/* faq_soyjoy.twig */
class __TwigTemplate_87ecee54452c828a49fc8f1fcb91f825a9ab552b73395773f86cba95b0707215 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
            'back_to_top_js' => array($this, 'block_back_to_top_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("frontend_css", $context, $blocks);
        echo "
    ";
        // line 5
        echo Asset::css("custom/faq.css");
        echo "
";
    }

    // line 7
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 8
        echo "    ";
        $this->env->loadTemplate("template_faq.twig")->display($context);
        // line 9
        echo "    <div class=\"large-9 columns qa\">
    ";
        // line 10
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["faq_soyjoy"]) ? $context["faq_soyjoy"] : null));
        foreach ($context['_seq'] as $context["category"] => $context["detail"]) {
            // line 11
            echo "        <a name=\"";
            echo (isset($context["category"]) ? $context["category"] : null);
            echo "\"><h5 id=\"soyjoy\"  data-magellan-target=\"";
            echo (isset($context["category"]) ? $context["category"] : null);
            echo "\">";
            echo (isset($context["category"]) ? $context["category"] : null);
            echo "</h5>
        <ul class=\"accordion\" data-accordion data-options=\"allowAllClosed: true\">
            ";
            // line 13
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["detail"]) ? $context["detail"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["faq"]) {
                // line 14
                echo "            <li class=\"accordion-item ";
                echo (($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "first")) ? ("is-active") : (""));
                echo "\" data-accordion-item>
                <a href=\"#\" class=\"accordion-title\">";
                // line 15
                echo $this->getAttribute((isset($context["faq"]) ? $context["faq"] : null), "seq");
                echo ". ";
                echo $this->getAttribute((isset($context["faq"]) ? $context["faq"] : null), "title");
                echo "</a>
                <div class=\"accordion-content\" data-tab-content>
                    ";
                // line 17
                echo $this->getAttribute((isset($context["faq"]) ? $context["faq"] : null), "answer");
                echo "
                </div>
            </li>
            ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['faq'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 21
            echo "        </ul>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['category'], $context['detail'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "    </div>
    ";
        // line 24
        $this->env->loadTemplate("template_faq_bottom.twig")->display($context);
    }

    // line 26
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 27
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    <script>
        ";
        // line 29
        $this->displayBlock('back_to_top_js', $context, $blocks);
        // line 32
        echo "    </script>
";
    }

    // line 29
    public function block_back_to_top_js($context, array $blocks = array())
    {
        // line 30
        echo "        ";
        $this->displayParentBlock("back_to_top_js", $context, $blocks);
        echo "
        ";
    }

    public function getTemplateName()
    {
        return "faq_soyjoy.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  148 => 30,  145 => 29,  140 => 32,  138 => 29,  132 => 27,  129 => 26,  125 => 24,  122 => 23,  115 => 21,  97 => 17,  90 => 15,  85 => 14,  68 => 13,  58 => 11,  54 => 10,  51 => 9,  48 => 8,  45 => 7,  39 => 5,  34 => 4,  31 => 3,);
    }
}
