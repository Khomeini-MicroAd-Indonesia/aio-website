<?php

/* pocari_sweat.twig */
class __TwigTemplate_1f6a393339d4d0b981652fdb180b23b812c120d014b6c714146515181ea846fa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        echo Asset::css("custom/animations.css");
        echo "
    ";
        // line 5
        $this->displayParentBlock("frontend_css", $context, $blocks);
        echo "
    ";
        // line 6
        echo Asset::css("custom/product.css");
        echo "
";
    }

    // line 8
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 9
        echo "    <div class=\"pocari columns\">
        <div class=\"columns sub-menu-product\" data-equalizer=\"menu-ionessense\">
            <div class=\"medium-2 medium-offset-3 small-4 columns\">
                <a href=\"";
        // line 12
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/brand/pocari-sweat\">
                    <div class=\"columns pocari-sweat-color\" data-equalizer-watch=\"menu-ionessense\">
                        Pocari Sweat
                        <span class=\"active-menu\"></span>
                    </div>
                </a>
            </div>
            <div class=\"medium-2 small-4 columns\">
                <a href=\"";
        // line 20
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/brand/soyjoy\">
                    <div class=\"columns soyjoy-color\" data-equalizer-watch=\"menu-ionessense\">
                        Soy Joy
                    </div>
                </a>
            </div>
            <div class=\"medium-2 small-4 end columns\">
                <a href=\"";
        // line 27
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/brand/ionessence\">
                    <div class=\"columns ionesence-color\" data-equalizer-watch=\"menu-ionessense\">
                        Ionessence
                    </div>
                </a>
            </div>
        </div>
        <div class=\"columns banner text-center\">
            <img data-interchange=\"[";
        // line 35
        echo Uri::base();
        echo "/assets/img/product/banner-pocari-sweat-small-new.png, small], [";
        echo Uri::base();
        echo "/assets/img/product/banner-pocari-sweat-medium-new.png, medium], [";
        echo Uri::base();
        echo "/assets/img/product/banner-pocari-sweat-new.png, large]\">
        </div>
        <div class=\"row\">
            <div class=\"columns product-detail\" data-equalizer=\"product-detail\">
                <div class=\"medium-6 columns\">
                    <div class=\"columns product-desc animatedParent\" data-sequence=\"300\" data-equalizer-watch=\"product-detail\">
                        <div class=\"title  animated fadeInUp\" data-id=\"1\">
                            <p>The ION Supply Drink</p>
                            <div style=\"width: 110px; height: 3px;background: #ffffff;margin-top: 23px;margin-bottom: 15px\"></div>
                        </div>
                        <p class=\"animated fadeInUp\" data-id=\"2\">Minuman pengganti ION tubuh yang hilang. Jaga keseimbangan ION tubuhmu dengan POCARI SWEAT  untuk hidup sehat dan aktivitas yang lebih baik!</p>
                    </div>
                </div>
                <div class=\"medium-6 columns animatedParent\">
                    <div class=\"columns product-desc-img  animated fadeInUp delay-250\">
                        <a href=\"http://pocarisweat.co.id/borntosweat/\" target=\"_blank\"><img class=\"\" src=\"";
        // line 50
        echo Uri::base();
        echo "/assets/img/product/pocari-detail.jpg\" data-equalizer-watch=\"product-detail\"></a>
                    </div>
                </div>
            </div>
            <div class=\"columns\">
                <div class=\"columns\">
                    <div class=\"product-variant columns\">
                        <div class=\"medium-6 columns variant-display\">
                            <div class=\"columns animatedParent\" data-sequence=\"200\" data-equalizer=\"product\">
                                <div class=\"small-2 columns animated fadeInUp\" data-id=\"1\"><img src=\"";
        // line 59
        echo Uri::base();
        echo "/assets/img/product/variant/pocari-variant-sachet-15g-new.png\"></div>
                                <div class=\"small-2 columns animated fadeInUp\" data-id=\"2\"><img src=\"";
        // line 60
        echo Uri::base();
        echo "/assets/img/product/variant/pocari-variant-kaleng-330ml-new.png\"></div>
                                <div class=\"small-2 columns animated fadeInUp\" data-id=\"3\"><img src=\"";
        // line 61
        echo Uri::base();
        echo "/assets/img/product/variant/pocari-variant-350ml-new.png\"> </div>
                                <div class=\"small-2 columns animated fadeInUp\" data-id=\"4\"><img src=\"";
        // line 62
        echo Uri::base();
        echo "/assets/img/product/variant/pocari-variant-500ml-new.png\"> </div>
                                <div class=\"small-2 columns animated fadeInUp\" data-id=\"5\"><img src=\"";
        // line 63
        echo Uri::base();
        echo "/assets/img/product/variant/pocari-variant-900ml-new.png\"> </div>
                                <div class=\"small-2 columns animated fadeInUp\" data-id=\"6\"><img src=\"";
        // line 64
        echo Uri::base();
        echo "/assets/img/product/variant/pocari-variant-2L.png\"> </div>
                            </div>
                        </div>
                        <div class=\"medium-6 columns\">
                            <div class=\"columns\">
                                <p class=\"variant-title\">KEMASAN</p>
                                <div class=\"line\"></div>
                                <ul class=\"variant-name medium-6 end columns animatedParent\" data-sequence=\"100\">
                                    <li class=\"animated fadeInUp\" data-id=\"1\">Sachet 15 gr</li>
                                    <li class=\"animated fadeInUp\" data-id=\"2\">Kaleng 330 ml</li>
                                    <li class=\"animated fadeInUp\" data-id=\"3\">PET 350 ml</li>
                                    <li class=\"animated fadeInUp\" data-id=\"4\">PET 500 ml</li>
                                    <li class=\"animated fadeInUp\" data-id=\"5\">PET 900 ml</li>
                                    <li class=\"animated fadeInUp\" data-id=\"6\">PET 2 L</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"columns\">
                <div class=\"columns\">
                    <div class=\"columns product-link\">
                        <div class=\"product-button\">
                            <a href=\"http://www.pocarisweat.co.id/\" target=\"_blank\" class=\"button\">GO TO <br/>PRODUCT WEBSITE</a>
                            <div style=\"border-bottom: 4px solid #ffffff ; width: 60px\"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
";
    }

    // line 98
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 99
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    ";
        // line 100
        echo Asset::js("css3-animate-it.js");
        echo "
";
    }

    public function getTemplateName()
    {
        return "pocari_sweat.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  191 => 100,  186 => 99,  183 => 98,  145 => 64,  141 => 63,  137 => 62,  133 => 61,  129 => 60,  125 => 59,  113 => 50,  91 => 35,  79 => 27,  68 => 20,  56 => 12,  51 => 9,  48 => 8,  42 => 6,  38 => 5,  33 => 4,  30 => 3,);
    }
}
