<?php
class Model_MailQueues extends \Orm\Model {
	protected static $_table_name = 'mail_queues';

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events'=>array('before_insert'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_UpdatedAt' => array(
			'events'=>array('before_update'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_Validation' => array(
			'events'=>array('before_save')
		)
	);
        
	protected static $_properties = array(
		'id',
		'email_from' => array(
			'label' => 'Email From',
			'validation' => array(
				'required',
			)
		),
		'email_to' => array(
			'label' => 'Email To',
			'validation' => array(
				'required',
			)
		),
		'email_reply_to',
		'email_subject' => array(
			'label' => 'Email Subject',
			'validation' => array(
				'required',
				'max_length' => array(150),
			)
		),
		'email_body',
		'email_data',
		'email_view',
		'email_sent' => array(
            'default' => 0,
            'null' => false
        ),
		'email_fail' => array(
            'default' => 0,
            'null' => false
        ),
		'created_at',
		'updated_at'
	);
	
	public static function in_queues() {
		return self::query()
			->where('email_sent', 0)
			->where('email_fail', 0)
			->order_by('created_at');
	}
}
