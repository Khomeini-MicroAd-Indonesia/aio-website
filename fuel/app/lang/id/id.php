<?php
/**
 * Created by PhpStorm.
 * User: Albert
 * Date: 5/18/15
 * Time: 11:04 AM
 */
return array(

/* -----------------------------menu-----------------------------------*/


/* --------------------------about_us----------------------------------*/
    'txt_home01'      => 'Jika anda tertarik untuk berkontribusi dalam meningkatkan kesejahteraan masyarakat dengan memproduksi produk yang menyehatkan dan berpartisipasi secara aktif dalam program-program kami, kami mengundang anda untuk bergabung dengan Amerta Indah Otsuka.',
    'txt_home02'      => 'Mari bergabung dengan kami dalam posisi di bawah ini:',
    'txt_home03'      => 'Jadilah bagian dari OTSUKA dan ciptakan masa depan Anda',
    'txt_home04'      => 'Lihat lainnya',
    'txt_home05'      => 'TERTARIK MENGUNJUNGI KAMI ?',
    'txt_home06'      => 'Silahkan baca ketentuannya dan isi formulirnya.',
    'txt_home07'      => 'Selengkapnya',
    'txt_home08'      => 'DI SINI',

    'txt_aboutus01'   => 'OTSUKA PEOPLE CREATING NEW PRODUCTS FOR BETTER HEALTH WORLDWIDE',
    'txt_aboutus02'   => 'MAKNA LOGO PERUSAHAAN',
    'txt_aboutus03'   => 'VISI',
    'txt_aboutus04'   => 'MISI',
    'txt_aboutus05'   => 'PT. Amerta Indah Otsuka berkomitmen untuk memberikan yang terbaik pada konsumen dan masyarakat sesuai dengan misinya untuk menjadi perusahaan yang brilian, dengan memberikan kontribusi yang signifikan dan terpercaya bagi konsumen serta masyarakat.',
    'txt_aboutus06'   => 'Komitmen tersebut diwujudkan dengan kegiatan operasional yang menjunjung tinggi kualitas dan standarisasi baik lokal maupun internasional yang telah diakui dunia. Komitmen dan kerja keras PT. Amerta Indah Otsuka telah diakui berbagai pihak melalui berbagai sertifikat sebagai berikut:',
    'txt_aboutus07'   => 'PERJALANAN KAMI DALAM MEMBERIKAN SESUATU YANG TERBAIK UNTUK INDONESIA',

    'txt_pocari01'    => 'POCARI SWEAT The ION Supply Drink',
    'txt_pocari02'    => 'Minuman pengganti ION tubuh yang hilang. Jaga keseimbangan ION tubuhmu dengan POCARI SWEAT untuk hidup sehat dan aktivitas yang lebih baik!',
    'txt_pocari03'    => 'KEMASAN',
    'txt_soyjoy01'    => 'merupakan pelopor cemilan sehat yang terbuat sepenuhnya dari tepung kedelai dan buah-buahan asli yang dikeringkan. Kedelai dan buah sebagai bahan dasar SOYJOY mengandung nutrisi penting seperti protein, serat, dan vitamin. SOYJOY juga dibuat tanpa pemanis buatan dan tanpa pengawet.',
    'txt_soyjoy02'    => 'VARIAN RASA',
    'txt_ionessence01'=> 'POCARI SWEAT The ION Supply Drink',
    'txt_ionessence02'=> 'IONESSENCE adalah minuman hipotonik rendah kalori (11 kcal/100mL). Untuk menjaga kelembaban kulit dan mencegah dehidrasi pada kulit. Kulit mudah iritasi. Kulit kering & kusam. Bibir pecah-pecah. Dehidrasi pada kulit dapat menyebabkan hilangnya faktor pelembap alami kulit yang biasa disebut Natural Moisturizing Factor (NMF).',

    'txt_csr'         => 'PT Amerta Indah Otsuka melalui yayasan <strong>Satu Hati</strong> mewujudkan kepedulian terhadap pendidikan dan lingkungan sebagai salah satu bentuk tanggung jawab sosial PT Amerta Indah Otsuka terhadap masyarakat dan alam.',

    'txt_tour01'      => 'SYARAT & KETENTUAN',
    'txt_tour02'      => 'FORMULIR PENDAFTARAN',
    'txt_tour03'      => 'Pada saat berkunjung, pengunjung di mohon untuk:',
    'txt_tour04'      => 'KUNJUNGI WEBSITE',
    'txt_tour05'      => 'Pabrik Kejayan',
    'txt_tour06'      => 'Pabrik Sukabumi',
    'txt_tour07'      => 'Jadwal Kunjungan',
    'txt_tour08'      => 'Tersedia',
    'txt_tour09'      => 'Tidak Tersedia',
    'txt_tour10'      => '*Pilih tanggal dan jam untuk menentukan waktu kunjungan',
    'txt_tourtnc01'   => 'Factory tour terbuka untuk sekolah, universitas, ataupun institusi lainnya, pengunjung minimum kelas 3 sekolah dasar.',
    'txt_tourtnc02'   => 'Pengunjung sehat, tidak terkena penyakit dan suhu tubuh tidak melebihi 37.50C ketika berkunjung.',
    'txt_tourtnc03'   => 'Proses produksi sewaktu-waktu dapat berhenti maka proses produksi tidak dapat diperlihatkan secara langsung namun hanya diperlihatkan melalui video.',
    'txt_tourtnc04'   => 'Sabtu/Minggu/Hari libur nasional tidak menerima kunjungan.',
    'txt_tourtnc05'   => 'Kapasitas pengunjung min. 20 orang max. 100 orang.',
    'txt_tourtnc06'   => 'Pengunjung berpakaian rapi dan sopan dan tidak menggunakan sandal.',
    'txt_tourtnc07'   => 'Ketersediaan tanggal dan jam kunjungan yang telah Anda ajukan sebelumnya, akan kami konfirmasikan terlebih dahulu. Apabila jam dan tanggal tidak tersedia, Anda bisa mengubah tanggal dan jam tersebut untuk menyesuaikan.',
    'txt_tourtnc08'   => 'Tidak merokok di dalam lingkungan pabrik.',
    'txt_tourtnc09'   => 'Tidak membuang sampah di sembarangan tempat.',
    'txt_tourtnc10'   => 'Tidak mengambil foto/video di koridor proses produksi.',
    'txt_tourtnc11'   => 'Mengikuti petunjuk pemandu selama kunjungan demi menjamin keselamatan pengunjung.',
    'txt_tourtnc12'   => 'Tidak membawa turun dari bus makanan dan minuman ke dalam lingkungan pabrik.',
    'txt_tourtnc13'   => 'Pada saat berkunjung, pengunjung di mohon untuk:',

    'txt_form_tour01'   => 'Tanggal Kunjungan',
    'txt_form_tour02'   => 'Lokasi Kunjungan',
    'txt_form_tour03'   => 'Nama Lengkap',
    'txt_form_tour04'   => 'Nama Institusi',
    'txt_form_tour05'   => 'Jumlah Peserta',
    'txt_form_tour05_01'=> 'Jumlah peserta 20-100',
    'txt_form_tour06'   => 'Alamat',
    'txt_form_tour07'   => 'No Telephone & HP',
    'txt_form_tour08'   => 'Email',
    'txt_form_tour09'   => 'Pesan & Keterangan',

    'txt_career01'      => 'JADILAH BAGIAN DARI PT AMERTA INDAH OTSUKA DAN CIPTAKAN MASA DEPAN ANDA',
    'txt_career02'      => 'Di PT Amerta Indah Otsuka, setiap orang / karyawan selalu tumbuh dan berkembang. Kami percaya bahwa pertumbuhan perusahaan harus sejalan dengan pertumbuhan karyawan. Oleh karena itulah, sebagai bagian dari PT Amerta Indah Otsuka, Anda akan mendapatkan berbagai program pengembangan yang tentunya Anda butuhkan untuk membangun karir Anda.',
    'txt_career03'      => 'PT AMERTA INDAH OTSUKA DEVELOPMENT CENTER',
    'txt_career04'      => 'Setiap orang / karyawan memiliki potensi dan kemampuan masing-masing yang harus selalu dikembangkan agar bisa memberikan kinerja terbaiknya. Oleh karena itu, kami mengembangkan Individual Development Plan (IDP) / rencana pengembangan pribadi berdasarkan kompetensi. Melalui IDP karyawan dapat mengetahui training, proyek khusus atau program pengembangn lain yang akan didapat selama satu tahun kedepan. Training yang direncanakan terkait dengan teknis pekerjaan ataupun yang berhubungan dengan pengembangan diri.',
    'txt_career05'      => 'BEASISWA',
    'txt_career06'      => 'Pendidikan adalah seumur hidup. Sebagai bagian dari PT Amerta Indah Otsuka, Anda sangat didukung untuk mendapatkan pendidikan yang lebih tinggi. Dukungan ini ditunjukkan melalui pemberian beasiswa, baik di Universitas-Universitas ternama di Indonesia maupun di luar negeri. Dengan demikian, Anda dapat meningkatkan kompetensi sekaligus membangun karir masa depan Anda.',
    'txt_career07'      => 'PT AMERTA INDAH OTSUKA DEVELOPMENT PROGRAM',
    'txt_career08'      => 'Ingin meraih karir cemerlang dalam waktu singkat? Kami tantang Anda untuk bergabung dengan "graduate development program" kami. Kami mencari orang-orang luar biasa yang memiliki karakter yang luar biasa untuk mengikuti program ini. Orang-orang yang memiliki semangat tinggi, punya jiwa kepemimpinanan, memiliki daya analisa kuat dan mampu berkomunikasi dengan baik kami persilahkan untuk mengikuti program ini.',
    'txt_career09'      => 'PT Amerta Indah Otsuka adalah bagian dari Otsuka Pharmaceutical Co., Ltd dengan produk terkemuka seperti Pocari Sweat dan SOYJOY. Otsuka memberikan apresiasi bagi orang-orang yang mempunyai keinginan untuk maju dan berani melakukan perubahan yang lebih baik.',
    'txt_career10'      => 'Bergabunglah dengan PT Amerta Indah Otsuka dan bawa perubahan yang lebih baik untuk kesehatan di seluruh dunia.',
    'txt_career11'      => 'Lokasi',
    'txt_career12'      => 'Batas Waktu',

    'txt_distribution'  => 'ALAMAT',

    'txt_csr01'  => 'Adopsi 25.000 pohon',
    'txt_csr02'  => 'Kawasan konservasi Taman Nasional Halimun Gunung Salak yang merupakan kawasan tangkapan air dan penyedia oksigen serta penyedia keanekaragaman hayati. Adopsi pohon ini merupakan upaya pro-aktif PT. Amerta Indah Otsuka untuk menyelamatkan hutan Indonesia serta mengedukasi masyarakat akan pentingnya melestarikan lingkungan. ',
    'txt_csr03'  => 'Donasi 250 tempat sampah ',
    'txt_csr04'  => 'PT Amerta Indah Otsuka memberikan bantuan penyediaan 250 tempat sampah di Pantai Citepus dan Pantai Karang Hawu Sukabumi pada April 2011. Tempat sampah yang diberikan merupakan hasil recycle dari drum material Pocari Sweat yang sudah tidak terpakai sehingga selain memberikan manfaat langsung kepada masyarakat dengan pengadaan tempat sampah, juga memberikan kontribusi secara tidak langsung dengan pengurangan limbah pabrik.',
    'txt_csr05'  => 'Bina Posyandu Mandiri',
    'txt_csr06'  => 'PT Amerta Indah Otsuka memberikan pembinaan bagi kaderkader Posyandu di sekitar pabrik sebagai upaya menjadikan desa yang menjadi wilayah penyuluhan kader binaan menjadi desa sehat dengan mengoptimalkan potensi yang mereka miliki.',
    'txt_csr07'  => 'Edukasi Demam Berdarah',
    'txt_csr08'  => 'Jumlah kasus demam berdarah yang tinggi merupakan salah satu hal yang melatarbelakangi PT Amerta Indah Otsuka melakukan edukasi pada masyarakat mengenai demam berdarah. Dilakukan sejak tahun 2011, pada kegiatan ini PT Amerta Indah Otsuka memberikan edukasi mengenai demam berdarah secara lengkap, mulai dari pencegahan hingga penanggulangan saat terkena demam berdarah. Kegiatan ini dilakukan sebagai upaya menekan angka penderita serta kematian akibat demam berdarah.',

    'explorion_title' => 'Dengan mengusung unsur experience dan teknologi digital, Explorion Tour merupakan konsep kunjungan pabrik terbaru dari</br>PT Amerta Indah Otsuka. Dilengkapi dengan teknologi terkini, explorion mengajak pengunjung untuk ikut serta dalam petualangan yang inovatif dan interaktif melalui perangkat digital cerdas dan game theater Explorion Theater.',
    'explorion_desc_1' => 'Konsep kunjungan terbaru dari PT Amerta Indah Otsuka diharapkan dapat memberikan edukasi bagi para pengunjung melalui cara yang menarik dan menyenangkan. Kunjungan pabrik Pocari Sweat yang mulai dibuka pada tahun 2008 ini merupakan suatu bentuk komitmen PT Amerta Indah Otsuka dalam mewujudkan prinsip open factory.',
    'explorion_desc_2' => 'Sejalan dengan filosofi dari Otsuka, yakni “Otsuka-people creating new products for better health worldwide.” Inovasi ini diharapkan dapat memberikan pengetahuan kepada masyarakat Indonesia mengenai pentingnya ion tubuh untuk kesehatan yang lebih baik.',

    'factory_title' => 'Sejak tahun 2011, PT Amerta Indah Otsuka telah membuka program kunjungan pabrik bagi masyarakat umum. Salah satu pabrik tersebut adalah Pabrik Kejayan di Pasuruan, Jawa Timur.',
    'factory_desc_1' => 'Pabrik Kejayan berdiri di atas lahan berukuran 11 hektar dengan luas bangunan sebesar 19.000 meter persegi. Dalam satu menit, Pabrik Kejayan mampu memproduksi Pocari Sweat dalam 600 PET botol per menit. Berbeda dari pabrik pertama AIO di Sukabumi yang menggunakan teknologi O-Hot (Otsuka Hot-Fill) untuk dua lini produksi PET-nya, Pabrik Kejayan telah menggunakan teknologi OC (Otsuka Clean). Hal ini menjadikan Pabrik Kejayan sebagai pabrik pertama Otsuka di luar Jepang yang menggunakan teknologi OC.',
    'factory_desc_2' => 'Pada teknologi O-Hot, proses pengisian botol dilakukan saat produk bersuhu panas, sedangkan teknologi OC memungkinkan proses tersebut berlangsung ketika larutan berkisar pada suhu ruangan. Tidak hanya itu, desain botol PET berteknologi OC pun turut mengalami perubahan, yakni 20% lebih ringan dari O-Hot. Artinya, resin/minyak bumi yang digunakan menjadi lebih rendah sehingga dapat mengurangi emisi karbon yang dilepas ke atmosfer bumi.',
    'factory_desc_3' => 'Di Pabrik Kejayan, AIO juga telah menggunakan pembangkit listrik sendiri (gas turbin) yang memanfaatkan gas alam. Hal ini tidak terlepas dari fakta bahwa proses produksi aseptik sangat rentan terhadap gangguan suplai listrik.',

    //    home new
    'home_latest' => 'BERITA TERKINI',
    'title_lao' => 'LIFE AT AMERTA INDAH OTSUKA',
    'title_feed' => 'INFO TERKINI',
    'txt_nutrition' => 'NEUTRACEUTICAL (Nutrition +Pharmaceutical)',
    'txt_nutrition_desc' => 'Kami percaya setiap orang memiliki hak untuk hidup sehat. Didukung dengan hasil riset dan penelitian ilmiah, PT Amerta Indah Otsuka menciptakan konsep neutraceutical product, yang merupakan paduan kata nutrisi (nutrition) dan farmasi (pharmaceutical). Produk neutraceutical diartikan sebagai produk yang memberikan manfaat kesehatan dan menjaga kualitas hidup menjadi lebih baik.',
    'txt_about_awards' => 'PENGHARGAAN & SERTIFIKAT',
    'txt_about_awards_desc' => '<p>PT Amerta Indah Otsuka berkomitmen untuk memberikan yang terbaik pada konsumen dan masyarakat sesuai dengan misinya untuk menjadi perusahaan yang brilian, dengan memberikan kontribusi yang signifikan dan terpercaya bagi konsumen serta masyarakat.</p>
                <p>Komitmen tersebut diwujudkan dengan kegiatan operasional yang menjunjung tinggi kualitas dan standarisasi baik lokal maupun internasional yang telah diakui dunia. Komitmen dan kerja keras PT Amerta Indah Otsuka telah diakui berbagai pihak melalui berbagai sertifikat sebagai berikut :</p>',
    'txt_kirim' => 'Kirim',
    'home_news' => 'Lihat Semua',
    'home_detail' => 'Baca Selengkapnya',
    'home_story' => 'Cerita Mereka',
    'home_detail_Story' => 'Lihat Cerita Lainnya',
    'home_story2' => 'Upaya untuk menjalankan visi terbaik dan mencapai kesuksesan bersama di dalam keberagaman',

    //    about us new
    'about_desc' => '<p><span class="first-letter">D</span>i awal berdirinya pada tahun 1997, Otsuka merupakan perusahaan afiliasi dari Otsuka Pharmaceutical Co, Ltd Jepang yang memulai perjalanannya di Indonesia dengan nama PT Kapal Indah Otsuka. Perusahaan ini terbentuk dari hasil investasi bersama  antara Otsuka Pharmaceutical Jepang dan PT Kapal Api dengan Pocari Sweat sebagai produk pertamanya. Kemudian di tahun 1999, PT Kapal Indah Otsuka merubah namanya menjadi <br> PT Amerta Indah Otsuka.</p>
                <p>Semakin berkembangnya pertumbuhan perusahaan, pada tahun 2004 Otsuka membuka pabrik pertama yang terletak di Sukabumi, Jawa Barat. Kemudian menyusul 6 tahun berikutnya, pada tahun 2010 pabrik di Kejayan, Jawa Timur didirikan. Dengan keberhasilannya dalam mengedukasi konsumen, maka produk PT Amerta Indah  akan produk yang lebih baik, produk PT Amerta Indah Otsuka telah tersebar melalui titk – titik distribusi di seluruh Indonesia.</p>
                <p>Seiring dari kemajuan perusahaan, Otsuka berkomitmen untuk terus meningkatkan kualitas dengan mengimplementasikan Sistem Manajemen Mutu ISO 9001:2008, Sistem Keamanan Pangan ISO 22000 : 2005, dan Sistem Manajemen Lingkungan ISO 14001 : 2004.</p>',
    'about_title' => 'Tentang Kami',
    'about_msg' => 'Pesan Presiden Direktur',
    'about_visi' => 'Visi & Misi',
    'about_product' => 'KONSEP PRODUK',
    'about_history' => 'Sejarah Kami',

    
    /* --------------------------brand----------------------------------*/
    // Pocari new    
    'pocari_header' => 'Minuman pengganti ion tubuh yang hilang.<br> Jaga keseimbangan ION tubuhmu dengan POCARI SWEAT untuk hidup sehat dan aktivitas yang lebih baik.',   
    'pocari_title' => 'LAHIRNYA POCARI SWEAT',
    'pocari_story_1' => 'Sekitar 40 tahun yang lalu, seorang peneliti Otsuka bernama Rokuro Harima yang sedang dalam perjalanan bisnis ke Meksiko,terpaksa dirawat di rumah sakit dikarenakan diare. Dokter menyuruhnya untuk mendapatkan air dan nutrisi yang cukup. Di sana, Harima melihat sang dokter  meminum cairan infus untuk mengembalikan cairan tubuhnya setelah menyelesaikan operasi. Terinspirasi dari ide ini, peneliti Otsuka pun mulai mengembangkan minuman rehidrasi yang bisa mengganti kandungan air dan elektrolit (ion) yang hilang saat berkeringat selama menjalankan aktivitas sehari-hari.',  
    'pocari_story_2' => 'Ketika masyarakat menjadi lebih sadar atas pentingnya kesehatan, para peneliti yang dipimpin oleh Harima dan Takaichi berusaha membuat minuman kesehatan untuk semua orang saat mereka berkeringat.', 
    'pocari_story_3' => 'Sepanjang tahun melakukan penelitian ini dengan tekun, mereka telah membuat lebih dari 1.000 minuman prototipe yang dapat mengembalikan komponen tubuh setelah berkeringat sehari-hari. Dalam pengembangannya, peneliti ingin membuat rasa minuman ini lezat terutama setelah berkeringat. Untuk menguji konsep produk, para peneliti  mendaki gunung untuk menguji coba produk  untuk diri mereka sendiri. Dengan itu, tanpa diduga minuman pengganti air dan elektrolit (ion) akhirnya resmi  diluncurkan pada tahun 1980 dengan nama Pocari Sweat.',
    'pocari_story_4' => 'Hingga kini Pocari Sweat dicintai tidak hanya di Jepang. Pocari Sweat telah menjadi minuman favorit di 17 negara dan di berbagai wilayah. Meskipun telah beradaptasi dengan masing-masing budaya yang beragam di seluruh dunia , Pocari Sweat tetap setia dan  tidak pernah berubah. Sebagai minuman yang diciptakan dari sains untuk mereka yang sedang berkeringat, Pocari Sweat selalu di hati masyarakat untuk membantu mereka tetap sehat. ',
    
    'what_is_ion_title' => 'Apa itu ION? ',
    'what_is_ion' => 'Ion adalah bagian dari elektrolit yaitu garam-garam mineral yang secara alami ada di dalam tubuh kita. Elektrolit di dalam tubuh kita akan terurai menjadi ion, baik yang bermuatan positif dan negatif dan penting untuk membantu semua proses yang ada di dalam tubuh. Elektrolit tidak dapat diproduksi sendiri oleh tubuh, jadi memang harus dapat asupan dari luar, baik lewat makanan atau minuman.',
    'pocari_for_body' => 'Pocari Sweat Bagi Tubuh', 
    'pocari_for_body_detail' => 'Kekurangan cairan tubuh dapat menurunkan daya tahan tubuh, kinerja dan juga gangguan pencernaan. POCARI SWEAT memiliki komposisi mirip dengan cairan tubuh sehingga diserap lebih cepat dan cepat gantikan cairan dan ion tubuh yang hilang. Dengan begitu, kesehatan dapat lebih terjaga dan aktivitas berjalan lancar.',    
    'pocari_sweat_web' => 'Kunjungi web pocari sweat',
        

// Soyjoy new    

'soyjoy_header' => 'Cemilan low GI (Glycemic Index) yang terbuat<br> sepenuhnya dari tepung kedelai dan buah – buahan asli yang dikeringkan ',    
'soyjoy_title' => 'SOY PRODUCTS + SOLUTIONS = SOYLUTION',
'soyjoy_story_1' => 'Mengusung konsep \'Soylution\'  kedelai dapat memberikan solusi untuk masalah kesehatan dan lingkungan, Otsuka Pharmaceutical  telah mengembangkan bentuk baru dari produk kedelai . Mereka menawarkan cara-cara baru nan lezat kepada orang – orang di seluruh dunia saat menikmati manfaat dari nutrisi kedelai utuh.',  
'soyjoy_story_2' => 'Konsep merk dibalik SOYJOY adalah \'untuk memungkinkan konsumen dalam memilih dan menikmati produk kedelai\’.  Produk SOYJOY membuat nutrisi kedelai mudah didapat bahkan untuk orang-orang sibuk sekalipun. SOYJOY menawarkan 5 varian rasa yang lezat untuk menikmati kebaikan nutrisi kedelai yaitu Hawthorn Berry, Raisin Almond, Strawberry, Pisang, dan Almond Chocolate.  ', 
'soyjoy_story_3' => 'SOYJOY sebagai cemilan sehat memiliki kemasan praktis, mudah dibawa, dan tidak merepotkan. Melalui proses oven bake, SOYJOY dapat tahan hingga 8-10 bulan ketika disimpan karena rasa dan nutrisi yang terkandung di dalam bar SOYJOY tetap terjaga',
'soyjoy_story_4' => 'SOYJOY termasuk dalam kategori cemilan low GI sehingga mampu memberikan rasa kenyang lebih lama dan membuat nafsu makan terkendali. Waktu yang disarankan untuk mengonsumsi SOYJOY adalah sebelum waktu makan besar. ',
    
'what_is_lowgi_title' => 'Apa Itu Makanan Low GI? ',
'what_is_lowgi' => 'Makanan low GI dapat dicerna secara perlahan, sehingga menaikkan kadar gula secara bertahap. Hal ini menyebabkan kadar gula secara bertahap. Hal ini menyebabkan kadar gula tidak naik secara drastis atau turun secara tiba-tiba sehingga makanan low GI seperti SOYJOY baik untuk diabetes. ',
'soyjoy_advantage' => 'SOYJOY Bagi Tubuh', 
'soyjoy_advantage_detail_1' => 'Terbuat dari tepung kedelai utuh, tinggi serat dan protein, SOYJOY dapat menjadi alternatif snack sehat yang dapat dimakan di sela waktu makan. Kedelai yang digunakan adalah kedelai non-GMO atau tidak melalui proses rekayasa genetik, tidak menggunakan pemanis buatan, dan bahan pengawet. ',    
'soyjoy_advantage_detail_2' => 'Selain merupakan low GI, SOYJOY juga mengandung isoflavon dan juga serat pangan yang dapat mengurangi kadar kolesterol jahat dan kolesterol total dalam tubuh. ',    
'soyjoy_sweat_web' => 'Kunjungi Web SOYJOY',
'soyjoy_web' => 'Kunjungi web SOYJOY',

 

// Ionessence new    

'ionessence_header' => 'Minuman  elektrolit rendah kalori <br>(11 kcal/100mL). Untuk menjaga kelembaban kulit dan mencegah dehidrasi pada kulit. ',    
'ionessence_title' => 'LAHIRNYA IONESSENCE',
'ionessence_story_1' => 'Sebagai perusahaan yang berkomitmen terhadap kesehatan, di tahun 2015 PT Amerta Indah Otsuka meluncurkan IONESSENCE. Produk minuman ini memiliki kandungan elektrolit yang mirip dengan pelembab alami atau Natural Moisturising Factor (NMF) pada kulit yang dapat mempengaruhi kinerja kulit dalam mengaktifkan enzim dan zat zat seperti kolagen dan serat elastin.',  
'ionessence_story_2' => 'NMF dapat berkurang akibat beberapa faktor seperti paparan sinar matahari, pendingin ruangan, penggunaan kosmetik yang tidak tepat, serta gaya hidup yang tidak sehat seperti mengkonsumsi alkohol, dan kurang asupan elektrolit. ', 
'ionessence_story_3' => 'Dengan kandungan kalori yang rendah, IONESSENCE dapat dikonsumsi kapan dan di mana saja tanpa perlu khawatir akan asupan kalori yang berlebihan. Disarankan konsumsi rutin selama minimal 1 bulan sebelum tidur. Dikarenakan pada malam hari terjadi proses detoksifikasi tubuh dan juga penyerapan nutrisi makanan, sehingga diharapkan di pagi hari kulit menjadi lebih lembab karena sudah menyerap cairan.',
    
'lack_of_imf' => 'Ketika Tubuh Kekurangan NMF ',
'lack_of_imf_detail' => 'Jika kekurangan NMF pada kulit maka akan menyebabkan bibir pecah-pecah, kulit kering dan kusam. Dan yang paling berdampak adalah kulit menjadi iritasi. Untuk itu, sangat dianjurkan untuk mengkonsumsi IONESSENCE. Dengan komposisi elektrolit mirip dengan NMF, IONESSENCE dapat membantu pembentukan NMF dalam mengaktifkan enzim dan zat-zat seperti kolagen dan serat elastin.',
'ionessence_advantage' => 'IONESSENCE Bagi Tubuh', 
'ionessence_advantage_detail_1' => 'Manfaat IONESSENCE yaitu membantu penyerapan cairan dalam tubuh hingga ke lapisan kulit, membantu pembentukan faktor pelembab alami (NMF) dan memelihara kulit senantiasa sehat, lembab, serta kenyal.',   
'ionessence_advantage_detail_2' => 'Kandungan elektrolit IONESSENCE terdiri dari Natrium (Na), Kalium (K), Kalsium (Ca), Magnesium (Mg), dan Lactate. ',   
'ionessence_web' => 'Kunjungi Web Ionessence', 

    
    // Career new
    'career_desc' => 'PT Amerta Indah Otsuka adalah bagian dari Otsuka Parmaceutical Co., Ltd Jepang dengan produk terkemuka seperti Pocari Sweat, SOYJOY, dan Ionessence. Otsuka memberikan apresiasi bagi orang-orang yang mempunyai keinginan untuk maju dan berani melakukan perubahan yang lebih baik.',
    'career_join' => 'Bergabung Dengan Kami',
    'career_title' => 'Mari bergabung dengan Otsuka & bersama membawa perubahan untuk masa depan yang lebih sehat.',
    'career_upload' => 'Cari dengan Klik Icon Unggah',
    'career_file' => 'Tipe file',
    'career_list' => 'DAFTAR <br/>LOWONGAN',
    'career_share' => 'Bagikan lowongan ini',
    'career_location' => 'Lokasi',
    'career_time' => 'Batas Waktu',
    'career_proceed' => 'Isi Form',
    'career_form' => 'FORM <br/>APLIKASI',
    'career_position' => 'Posisi',
    'career_fullname' => 'Nama Lengkap',
    'career_pob' => 'Tempat Lahir',
    'career_dob' => 'Tanggal Lahir',
    'career_gender' => 'Jenis Kelamin',
    'career_gd_male' => 'Laki-Laki',
    'career_gd_female' => 'Perempuan',
    'career_st_single' => 'Belum Menikah',
    'career_st_married' => 'Menikah',
    'career_st_widower' => 'Janda/Duda',
    'career_address' => 'Alamat',
    'career_city' => 'Kota',
    'career_country' => 'Negara',
    'career_hp' => 'No. Handphone',
    'career_name' => 'nama',
    'career_pob2' => 'tempat lahir',

    // Explorion new
    'explorion_detail' => 'Dengan mengusung prinsip <i>open factory</i>, sejak 2008 PT Amerta Indah Otsuka membuka kesempatan bagi masyarakat untuk melihat proses produksi Pocari Sweat yang terletak di Sukabumi dan Kejayan.',
    'explorion_sub_title' => 'Satu Kegiatan,<br/> Seribu Pengalaman, Sejuta Senyuman',
    'explorion_reg_tour' => 'Daftar tour sekarang',
    'explorion_form' => 'FORMULIR KUNJUNGAN',
    'explorion_reg' => 'PENDAFTARAN',
    'explorion_tnc' => 'SYARAT & KETENTUAN',
    'explorion_schedule' => 'Jadwal Acara',
    'explorion_schedule2' => 'Lihat Jadwal',
    'explorion_sbmt_form' => 'Kirim Formulir',
    'explorion_note' => 'Catatan',

    // Galeri Explorion
    'galeri_detail' => 'Ayo bagikan fotomu di Instagram saat menyaksikan langsung proses lahirnya sebuah inovasi, dalam kesehatan dan nutrisi, di pabrik Otsuka Sukabumi & Kejayan dengan hastag <strong>#kunjunganpabrikAIO</strong> seperti mereka.',
    'galeri_title' => 'Galeri Foto <br/>
                        Terkini',

    // LAO Page
    'lao_title' => '<span> Keberagaman Adalah Awal dari Inovasi</span>',
    'lao_msg' => '<p>"Kami secara kontinyu menciptakan diferensiasi dan melakukan lebih banyak inovasi hanya untuk membuat kami terlihat berbeda. Sepanjang perjalanan, kami terus berupaya mencoba menemukannya, kami mendedikasikan semua usaha dan waktu semata-mata hanya pada apa yang Otsuka bisa lakukan. Tentunya, demi kesehatan dunia yang lebih baik."</p>',
    'lao_filosofi' => '<p>Kami berupaya keras dalam memanfaatkan aset dan keterampilan khusus untuk mengembangkan keberagaman solusi ilmiah yang berkontribusi demi kehidupan masyarakat dunia dalam bentuk produk yang inovatif dan kreatif. Dengan ini, kami ingin menumbuhkan iklim dan budaya perusahaan yang dinamis yang mencerminkan visi kami sebagai  perusahaan yang sehat.</p>',
    'lao_desc' => '<p>Monumen ini mewakili perwujudan filosofi Otsuka yang mengingatkan akan tempat kelahiran Otsuka di Tokushima dan menunjukkan pentingnya menjadi KREATIF dan BERPIKIR TERBUKA terhadap ide-ide baru.</p>',
    'lao_title2' => 'Kesempatan yang setara',
    'lao_desc2' => '<p class="text-left">Keberagaman adalah Awal dari Inovasi. Sebagai individu, karyawan kami memiliki keunikan dan potensi yang berbeda–beda.<br>PT Amerta Indah Otsuka senantiasa menghargai keberagaman, memberikan kesempatan yang sama bagi setiap individu tanpa memandang usia, jenis kelamin, agama, ras, dan kebangsaan untuk terus berkembang.  Dengan semangat inilah, para karyawan <br/> PT Amerta Indah Otsuka bekerja secara profesional.</p>',
    'lao_title3' => 'Kehidupan kerja di Otsuka',
    'lao_desc3' => '<p class="text-left">Karyawan merupakan aset utama kami, Untuk itu, kami senantiasa berusaha memberikan yang terbaik bagi karyawan dalam bentuk apresiasi ataupun fasilitas yang menunjang pekerjaan. Melalui program Talent Management, setiap karyawan diberi kesempatan untuk mengembangkan diri sesuai dengan potensinya. Tak hanya itu, PT Amerta Indah Otsuka juga membantu meningkatkan kualitas karyawannya dengan memberikan training khusus hingga beasiswa ke luar negeri.</p>',
    'lao_story' => 'Cerita Mereka',
    'lao_vision' => 'Visi Kepemimpinan',
    'lao_filosofi2' => 'Filosofi Perusahaan',
    'lao_join' => 'Bergabunglah Dengan Kami',

//CSR
    'csr_title' => 'Tanggung Jawab Sosial Perusahaan⁠⁠⁠⁠',
    'sh' => 'Wujud kepedulian PT Amerta Indah Otsuka terhadap pendidikan, lingkungan, dan kesehatan',
    
// CSR Satu Hati Cerdaskan Bangsa Page
    'shcb_main_title' => 'YAYASAN SATU HATI CERDASKAN BANGSA',
    'shcb_first_paragraph' => 'Kontribusi sosial PT Amerta Indah Otsuka dimulai ketika kota Yogyakarta dilanda gempa pada tahun 2006 yang mengakibatkan rusaknya beberapa bangunan sekolah. Saat itu, sebuah konser amal digelar untuk menggalang dana dan menarik hati masyarakat untuk ikut berkontribusi terhadap masa depan pendidikan kota Yogyakarta. ',
    'shcb_second_paragraph' => 'Gerakan kepedulian PT Amerta Indah Otsuka di bidang pendidikan terus berlanjut, di tahun 2007, dicanangkan program “Satu Hati Cerdaskan Bangsa” sebagai program Corporate Social Responsibility PT Amerta Indah Otsuka di bidang pendidikan. Konsistensi dan komitmen PT Amerta Indah Otsuka untuk berkontribusi dalam memajukan kualitas pendidikan Indonesia kemudian diwujudkan dengan didirikannya Yayasan satu Hati Cerdaskan Bangsa pada tahun 2011.',
    
    'shcb_title' => 'Bentuk Kontribusi PT Amerta Indah Otsuka Kepada Pendidikan',
    
// CSR Satu Hati Cerdaskan Bangsa Page

    'shpl_main_paragraph' => 'Kehidupan dan masyarakat kita bergantung kepada karunia alam. Oleh sebab itu, PT Amerta Indah Otsuka turut bertanggung jawab untuk melestarikan alam dan lingkungan.',
    'shpl_title' => 'Bentuk Kontribusi PT Amerta Indah Otsuka Kepada Lingkungan',
    
// CSR Satu Hati Sehatkan Bangsa Page

    'shsb_main_paragraph' => 'PT Amerta Indah Otsuka berkomitmen untuk berkontribusi dengan kesehatan masyarakat indonesia dengan kegiatan yang terpadu.',
    'shsb_title' => 'Bentuk Kontribusi PT Amerta Indah Otsuka Untuk Kesehatan',
    
    
    // Header Menu
    'menu_about' => 'Tentang Kami',
    'menu_news' => 'Berita',
    'menu_career' => 'Karir',
    'menu_distribution' => 'Distribusi',
    'menu_contact' => 'Kontak Kami',
    'menu_lang' => 'Bahasa',
    'menu_search' => 'Pencarian',

    // Sub Menu

    'menu_history' => 'Sejarah',
    'menu_press' => 'Press',
    'menu_join' => 'Bergabung Dengan Kami',

    // Footer
    'foot_follow' => 'Ikuti Kami',
    'foot_pvp' => 'Kebijakan Privasi',
    'foot_tnc' => 'Ketentuan Penggunaan',

    //News and Press

    'news_more' => 'Artikel Lainnya',
    'news_share' => 'Bagikan Artikel Ini...',
    'news_title' => 'BERITA TERKINI',
    'news_title2' => 'SIARAN PERS',
    'news_detail' => 'Baca Selengkapnya',
    'news_more2' => 'lihat berita lainnya',


    //Distribution
    'txt_distri_1' => 'Peta Distribusi',
    'txt_distri_2' => 'Daerah Distribusi'
);
