<?php
/**
 * Created by PhpStorm.
 * User: Albert
 * Date: 5/18/15
 * Time: 11:04 AM
 */
return array(
/* -----------------------------menu-----------------------------------*/

    
/* --------------------------about_us----------------------------------*/
    'txt_home01'      => 'If you are into challenge and would like to contribute in improving the welfare of society by producing healthy products and actively participating in our programs, let’s be part of PT. Amerta Indah Otsuka.',
    'txt_home02'      => 'Let’s join us in positions as follows:',
    'txt_home03'      => 'Be a part of Otsuka and create your future',
    'txt_home04'      => 'See more',
    'txt_home05'      => 'INTERESTED<br/>TO VISIT<br/>US?',
    'txt_home06'      => 'Please read the terms and fill the form',
    'txt_home07'      => 'See more',
    'txt_home08'      => '&nbsp;HERE&nbsp;',

    'txt_aboutus01'   => 'OTSUKA PEOPLE CREATING NEW PRODUCTS FOR BETTER HEALTH WORLDWIDE',
    'txt_aboutus02'   => 'COMPANY\'S LOGO',
    'txt_aboutus03'   => 'VISION',
    'txt_aboutus04'   => 'MISSION',
    'txt_aboutus05'   => 'PT. Amerta Indah Otsuka berkomitmen untuk memberikan yang terbaik pada konsumen dan masyarakat sesuai dengan misinya untuk menjadi perusahaan yang brilian, dengan memberikan kontribusi yang signifikan dan terpercaya bagi konsumen serta masyarakat.', 
    'txt_aboutus06'   => 'Komitmen tersebut diwujudkan dengan kegiatan operasional yang menjunjung tinggi kualitas dan standarisasi baik lokal maupun internasional yang telah diakui dunia. Komitmen dan kerja keras PT. Amerta Indah Otsuka telah diakui berbagai pihak melalui berbagai sertifikat sebagai berikut:',
    'txt_aboutus07'   => 'OUR JOURNEY IN PROVIDING THE BEST<br/> FOR INDONESIA',
    
    'txt_pocari01'    => 'POCARI SWEAT The ION Supply Drink',
    'txt_pocari02'    => 'The drink that replenishes the lost body ION. Keep your ION balance with POCARI SWEAT for healthy life and better activities!',
    'txt_pocari03'    => 'PACKAGES',
    'txt_soyjoy01'    => 'is the pioneer of healthy snack made wholly from soy flour and natural dried fruits. Soy and fruit as SOYJOY basic ingredient contains of essential nutrition such as protein, fiber, and vitamin. SOYJOY also made with no artificial sweetener and preservatives.',
    'txt_soyjoy02'    => 'VARIETY of FLAVOURS',
    'txt_ionessence01'=> 'POCARI SWEAT The ION Supply Drink',
    'txt_ionessence02'=> 'IONESSENCE is a low-calorie hypotonic drink (11 kcal/100mL). To prevent dehydration and keep skin moisture. Sensitive Skin, Dry & Dull Skin, Chapped Lips. Dehydration to skin may cause the loss of skin natural moisturizer usually called as Natural Moisturizing Factor (NMF).',

    'txt_csr'         => 'Through <strong>Satu Hati</strong> program, PT. Amerta Indah Otsuka has a mission to constantly contribute in enhancing a better living for people and the environment.',
    
    'txt_tour01'      => 'TERMS & CONDITIONS',
    'txt_tour02'      => 'REGISTRATION FORM',
    'txt_tour03'      => 'During visit, visitors are requested to:',
    'txt_tour04'      => 'VISIT WEBSITE',
    'txt_tour05'      => 'Kejayan Factory',
    'txt_tour06'      => 'Sukabumi Factory',
    'txt_tour07'      => 'Tour Schedule',
    'txt_tour08'      => 'Available',
    'txt_tour09'      => 'Not Available',
    'txt_tour10'      => '*Choose date and time to visit',
    'txt_tourtnc01'   => 'Factory tour is open for schools, universities, or other institutions, the minimum age for visitor is third grade elementary school students.',
    'txt_tourtnc02'   => 'Visitor must be healthy, not affected to any disease and body temperature not more than 37.50C when visiting.',
    'txt_tourtnc03'   => 'Production process may stop at anytime, thus production process will not be shown directly, yet only by video.',
    'txt_tourtnc04'   => 'No visits on Saturday/Sunday/Public holiday.',
    'txt_tourtnc05'   => 'Capacity of visitors is minimum 20 people max 100 people.',
    'txt_tourtnc06'   => 'Visitor must dress politely and do not wear sandals/slippers.',
    'txt_tourtnc07'   => 'Availability of date and time of visit that you have proposed will be confirmed in advance. If the requested date and time is not available you may change the date and time to adjust.',
    'txt_tourtnc08'   => 'Not smoking in the factory area',
    'txt_tourtnc09'   => 'Not littering.',
    'txt_tourtnc10'   => 'Not taking picture/video at the production process corridor.',
    'txt_tourtnc11'   => 'Follow the lead from the guide during the visit for visitor’s safety.',
    'txt_tourtnc12'   => 'Not taking foods and beverages into factory area.',
    'txt_tourtnc13'   => 'When visiting, visitor should:',

    'txt_form_tour01'   => 'Visiting Date',
    'txt_form_tour02'   => 'Tour Location',
    'txt_form_tour03'   => 'Full Name',
    'txt_form_tour04'   => 'Institution Name',
    'txt_form_tour05'   => 'Total Participants',
    'txt_form_tour05_01'=> 'Total Participants 20-100',
    'txt_form_tour06'   => 'Address',
    'txt_form_tour07'   => 'Telephone & HP number',
    'txt_form_tour08'   => 'Email',
    'txt_form_tour09'   => 'Message & Caption',

    'txt_career01'      => 'BE PART OF PT AMERTA INDAH OTSUKA AND CREATE YOUR FUTURE',
    'txt_career02'      => 'In PT Amerta Indah Otsuka, everyone/employee always grow and develop. We believe that the growth of the company should be in line with the growth of employees. Therefore, as part of PT Amerta Indah Otsuka, You will obtain various development programs which will definitely required for Your career path.', 
    'txt_career03'      => 'INDIVIDUAL DEVELOPMENT PLAN',
    'txt_career04'      => 'Every employee has their own potential and capability that must be developed in order to contribute their best performance. Therefore, we develop Individual Development Plan (IDP) based on competency. Through the IDP employees can seek and join trainings, special projects or other development programs for a year. The trainings planned are related to the technicality of work or self development.',
    'txt_career05'      => 'SCHOLARSHIP',
    'txt_career06'      => 'Education is a lifetime process. As part of PT Amerta Indah Otsuka, You are fully supported to obtain higher education. This support is conducted through scholarship awards in recognized universities in Indonesia and overseas. Therefore, You can improve competency as well as building your future career.',
    'txt_career07'      => 'GRADUATE DEVELOPMENT PROGRAM',
    'txt_career08'      => 'Do you want to achieve a brilliant career in no time? We challenge you to join our “graduate development program”. We are looking for remarkable people wih outstnding character to join this program. Individuals with high spirit, sense of leadership, strong analytical skill and able to communicate well, we are welcoming you into this program.',
    'txt_career09'      => 'PT Amerta Indah Otsuka is a part of Otsuka Pharmaceutical Co., Ltd. with leading products such as Pocari Sweat, SOYJOY and Ionessence. Otsuka appreciates people with the willingness to move forward and has the courage to make better changes.',
    'txt_career10'      => 'PT Amerta Indah Otsuka and bring the better changes for the better health.',
    'txt_career11'      => 'Location',
    'txt_career12'      => 'Due Date',
    
    'txt_distribution'  => 'ADDRESS',
    
    'txt_csr01'  => '25.000 Tree Adoption',
    'txt_csr02'  => 'The conservation area of Taman Nasional Halimun Gunung Salak is a catchment area and oxygen supplier also has biological diversity. The adoption of the trees is one of the pro-active efforts of PT. Amerta Indah Otsuka to save Indonesia’s forest and to educate people about the importance of preserving the nature.',
    'txt_csr03'  => 'Donating 250 Trash Bins',
    'txt_csr04'  => 'PT. Amerta Indah Otsuka helps  to provide 250 trash bins in Cilepus Beach and Karang Hawu Beach, Sukabumi on April 2011. The trash bins were recycled from Pocari Sweat’s drum material  so that this activity would contribute indirectly to the reduction of plant waste.',
    'txt_csr05'  => 'Bina Posyandu Mandiri',
    'txt_csr06'  => 'PT. Amerta Indah Otsuka give development to Posyandu’s future committees around the factory as an attempt to make the village nearby be a healthy village by optimizing the potential they have.',
    'txt_csr07'  => 'Dengue Fever Education Program',
    'txt_csr08'  => 'The high amount of dengue fever cases is one of the cause that trig PT. Amerta Indah Otsuka to educate the public  about this disease. Began on 2011, in this activity, PT. Amerta Indah Otsuka educate the people on how to prevent and how to overcome dengue fever. This activity was done in order to decrease the mortality number caused by dengue fever.',


    'explorion_title' => 'Explorion Tour is the latest factory visit program from PT Amerta Indah Otsuka, emphasizing in the element of experience and digital technology. Visitors can engage in an innovative and interactive experience through a smart digital gadget and a theater game called Explorion Theater.',
    'explorion_desc_1' => 'With this new concept, PT Amerta Indah Otsuka hopes that Explorion Tour can provide education to the visitors in a fun and exciting way. Pocari Sweat factory visit, which has been opened since 2008, is PT Amerta Indah Otsuka’s commitment to keep the “open space factory” principle alive.',
    'explorion_desc_2' => 'The factory visit program is also conducted based on Otsuka’s philosophy, which is “Otsuka people creating new product for better health worldwide”. PT Amerta Indah Otsuka hopes this innovation can provide knowledge about the importance of ion to improve the health of Indonesian people.',

    'factory_title' => 'Since 2004, PT Amerta Indah Otsuka (AIO) has opened a factory visit program for  public. One of them is Kejayan Factory in Pasuruan, East Java.',
    'factory_desc_1' => 'Kejayan Factory was built on a 11 Ha land, while its building has a total area of 19.000 m2. In one minute, Kejayan Factory is able to produce Pocari Sweat in 600 PET bottles. This process is slightly different from the one AIO does in Sukabumi Factory because it still uses O-Hot (Otsuka Hot-Fill) technology there. Meanwhile, Kejayan Factory already uses OC (Otsuka Clean) technology. It makes Kejayan Factory become the first Otsuka factory outside Japan that uses OC technology.',
    'factory_desc_2' => 'When AIO use O-Hot technology, the bottle filling process can only be performed when the product has hot temperature, while OC technology allows us to undergo that process when the product is at room temperature. The PET bottle design with OC technology has also experienced some changes, which is 20% lighter than the ones with O-Hot technology. It means that the amount of resin we use in the production process has been lowered, so it will decrease the carbon emissions.',
    'factory_desc_3' => 'In Kejayan Factory, AIO has also used its own powerhouse with gas turbine. This has something to do with the fact that the aseptic production process is very susceptive with the power supply disruption.',

//    home new
    'home_latest' => 'Latest News',
    'title_lao' => 'Life At Amerta Indah Otsuka',
    'title_feed' => 'Feeds Updates',
    'txt_nutrition' => 'NEUTRACEUTICAL (Nutrition +Pharmaceutical)',
    'txt_nutrition_desc' => 'We believe everyone has the right to live a long and healthy life. Driven by our excellent scientific research and a desire to promote healthy lifestyle,<br>PT Amerta Indah Otsuka created the neutraceutical concept to give people sense of a better, hopeful future. We provide a range of innovative products that save and improve lives.',
    'txt_about_awards' => 'AWARD AND CERTIFICATE',
    'txt_about_awards_desc' => '<p>We believe everyone has the right to live a long and healthy life. Driven by our excellent scientific research and a desire to promote healthy lifestyle, PT Amerta Indah Otsuka created the neutraceutical concept to give people sense of a better, hopeful future. We provide a range of innovative products that save and improve lives.</p>',
    'txt_kirim' => 'Submit',
    'home_news' => 'See All',
    'home_detail' => 'Read More',
    'home_story' => 'Their Story',
    'home_detail_Story' => 'See More Stories',
    'home_story2' => 'The effort to run the best vision and achieve mutual success in diversity',

 //   about us
 'about_desc' => '<p><span class="first-letter">F</span>ounded in 1997, PT Amerta Indah Otsuka was built through a joint venture between Otsuka Pharmaceutical Japan and PT Kapal Api. Firstly the company was known as PT Kapal Indah Otsuka before changing to its current name in 1999. The company’s very first product was Pocari Sweat.</p>
                <p>As the company started to grow, in 2004 PT Amerta Indah Otsuka opened their first manufacturing facility in Sukabumi, West Java. Followed by their manufacturing in Kejayan, East Java 6, years later. Since being established, PT Amerta Indah Otsuka has reached customer through focused product distribution strategy. The distribution was not only done through the official branch office, but also from distributors. Thus, the companies product has managed to fulfill the customers need from all across Indonesia.
Eventually, the company’s product managed to reach their customer from all across Indonesia.</p>
                <p>At PT Amerta Indah Otsuka, robust quality control and reliable supply are very important. Thus, the company implemented Quality Management System of ISO 9001:2008, Food Safety System of ISO 22000 : 2005, and Environment Management System of ISO 14001 : 2004.</p>',   
'about_title' => 'About Us',
'about_msg' => 'Message From President Director',
'about_visi' => 'Our Purpose',
'about_product' => 'Product Concept',
'about_history' => 'Our History',

    
/* --------------------------brand----------------------------------*/
// Pocari new    

'pocari_header' => 'The drink that easily replenish your body’s ION lost.<br> Maintain your body’s ION balance with Pocari Sweat For a better and healthy life.',    
'pocari_title' => 'THE BIRTH OF POCARI SWEAT',
'pocari_story_1' => 'About 40 years ago, an Otsuka researcher named Rokuro Harima was on a business trip to Mexico, when he was hospitalized due to diarrhea. The doctor told him to get enough water and nutrition, Harima saw a doctor drinking an I.V solution to rehydrate himself after finishing a surgery. With this idea, Otsuka researchers began to develop a rehydration beverage that could replenish the water and electrolytes (ions) lost while sweating during daily activities.',  
'pocari_story_2' => 'When society had become more health conscious, the researchers, led by Harima and Takaichi, sought to create a health beverage for everyone in situations where they sweat.', 
'pocari_story_3' => 'Throughout years of keen research, researchers came up with over 1,000 prototype beverages that recreated the components of everyday perspiration. In order to test the product concept to develop a drink that tastes great especially after sweating, the researchers climbed a mountain to try out the test products for themselves. With that, the unprecedented water and electrolyte replenishment beverage Pocari Sweat was launched in 1980. ',
'pocari_story_4' => 'Today, Pocari Sweat is loved not only in Japan ; it is a favorite in 17 countries and regions. By adapting to each diverse culture around the world while staying true to the unchanging, science based concept of a beverage for situations where people perspire, Pocari Sweat has won a special place in people’s hearts for helping them stay healthy.',
    
'what_is_ion_title' => 'What is ION?',
'what_is_ion' => 'Ion is part of electrolyte, which are mineral salts that are naturally present in our body. Electrolytes in our bodies will break down into ions, with both have positive and negative load and also necessary for the body. Electrolytes cannot be produced by our own body, so got it either through food or beverages that we consume.',
'pocari_for_body' => 'Pocari Sweat for Our Body', 
'pocari_for_body_detail' => 'The lack of body fluids will decrease our body’s endurance, performance, and also can cause indigestion. Pocari Sweat has a composition that similar to the body fluids so it will be absorbed faster and quickly replace the fluids and ions lost in our body. Therefore, people can maintained their health and actively living their life.',    
'pocari_sweat_web' => 'Visit Pocari Sweat Web',
    

// Soyjoy new    

'soyjoy_header' => 'SOYJOY is a low GI (Glycemic Index) snacks, <br>which is made entirely from soy flour and drained fruits. ',    
'soyjoy_title' => 'SOY PRODUCTS + SOLUTIONS = SOYLUTION',
'soyjoy_story_1' => 'With the concept of \'Soylution\' where soybeans can provide solutions to health and environmental problems, Otsuka Pharmaceutical has developed a new form of soy products. Otsuka offered new and fun ways to people all around the world so they could enjoy the benefits of soy nutrients. ',  
'soyjoy_story_2' => 'The concept behind SOYJOY is \'to allow consumers to choose and enjoy soy products\'. SOYJOY makes soy nutrients easily consumed by people despite of their busy life. SOYJOY offers five delicious flavors to enjoy the goodness in soy  Hawthorn Berry, Almond Raisin, Strawberry, Banana and Almond Chocolate.', 
'soyjoy_story_3' => 'as a healthy snack, SOYJOY has a sensibleand easy to carry package. Through the oven bake process, SOYJOY have a tangible quality up to 8 until 10 months when stored properly and its flavor and nutrients will stay the same. ',
'soyjoy_story_4' => 'SOYJOY is included under the category of low GI snacks so it will provide a longer satiety and control our appetite. The time advised to eat SOYJOY is prior eating a complete meal. ',
    
'what_is_lowgi_title' => 'What Is a Low GI food? ',
'what_is_lowgi' => 'Low GI foods can be digested slowly, thus, the sugar levels in our body will rise gradually. Therefore, the sugar levels do not rise so sudden or drops significantly. Low GI foods, such as SOYJOY is good for diabetics.',
'soyjoy_advantage' => 'SOYJOY for the Body', 
'soyjoy_advantage_detail_1' => 'SOYJOY is made from whole soy flour, high in fiber and protein, and can be an alternative as a healthy snack that can be eaten in between meals. Soybeans that being used are a non GMO soybeans. It means the soybeans is not going through the process of genetic engineering, and also do not use any artificial sweeteners and preservatives. ',    
'soyjoy_advantage_detail_2' => 'In addition to its low GI, SOYJOY also contains isoflavones and dietary fiber which can reduce bad cholesterol level and the total cholesterol in the body.',   
'soyjoy_web' => 'Visit SOYJOY Web',
    

// Ionessence new    

'ionessence_header' => 'IONESSENCE is a low calorie electrolyte drinks <br>(11 kcal / 100mL). This drink is good to keep the skin moisture and prevents dehydration of the skin',    
'ionessence_title' => 'THE BIRTH IONESSENCE',
'ionessence_story_1' => 'As a company committed to health, PT Amerta Indah Otsuka launched IONESSENCE in 2015. This beverage product contains electrolytes that are similar to natural moisturizer or Natural Moisturizing Factor (NMF) in the skin that can affect the performance of the skin inactivating enzymes and other substances such as collagen and elastin fibers. ',  
'ionessence_story_2' => 'NMF can be reduced due to several factors such as exposure to sunlight, air conditioning, improper use of cosmetics, as well as unhealthy lifestyle such as consuming alcohol, and lower electrolytes consumption.', 
'ionessence_story_3' => 'With a low calorie content, IONESSENCE can be consumed anytime and anywhere without worrying about the amount of calorie intake. It is suggested to routinely consume the product for at least 1 month before going to bed, to help detoxification process at night and helps absorb nutrients from food within our body. So, in the morning we could expect our skin to become moist after absorbs the liquid. ',
    
'lack_of_imf' => 'When the Body is Lacking of NMF ',
'lack_of_imf_detail' => 'The lack of NMF in the skin will cause chapped lips, dry and dull skin. Also, could caused skin irritation . Therefore, it is recommended to consume IONESSENCE. With the electrolyte composition similar to NMF, IONESSENCE can assist the formation of the NMF in activating enzymes and substances such as collagen and elastin fibers within our body. ',
'ionessence_advantage' => 'IONESSENCE for Body ', 
'ionessence_advantage_detail_1' => 'IONESSENCE helps the absorption of fluids in the body through skin layer and shapes skin’s Natural Moisturizing Factor (NMF). So our skin will looks healthy, moist, and supple. ',   
'ionessence_advantage_detail_2' => 'IONESSENCE’s electrolyte content consists of Sodium (Na), Potassium (K), calcium (Ca), magnesium (Mg), and Lactate.',   
'ionessence_web' => 'Visit Ionessence Web',    
    
// Career new
'career_desc' => 'PT Amerta Indah Otsuka is a part of Otsuka Parmaceutical Co., Ltd Japan which has popular product as Pocari Sweat, Soyjoy, and IONESSENCE. Otsuka gives appreciation for people who have dreams to desire move forward and dare to make a better change.',
'career_join' => 'Join Us',
'career_title' => 'Start your journey with<br>PT Amerta Indah Otsuka and together we create a healthy future.',
'career_upload' => 'Browse by click upload icon',
'career_file' => 'File type',
'career_list' => 'LIST <br/>OF VACANCY',
'career_share' => 'Share this vacancy',
'career_location' => 'Location',
'career_time' => 'Deadline',
'career_proceed' => 'Proceed',
'career_form' => 'APPLICATION <br/>FORM',
'career_position' => 'Position',
'career_fullname' => 'Full Name',
 'career_pob' => 'Place of Birth',
 'career_dob' => 'Date of Birth',
 'career_gender' => 'Gender',
 'career_gd_male' => 'Male',
 'career_gd_female' => 'Female',
 'career_st_single' => 'Single',
 'career_st_married' => 'Married',
 'career_st_widower' => 'Widower',
 'career_address' => 'Address',
 'career_city' => 'City',
 'career_country' => 'Country',
 'career_hp' => 'No. Handphone',
 'career_name' => 'name',
 'career_pob2' => 'place of birth',

// Explorion new
    'explorion_detail' => 'By carrying the principle of open factory, PT Amerta Indah Otsuka is giving the opportunity for public to see the production process of Pocari Sweat at their manufacturing facilities in Sukabumi and Kejayan since 2008.',
    'explorion_sub_title' => 'One Activity,<br/> Thousand Experiences, Million Smiles',
    'explorion_reg_tour' => 'Register Now',
    'explorion_form' => 'VISIT FORM',
    'explorion_reg' => 'REGISTRATION',
    'explorion_tnc' => 'TERMS & CONDITIONS',
    'explorion_schedule' => 'Event Schedule',
    'explorion_schedule2' => 'See Schedule',
    'explorion_sbmt_form' => 'Submit Form',
    'explorion_note' => 'Notes',

// Galeri Explorion
    'galeri_detail' => 'Lets share your pictures in Instagram directly when you watched the birth of an innovation process, in health and nutrition, in Sukabumi Otsuka Factory & Kejayan with hashtag <strong>#kunjunganpabrikAIO</strong> like them.',
    'galeri_title' => 'Photo Gallery </br> Updates',

// LAO Page
    'lao_title' => '<span>Diversity is the beginning of innovation</span>',
    'lao_msg' => '<p>"We continously make differentiation and do more innovation just to make us distinct. Along the way we keep on trying to find what makes us differ. Once it is found, we dedicate all efforts and time solely on what only Otsuka can do, a better health worldwide."<br/><br/></p>',
    'lao_filosofi' => '<p>We strive to utilize our unique assets and skills to develop differentiating scientific solutions which contribute to the lives of people worldwide in the form of innovative and creative products, to cultivate a culture and dynamic corporate climate reflecting our vision as a health company.</p>',
    'lao_desc' => '<p>These monuments embodying the Otsuka philosophy, reminding all who visit the birthplace of Otsuka in Tokushima of the importance of <br>BEING CREATIVE And OPEN MINDED To New Ideas.</p>',
    'lao_title2' => 'Equal Opportunity',
    'lao_desc2' => '<p class="text-left">As an individual, our employee has their own uniqueness and potential. all facets of diversity are important to us and we strive to create an environment where everyone regardless of age, gender, race, religious background and nationality could get equal chances and realize their potential Our success in creating a truly diverse and inclusive workplace will directly impact our employee’s ability to work professionally.</p>',
    'lao_title3' => 'Otsuka’s Work life',
    'lao_desc3' => '<p class="text-left">Our people make our business. Therefore, we’ve strived to make a supportive working environment by giving our employee supporting facilities and token of appreciation. Through Talent Management program, everyone has a chance to develop their potential. We also provide training and scholarship abroad to enhance our people’s quality.</p>',
    'lao_story' => 'Their Story',
    'lao_vision' => 'Vision of Leadership ',
    'lao_filosofi2' => 'Corporate Philosophy',
    'lao_join' => 'Join Us',


//CSR
    'csr_title' => 'Corporate Social Responsibility',
    'sh' => 'The corporate social responsibility towards the education, environment, and healthy',

// CSR Satu Hati Cerdaskan Bangsa Page
    'shcb_main_title' => 'YAYASAN SATU HATI CERDASKAN BANGSA',
    'shcb_first_paragraph' => 'PT Amerta Indah Otsuka’s social contributions started when the quake-hit Yogyakarta in 2006, resulted  in massive destruction of the school buildings within the area. At that time, a charity concert was held to raise donations to moved million hearts of people to contribute to the future of education in Yogyakarta. ',
    'shcb_second_paragraph' => 'Afterwards, PT Amerta Indah Otsuka’s movement regarding education continues. In 2007, the "Satu Hati Cerdaskan Bangsa” program was established as the company’s Corporate Social Responsibility program in the field of education. PT Amerta Indah Otsuka’s commitment and consistencyto contribute in advancing the quality of education in Indonesia later manifested through the establishment of the Yayasan Satu Hati Cerdaskan Bangsa in 2011.',
    
    'shcb_title' => 'PT Amerta Indah Otsuka’s Social Contribution Towards Education ',

// CSR Satu Hati Cerdaskan Bangsa Page

    'shpl_paragraph' => 'Our life and society are very dependent on nature. Therefore, PT Amerta Indah Otsuka contribute to preserve nature and the environment.',
    'shpl_title' => 'PT Amerta Indah Otsuka’s Social Contribution Towards Environment ',

// CSR Satu Hati Sehatkan Bangsa Page

    'shsb_main_paragraph' => 'PT Amerta Indah Otsuka is committed to contribute to the improvement of public health in Indonesia through series of contributions ',
    'shsb_title' => 'PT Amerta Indah Otsuka’s Social Contribution towards Public Health',
    
    
    // Header Menu
    'menu_about' => 'About Us',
    'menu_news' => 'News',
    'menu_career' => 'Career',
    'menu_distribution' => 'Distribution',
    'menu_contact' => 'Contact Us',
    'menu_lang' => 'Language',
    'menu_search' => 'Search',

    // Sub Menu

    'menu_history' => 'History',
    'menu_press' => 'Press',
    'menu_join' => 'Join Us',

    // Footer
    'foot_follow' => 'Follow Us',
    'foot_pvp' => 'Privacy Policy',
    'foot_tnc' => 'Term of Use',

    //News and Press

    'news_more' => 'More Articles',
    'news_share' => 'Share This Article...',
    'news_title' => 'NEWS RELEASE',
    'news_title2' => 'PRESS RELEASE',
    'news_detail' => 'Read More',
    'news_more2' => 'More News',

    //Distribution
    'txt_distri_1' => 'Distribution Map',
    'txt_distri_2' => 'Distribution Area'
);

