<?php

return array(
    '_404_' => 'welcome/404', // The main 404 route
    '_root_' => 'pages/frontend/home', // The default route

    'subscribe'   =>  'newsletter/frontend/subscription/entry',
    'register-visitor' => 'factorytour/frontend/registration/register',


// Backend //

    // Installation
    'backend/installation' => 'backend/install/setup',

    // Backend routes
    'backend' => 'backend/dashboard', // The default route for backend
    'backend/sign-in' => 'backend/dashboard/sign_in',
    'backend/sign-out' => 'backend/dashboard/sign_out',
    'backend/change-password' => 'backend/dashboard/change_current_password',
    'backend/my-profile' => 'backend/dashboard/my_profile_form',
    'backend/no-permission' => 'error/no_permission/backend',


// About Us //

    // All About Us
    'backend/about' => 'about/backend/',
    'backend/about/add' => 'about/backend/form/0',
    'backend/about/edit/(:num)' => 'about/backend/form/$1',
    'backend/about/delete/(:num)' => 'about/backend/delete/$1',

    // Award & Certificate
    'backend/about/awardcertificate' => 'about/backend/awardcertificate',
    'backend/about/awardcertificate/add' => 'about/backend/awardcertificate/form/0',
    'backend/about/awardcertificate/edit/(:num)' => 'about/backend/awardcertificate/form/$1',
    'backend/about/awardcertificate/delete/(:num)' => 'about/backend/awardcertificate/delete/$1',

    // President
    'backend/about/president' => 'about/backend/president',
    'backend/about/president/add' => 'about/backend/president/form/0',
    'backend/about/president/edit/(:num)' => 'about/backend/president/form/$1',
    'backend/about/president/delete/(:num)' => 'about/backend/president/delete/$1',


// About Banner //

    // Organize
    'backend/about/banner' => 'about/backend/banner',
    'backend/about/banner/add' => 'about/backend/banner/form/0',
    'backend/about/banner/edit/(:num)' => 'about/backend/banner/form/$1',
    'backend/about/banner/delete/(:num)' => 'about/backend/banner/delete/$1',

    // Banner
    'backend/about/image' => 'about/backend/image',
    'backend/about/image/add' => 'about/backend/image/form/0',
    'backend/about/image/edit/(:num)' => 'about/backend/image/form/$1',
    'backend/about/image/delete/(:num)' => 'about/backend/image/delete/$1',

// Admin Management //

    // Admin User
    'backend/admin-user' => 'adminmanagement/adminuser',
    'backend/admin-user/add' => 'adminmanagement/adminuser/form/0',
    'backend/admin-user/edit/(:num)' => 'adminmanagement/adminuser/form/$1',
    'backend/admin-user/delete/(:num)' => 'adminmanagement/adminuser/delete/$1',
    'backend/admin-user/reset-password/(:num)' => 'adminmanagement/adminuser/reset_password/$1',

    // Admin Role Permission
    'backend/admin-role-permission' => 'adminmanagement/adminrolepermission',
    'backend/admin-role-permission/add' => 'adminmanagement/adminrolepermission/form/0',
    'backend/admin-role-permission/edit/(:num)' => 'adminmanagement/adminrolepermission/form/$1',
    'backend/admin-role-permission/delete/(:num)' => 'adminmanagement/adminrolepermission/delete/$1',
    'backend/admin-role-permission/assign-admin/(:num)' => 'adminmanagement/adminrolepermission/assign_admin/$1',
    'backend/admin-role-permission/do-assign-admin/(:num)/(:num)' => 'adminmanagement/adminrolepermission/do_assign_admin/$1/$2/1',
    'backend/admin-role-permission/do-unassign-admin/(:num)/(:num)' => 'adminmanagement/adminrolepermission/do_assign_admin/$1/$2/0',
    'backend/admin-role-permission/set-permission/(:num)' => 'adminmanagement/adminrolepermission/set_permission/$1',

    // Basic Setting
    'backend/setting' => 'adminmanagement/setting',


// Contact Us //
    // Content
    'backend/contactus' => 'contactus/backend',
    'backend/contactus/add' => 'contactus/backend/form/0',
    'backend/contactus/edit/(:num)' => 'contactus/backend/form/$1',
    'backend/contactus/delete/(:num)' => 'contactus/backend/delete/$1',


// Career //
    // Content
    'backend/career' => 'career/backend',
    'backend/career/add' => 'career/backend/form/0',
    'backend/career/edit/(:num)' => 'career/backend/form/$1',
    'backend/career/delete/(:num)' => 'career/backend/delete/$1',

    // List
    'backend/career/content' => 'career/backend/careercontent',
    'backend/career/content/add' => 'career/backend/careercontent/form/0',
    'backend/career/content/edit/(:num)' => 'career/backend/careercontent/form/$1',
    'backend/career/content/delete/(:num)' => 'career/backend/careercontent/delete/$1',

// Facebook Management //
    'backend/fb-participant' => 'facebookmanagement/participant',
    'backend/fb-setting' => 'facebookmanagement/setting',

    // Twitter Management
    'backend/twitter-setting' => 'twittermanagement/setting',

// FAQ //

    // List
    'backend/faq' => 'faq/backend',
    'backend/faq/add' => 'faq/backend/form/0',
    'backend/faq/edit/(:num)' => 'faq/backend/form/$1',
    'backend/faq/delete/(:num)' => 'faq/backend/delete/$1',

// Explorion //

    // List
    'backend/explorion' => 'explorion/backend',
    'backend/explorion/add' => 'explorion/backend/form/0',
    'backend/explorion/edit/(:num)' => 'explorion/backend/form/$1',
    'backend/explorion/delete/(:num)' => 'explorion/backend/delete/$1',

    // Image
    'backend/explorion/image' => 'explorion/backend/image',
    'backend/explorion/image/add' => 'explorion/backend/image/form/0',
    'backend/explorion/image/edit/(:num)' => 'explorion/backend/image/form/$1',
    'backend/explorion/image/delete/(:num)' => 'explorion/backend/image/delete/$1',



// Factory Tour //

    // List
    'backend/factorytour' => 'factorytour/backend',
    'backend/factorytour/add' => 'factorytour/backend/form/0',
    'backend/factorytour/edit/(:num)' => 'factorytour/backend/form/$1',
    'backend/factorytour/delete/(:num)' => 'factorytour/backend/delete/$1',

    // Image
    'backend/factorytour/image' => 'factorytour/backend/image',
    'backend/factorytour/image/add' => 'factorytour/backend/image/form/0',
    'backend/factorytour/image/edit/(:num)' => 'factorytour/backend/image/form/$1',
    'backend/factorytour/image/delete/(:num)' => 'factorytour/backend/image/delete/$1',

    // Side Image
    'backend/factorytour/sideimage' => 'factorytour/backend/sideimage',
    'backend/factorytour/sideimage/add' => 'factorytour/backend/sideimage/form/0',
    'backend/factorytour/sideimage/edit/(:num)' => 'factorytour/backend/sideimage/form/$1',
    'backend/factorytour/sideimage/delete/(:num)' => 'factorytour/backend/sideimage/delete/$1',

    // Schedule
    'factorytour-api/schedule' => 'factorytour/backend/schedule',
    'backend/factorytour/scheduledetail' => 'factorytour/backend/scheduledetail',

    // History
    'backend/factorytour/schedulehistory' => 'factorytour/backend/schedulehistory',


// History //

    // Organize
    'backend/history' => 'history/backend',
    'backend/history/add' => 'history/backend/form/0',
    'backend/history/edit/(:num)' => 'history/backend/form/$1',
    'backend/history/delete/(:num)' => 'history/backend/delete/$1',

    // Image
    'backend/history/image' => 'history/backend/image',
    'backend/history/image/add' => 'history/backend/image/form/0',
    'backend/history/image/edit/(:num)' => 'history/backend/image/form/$1',
    'backend/history/image/delete/(:num)' => 'history/backend/image/delete/$1',

// Home Banner //

    // Organize
    'backend/homebanner' => 'homebanner/backend',
    'backend/homebanner/add' => 'homebanner/backend/form/0',
    'backend/homebanner/edit/(:num)' => 'homebanner/backend/form/$1',
    'backend/homebanner/delete/(:num)' => 'homebanner/backend/delete/$1',

    // Banner
    'backend/homebanner/image' => 'homebanner/backend/image',
    'backend/homebanner/image/add' => 'homebanner/backend/image/form/0',
    'backend/homebanner/image/edit/(:num)' => 'homebanner/backend/image/form/$1',
    'backend/homebanner/image/delete/(:num)' => 'homebanner/backend/image/delete/$1',

    // Side Image
    'backend/homebanner/sideimage' => 'homebanner/backend/sideimage',
    'backend/homebanner/sideimage/add' => 'homebanner/backend/sideimage/form/0',
    'backend/homebanner/sideimage/edit/(:num)' => 'homebanner/backend/sideimage/form/$1',
    'backend/homebanner/sideimage/delete/(:num)' => 'homebanner/backend/sideimage/delete/$1',


// Pages //
    'backend/pages' => 'pages/backend',
    'backend/pages/add' => 'pages/backend/form/0',
    'backend/pages/edit/(:num)' => 'pages/backend/form/$1',
    'backend/pages/delete/(:num)' => 'pages/backend/delete/$1',


// Media Uploader //

    // Common
    'backend/media-uploader/common' => 'mediauploader/common',
    'backend/media-uploader/common/upload' => 'mediauploader/common/upload',

    // Campaign
    'backend/media-uploader/campaign' => 'mediauploader/campaign',
    'backend/media-uploader/campaign/upload' => 'mediauploader/campaign/upload',

    // LAO Photo
    'backend/media-uploader/lao-photo' => 'mediauploader/laophoto',
    'backend/media-uploader/lao-photo/upload' => 'mediauploader/laophoto/upload',

    // Press Room Image
    'backend/media-uploader/press-image' => 'mediauploader/pressimage',
    'backend/media-uploader/press-image/upload' => 'mediauploader/pressimage/upload',

    // About Banner
    'backend/media-uploader/aboutbanner' => 'mediauploader/aboutbanner',
    'backend/media-uploader/aboutbanner/upload' => 'mediauploader/aboutbanner/upload',

    // Award & Certificate
    'backend/media-uploader/awardcertificate' => 'mediauploader/awardcertificate',
    'backend/media-uploader/awardcertificate/upload' => 'mediauploader/awardcertificate/upload',

    // Home Banner
    'backend/media-uploader/homebanner' => 'mediauploader/homebanner',
    'backend/media-uploader/homebanner/upload' => 'mediauploader/homebanner/upload',

    // History
    'backend/media-uploader/history' => 'mediauploader/history',
    'backend/media-uploader/history/upload' => 'mediauploader/history/upload',

    // Career Content
    'backend/media-uploader/careercontent' => 'mediauploader/careercontent',
    'backend/media-uploader/careercontent/upload' => 'mediauploader/careercontent/upload',

    // CSR Image
    'backend/media-uploader/csrimage' => 'mediauploader/csrimage',
    'backend/media-uploader/csrimage/upload' => 'mediauploader/csrimage/upload',

    // Factory Tour
    'backend/media-uploader/factorytour' => 'mediauploader/factorytour',
    'backend/media-uploader/factorytour/upload' => 'mediauploader/factorytour/upload',

    // Explorion
    'backend/media-uploader/explorion' => 'mediauploader/explorion',
    'backend/media-uploader/explorion/upload' => 'mediauploader/explorion/upload',

    // Other
    'backend/media-uploader/other' => 'mediauploader/other',
    'backend/media-uploader/other/upload' => 'mediauploader/other/upload',

    // Product
    'backend/media-uploader/product' => 'mediauploader/product',
    'backend/media-uploader/product/upload' => 'mediauploader/product/upload',

    // Promo
    'backend/media-uploader/promo' => 'mediauploader/promo',
    'backend/media-uploader/promo/upload' => 'mediauploader/promo/upload',

    // Gallery
    'backend/media-uploader/gallery' => 'mediauploader/gallery',
    'backend/media-uploader/gallery/upload' => 'mediauploader/gallery/upload',

    // President
    'backend/media-uploader/president' => 'mediauploader/president',
    'backend/media-uploader/president/upload' => 'mediauploader/president/upload',

    // Product Concept
    'backend/media-uploader/productconcept' => 'mediauploader/productconcept',
    'backend/media-uploader/productconcept/upload' => 'mediauploader/productconcept/upload',

    // Product Nutrition
    'backend/media-uploader/productnutrition' => 'mediauploader/productnutrition',
    'backend/media-uploader/productnutrition/upload' => 'mediauploader/productnutrition/upload',

// Campaign Management //
    // List
    'backend/campaign' => 'campaign/backend',
    'backend/campaign/add' => 'campaign/backend/form/0',
    'backend/campaign/edit/(:num)' => 'campaign/backend/form/$1',
    'backend/campaign/delete/(:num)' => 'campaign/backend/delete/$1',

    // Image
    'backend/campaign/image' => 'campaign/backend/image',
    'backend/campaign/image/add' => 'campaign/backend/image/form/0',
    'backend/campaign/image/edit/(:num)' => 'campaign/backend/image/form/$1',
    'backend/campaign/image/delete/(:num)' => 'campaign/backend/image/delete/$1',


// CSR Management //
    // List
    'backend/csr-management' => 'csrmanagement/backend',
    'backend/csr-management/add' => 'csrmanagement/backend/form/0',
    'backend/csr-management/edit/(:num)' => 'csrmanagement/backend/form/$1',
    'backend/csr-management/delete/(:num)' => 'csrmanagement/backend/delete/$1',

    // Image
    'backend/csr-management/image' => 'csrmanagement/backend/image',
    'backend/csr-management/image/add' => 'csrmanagement/backend/image/form/0',
    'backend/csr-management/image/edit/(:num)' => 'csrmanagement/backend/image/form/$1',
    'backend/csr-management/image/delete/(:num)' => 'csrmanagement/backend/image/delete/$1',


// Life At Otsuka //
    'backend/lifeatotsuka' => 'lifeatotsuka/backend',
    'backend/lifeatotsuka/add' => 'lifeatotsuka/backend/form/0',
    'backend/lifeatotsuka/edit/(:num)' => 'lifeatotsuka/backend/form/$1',
    'backend/lifeatotsuka/delete/(:num)' => 'lifeatotsuka/backend/delete/$1',

    // Vision

    'backend/lifeatotsuka/vision' => 'lifeatotsuka/backend/vision',
    'backend/lifeatotsuka/vision/add' => 'lifeatotsuka/backend/vision/form/0',
    'backend/lifeatotsuka/vision/send/(:num)' => 'lifeatotsuka/backend/vision/send/$1',
    'backend/lifeatotsuka/vision/edit/(:num)' => 'lifeatotsuka/backend/vision/form/$1',
    'backend/lifeatotsuka/vision/delete/(:num)' => 'lifeatotsuka/backend/vision/delete/$1',

    // Philosopy

    'backend/lifeatotsuka/philosophy' => 'lifeatotsuka/backend/philosophy',
    'backend/lifeatotsuka/philosophy/add' => 'lifeatotsuka/backend/philosophy/form/0',
    'backend/lifeatotsuka/philosophy/send/(:num)' => 'lifeatotsuka/backend/philosophy/send/$1',
    'backend/lifeatotsuka/philosophy/edit/(:num)' => 'lifeatotsuka/backend/philosophy/form/$1',
    'backend/lifeatotsuka/philosophy/delete/(:num)' => 'lifeatotsuka/backend/philosophy/delete/$1',



    // Channel

    // Home Banner or Main Banner
    'backend/channel/home-banner' => 'channel/backend/homebanner',
    'backend/channel/home-banner/add' => 'channel/backend/homebanner/form/0',
    'backend/channel/home-banner/edit/(:num)' => 'channel/backend/homebanner/form/$1',
    'backend/channel/home-banner/rearrange/(:num)' => 'channel/backend/homebanner/rearrange/$1',
    'backend/channel/home-banner/delete/(:num)' => 'channel/backend/homebanner/delete/$1',

// Event //

    // Organize
    'backend/event' => 'event/backend',
    'backend/event/add' => 'event/backend/form/0',
    'backend/event/edit/(:num)' => 'event/backend/form/$1',
    'backend/event/delete/(:num)' => 'event/backend/delete/$1',

    // Banner
    'backend/event/banner' => 'event/backend/banner',
    'backend/event/banner/add' => 'event/backend/banner/form/0',
    'backend/event/banner/edit/(:num)' => 'event/backend/banner/form/$1',
    'backend/event/banner/delete/(:num)' => 'event/backend/banner/delete/$1',

    // Slide Image
    'backend/event/slideimage' => 'event/backend/slideimage',
    'backend/event/slideimage/add' => 'event/backend/slideimage/form/0',
    'backend/event/slideimage/edit/(:num)' => 'event/backend/slideimage/form/$1',
    'backend/event/slideimage/delete/(:num)' => 'event/backend/slideimage/delete/$1',

    // Image
    'backend/event/image' => 'event/backend/image',
    'backend/event/image/add' => 'event/backend/image/form/0',
    'backend/event/image/edit/(:num)' => 'event/backend/image/form/$1',
    'backend/event/image/delete/(:num)' => 'event/backend/image/delete/$1',

    // Publication
    'backend/event/publication' => 'event/backend/publication',
    'backend/event/publication/add' => 'event/backend/publication/form/0',
    'backend/event/publication/edit/(:num)' => 'event/backend/publication/form/$1',
    'backend/event/publication/delete/(:num)' => 'event/backend/publication/delete/$1',


// Press  //
    'backend/press' => 'press/backend',
    'backend/press/add' => 'press/backend/form/0',
    'backend/press/edit/(:num)' => 'press/backend/form/$1',
    'backend/press/delete/(:num)' => 'press/backend/delete/$1',

// Product //

    // List
    'backend/product' => 'product/backend',
    'backend/product/add' => 'product/backend/form/0',
    'backend/product/edit/(:num)' => 'product/backend/form/$1',
    'backend/product/delete/(:num)' => 'product/backend/delete/$1',

    // Concept
    'backend/productconcept' => 'productconcept/backend',
    'backend/productconcept/add' => 'productconcept/backend/form/0',
    'backend/productconcept/edit/(:num)' => 'productconcept/backend/form/$1',
    'backend/productconcept/delete/(:num)' => 'productconcept/backend/delete/$1',

    // Product Nutrition
    'backend/nutrition/(:num)' => 'nutrition/backend/index/$1',
    'backend/nutrition/(:num)/add' => 'nutrition/backend/form/$1/0',
    'backend/nutrition/(:num)/edit/(:num)' => 'nutrition/backend/form/$1/$2',
    'backend/nutrition/(:num)/delete/(:num)' => 'nutrition/backend/delete/$1/$2',


// Achievement //
    'backend/achievement' => 'achievement/backend',
    'backend/achievement/add' => 'achievement/backend/form/0',
    'backend/achievement/edit/(:num)' => 'achievement/backend/form/$1',
    'backend/achievement/delete/(:num)' => 'achievement/backend/delete/$1',


// Newsletter //

    // Organize Subscription

    'backend/newsletter' => 'newsletter/backend',
    'backend/newsletter/add' => 'newsletter/backend/form/0',
    'backend/newsletter/edit/(:num)' => 'newsletter/backend/form/$1',
    'backend/newsletter/delete/(:num)' => 'newsletter/backend/delete/$1',

    // Broadcast Newsletter

    'backend/newsletter/broadcast' => 'newsletter/backend/broadcast',
    'backend/newsletter/broadcast/add' => 'newsletter/backend/broadcast/form/0',
    'backend/newsletter/broadcast/send/(:num)' => 'newsletter/backend/broadcast/send/$1',
    'backend/newsletter/broadcast/edit/(:num)' => 'newsletter/backend/broadcast/form/$1',
    'backend/newsletter/broadcast/delete/(:num)' => 'newsletter/backend/broadcast/delete/$1',


    // Distributor //
    'backend/distributor' => 'distributor/backend',
    'backend/distributor/add' => 'distributor/backend/form/0',
    'backend/distributor/edit/(:num)' => 'distributor/backend/form/$1',
    'backend/distributor/delete/(:num)' => 'distributor/backend/delete/$1',

    // Province //
    'backend/province' => 'distributor/backend/province',
    'backend/province/add' => 'distributor/backend/province/form/0',
    'backend/province/edit/(:num)' => 'distributor/backend/province/form/$1',
    'backend/province/delete/(:num)' => 'distributor/backend/province/delete/$1',

    // City //
    'backend/city' => 'distributor/backend/city',
    'backend/city/add' => 'distributor/backend/city/form/0',
    'backend/city/edit/(:num)' => 'distributor/backend/city/form/$1',
    'backend/city/delete/(:num)' => 'distributor/backend/city/delete/$1',

    'backend/data-report-applicant' => 'pages/datareportapplicants',
    'backend/data-report-customer' => 'pages/datareportcustomer',
    'backend/data-report-factory-tour' => 'pages/datareportfactorytour',

// History //
    'backend/history' => 'history/backend',
    'backend/history/add' => 'history/backend/form/0',
    'backend/history/edit/(:num)' => 'history/backend/form/$1',
    'backend/history/delete/(:num)' => 'history/backend/delete/$1',

// Gallery //
    'backend/gallery' => 'factorytour/backend/photoalbum',
    'backend/gallery/add' => 'factorytour/backend/photoalbum/form/0',
    'backend/gallery/edit/(:num)' => 'factorytour/backend/photoalbum/form/$1',
    'backend/gallery/delete/(:num)' => 'factorytour/backend/photoalbum/delete/$1',

// Gallery //
    'backend/gallery/banner' => 'factorytour/backend/gallerybanner',
    'backend/gallery/banner/add' => 'factorytour/backend/gallerybanner/form/0',
    'backend/gallery/banner/edit/(:num)' => 'factorytour/backend/gallerybanner/form/$1',
    'backend/gallery/banner/delete/(:num)' => 'factorytour/backend/gallerybanner/delete/$1',

// Testimonial //
    'backend/gallery/testimonial' => 'factorytour/backend/testimonial',
    'backend/gallery/testimonial/add' => 'factorytour/backend/testimonial/form/0',
    'backend/gallery/testimonial/edit/(:num)' => 'factorytour/backend/testimonial/form/$1',
    'backend/gallery/testimonial/delete/(:num)' => 'factorytour/backend/testimonial/delete/$1',

// Comment //
    'backend/gallery/comment' => 'factorytour/backend/comment',
    'backend/gallery/comment/add' => 'factorytour/backend/comment/form/0',
    'backend/gallery/comment/edit/(:num)' => 'factorytour/backend/comment/form/$1',
    'backend/gallery/comment/delete/(:num)' => 'factorytour/backend/comment/delete/$1',

// Applicant //
    'backend/career/applicant' => 'career/backend/careerapplicant',
    'backend/career/applicant/view/(:num)' => 'career/backend/careerapplicant/form/$1',

    // Frontend //



    '(:lang_code)/home' => 'pages/frontend/home',

    // Event
    '(:lang_code)/event' => 'event/frontend/home',
    '(:lang_code)/event/detail/(:slug)' => 'event/frontend/home/detail',
    '(:lang_code)/event/(:selected_year)/(:selected_month)' => 'event/frontend/home',
    '(:lang_code)/event/(:selected_year)/(:selected_month)' => 'event/frontend/home',

    // Career
    '(:lang_code)/career' => 'career/frontend/home',

    // About Us
    '(:lang_code)/about-us' => 'about/frontend/home/',
    '(:lang_code)/about-us/history' => 'about/frontend/home/history',
    '(:lang_code)/about-us/awards-and-certificates' => 'about/frontend/home/awards_and_certificates',

    // Press Room
    '(:lang_code)/our-event' => 'press/frontend/home/',
    '(:lang_code)/brand-news' => 'press/frontend/home/brand_news',
    '(:lang_code)/press-release' => 'press/frontend/home/press_release',
    '(:lang_code)/our-event/detail/(:slug)' => 'press/frontend/home/detail',

    // CSR
    '(:lang_code)/csr' => 'csrmanagement/frontend/home',
//    '(:lang_code)/csr/detail/(:slug)' => 'csrmanagement/frontend/home/detail',
    '(:lang_code)/csr/cerdaskan-bangsa' => 'csrmanagement/frontend/home',
    '(:lang_code)/csr/peduli-lingkungan' => 'csrmanagement/frontend/home/peduli_lingkungan',
    '(:lang_code)/csr/sehatkan-bangsa' => 'csrmanagement/frontend/home/sehatkan_bangsa',


    // Contact Us
    '(:lang_code)/contact-us' => 'pages/frontend/home/contact_us',
    '(:lang_code)/career/sendmail' => 'career/frontend/home/sendmail',

    // Search
    '(:lang_code)/search' => 'pages/frontend/home/search',

    //policy
    '(:lang_code)/policy' => 'pages/frontend/home/policy',

    //term-of-use
    '(:lang_code)/term-of-use' => 'pages/frontend/home/term_of_use',

    //factory
    '(:lang_code)/factory-tour' => 'factorytour/frontend/home/factory_tour',
    '(:lang_code)/explorion-advanture' => 'factorytour/frontend/home',
    //gallery
    '(:lang_code)/gallery-detail' => 'factorytour/frontend/home/gallery_detail',
    '(:lang_code)/change-image' => 'factorytour/frontend/home/change_image',

    //product
    '(:lang_code)/brand/pocari-sweat' => 'campaign/frontend/home',
    '(:lang_code)/brand/soyjoy' => 'campaign/frontend/home/soyjoy',
    '(:lang_code)/brand/ionessence' => 'campaign/frontend/home/ionessence',

    //Life At Otsuka
    '(:lang_code)/lifeatotsuka' => 'lifeatotsuka/frontend/home/index',
    '(:lang_code)/lifeatotsuka/list' => 'lifeatotsuka/frontend/home/get_testimony',

    //distribution
    '(:lang_code)/distribution' => 'distributor/frontend/home',
    '(:lang_code)/distribution/get_city' => 'distributor/frontend/home/get_city',

    //FAQ
    '(:lang_code)/faq' => 'pages/frontend/home/faq',
    '(:lang_code)/faq-soyjoy' => 'pages/frontend/home/faq_soyjoy',
    '(:lang_code)/faq-ionessence' => 'pages/frontend/home/faq_ionessence',

    //revamp

    '(:lang_code)/home-new' => 'pages/frontend/home/home_new',
    '(:lang_code)/about-us-new' => 'about/frontend/home/about_us_new',
    '(:lang_code)/about-us-new/history' => 'about/frontend/home/history_new',
    '(:lang_code)/career-new' => 'career/frontend/home/career_new',
    '(:lang_code)/life-at-otsuka-new' => 'career/frontend/home/life_at_otsuka_new',
    '(:lang_code)/explorion-new' => 'factorytour/frontend/home/factory_tour_new',
    '(:lang_code)/news' => 'press/frontend/home/news',
    '(:lang_code)/news/detail/(:slug)' => 'press/frontend/home/detail_new',
    '(:lang_code)/press' => 'press/frontend/home/press',
    '(:lang_code)/press/detail/(:slug)' => 'press/frontend/home/detail_new',
    '(:lang_code)/distributor-new' => 'distributor/frontend/home/distributor',
    '(:lang_code)/get-data-distributor' => 'distributor/frontend/home/getDistributor',
    '(:lang_code)/get-data-province' => 'distributor/frontend/home/getProvince',
    '(:lang_code)/csr/shcb' => 'csrmanagement/frontend/home/shcb',
    '(:lang_code)/csr/shpl' => 'csrmanagement/frontend/home/shpl',
    '(:lang_code)/csr/shsb' => 'csrmanagement/frontend/home/shsb',
    '(:lang_code)/get-list' => 'press/frontend/home/load_list',

    //product
    '(:lang_code)/brand-new/pocari-sweat' => 'campaign/frontend/home/pocari_new',
    '(:lang_code)/brand-new/soyjoy' => 'campaign/frontend/home/soyjoy_new',
    '(:lang_code)/brand-new/ionessence' => 'campaign/frontend/home/ionessence_new',


);
