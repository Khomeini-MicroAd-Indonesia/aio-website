<?php

return array(
    'cms_name' => 'AIO',
    'max_lock_count' => 5,
    'cms_session_name' => array(
        'admin_id' => 'AmertaIndahOtsuka'
    ),
    'admin_default_password' => 'aio',
    'max_email_sent_daily' => 500,
    'max_email_sent_per_task' => 10,
    'menus' => array(
        'dashboard' => array(
            'label' => 'Dashboard',
            'route' => 'backend',
            'icon_class' => 'fa fa-dashboard',
            'permission' => false,
        ),
        'data_report_applicants' => array(
            'label' => 'Data Report Applicants',
            'route' => 'backend/data-report-applicant',
            'icon_class' => 'fa fa-file-o',
            'permission' => true,
        ),
        'data_report_customers' => array(
            'label' => 'Data Report Contact Us',
            'route' => 'backend/data-report-customer',
            'icon_class' => 'fa fa-file-o',
            'permission' => true,
        ),
        'data_report_factorytour' => array(
            'label' => 'Data Report Factory Tour',
            'route' => 'backend/data-report-factory-tour',
            'icon_class' => 'fa fa-file-o',
            'permission' => true,
        ),
        'about_management' => array(
            'label' => 'About Us Management',
            'icon_class' => 'fa fa-child',
            'permission' => false,
            'submenus' => array(
                'about_organizer' => array(
                    'label' => 'All About Otsuka',
                    'route' => 'backend/about',
                    'icon_class' => 'fa fa-group',
                    'permission' => true,
                ),
                'awardcertificate' => array(
                    'label' => 'Award & Certificate',
                    'route' => 'backend/about/awardcertificate',
                    'icon_class' => 'fa fa-trophy',
                    'permission' => true,
                ),
                'president_organizer' => array(
                    'label' => 'President',
                    'route' => 'backend/about/president',
                    'icon_class' => 'fa fa-user',
                    'permission' => true,
                )
            ),
        ),
        'pages' => array(
            'label' => 'Pages',
            'route' => 'backend/pages',
            'icon_class' => 'fa fa-sitemap',
            'permission' => true,
        ),
        'aboutbanner_management' => array(
            'label' => 'About Banner Management',
            'icon_class' => 'fa fa-map',
            'permission' => false,
            'submenus' => array(
                'aboutbanner_organizer' => array(
                    'label' => 'Organize',
                    'route' => 'backend/about/banner',
                    'icon_class' => 'fa fa-puzzle-piece',
                    'permission' => true,
                ),
                'aboutbanner_image' => array(
                    'label' => 'Image',
                    'route' => 'backend/about/image',
                    'icon_class' => 'fa fa-camera-retro',
                    'permission' => true,
                )
            )
        ),
        'admin_management' => array(
            'label' => 'Admin Management',
            'icon_class' => 'fa fa-columns',
            'permission' => false,
            'submenus' => array(
                'admin_user' => array(
                    'label' => 'Admin User',
                    'route' => 'backend/admin-user',
                    'icon_class' => 'fa fa-users',
                    'permission' => true,
                ),
                'admin_role_permission' => array(
                    'label' => 'Admin Role Permission',
                    'route' => 'backend/admin-role-permission',
                    'icon_class' => 'fa fa-shield',
                    'permission' => true,
                ),
                'admin_setting' => array(
                    'label' => 'Basic Setting',
                    'route' => 'backend/setting',
                    'icon_class' => 'fa fa-gear',
                    'permission' => true,
                ),
                'admin_role_permission' => array(
                    'label' => 'Admin Role Permission',
                    'route' => 'backend/admin-role-permission',
                    'icon_class' => 'fa fa-shield',
                    'permission' => true,
                )
            )
        ),
        'campaign_management' => array(
            'label' => 'Campaign Management',
            'icon_class' => 'fa fa-microphone',
            'permission' => false,
            'submenus' => array(
                'campaign_organizer' => array(
                    'label' => 'Organize',
                    'route' => 'backend/campaign',
                    'icon_class' => 'fa fa-puzzle-piece',
                    'permission' => true,
                ),
                'campaign_image' => array(
                    'label' => 'Image',
                    'route' => 'backend/campaign/image',
                    'icon_class' => 'fa fa-camera-retro',
                    'permission' => true,
                )
            )
        ),
        'career' => array(
            'label' => 'Career Management',
            'icon_class' => 'fa fa-suitcase',
            'permission' => false,
            'submenus' => array(
                'careercontent' => array(
                    'label' => 'Content',
                    'route' => 'backend/career/content',
                    'icon_class' => 'fa fa-file-image-o',
                    'permission' => true,
                ),
                'career' => array(
                    'label' => 'Organize',
                    'route' => 'backend/career/',
                    'icon_class' => 'fa fa-user-plus',
                    'permission' => true,
                ),
                'career_applicants' => array(
                    'label' => 'Applicants',
                    'route' => 'backend/career/applicant/',
                    'icon_class' => 'fa fa-file-pdf-o',
                    'permission' => true,
                )
            )
        ),
        'contact_us' => array(
            'label' => 'Contact Us',
            'route' => 'backend/contactus',
            'icon_class' => 'fa fa-paper-plane',
            'permission' => true,
        ),
        'csr_management' => array(
            'label' => 'CSR Management',
            'icon_class' => 'fa fa-street-view',
            'permission' => false,
            'submenus' => array(
                'csr_organizer' => array(
                    'label' => 'Organize',
                    'route' => 'backend/csr-management/',
                    'icon_class' => 'fa fa-puzzle-piece',
                    'permission' => true,
                ),
            )     
        ),
        'distributor_organizer' => array(
            'label' => 'Distributor Management',
            'icon_class' => 'fa fa-building',
            'permission' => false,
            'submenus' => array(
                'distributor' => array(
                    'label' => 'Distributor',
                    'route' => 'backend/distributor/',
                    'icon_class' => 'fa fa-building-o',
                    'permission' => true,
                ),
                'province' => array(
                    'label' => 'Province',
                    'route' => 'backend/province/',
                    'icon_class' => 'fa fa-flag',
                    'permission' => true,
                ),
                'city' => array(
                    'label' => 'City',
                    'route' => 'backend/city/',
                    'icon_class' => 'fa fa-flag-o',
                    'permission' => true,
                )
            )
        ),
        'explorion' => array(
            'label' => 'Explorion',
            'icon_class' => 'fa fa-flask',
            'permission' => false,
            'submenus' => array(
                'explorion_organizer' => array(
                    'label' => 'Organize',
                    'route' => 'backend/explorion/',
                    'icon_class' => 'fa fa-calendar-plus-o',
                    'permission' => true,
                ),
                'explorion_image' => array(
                    'label' => 'Image',
                    'route' => 'backend/explorion/image',
                    'icon_class' => 'fa fa-camera-retro',
                    'permission' => true,
                )
            )     
        ),
        'factorytour' => array(
            'label' => 'Factory Tour',
            'icon_class' => 'fa fa-industry',
            'permission' => false,
            'submenus' => array(
                'factorytour_organizer' => array(
                    'label' => 'Organize',
                    'route' => 'backend/factorytour/',
                    'icon_class' => 'fa fa-calendar-plus-o',
                    'permission' => true,
                ),
                'factorytour_sideimage' => array(
                    'label' => 'Side Image',
                    'route' => 'backend/factorytour/sideimage',
                    'icon_class' => 'fa fa-camera',
                    'permission' => true,
                ),
                'factorytour_schedule' => array(
                    'label' => 'Schedule',
//                    'route' => 'factorytour/backend/scheduledetail',
                    'route' => 'backend/factorytour/scheduledetail',
                    'icon_class' => 'fa fa-calendar',
                    'permission' => true,
                ),
                'factorytour_schedule_history' => array(
                    'label' => 'Schedule History',
                    'route' => 'backend/factorytour/schedulehistory',
                    'icon_class' => 'fa fa-clock-o',
                    'permission' => true,
                )
            )     
        ),
        'faq' => array(
            'label' => 'FAQ',
            'route' => 'backend/faq',
            'icon_class' => 'fa fa-question',
            'permission' => true,
        ),
        'history_management' => array(
            'label' => 'History Management',
            'icon_class' => 'fa fa-history',
            'permission' => false,
            'submenus' => array(
                'history' => array(
                    'label' => 'Organize',
                    'route' => 'backend/history/',
                    'icon_class' => 'fa fa-puzzle-piece',
                    'permission' => true,
                ),
                /*'history_image' => array(
                    'label' => 'Image',
                    'route' => 'backend/history/image',
                    'icon_class' => 'fa fa-camera-retro',
                    'permission' => true,
                )*/
            )
        ),
        'homebanner_management' => array(
            'label' => 'Home Banner Management',
            'icon_class' => 'fa fa-map',
            'permission' => false,
            'submenus' => array(
                'homebanner_organizer' => array(
                    'label' => 'Organize',
                    'route' => 'backend/homebanner/',
                    'icon_class' => 'fa fa-puzzle-piece',
                    'permission' => true,
                ),
                'homebanner_image' => array(
                    'label' => 'Image',
                    'route' => 'backend/homebanner/image',
                    'icon_class' => 'fa fa-camera-retro',
                    'permission' => true,
                ),
                'homebanner_sideimage' => array(
                    'label' => 'Side Image',
                    'route' => 'backend/homebanner/sideimage',
                    'icon_class' => 'fa fa-camera',
                    'permission' => true,
                )
            )
        ),
        'gallery_management' => array(
            'label' => 'Gallery Factory Tour',
            'icon_class' => 'fa fa-folder',
            'permission' => false,
            'submenus' => array(
                'gallery_banner' => array(
                    'label' => 'Gallery Banner',
                    'route' => 'backend/gallery/banner',
                    'icon_class' => '',
                    'permission' => true,
                ),
                'gallery_organizer' => array(
                    'label' => 'Photo Album',
                    'route' => 'backend/gallery/',
                    'icon_class' => '',
                    'permission' => true,
                ),
                'factorytour_image' => array(
                    'label' => 'Image',
                    'route' => 'backend/factorytour/image',
                    'icon_class' => 'fa fa-camera-retro',
                    'permission' => true,
                ),
                'gallery_comments' => array(
                    'label' => 'Manage Comment',
                    'route' => 'backend/gallery/comment',
                    'icon_class' => '',
                    'permission' => true,
                ),
                'gallery_testimonial' => array(
                    'label' => 'Manage Testimonial',
                    'route' => 'backend/gallery/testimonial',
                    'icon_class' => '',
                    'permission' => true,
                )
            )
        ),
        'life_at_otsuka' => array(
            'label' => 'LAO Management',
            'icon_class' => 'fa fa-group',
            'permission' => false,
            'submenus' => array(
                'lao_organizer' => array(
                    'label' => 'Organize',
                    'route' => 'backend/lifeatotsuka/',
                    'icon_class' => 'fa fa-user-plus',
                    'permission' => true,
                ),
                'leadership_vision' => array(
                    'label' => 'Vision',
                    'route' => 'backend/lifeatotsuka/vision',
                    'icon_class' => 'fa fa-bullhorn',
                    'permission' => true,
                ),
                'philosophy' => array(
                    'label' => 'Philosopy',
                    'route' => 'backend/lifeatotsuka/philosophy',
                    'icon_class' => 'fa fa-graduation-cap',
                    'permission' => true,
                )
            )
        ),
        'media_uploader' => array(
            'label' => 'Media Uploader',
            'icon_class' => 'fa fa-upload',
            'permission' => false,
            'submenus' => array(
                'media_uploader_campaign' => array(
                    'label' => 'Campaign',
                    'route' => 'backend/media-uploader/campaign',
                    'icon_class' => '',
                    'permission' => true,
                ),
                'media_uploader_common' => array(
                    'label' => 'Common',
                    'route' => 'backend/media-uploader/common',
                    'icon_class' => '',
                    'permission' => true,
                ),
                'media_uploader_laophoto' => array(
                    'label' => 'LAO Photo',
                    'route' => 'backend/media-uploader/lao-photo',
                    'icon_class' => '',
                    'permission' => true,
                ),
                'media_uploader_pressimage' => array(
                    'label' => 'Press Room Image',
                    'route' => 'backend/media-uploader/press-image',
                    'icon_class' => '',
                    'permission' => true,
                ),
                'media_uploader_aboutbanner' => array(
                    'label' => 'About Banner',
                    'route' => 'backend/media-uploader/aboutbanner',
                    'icon_class' => '',
                    'permission' => true,
                ),
                'media_uploader_awardcertificate' => array(
                    'label' => 'Award & Certificate',
                    'route' => 'backend/media-uploader/awardcertificate',
                    'icon_class' => '',
                    'permission' => true,
                ),
                'media_uploader_careercontent' => array(
                    'label' => 'Career Banner',
                    'route' => 'backend/media-uploader/careercontent',
                    'icon_class' => '',
                    'permission' => true,
                ),
                'media_uploader_csr' => array(
                    'label' => 'CSR',
                    'route' => 'backend/media-uploader/csrimage',
                    'icon_class' => '',
                    'permission' => true,
                ),
                'media_uploader_explorion' => array(
                    'label' => 'Explorion',
                    'route' => 'backend/media-uploader/explorion',
                    'icon_class' => '',
                    'permission' => true,
                ),
                'media_uploader_factorytour' => array(
                    'label' => 'Factory Tour',
                    'route' => 'backend/media-uploader/factorytour',
                    'icon_class' => '',
                    'permission' => true,
                ),
                'media_uploader_history' => array(
                    'label' => 'History',
                    'route' => 'backend/media-uploader/history',
                    'icon_class' => '',
                    'permission' => true,
                ),
                'media_uploader_homebanner' => array(
                    'label' => 'Home Banner',
                    'route' => 'backend/media-uploader/homebanner',
                    'icon_class' => '',
                    'permission' => true,
                ),
                'media_uploader_gallery' => array(
                    'label' => 'Gallery',
                    'route' => 'backend/media-uploader/gallery',
                    'icon_class' => '',
                    'permission' => true,
                ),
                'media_uploader_president' => array(
                    'label' => 'President',
                    'route' => 'backend/media-uploader/president',
                    'icon_class' => '',
                    'permission' => true,
                ),
                'media_uploader_productconcept' => array(
                    'label' => 'Product Concept',
                    'route' => 'backend/media-uploader/productconcept',
                    'icon_class' => '',
                    'permission' => true,
                ),
                'media_uploader_product_nutrition' => array(
                    'label' => 'Product Nutrition',
                    'route' => 'backend/media-uploader/productnutrition',
                    'icon_class' => '',
                    'permission' => true,
                ),
            ),
        ),
        'newsletter' => array(
            'label' => 'Newsletter Management',
            'icon_class' => 'fa fa-newspaper-o',
            'permission' => false,
            'submenus' => array(
                'newsletter_organizer' => array(
                    'label' => 'Organize',
                    'route' => 'backend/newsletter/',
                    'icon_class' => 'fa fa-user-plus',
                    'permission' => true,
                ),
                'newsletter_broadcast' => array(
                    'label' => 'Broadcast Newsletter',
                    'route' => 'backend/newsletter/broadcast',
                    'icon_class' => 'fa fa-arrows-alt',
                    'permission' => true,
                )
            )
        ),
        'press' => array(
            'label' => 'Press Management',
            'icon_class' => 'fa fa-bullhorn',
            'permission' => false,
            'submenus' => array(
                'press_organizer' => array(
                    'label' => 'Organize',
                    'route' => 'backend/press/',
                    'icon_class' => 'fa fa-user-plus',
                    'permission' => true,
                )
            )
        ),
        'product_management' => array(
            'label' => 'Product Management',
            'icon_class' => 'fa fa-cube',
            'permission' => false,
            'submenus' => array(
                'product' => array(
                    'label' => 'Organize',
                    'route' => 'backend/product',
                    'icon_class' => 'fa fa-user-plus',
                    'permission' => true,
                ),
                'product_concept' => array(
                    'label' => 'Concept',
                    'route' => 'backend/productconcept',
                    //'icon_class' => 'fa fa-user-plus',
                    'permission' => true,
                )
            )
        )
    ),
);
