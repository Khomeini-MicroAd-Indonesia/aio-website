<?php

namespace Nutrition;

class Controller_Backend extends \Controller_Backend {
    private $_parent_url = 'backend/product';
    private $_module_url = 'backend/nutrition';
    private $_menu_key = 'product';

    public function before() {
        parent::before();
        $this->authenticate();
        // Check menu permission
        if (!$this->check_menu_permission($this->_menu_key, 'read')) {
            // if not have an access then redirect to error page
            \Response::redirect(\Uri::base() . 'backend/no-permission');
        }
        $this->_data_template['meta_title'] = 'Product';
        $this->_data_template['menu_current_key'] = $this->_menu_key;
        $this->_data_template['menu_parent_key'] = 'product';
    }

    public function action_index($product_id) {
        
        $this->_data_template['model_product'] = \Product\Model_Products::find($product_id);
        
        if (empty($this->_data_template['model_product'])) {
            \Session::set_flash('error_message', 'Product with ID "'.$product_id.'" is not found here');
            \Response::redirect(\Uri::base().$this->_parent_url);
        }
        if ($this->check_limitation_by_admin()) {
            if ($this->_data_template['model_product']->created_by != $this->admin_auth->getCurrentAdmin()->id) {
                \Session::set_flash('error_message', 'Only owner can access Product with ID "'.$product_id.'"!');
                \Response::redirect(\Uri::base().$this->_parent_url);
            }
        }
        $query = Model_Nutritions::query()->where('product_id',$product_id);
        if ($this->check_limitation_by_admin()) {
            $query->where('created_by', $this->admin_auth->getCurrentAdmin()->id);
        }
        $this->_data_template['nutrition_list'] = $query->get();
        $this->_data_template['success_message'] = \Session::get_flash('success_message');
        $this->_data_template['error_message'] = \Session::get_flash('error_message');
        return \Response::forge(\View::forge('nutrition::backend/list.twig', $this->_data_template, FALSE));
    }

    public function action_form($product_id, $id) {
        $this->_data_template['model_product'] = \Product\Model_Products::find($product_id);
        // find model by id

        if (empty($this->_data_template['model_product'])) {
            \Session::set_flash('error_message', 'Product with ID "'.$product_id.'" is not found here');
            \Response::redirect(\Uri::base().$this->_parent_url);
        }
        if ($this->check_limitation_by_admin()) {
            if ($this->_data_template['model_product']->created_by != $this->admin_auth->getCurrentAdmin()->id) {
                \Session::set_flash('error_message', 'Only owner can access Product with ID "'.$product_id.'"!');
                \Response::redirect(\Uri::base().$this->_parent_url);
            }
        }
        // find model by id
        $the_model = Model_Nutritions::find($id);
        // if empty then define empty model
        if (empty($the_model)) {
            // The ID "0" is only for add new thing, if greater than 0 then its mean edit thing
            if ($id > 0) {
                \Session::set_flash('error_message', 'Nutrition with ID "'.$id.'" is not found here');
                \Response::redirect(\Uri::base().$this->_module_url.'/'.$product_id);
            }
            $the_model = Model_Nutritions::forge();
        }
        if ($this->check_limitation_by_admin() && $id > 0) {
            if ($the_model->created_by != $this->admin_auth->getCurrentAdmin()->id) {
                \Session::set_flash('error_message', 'Only owner can access Nutrition with ID "'.$id.'"!');
                \Response::redirect(\Uri::base().$this->_module_url.'/'.$product_id);
            }
        }
        $this->_save_setting_data($the_model, $product_id);
        $this->_data_template['content_header'] = 'Nutrition';
        $this->_data_template['content_subheader'] = 'Form';
        $this->_data_template['breadcrumbs'] = array(
            array(
                'label' => 'Product',
                'link' => \Uri::base().$this->_parent_url
            ),
            array(
                'label' => 'Nutrition',
                'link' => \Uri::base().$this->_module_url.'/'.$product_id
            ),
            array(
                'label' => 'Form'
            )
        );
        
//        $image_data = Model_Nutritions::get_as_array();
        $this->_data_template['form_data'] = $the_model->get_form_data_basic($product_id);
        $this->_data_template['cancel_button_link'] = \Uri::base().$this->_module_url.'/'.$product_id;
        $this->_data_template['success_message'] = \Session::get_flash('success_message');
        return \Response::forge(\View::forge('backend/form/basic.twig', $this->_data_template, FALSE));
    }

    private function _save_setting_data($the_model, $product_id) {
        $all_post_input = \Input::post();
            if (count($all_post_input)) {
                // Check menu permission
                $access_name = ($the_model->id > 0) ? 'update' : 'create';
                if (!$this->check_menu_permission($this->_menu_key, $access_name)) {
                        // if not have an access then redirect to error no permission page
                        \Response::redirect(\Uri::base().'backend/no-permission');
                }
                // Set created_by/updated_by
                if ($the_model->id > 0) {
                    $the_model->updated_by = $this->admin_auth->getCurrentAdmin()->id;
                } else {
                    $the_model->created_by = $this->admin_auth->getCurrentAdmin()->id;
                    $all_post_input['product_id'] = $product_id;
                }
                $the_model->product_id = $product_id;
                $the_model->name = isset($all_post_input['name']) ? $all_post_input['name'] : null;
                $the_model->image = isset($all_post_input['image']) ? $all_post_input['image'] : null;
                $the_model->desc_en = isset($all_post_input['desc_en']) ? $all_post_input['desc_en'] : null;
                $the_model->desc_id = isset($all_post_input['desc_id']) ? $all_post_input['desc_id'] : null;
                $the_model->status = isset($all_post_input['status']) ? $all_post_input['status'] : null;
                // Set created_by/updated_by
                if ($the_model->id > 0) {
                        $the_model->updated_by = $this->admin_auth->getCurrentAdmin()->id;
                } else {
                        $the_model->created_by = $this->admin_auth->getCurrentAdmin()->id;
                }
                // Save with validation, if error then throw the error
                try {
                        $the_model->save();
                        \Session::set_flash('success_message', 'Successfully Saved');
                        \Response::redirect(\Uri::current());
                } catch (\Orm\ValidationFailed $e) {
                        $this->_data_template['error_message'] = $e->getMessage();
                }
        }
    }

    public function action_delete($product_id, $id) {
        $model_product = \Product\Model_Products::find($product_id);
        if (empty($model_product)) {
            \Session::set_flash('error_message', 'Product with ID "'.$product_id.'" is not found here');
            \Response::redirect(\Uri::base().$this->_parent_url);
        }
        if ($this->check_limitation_by_admin()) {
            if ($model_product->created_by != $this->admin_auth->getCurrentAdmin()->id) {
                \Session::set_flash('error_message', 'Only owner can access Product with ID "'.$product_id.'"!');
                \Response::redirect(\Uri::base().$this->_parent_url);
            }
        }
        // Check menu permission
        if (!$this->check_menu_permission($this->_menu_key, 'delete')) {
            // if not have an access then redirect to no permission page
            \Response::redirect(\Uri::base() . 'backend/no-permission');
        }
        // find model by id
        $the_model = Model_Nutritions::find($id);
        // if empty then redirect back with error message
        if (empty($the_model)) {
            \Session::set_flash('error_message', 'The Nutrition with ID "' . $id . '" is not found here');
            \Response::redirect(\Uri::base().$this->_module_url.'/'.$product_id);
            exit;
        }
        // Delete the admin
        try {
            $the_model->delete();
            \Session::set_flash('success_message', 'Delete Nutrition "' . $the_model->name . '" with ID "' . $id . '" is successfully');
        } catch (Orm\ValidationFailed $e) {
            \Session::set_flash('error_message', $e->getMessage());
        }
        \Response::redirect(\Uri::base().$this->_module_url.'/'.$product_id);
    }

}
