<?php
namespace Nutrition;

class Model_Nutritions extends \Orm\Model {
    
    protected static $_table_name = 'product_nutritions';
    private $image_path = 'media/product/nutrition/';
    private $status_name = array('InActive', 'Active');

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'product_nutritions'=>array('before_insert'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_UpdatedAt' => array(
            'product_nutritions'=>array('before_update'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_Validation' => array(
            'product_nutritions'=>array('before_save')
        )
    );

    protected static $_properties = array(
        'id',
        'product_id' => array(
            'label' => 'Product',
            'validation' => array(
                'required',
            )
        ),
        'name' => array(
            'label' => 'Name',
            'validation' => array(
                'required',
                'max_length' => array(50),
            )
        ),
        'image' => array(
            'label' => 'Image',
            'validation' => array(
                'required',
                'max_length' => array(50),
            )
        ),
        'desc_en' => array(
            'label' => 'Description(EN)',
            'validation' => array()
        ),
        'desc_id' => array(
            'label' => 'Description(ID)',
            'validation' => array()
        ),
        'status' => array(
            'label' => 'Status',
            'validation' => array(
                'required',
                'max_length' => array(11),
            )
        ),
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    );
    
    protected static $_belongs_to = array(
        'products' => array(
            'key_from' => 'product_id',
            'model_to' => '\Product\Model_Products',
            'key_to' => 'id',
            'cascade_save' => true,
            'cascade_delete' => false,
        )
    );
    
    private static $_products;
    
    public function get_product_name(){
        if(empty(self::$_products)){
            self::$_products = \Product\Model_Products::get_as_array();
        }
        $flag = $this->product_id;
        return isset(self::$_products[$flag]) ? self::$_products[$flag] : '-';
    }
    
    public function get_image_path() {
        return $this->image_path;
    }
    
    public function get_all_images() {
        if (file_exists(DOCROOT.$this->image_path)) {
            $contents = \File::read_dir(DOCROOT.$this->image_path);
        } else {
            $contents = array();
        }
        return $contents;
    }
    
    public function get_status_name() {
        $flag = $this->status;
        return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
    }
    
//    public static function get_as_array ($filter=array()) {
//        $items = self::find('all', $filter);
//        if (empty($items)) {
//            $data = array();
//        } else {
//            foreach ($items as $item) {
//            $data[$item->id] = $item->nam;
//            }
//        }
//        return $data;
//    }
    
    public function get_form_data_basic($product) {
        return array(
            'attributes' => array(
                'name' => 'frm_banners',
                'class' => 'form-horizontal',
                'role' => 'form',
                'action' => '',
                'method' => 'post',
            ),
            'hidden' => array(),
            'fieldset' => array(
                array(
                    'label' => array(
                        'label' => '',
                        'id' => 'product_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'hidden' => array(
                        'name' => 'product_id',
                        'value' => $product,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => '',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Name',
                        'id' => 'name',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'name',
                        'value' => $this->name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => '',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Image',
                        'id' => 'image',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select_image_picker' => array(
                        'name' => 'image',
                        'value' => $this->image,
                        'options' => $this->get_all_images(),
                        'attributes' => array(
                            'class' => 'form-control image-picker',
                        ),
                        'container_class' => 'col-sm-10',
                        'image_url' => \Uri::base().$this->image_path,
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Description(EN)',
                        'id' => 'desc_en',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'desc_en',
                        'value' => $this->desc_en,
                        'attributes' => array(
                            'class' => 'form-control ckeditor',
                            'placeholder' => 'Description(EN)',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Description(ID)',
                        'id' => 'desc_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'desc_id',
                        'value' => $this->desc_id,
                        'attributes' => array(
                            'class' => 'form-control ckeditor',
                            'placeholder' => 'Description(ID)',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Status',
                        'id' => 'status',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'status',
                        'value' => $this->status,
                        'options' => $this->status_name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Status',
                            'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                    )
                )
            )
        );
    }
}
