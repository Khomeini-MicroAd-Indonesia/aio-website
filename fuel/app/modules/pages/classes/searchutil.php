<?php

namespace Pages;


class Searchutil {

    
    /*
     * get career
     */

    public static function get_career($keyword) {
        
        $careers = \Career\Model_Careers::query()
            ->where('name', 'like', '%'.$keyword.'%')
            ->where('status', 1)
            ->get();
        
        if($careers){
            $status = 1;
        }else{
            $status = 0;
        }
        
        $data = array();

        foreach ($careers as $career) {

            $data[] = array(
                'name'      => $career->name,
                'level'     => $career->level,
                'location'  => $career->location,
                'req_id'    => $career->req_id,
                'req_en'    => $career->req_en,
                'date'      => date('l, d F Y', strtotime($career->date)),
                'status'    => $status
            );
        }
        return $data;
    }
    
    /*
     * get press room
     */

    public static function get_press($keyword) {
        $title = 'title_'.\Config::get('language');
        $highlight = 'highlight_'.\Config::get('language');
        $content = 'content_'.\Config::get('language');
        $presses = \Press\Model_Press::query()
            ->where($title, 'like', '%'.$keyword.'%')
            ->where('status', 1)
            ->get();
        if($presses){
            $status = 1;
        }else{
            $status = 0;
        }
        $data = array();
        foreach ($presses as $press) {
            $data[] = array(
                'slug'       => $press->slug,
                'title'      => $press->$title,
                'highlight'  => $press->$highlight,
                'content'    => $press->$content,
                'image'      => $press->image,
                'status'     => $status
            );
        }
        return $data;
    }
    
    
    /*
     * get csr activity
     */

    public static function get_activity($keyword) {
        
        
        $activities = \Csrmanagement\Model_Activities::query()
            ->where('title_id', 'like', '%'.$keyword.'%')
            ->or_where('title_en', 'like', '%'.$keyword.'%')
            ->where('status', 1)
            ->get();
        
        //var_dump($activities);exit;
        if($activities){
            $status = 'activity';
        }else{
            $status = 0;
        }
        $title = 'title_'.\Config::get('language');
        
        
        $data = array();
        foreach ($activities as $activity) {
            $data[] = array(
                'slug'          => $activity->slug,
                'title'         => $activity->$title,
                'image'         => $activity->image,
                'highlight'     => $activity->highlight,
                'status'        => $status
            );
        }
        
        return $data;
        
    }
    
}
