<?php
namespace Pages;

class Controller_DataReportApplicants extends \Controller_Backend
{
    private $_module_url = 'backend/data-report-applicant';
    private $_menu_key = 'data_report_applicants';

    public function before() {
        parent::before();
        $this->authenticate();
        // Check menu permission
        if (!$this->check_menu_permission($this->_menu_key, 'read')) {
            // if not have an access then redirect to error page
            \Response::redirect(\Uri::base().'backend/no-permission');
        }
        $this->_data_template['meta_title'] = 'Data Report - Career Applicants';
        $this->_data_template['menu_parent_key'] = 'admin_app_control';
        $this->_data_template['menu_current_key'] = 'admin_app_data_report';
    }

    public function action_index() {
        $post_data = \Input::post();
        if(!empty($post_data)){
            $from_to = $post_data['data_from_to'];
            $temp = explode('-', $from_to);
            if (count($temp) == 2) {
                $string_from = trim($temp[0]);
                $temp_from = explode('/', $string_from);
                $string_to = trim($temp[1]);
                $temp_to = explode('/', $string_to);
                if (count($temp_from) == 3 && count($temp_to) == 3) {
                    $date_from = $temp_from[2].'-'.$temp_from[0].'-'.$temp_from[1].' 00:00:00';
                    $date_to = $temp_to[2].'-'.$temp_to[0].'-'.$temp_to[1].' 23:59:59';

                    $this->_get_applicants($date_from, $date_to);
                }
            }
        }
        $this->_data_template['success_message'] = \Session::get_flash('success_message');
        $this->_data_template['error_message'] = \Session::get_flash('error_message');
        return \Response::forge(\View::forge('pages::data_report_applicants.twig', $this->_data_template, FALSE));
    }

    /* creates a compressed zip file (to compress all winner's photos into a zip file) */
    private function _create_zip($files = array(),$destination = '',$overwrite = false) {
        //if the zip file already exists and overwrite is false, return false
        if(file_exists($destination) && !$overwrite) { return false; }
        //vars
        $valid_files = array();
        //if files were passed in...
        if(is_array($files)) {
            //cycle through each file
            foreach($files as $file) {
                //make sure the file exists
                if(is_file($file)) {
                    $valid_files[] = $file;
                }
            }
        }
        //if we have good files...
        if(count($valid_files)) {
            //create the archive
            $zip = new \ZipArchive();
            if($zip->open($destination, (file_exists($destination) && $overwrite) ? \ZIPARCHIVE::OVERWRITE : \ZIPARCHIVE::CREATE) !== true) {
                return false;
            }
            //add the files
            foreach($valid_files as $file) {
                $zip->addFile($file, basename($file));
            }
            //debug
            //echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;

            //close the zip -- done!
            $zip->close();

            //check to make sure the file exists
            return file_exists($destination);
        } else {
            return false;
        }
    }

    private function _get_phpexcel() {
        include_once(APPPATH."classes/phpexcel/PHPExcel.php");
        include_once(APPPATH."classes/phpexcel/PHPExcel/Writer/Excel2007.php");
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getProperties()->setCreator("MAI");
        $objPHPExcel->setActiveSheetIndex(0);
        return $objPHPExcel;
    }

    private function _get_applicants($from, $to){
        $string_from = date('Y_m_d', strtotime($from));
        $string_to = date('Y_m_d', strtotime($to));
		ini_set('memory_limit', '1024M');
        $objPHPExcel = $this->_get_phpexcel();
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'NO');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'NAME');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'POSITION');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'PLACE-DOB');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'GENDER');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'ADDRESS');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'EMAIL');
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'PHONE');
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'EMAIL');
        $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'STATUS');
        $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'FILENAME');
        $objPHPExcel->getActiveSheet()->setAutoFilter("A1:K1");

        $applicants = \Career\Model_CareerApplicants::query()
            ->where('register_at', '>=', $from)
            ->where('register_at', '<=', $to)
            ->order_by('id')
            ->get();

        $i = 2;
        foreach($applicants as $row){
            $objPHPExcel->getActiveSheet()->SetCellValue('A'.$i, $i-1);
            $objPHPExcel->getActiveSheet()->SetCellValue('B'.$i, $row->name);
            $objPHPExcel->getActiveSheet()->SetCellValue('C'.$i, $row->position);
            $objPHPExcel->getActiveSheet()->SetCellValue('D'.$i, $row->birth_place.', '.$row->dob);
            $objPHPExcel->getActiveSheet()->SetCellValue('E'.$i, $row->gender);
            $objPHPExcel->getActiveSheet()->SetCellValue('F'.$i, $row->address);
            $objPHPExcel->getActiveSheet()->SetCellValue('G'.$i, $row->email);
            $objPHPExcel->getActiveSheet()->SetCellValue('H'.$i, $row->phone);
            $objPHPExcel->getActiveSheet()->SetCellValue('I'.$i, $row->email);
            $objPHPExcel->getActiveSheet()->SetCellValue('J'.$i, $row->marriage_status);
            $objPHPExcel->getActiveSheet()->SetCellValue('K'.$i, $row->filename);
            $i++;
        }
        // Create Excel File
        $objPHPExcel->getActiveSheet()->setTitle('Data Report Applicants');
        $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);
        $filepath = DOCROOT.'report_file';
        if (!file_exists($filepath)) {
            \File::create_dir(DOCROOT, 'report_file', 0775);
        }
        $filepath .= DS.'data_report_career_applicants ('.$string_from.' - '.$string_to.').xlsx';
        $objWriter->save($filepath);
        // Download
        \File::download($filepath, null, null, null, true);
    }
    
}

