<?php

namespace Pages;

class Controller_Frontend_Home extends \Controller_Frontend {

    private $_module_url = '';
    private $_menu_key = 'home';
    private $_meta_slug = '/';

    public function before() {
        parent::before();
    }


    public function action_index() {
        $this->set_meta_info($this->_meta_slug.$this->_current_lang.'/home');
        $this->_data_template['banner_detail']          = \Homebanner\Bannerutil::get_home_banner();
        $this->_data_template['sideimage_detail']       = \Homebanner\Bannerutil::get_side_image($this->_current_lang);
        //$this->_data_template['press_detail']           = \Press\Pressutil::get_press_data();
        $this->_data_template['president_detail']       = \About\Aboututil::get_president_statement($this->_current_lang);
        $this->_data_template['lao_detail']             = \Lifeatotsuka\LAOutil::get_lao_data();
        $this->_data_template['career_detail']          = \Career\Careerutil::get_career_data($this->_current_lang);
        $this->_data_template['factorytour_detail']     = \Factorytour\Factorytourutil::get_factorytour_data($this->_current_lang);
        //$this->_data_template['factorytour_images']     = \Factorytour\Factorytourutil::get_factorytour_images();
        $this->_data_template['csr_detail']             = \Csrmanagement\Activityutil::get_activity_data();
        $this->_data_template['factorytour_images']     = \Homebanner\Bannerutil::get_gallery();
        
        return \Response::forge(\View::forge('pages::frontend/home.twig', $this->_data_template, FALSE));
    }

    public function action_home_new() {
        
        $status = 'yes';
        $category = '';
        $this->set_meta_info($this->_meta_slug.$this->_current_lang.'/home-new');
        $this->_data_template['banner_detail']  = \Homebanner\Bannerutil::get_home_banner();
        $this->_data_template['press_detail']   = \Press\Pressutil::get_press_data($status, $category);
        $this->_data_template['lao_detail']     = \Lifeatotsuka\LAOutil::get_lao_data();
            // echo "<pre>";
            // var_dump( $this->_data_template['banner_detail']);
            // exit;
        $json = file_get_contents($this->_instagram_feed());
        $obj = json_decode($json, true, 512, JSON_BIGINT_AS_STRING);
        
        foreach ($obj['data'] as $post) {
     
            $pic_text = $post['caption']['text'];
            $pic_link = $post['link'];
            $pic_like_count = $post['likes']['count'];
            $pic_comment_count = $post['comments']['count'];
            $pic_src = str_replace("http://", "https://", $post['images']['standard_resolution']['url']);
            $pic_created_time = date("F j, Y", $post['caption']['created_time']);
            $pic_created_time = date("F j, Y", strtotime($pic_created_time . " +1 days"));
        }
        
        $this->_data_template['pic_text']           = $pic_text;
        $this->_data_template['pic_link']           = $pic_link;
        $this->_data_template['pic_like_count']     = $pic_like_count;
        $this->_data_template['pic_comment_count']  = $pic_comment_count;
        $this->_data_template['pic_src']            = $pic_src;
        $this->_data_template['pic_created_time']   = $pic_created_time;
        $this->_data_template['pic_created_time']   = $pic_created_time;
        $this->_data_template['active_home']            = 'active';

        return \Response::forge(\View::forge('pages::frontend/home_new.twig', $this->_data_template, FALSE));
    }
    
    private function _instagram_feed() {
        
        $access_token = "1932917610.1677ed0.68df461a748e4bebbdeea778cc9ab426";
        $photo_count = 1;
            
        $json_link = "https://api.instagram.com/v1/users/self/media/recent/?";
        $json_link .= "access_token={$access_token}&count={$photo_count}";
        
        return $json_link;
    }

    public function action_search() {

        $this->set_meta_info($this->_meta_slug . $this->_current_lang . '/search');
        
        $keyword = \Input::get('keyword'); 
        
        if($keyword != "") {
            $this->_data_template['search_result_activity'] = \Pages\Searchutil::get_activity($keyword);
            $this->_data_template['search_result_press'] = \Pages\Searchutil::get_press($keyword);
            $this->_data_template['search_result_career'] = \Pages\Searchutil::get_career($keyword);
            
            $this->_data_template['keyword'] = $keyword;
        }
        
        
        
        return \Response::forge(\View::forge('pages::frontend/search.twig', $this->_data_template, FALSE));
    }

    public function action_policy() {

        $this->set_meta_info($this->_meta_slug . $this->_current_lang . '/policy');
        $model_page = \Pages\Model_Pages::query()
            ->where('url_path', '/'.$this->_current_lang.'/policy')
            ->where('status', 1)
            ->get_one();
        $this->_data_template['policy_detail'] = $model_page;
        return \Response::forge(\View::forge('pages::frontend/policy.twig', $this->_data_template, FALSE));
    }

    public function action_term_of_use() {
        $this->set_meta_info($this->_meta_slug . $this->_current_lang . '/term-of-use');
        $model_page = \Pages\Model_Pages::query()
            ->where('url_path', '/'.$this->_current_lang.'/term-of-use')
            ->where('status', 1)
            ->get_one();
        $this->_data_template['term_detail'] = $model_page;
        return \Response::forge(\View::forge('pages::frontend/term-of-use.twig', $this->_data_template, FALSE));
    }

    public function action_faq() {
        $this->set_meta_info($this->_meta_slug . $this->_current_lang . '/faq');
        
        $this->_data_template['faq_pocarisweat'] = \Faq\Faqutil::get_faq_data(1);
        return \Response::forge(\View::forge('pages::frontend/faq.twig', $this->_data_template, FALSE));
    }
    public function action_faq_soyjoy() {
        $this->set_meta_info($this->_meta_slug . $this->_current_lang . '/faq-soyjoy');
        
        $this->_data_template['faq_soyjoy'] = \Faq\Faqutil::get_faq_data(2);
        return \Response::forge(\View::forge('pages::frontend/faq_soyjoy.twig', $this->_data_template, FALSE));
    }
    public function action_faq_ionessence() {
        $this->set_meta_info($this->_meta_slug . $this->_current_lang . '/faq-ionessence');
        
        $this->_data_template['faq_ionessence'] = \Faq\Faqutil::get_faq_data(3);
        return \Response::forge(\View::forge('pages::frontend/faq_ionessence.twig', $this->_data_template, FALSE));
    }
    
    public function action_contact_us() {
        
        $this->_contact_submission();
        $this->set_meta_info($this->_meta_slug . $this->_current_lang . '/contact-us');
        return \Response::forge(\View::forge('pages::frontend/contact_us.twig', $this->_data_template, FALSE));
    }


    private function _send_email($post_data) {
        
        $data = array(
            'email_from' => \Config::get('config_basic.sender_email'),//'website@microad.co.id',//
            'email_to' => \Config::get('config_basic.contact_us_email_to'),
            'email_subject' => '[Website aio.co.id] Contact Us message from ' . $post_data['name'],
            'email_reply_to' => array(
                'email' => $post_data['email'],
                'name' => $post_data['name']
            ), // Optional
            'email_data' => array(
                'base_url' => \Uri::base(),
                'name' => $post_data['name'],
                'email' => $post_data['email'],
                'phone' => $post_data['phone'],
                'fax' => $post_data['fax'],
                'msg' => $post_data['msg']
            ),
            'email_view' => 'pages::email/contact_us.twig',
            'email_respond_view' => 'pages::email/email_responder.twig',
        );
        
        \Util_Email::queue_send_email($data);
        $email = \Email::forge();

        // Set the from address
        $email->from($data['email_from'], 'Amerta Indah Otsuka');
        // Set the to address
        $email->to($data['email_to'],'Customer Care Amerta Indah Otsuka');
        // Set a subject
        $email->subject($data['email_subject']);
        // And set the body.
        $email->html_body(\View::forge($data['email_view'], $data['email_data']));
        
        $email->send();

        $mail = \Email::forge();
        $mail->from($data['email_from'], 'Amerta Indah Otsuka');
        $mail->to($data['email_data']['email'],$data['email_data']['name']);
        $email->subject('Reply from PT Amerta Indah Otsuka');
        $mail->html_body(\View::forge($data['email_respond_view'], $data['email_data']));

        $mail->send();
        
        $contact = \Contactus\Model_Contactus::forge(array(
            'name' => $post_data['name'],
            'email' => $post_data['email'],
            'phone' => $post_data['phone'],
            'fax' => $post_data['fax'],
            'msg' => $post_data['msg'],
        ));
        
        $contact->save();
    }

    private function _contact_submission() {
        //var_dump('sampai');exit;
        $_post_data = \Input::post();
        if (count($_post_data) > 0) {
            $_err_msg = [];
            $val = \Validation::forge('contact_validation');
            $val->add('name', 'Your Name')->add_rule('required');
            $val->add('email', 'Your Email')->add_rule('required|valid_email');
            $val->set_message('required', 'Please fill in :label');
            $val->set_message('valid_email', 'Please fill in :label with valid email');
            if (!$val->run()) {
                foreach ($val->error() as $field => $error) {
                    $_err_msg[$field] = $error->get_message();
                }
            } else {

                $this->_send_email($_post_data);
                \Session::set_flash('contact_success_message', 'Thank you for your comment.');
                \Response::redirect(\Uri::current());
            }
            $this->_data_template['err_msg'] = $_err_msg;
            $this->_data_template['post_data'] = $_post_data;
        }
    }

    
    public function action_test(){
        \Response::redirect($this->_current_lang.'/career');
    }
    
}
