<?php
namespace Pages;

class Controller_DataReportCustomer extends \Controller_Backend
{
    private $_module_url = 'backend/data-report-customer';
    private $_menu_key = 'data_report_customers';

    public function before() {
        parent::before();
        $this->authenticate();
        // Check menu permission
        if (!$this->check_menu_permission($this->_menu_key, 'read')) {
            // if not have an access then redirect to error page
            \Response::redirect(\Uri::base().'backend/no-permission');
        }
        $this->_data_template['meta_title'] = 'Data Report - Contact Us';
        $this->_data_template['menu_parent_key'] = 'admin_app_control';
        $this->_data_template['menu_current_key'] = 'admin_app_data_report';
    }

    public function action_index() {
        $post_data = \Input::post();
        if(!empty($post_data)){
            $from_to = $post_data['data_from_to'];
            $temp = explode('-', $from_to);
            if (count($temp) == 2) {
                $string_from = trim($temp[0]);
                $temp_from = explode('/', $string_from);
                $string_to = trim($temp[1]);
                $temp_to = explode('/', $string_to);
                if (count($temp_from) == 3 && count($temp_to) == 3) {
                    $date_from = $temp_from[2].'-'.$temp_from[0].'-'.$temp_from[1].' 00:00:00';
                    $date_to = $temp_to[2].'-'.$temp_to[0].'-'.$temp_to[1].' 23:59:59';

                    $this->_get_customers($date_from, $date_to);
                }
            }
        }
        $this->_data_template['success_message'] = \Session::get_flash('success_message');
        $this->_data_template['error_message'] = \Session::get_flash('error_message');
        return \Response::forge(\View::forge('pages::data_report_customers.twig', $this->_data_template, FALSE));
    }

    /* creates a compressed zip file (to compress all winner's photos into a zip file) */
    private function _create_zip($files = array(),$destination = '',$overwrite = false) {
        //if the zip file already exists and overwrite is false, return false
        if(file_exists($destination) && !$overwrite) { return false; }
        //vars
        $valid_files = array();
        //if files were passed in...
        if(is_array($files)) {
            //cycle through each file
            foreach($files as $file) {
                //make sure the file exists
                if(is_file($file)) {
                    $valid_files[] = $file;
                }
            }
        }
        //if we have good files...
        if(count($valid_files)) {
            //create the archive
            $zip = new \ZipArchive();
            if($zip->open($destination, (file_exists($destination) && $overwrite) ? \ZIPARCHIVE::OVERWRITE : \ZIPARCHIVE::CREATE) !== true) {
                return false;
            }
            //add the files
            foreach($valid_files as $file) {
                $zip->addFile($file, basename($file));
            }
            //debug
            //echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;

            //close the zip -- done!
            $zip->close();

            //check to make sure the file exists
            return file_exists($destination);
        } else {
            return false;
        }
    }

    private function _get_phpexcel() {
        include_once(APPPATH."classes/phpexcel/PHPExcel.php");
        include_once(APPPATH."classes/phpexcel/PHPExcel/Writer/Excel2007.php");
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getProperties()->setCreator("MAI");
        $objPHPExcel->setActiveSheetIndex(0);
        return $objPHPExcel;
    }

    private function _get_customers($from, $to){
        $string_from = date('Y_m_d', strtotime($from));
        $string_to = date('Y_m_d', strtotime($to));
		ini_set('memory_limit', '1024M');
        $objPHPExcel = $this->_get_phpexcel();
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'NO');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'EMAIL FROM');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'EMAIL SENDER');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'PHONE');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'FAX');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'MESSAGE');
<<<<<<< HEAD
        $objPHPExcel->getActiveSheet()->setAutoFilter("A1:F1");
=======
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'CREATED AT');
        $objPHPExcel->getActiveSheet()->setAutoFilter("A1:G1");
>>>>>>> 57350210e4709c934b6e2a31ed2d7381ddcf1876

        $customers = \Contactus\Model_Contactus::query()
            ->where('created_at', '>=', $from)
            ->where('created_at', '<=', $to)
            ->order_by('created_at')
            ->get();

        $i = 2;
        foreach($customers as $row){
<<<<<<< HEAD
            $chunk_contact = explode('","', $row->email_data);
            $len_name = strlen($chunk_contact[1]);
            $name = substr($chunk_contact[1], 15,$len_name);
            $len_email = strlen($chunk_contact[2]);
            $email = substr($chunk_contact[2], 16,$len_email);
            $len_phone = strlen($chunk_contact[3]);
            $phone = substr($chunk_contact[3], 16,$len_phone);
            $len_fax = strlen($chunk_contact[4]);
            $fax = substr($chunk_contact[4], 14,$len_fax);
            $len_msg = strlen($chunk_contact[5]);
            $clear_msg = $len_msg - 16;
            $msg = substr($chunk_contact[5], 14,$clear_msg);
            $objPHPExcel->getActiveSheet()->SetCellValue('A'.$i, $i-1);
            $objPHPExcel->getActiveSheet()->SetCellValue('B'.$i, $name);
            $objPHPExcel->getActiveSheet()->SetCellValue('C'.$i, $email);
            $objPHPExcel->getActiveSheet()->SetCellValue('D'.$i, $phone);
            $objPHPExcel->getActiveSheet()->SetCellValue('E'.$i, $fax);
            $objPHPExcel->getActiveSheet()->SetCellValue('F'.$i, $msg);
=======
            
            $objPHPExcel->getActiveSheet()->SetCellValue('A'.$i, $i-1);
            $objPHPExcel->getActiveSheet()->SetCellValue('B'.$i, $row->name);
            $objPHPExcel->getActiveSheet()->SetCellValue('C'.$i, $row->email);
            $objPHPExcel->getActiveSheet()->SetCellValue('D'.$i, $row->phone);
            $objPHPExcel->getActiveSheet()->SetCellValue('E'.$i, $row->fax);
            $objPHPExcel->getActiveSheet()->SetCellValue('F'.$i, $row->msg);
            $objPHPExcel->getActiveSheet()->SetCellValue('G'.$i, $row->created_at);
>>>>>>> 57350210e4709c934b6e2a31ed2d7381ddcf1876
            $i++;
        }
        // Create Excel File
        $objPHPExcel->getActiveSheet()->setTitle('Data Report Contact Us');
        $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);
        $filepath = DOCROOT.'report_file';
        if (!file_exists($filepath)) {
            \File::create_dir(DOCROOT, 'report_file', 0775);
        }
        $filepath .= DS.'data_report_contactus ('.$string_from.' - '.$string_to.').xlsx';
        $objWriter->save($filepath);
        // Download
        \File::download($filepath, null, null, null, true);
    }
    
}

