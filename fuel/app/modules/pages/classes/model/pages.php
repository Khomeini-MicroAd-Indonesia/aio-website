<?php
namespace Pages;

class Model_Pages extends \Orm\Model {
	private $status_name = array('InActive', 'Active');
	
	protected static $_table_name = 'pages';

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events'=>array('before_insert'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_UpdatedAt' => array(
			'events'=>array('before_update'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_Validation' => array(
			'events'=>array('before_save')
		)
	);
        
	protected static $_properties = array(
		'id',
		'title' => array(
			'label' => 'Page Title',
			'validation' => array(
				'required',
				'max_length' => array(200),
			)
		),
		'content' => array(
			'label' => 'Page Content',
			'validation' => array()
		),
		'meta_title' => array(
			'label' => 'Meta Title',
			'validation' => array(
				'max_length' => array(50),
			)
		),
		'meta_desc' => array(
			'label' => 'Meta Description',
			'validation' => array(
				'max_length' => array(150),
			)
		),
		'url_path' => array(
			'label' => 'URL Path',
			'validation' => array(
				'required',
				'max_length' => array(50),
				'unique_url_path' => array(),
			)
		),
		'status' => array(
			'label' => 'Status',
			'validation' => array(
				'required',
			)
		),
		'created_by',
		'created_at',
		'updated_by',
		'updated_at'
	);
	
	public static function _validation_unique_url_path($url_path) {
		$exists = \DB::select(\DB::expr('COUNT(id) as total_count'))
			->from(self::$_table_name)
			->where('url_path', '=', $url_path)
			->execute()->get('total_count');

		return (bool) !$exists;
	}
	
	public function get_status_name() {
		$flag = $this->status;
		return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
	}
	
	public function get_form_data_basic() {
		return array(
			'attributes' => array(
				'name' => 'frm_pages',
				'class' => 'form-horizontal',
				'role' => 'form',
				'action' => '',
				'method' => 'post',
			),
			'hidden' => array(),
			'fieldset' => array(
				array(
					'label' => array(
						'label' => 'Page Title',
						'id' => 'page_title',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'page_title',
						'value' => $this->title,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Page Title',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'URL Path',
						'id' => 'page_url_path',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'page_url_path',
						'value' => $this->url_path,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => '/url/path/to/page',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Meta Title',
						'id' => 'page_meta_title',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'page_meta_title',
						'value' => $this->meta_title,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Meta Title',
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Meta Description',
						'id' => 'page_meta_desc',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'page_meta_desc',
						'value' => $this->meta_desc,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Meta Description',
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Status',
						'id' => 'status',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
						'name' => 'status',
						'value' => $this->status,
						'options' => $this->status_name,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Status',
							'required' => ''
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Page Content',
						'id' => 'page_content',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'textarea' => array(
						'name' => 'page_content',
						'value' => $this->content,
						'attributes' => array(
							'class' => 'form-control ckeditor',
							'placeholder' => '',
						),
						'container_class' => 'col-sm-10'
					)
				)
			)
		);
	}
}
