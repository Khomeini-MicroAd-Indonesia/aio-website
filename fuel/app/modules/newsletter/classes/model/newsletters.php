<?php
namespace Newsletter;

class Model_Newsletters extends \Orm\Model {
	
        
    protected static $_table_name = 'newsletters';

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'newsletters'=>array('before_insert'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_UpdatedAt' => array(
            'newsletters'=>array('before_update'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_Validation' => array(
            'newsletters'=>array('before_save')
        )
    );

    protected static $_properties = array(
        'id',
        'email' => array(
            'label' => 'Email',
            'validation' => array(
                'required',
                'max_length' => array(100),
            )
        )
    );
	
    public static function get_as_array ($filter=array()) {
        $items = self::find('all', $filter);
        if (empty($items)) {
            $data = array();
        } else {
            foreach ($items as $item) {
                $data[$item->id] = $item->email;
            }
        }
        return $data;
    }
    
    public function get_form_data_basic() {
        return array(
            'attributes' => array(
                'name'       => 'frm_newsletters',
                'class'      => 'form-horizontal',
                'role'       => 'form',
                'action'     => '',
                'method'     => 'post',
        ),
            'hidden' => array(),
            'fieldset' => array(
                array(
                    'label' => array(
                        'label' => 'Email',
                        'id' => 'email',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'email',
                        'value' => $this->email,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Email',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                )  
            )
        );
    }
}
