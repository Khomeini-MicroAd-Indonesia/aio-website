<?php
namespace Newsletter;

class Model_NewsletterBroadcasts extends \Orm\Model {
	
    private $status_name = array('InActive', 'Active');
	    
    protected static $_table_name = 'newsletter_broadcasts';

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'newsletter_broadcasts'=>array('before_insert'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_UpdatedAt' => array(
            'newsletter_broadcasts'=>array('before_update'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_Validation' => array(
            'newsletter_broadcasts'=>array('before_save')
        )
    );

    protected static $_properties = array(
        'id',
        'email' => array(
            'label' => 'Email',
            'validation' => array(
                'required',
            )
        ),
        'title' => array(
            'label' => 'Title',
            'validation' => array(
                'required',
                'max_length' => array(50),
            )
        ),
        'date' => array(
            'label' => 'Date',
            'validation' => array(
                'required',
            )
        ),
        'content' => array(
            'label' => 'Content',
            'validation' => array(
                'required',
            )
        ),
        'status' => array(
            'label' => 'Status',
            'validation' => array(
                'required',
            )
        ),
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    );
	
    
    protected static $_has_many = array(
        
        'emails' => array(
            'key_from' => 'email',
            'model_to' => '\newsletter\Model_NewsletterBroadcasts',
            'key_to' => 'id',
            'cascade_save' => true,
            'cascade_delete' => false,
        )
        
    );
    
    private static $_emails;
	
    public function get_status_name() {
        $flag = $this->status;
        return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
    }
        
    public function get_all_email(){
        if(empty(self::$_emails)){
            self::$_emails = Model_Newsletters::get_as_array();
        }
        $flag = $this->email_id;
        return isset(self::$_emails[$flag]) ? self::$_emails[$flag] : '-';
    }
    
    
    public function get_form_data_basic($_emails) {
        return array(
            'attributes' => array(
                'name' => 'frm_newsletter_broadcasts',
                'class' => 'form-horizontal',
                'role' => 'form',
                'action' => '',
                'method' => 'post',
            ),
            'hidden' => array(),
            'fieldset' => array(
                array(
                    'label' => array(
                        'label' => 'Email',
                        'id' => 'email',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'email',
                        'value' => implode(",",$_emails),//$this->email_id,
                        //'options' => $_emails,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Email',
                            'data-live-search' => 'true',
                            'data-size' => '3',
                            //'multiple' => ''
                        ),
                        'container_class' => 'col-sm-10'
                     )
                ),
                array(
                    'label' => array(
                        'label' => 'Title',
                        'id' => 'title',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'title',
                        'value' => $this->title,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Title',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Date',
                        'id' => 'date',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'date',
                        'value' => $this->date,
                        'attributes' => array(
                            'class' => 'form-control mask-date',
                            'placeholder' => 'Date',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Content',
                        'id' => 'content',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'content',
                        'value' => $this->content,
                        'attributes' => array(
                            'class' => 'form-control ckeditor',
                            'placeholder' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Status',
                        'id' => 'status',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'status',
                        'value' => $this->status,
                        'options' => $this->status_name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Status',
                            'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
            )
        );      
    }
}
