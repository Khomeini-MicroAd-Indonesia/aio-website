<?php

namespace Newsletter;

class Controller_Frontend_Subscription extends \Controller_Frontend
{
    
    private $_module_url = '';
    private $_menu_key = 'newsletter';
	
    public function before() {
        $this->_check_lang = false;
        parent::before();
    }
    
    public function action_entry(){
        
        $email = \Input::get('email');
        
        $subscribe = \Newsletter\Model_Newsletters::forge(array(
            'email' => $email,
        ));
        
        $subscribe->save();
    }
    
}