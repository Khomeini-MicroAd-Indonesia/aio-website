<?php
namespace Newsletter;

class Controller_Backend_Broadcast extends \Controller_Backend
{
    private $_module_url = 'backend/newsletter/broadcast';
    private $_menu_key = 'newsletter_broadcast';

    public function before() {
            parent::before();
            $this->authenticate();
            // Check menu permission
            if (!$this->check_menu_permission($this->_menu_key, 'read')) {
                    // if not have an access then redirect to error page
                    \Response::redirect(\Uri::base().'backend/no-permission');
            }
            $this->_data_template['meta_title'] = 'Newsletter Broadcast';
            $this->_data_template['menu_current_key'] = 'newsletter_broadcast';
    }

    public function action_index() {
            $this->_data_template['broadcast_list'] = Model_NewsletterBroadcasts::find('all');
            $this->_data_template['success_message'] = \Session::get_flash('success_message');
            $this->_data_template['error_message'] = \Session::get_flash('error_message');

            return \Response::forge(\View::forge('newsletter::backend/list_broadcast.twig', $this->_data_template, FALSE));
    }

    public function action_form($id) {
            // find model by id
            $the_model = Model_NewsletterBroadcasts::find($id);
            // if empty then define empty model
            if (empty($the_model)) {
                    // The ID "0" is only for add new thing, if greater than 0 then its mean edit thing
                    if ($id > 0) {
                            \Session::set_flash('error_message', 'The Broadcast with ID "'.$id.'" is not found here');
                            \Response::redirect(\Uri::base().$this->_module_url);
                    }
                    $the_model = Model_NewsletterBroadcasts::forge();
            }
            $this->_save_setting_data($the_model);
            $this->_data_template['content_header'] = 'Newsletter Broadcast';
            $this->_data_template['content_subheader'] = 'Form';
            $this->_data_template['breadcrumbs'] = array(
                    array(
                            'label' => 'Newsletter Broadcast',
                            'link' => \Uri::base().$this->_module_url
                    ),
                    array(
                            'label' => 'Form'
                    )
            );

            $newsletter_data = Model_Newsletters::get_as_array();

            $this->_data_template['form_data'] = $the_model->get_form_data_basic($newsletter_data);
            $this->_data_template['cancel_button_link'] = \Uri::base().$this->_module_url;
            $this->_data_template['success_message'] = \Session::get_flash('success_message');
            return \Response::forge(\View::forge('backend/form/basic.twig', $this->_data_template, FALSE));
    }

    private function _save_setting_data($the_model) {
            $all_post_input = \Input::post();
            if (count($all_post_input)) {
                    // Check menu permission
                    $access_name = ($the_model->id > 0) ? 'update' : 'create';
                    if (!$this->check_menu_permission($this->_menu_key, $access_name)) {
                            // if not have an access then redirect to error no permission page
                            \Response::redirect(\Uri::base().'backend/no-permission');
                    }
                    $the_model->email = $all_post_input['email'];
                    $the_model->title = $all_post_input['title'];
                    $the_model->date = $all_post_input['date'];
                    $the_model->content = $all_post_input['content'];
                    $the_model->status = $all_post_input['status'];
                    // Set created_by/updated_by
                    if ($the_model->id > 0) {
                            $the_model->updated_by = $this->admin_auth->getCurrentAdmin()->id;
                            $is_edit = true;
                    } else {
                            $the_model->created_by = $this->admin_auth->getCurrentAdmin()->id;
                            $is_edit = false;
                    }

                    // Save with validation, if error then throw the error
                    try {
                            $the_model->save();
                            \Session::set_flash('success_message', 'Successfully Saved');
                            \Response::redirect(\Uri::current());
                    } catch (\Orm\ValidationFailed $e) {
                            $this->_data_template['error_message'] = $e->getMessage();
                    }
            }
    }

    public function action_send($id) {
        // find model by id
        $post_data = Model_NewsletterBroadcasts::find($id);
        
        $email_send = explode(',', $post_data['email']);
        $resend = count($email_send);
        //var_dump(count($email_send)); exit;
        //$myemail = 'khomeini@microad.co.id';
        
        for($i=0; $i < $resend; $i++){
            
            if(!empty($post_data)){

                $date = $post_data['date'];
                $title = $post_data['title'];
                $content = $post_data['content'];


                $email_body = "You have received a new message. $date ".
                " Here are the details from PT Amerta Indah Otsuka about $title \n ".
                "Message: $content dengan versi baru\n ";

                $transport = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 465,'ssl')
                    ->setUsername('khomeini@microad.co.id')
                    ->setPassword('K1q2w3e@#');

                //Create the Mailer using your created Transport
                $mailer = \Swift_Mailer::newInstance($transport);


                //Create a message
                $message = \Swift_Message::newInstance($title)
                    ->setFrom(array($email_send[$i] => 'Broadcast Message'))
                    ->setTo(array($email_send[$i]))
                    ->setBody($email_body);

                //Send the message
                $mailer->send($message); 

            }else {

            }
        
        }
        $this->_data_template['broadcast_list'] = Model_NewsletterBroadcasts::find('all');
        return \Response::forge(\View::forge('newsletter::backend/list_broadcast.twig', $this->_data_template, FALSE));
        
               // $string = $loas[1]['story'];
        //$str = explode('.', $string);
//        var_dump($str); exit;
       
        
        
    }

    public function action_delete($id) {
            // Check menu permission
            if (!$this->check_menu_permission($this->_menu_key, 'delete')) {
                // if not have an access then redirect to no permission page
                \Response::redirect(\Uri::base().'backend/no-permission');
            }
            // find model by id
            $the_model = Model_NewsletterBroadcasts::find($id);
            // if empty then redirect back with error message
            if (empty($the_model)) {
                    \Session::set_flash('error_message', 'The Broadcast with ID "'.$id.'" is not found here');
                    \Response::redirect(\Uri::base().$this->_module_url);
                    exit;
            }
            // Delete the admin
            try {
                    $the_model->delete();
                    \Session::set_flash('success_message', 'Delete The Broadcast "'.$the_model->title.'" with ID "'.$id.'" is successful');
            } catch (Orm\ValidationFailed $e) {
                    \Session::set_flash('error_message', $e->getMessage());
            }
            \Response::redirect(\Uri::base().$this->_module_url);
    }
}

