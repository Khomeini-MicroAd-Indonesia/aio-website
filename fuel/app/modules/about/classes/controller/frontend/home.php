<?php
namespace About;

class Controller_Frontend_Home extends \Controller_Frontend
{
    private $_module_url = '';
    private $_menu_key = 'about';
    private $_meta_slug = '/';

    public function before() {
        parent::before();
    }

    public function action_index() {
        
        $this->_data_template['meta_title'] = \Config::get('config_basic.app_name');
        $this->_data_template['about_detail']   = \About\Aboututil::get_about_detail();
        $this->_data_template['active_about'] = "active";
        return \Response::forge(\View::forge('about::frontend/about.twig', $this->_data_template, FALSE));
    }
    public function action_awards_and_certificates() {
        
        $this->_data_template['meta_title'] = \Config::get('config_basic.app_name');
        $this->_data_template['award_detail']           = \About\Aboututil::get_award_detail();
        $this->_data_template['certificate_detail']     = \About\Aboututil::get_certificate_detail();
        $this->_data_template['active_award'] = "active";
        return \Response::forge(\View::forge('about::frontend/awards_and_certificates.twig', $this->_data_template, FALSE));
    }
    
    public function action_history() {
        
        $this->_data_template['meta_title'] = \Config::get('config_basic.app_name');
        $this->_data_template['history_detail'] = \History\Historyutil::get_history_data();
        $this->_data_template['active_history'] = "active";
        return \Response::forge(\View::forge('about::frontend/history.twig', $this->_data_template,FALSE));
    }

    public function action_about_us_new() {
        
        $this->_data_template['meta_title'] = \Config::get('config_basic.app_name');
        $this->_data_template['president_msg']   = \About\Aboututil::get_president_statement($this->_current_lang);
        $this->_data_template['about_detail']   = \About\Aboututil::get_about_detail();
        $this->_data_template['product_concept']    = \About\Aboututil::get_product_concept();
        $this->_data_template['award_certificate'] = \About\Aboututil::get_award_certificate();
        $this->_data_template['active_about'] = "active";
        
        return \Response::forge(\View::forge('about::frontend/about_new.twig', $this->_data_template,FALSE));
    }

    public function action_history_new() {
        $this->_data_template['meta_title'] = \Config::get('config_basic.app_name');
        $year = \History\Model_Histories::query()
                ->where('status', 1)
                ->order_by('seq', 'ASC')
                ->get();
        
        foreach ($year as $k => $v) {
            $years[] = $year[$k]['year'];
        }
        $u_year = array_unique($years);
        foreach($u_year as $y){
            $month[$y] = \History\Model_Histories::query()
                    ->where('year', $y)
                    ->order_by('seq', 'ASC')
                    ->get();
        }
        
        $this->_data_template['years'] = $u_year;
        $this->_data_template['months'] = $month;
        $this->_data_template['active_about'] = "active";
        return \Response::forge(\View::forge('about::frontend/history_new.twig', $this->_data_template,FALSE));
    }
}

