<?php

namespace About;

class Aboututil{
    
    
    /**
     * get about banner 
     */

    public static function get_about_banner(){

        $banners = \about\Model_Aboutbanners::query()
                ->related('images')
                ->related('images_m')
                ->related('images_s')
                ->where('status', 1)
                ->get();

        $data = array();

        foreach ($banners as $banner){

            $data[] = array(
                'id'          => $banner->id,
                'image'       => $banner->images->filename,
                'image_m'     => $banner->images_m->filename,
                'image_s'     => $banner->images_s->filename,
            );
        }

        return $data;
    }
    
    /**
     * get president statement
     */
    
    public static function get_president_statement($lang){
        
        $presidents = \About\Model_Aboutpresidents::query()
                ->where('status', 1)
                ->limit(1)
                ->get();
        
//        $key = array_keys($presidents);
//        $string = $presidents[$key[0]]['msg_'.$lang];
//        $str    = explode('.', $string);
        
        
        $data = array();
        
        foreach ($presidents as $president){
            
            if($lang == 'id'){
                $msg = $president->msg_id;
            }else if($lang == 'en'){
                $msg = $president->msg_en;
            }   
            $data[] = array(
                'name'      => $president->name,
                'position'  => $president->position,
                'img'       => $president->image,
                'msg'       => $msg
//                'msg2'      => $str[1],
//                'msg3'      => $str[2],
//                'msg4'      => $str[3]
            );
        }
//        var_dump($data);exit;
        return $data;
    }

    /**
     * get all about otsuka
     */
    
    public static function get_about_detail(){
        
        $abouts = \About\Model_Abouts::query()
                ->where('status', 1)
                ->limit(1)
                ->get();
        
        $data = array();
        
        foreach ($abouts as $about){
            $objective = 'objective_'.\Config::get('language');
//            $logo_meaning = 'logo_meaning_'.\Config::get('language');
            $visionmission_hl = 'visionmission_hl_'.\Config::get('language');
            $visionmission_detail = 'visionmission_detail_'.\Config::get('language');
            
            $data[] = array(
                'obj'           => $about->$objective,
//                'meaning'       => $about->$logo_meaning,
                'highlight'     => $about->$visionmission_hl,
                'detail'        => $about->$visionmission_detail,
            );
        }
        return $data;
    }
    
    /**
     * get product concept
     */
    
    public static function get_product_concept(){
        
        $concepts = \Productconcept\Model_Productconcepts::query()
                ->where('status', 1)
                ->order_by('seq', 'ASC')
                ->get();
        
        $data = array();
        
        foreach ($concepts as $concept){
            
            $data[] = array(
                'img'           => $concept->image,
                'title'         => $concept->title,
            );
        }
        return $data;
    }
    
    /**
     * get award
     */
    
    public static function get_award_detail(){
        
        $awards = \About\Model_Awardcertificates::query()
                ->where('status', 1)
                ->where('type', 0)
                ->get();
        
        $data = array();
        
        foreach ($awards as $award){
            
            $title = 'title_'.\Config::get('language');
            $desc = 'desc_'.\Config::get('language');
            
            $data[] = array(
                'id'        => $award->id,
                'image'     => $award->filename,
                'desc'      => $award->$desc,
                'title'      => $award->$title
            );
        }
        return $data;
    }
    
    /**
     * get certificate
     */
    
    public static function get_certificate_detail(){
        
        $certificates = \About\Model_Awardcertificates::query()
                ->where('status', 1)
                ->where('type', 1)
                ->get();
        
        $data = array();
        
        foreach ($certificates as $certificate){
            
            $title = 'title_'.\Config::get('language');
            $desc = 'desc_'.\Config::get('language');
            
            $data[] = array(
                'id'        => $certificate->id,
                'image'     => $certificate->filename,
                'desc'      => $certificate->$desc,
                'title'      => $certificate->$title
            );
        }
        return $data;
    }
    
    /**
     * get award & certificate
     */
    
    public static function get_award_certificate(){
        
        $awards = \About\Model_Awardcertificates::query()
                ->where('status', 1)
                ->get();
        
        $data = array();
        
        foreach ($awards as $award){
            
            if($award->type == 0){
                $type = "Award";
            }else if($award->type == 1){
                $type = "Certificate";
            }
            
            $title = 'title_'.\Config::get('language');
            $desc = 'desc_'.\Config::get('language');
            
            $data[] = array(
                'id'        => $award->id,
                'type'      => $type,
                'img'       => $award->filename,
                'desc'      => $award->$desc,
                'title'      => $award->$title
            );
        }
        return $data;
    }
    
}