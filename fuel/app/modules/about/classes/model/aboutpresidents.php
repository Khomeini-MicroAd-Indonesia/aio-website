<?php
namespace About;

class Model_Aboutpresidents extends \Orm\Model {
	private $status_name = array('InActive', 'Active');
	
	protected static $_table_name = 'aboutpresidents';
        private $image_path = 'media/president/';

	protected static $_observers = array(
            'Orm\Observer_CreatedAt' => array(
                'aboutpresidents'=>array('before_insert'),
                'mysql_timestamp' => true
            ),
            'Orm\Observer_UpdatedAt' => array(
                'aboutpresidents'=>array('before_update'),
                'mysql_timestamp' => true
            ),
            'Orm\Observer_Validation' => array(
                'aboutpresidents'=>array('before_save')
            )
	);
        
	protected static $_properties = array(
            'id',
            'name' => array(
                'label' => 'Name',
                'validation' => array(
                    'required',
                    'max_length' => array(50),
                )
            ),
            'image' => array(
                'label' => 'Image',
                'validation' => array(
                    'required',
                    'max_length' => array(50),
                )
            ),
            'position' => array(
                'label' => 'Position',
                'validation' => array(
                    'max_length' => array(50)
                )
            ),
            'msg_id' => array(
                'label' => 'Message ID',
                'validation' => array()
            ),
            'msg_en' => array(
                'label' => 'Message EN',
                'validation' => array()
            ),
            'status' => array(
                'label' => 'Status',
                'validation' => array(
                    'required',
                )
            ),
            'created_by',
            'created_at',
            'updated_by',
            'updated_at'
	);
	
        public function get_image_path() {
            return $this->image_path;
        }
    
        public function get_all_images() {
            if (file_exists(DOCROOT.$this->image_path)) {
                $contents = \File::read_dir(DOCROOT.$this->image_path);
            } else {
                $contents = array();
            }
            return $contents;
        }
    
        public function get_status_name() {
            $flag = $this->status;
            return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
	}
        
        public static function get_as_array ($filter=array()) {
            $items = self::find('all', $filter);
            if (empty($items)) {
                $data = array();
            } else {
                foreach ($items as $item) {
                $data[$item->id] = $item->name;
                }
            }
            return $data;
	}
        
	public function get_form_data_basic() {
            return array(
                'attributes' => array(
                    'name'       => 'frm_about_presidents',
                    'class'      => 'form-horizontal',
                    'role'       => 'form',
                    'action'     => '',
                    'method'     => 'post',
                ),
                'hidden' => array(),
                'fieldset' => array(
                    array(
                        'label' => array(
                            'label' => 'Name',
                            'id' => 'name',
                            'attributes' => array(
                                'class' => 'col-sm-2 control-label'
                            )
                        ),
                        'input' => array(
                                'name' => 'name',
                                'value' => $this->name,
                                'attributes' => array(
                                    'class' => 'form-control',
                                    'placeholder' => 'Name',
                                    'required' => '',
                                ),
                            'container_class' => 'col-sm-10'
                            )
                    ),
                    array(
                        'label' => array(
                            'label' => 'Image',
                            'id' => 'image',
                            'attributes' => array(
                                'class' => 'col-sm-2 control-label'
                            )
                        ),
                        'select_image_picker' => array(
                            'name' => 'image',
                            'value' => $this->image,
                            'options' => $this->get_all_images(),
                            'attributes' => array(
                                'class' => 'form-control image-picker',
                            ),
                            'container_class' => 'col-sm-10',
                            'image_url' => \Uri::base().$this->image_path,
                        )
                    ),
                    array(
                        'label' => array(
                            'label' => 'Position',
                            'id' => 'position',
                            'attributes' => array(
                                'class' => 'col-sm-2 control-label'
                            )
                        ),
                        'input' => array(
                                'name' => 'position',
                                'value' => $this->position,
                                'attributes' => array(
                                    'class' => 'form-control',
                                    'placeholder' => 'Position',
                                    'required' => '',
                                ),
                            'container_class' => 'col-sm-10'
                            )
                    ),
                    array(
                        'label' => array(
                            'label' => 'Message ID',
                            'id' => 'msg_id',
                            'attributes' => array(
                                'class' => 'col-sm-2 control-label'
                            )
                        ),
                        'textarea' => array(
                            'name' => 'msg_id',
                            'value' => $this->msg_id,
                            'attributes' => array(
                                'class' => 'form-control ckeditor',
                                'placeholder' => '',
                            ),
                            'container_class' => 'col-sm-10'
                        )
                    ),
                    array(
                        'label' => array(
                            'label' => 'Message EN',
                            'id' => 'msg_en',
                            'attributes' => array(
                                'class' => 'col-sm-2 control-label'
                            )
                        ),
                        'textarea' => array(
                            'name' => 'msg_en',
                            'value' => $this->msg_en,
                            'attributes' => array(
                                'class' => 'form-control ckeditor',
                                'placeholder' => '',
                            ),
                            'container_class' => 'col-sm-10'
                        )
                    ), 
                    array(
                        'label' => array(
                            'label' => 'Status',
                            'id' => 'status',
                            'attributes' => array(
                                'class' => 'col-sm-2 control-label'
                            )
                        ),
                        'select' => array(
                            'name' => 'status',
                            'value' => $this->status,
                            'options' => $this->status_name,
                            'attributes' => array(
                                'class' => 'form-control',
                                'placeholder' => 'Status',
                                'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                        )
                    ),
                )
            );
	}
}
