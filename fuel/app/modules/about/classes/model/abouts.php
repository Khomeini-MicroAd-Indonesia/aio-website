<?php
namespace About;

class Model_Abouts extends \Orm\Model {
	private $status_name = array('InActive', 'Active');
	
	protected static $_table_name = 'abouts';

	protected static $_observers = array(
            'Orm\Observer_CreatedAt' => array(
                'abouts'=>array('before_insert'),
                'mysql_timestamp' => true
            ),
            'Orm\Observer_UpdatedAt' => array(
                'abouts'=>array('before_update'),
                'mysql_timestamp' => true
            ),
            'Orm\Observer_Validation' => array(
                'abouts'=>array('before_save')
            )
	);
        
	protected static $_properties = array(
            'id',
            'title_id' => array(
                'label' => 'Title',
                'validation' => array(
                    'required',
                )
            ),
            'title_en' => array(
                'label' => 'Title(EN)',
                'validation' => array(
                    'required',
                )
            ),
            'objective_id' => array(
                'label' => 'Objective',
                'validation' => array(
                    'required',
                )
            ),
            'objective_en' => array(
                'label' => 'Objective(EN)',
                'validation' => array(
                    'required',
                )
            ),
            'visionmission_hl_id' => array(
                'label' => 'Vision & Mission Highlight',
                'validation' => array(
                    'required',
                )
            ),
            'visionmission_hl_en' => array(
                'label' => 'Vision & Mission Highlight(EN)',
                'validation' => array(
                    'required',
                )
            ),
            'visionmission_detail_id' => array(
                'label' => 'Vision & Mission Detail',
                'validation' => array(
                    'required',
                )
            ),
            'visionmission_detail_en' => array(
                'label' => 'Vision & Mission Detail(EN)',
                'validation' => array(
                    'required',
                )
            ),
            'status' => array(
                'label' => 'Status',
                'validation' => array(
                    'required',
                )
            ),
            'created_by',
            'created_at',
            'updated_by',
            'updated_at'
	);
        
        public function get_status_name() {
            $flag = $this->status;
            return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
	}
        
        public static function get_as_array ($filter=array()) {
            $items = self::find('all', $filter);
            if (empty($items)) {
                $data = array();
            } else {
                foreach ($items as $item) {
                $data[$item->id] = $item->name;
                }
            }
            return $data;
	}
        
	public function get_form_data_basic() {
            return array(
                'attributes' => array(
                    'name'       => 'frm_abouts',
                    'class'      => 'form-horizontal',
                    'role'       => 'form',
                    'action'     => '',
                    'method'     => 'post',
                ),
                'hidden' => array(),
                'fieldset' => array(
                    array(
                        'label' => array(
                            'label' => 'Title',
                            'id' => 'title_id',
                            'attributes' => array(
                                'class' => 'col-sm-2 control-label'
                            )
                        ),
                        'input' => array(
                            'name' => 'title_id',
                            'value' => $this->title_id,
                            'attributes' => array(
                                'class' => 'form-control ckeditor',
                                'placeholder' => '',
                            ),
                            'container_class' => 'col-sm-10'
                        )
                    ), 
                    array(
                        'label' => array(
                            'label' => 'Title(EN)',
                            'id' => 'title_en',
                            'attributes' => array(
                                'class' => 'col-sm-2 control-label'
                            )
                        ),
                        'input' => array(
                            'name' => 'title_en',
                            'value' => $this->title_en,
                            'attributes' => array(
                                'class' => 'form-control ckeditor',
                                'placeholder' => '',
                            ),
                            'container_class' => 'col-sm-10'
                        )
                    ), 
                    array(
                        'label' => array(
                            'label' => 'Objective',
                            'id' => 'objective_id',
                            'attributes' => array(
                                'class' => 'col-sm-2 control-label'
                            )
                        ),
                        'textarea' => array(
                            'name' => 'objective_id',
                            'value' => $this->objective_id,
                            'attributes' => array(
                                'class' => 'form-control ckeditor',
                                'placeholder' => '',
                            ),
                            'container_class' => 'col-sm-10'
                        )
                    ), 
                    array(
                        'label' => array(
                            'label' => 'Objective(EN)',
                            'id' => 'objective_en',
                            'attributes' => array(
                                'class' => 'col-sm-2 control-label'
                            )
                        ),
                        'textarea' => array(
                            'name' => 'objective_en',
                            'value' => $this->objective_en,
                            'attributes' => array(
                                'class' => 'form-control ckeditor',
                                'placeholder' => '',
                            ),
                            'container_class' => 'col-sm-10'
                        )
                    ), 
                    array(
                        'label' => array(
                            'label' => 'Vision & Mission Highlight',
                            'id' => 'visionmission_hl_id',
                            'attributes' => array(
                                'class' => 'col-sm-2 control-label'
                            )
                        ),
                        'textarea' => array(
                            'name' => 'visionmission_hl_id',
                            'value' => $this->visionmission_hl_id,
                            'attributes' => array(
                                'class' => 'form-control ckeditor',
                                'placeholder' => '',
                            ),
                            'container_class' => 'col-sm-10'
                        )
                    ), 
                    array(
                        'label' => array(
                            'label' => 'Vision & Mission Highlight(EN)',
                            'id' => 'visionmission_hl_en',
                            'attributes' => array(
                                'class' => 'col-sm-2 control-label'
                            )
                        ),
                        'textarea' => array(
                            'name' => 'visionmission_hl_en',
                            'value' => $this->visionmission_hl_en,
                            'attributes' => array(
                                'class' => 'form-control ckeditor',
                                'placeholder' => '',
                            ),
                            'container_class' => 'col-sm-10'
                        )
                    ), 
                    array(
                        'label' => array(
                            'label' => 'Vision & Mission Detail',
                            'id' => 'visionmission_detail_id',
                            'attributes' => array(
                                'class' => 'col-sm-2 control-label'
                            )
                        ),
                        'textarea' => array(
                            'name' => 'visionmission_detail_id',
                            'value' => $this->visionmission_detail_id,
                            'attributes' => array(
                                'class' => 'form-control ckeditor',
                                'placeholder' => '',
                            ),
                            'container_class' => 'col-sm-10'
                        )
                    ), 
                    array(
                        'label' => array(
                            'label' => 'Vision & Mission Detail(EN)',
                            'id' => 'visionmission_detail_en',
                            'attributes' => array(
                                'class' => 'col-sm-2 control-label'
                            )
                        ),
                        'textarea' => array(
                            'name' => 'visionmission_detail_en',
                            'value' => $this->visionmission_detail_en,
                            'attributes' => array(
                                'class' => 'form-control ckeditor',
                                'placeholder' => '',
                            ),
                            'container_class' => 'col-sm-10'
                        )
                    ),
                    array(
                        'label' => array(
                            'label' => 'Status',
                            'id' => 'status',
                            'attributes' => array(
                                'class' => 'col-sm-2 control-label'
                            )
                        ),
                        'select' => array(
                            'name' => 'status',
                            'value' => $this->status,
                            'options' => $this->status_name,
                            'attributes' => array(
                                'class' => 'form-control',
                                'placeholder' => 'Status',
                                'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                        )
                    ),
                )
            );
	}
}
