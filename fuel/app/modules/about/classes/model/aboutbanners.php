<?php

namespace About;

class Model_Aboutbanners extends \Orm\Model {

    private $status_name = array('InActive', 'Active');
    protected static $_table_name = 'aboutbanners';
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'aboutbanners' => array('before_insert'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_UpdatedAt' => array(
            'aboutbanners' => array('before_update'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_Validation' => array(
            'aboutbanners' => array('before_save')
        )
    );
    protected static $_properties = array(
        'id',
        'image_id' => array(
            'label' => 'Image Original',
            'validation' => array(
                'required',
            )
        ),
        'image_m_id' => array(
            'label' => 'Image Medium',
            'validation' => array(
                'required',
            )
        ),
        'image_s_id' => array(
            'label' => 'Image Small',
            'validation' => array(
                'required',
            )
        ),
        'banner_title' => array(
            'label' => 'Banner Title',
            'validation' => array(
                'required',
                'max_length' => array(50),
            )
        ),
        'status' => array(
            'label' => 'Status',
            'validation' => array(
                'required',
            )
        ),
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    );
    
    protected static $_belongs_to = array(
        'images' => array(
            'key_from' => 'image_id',
            'model_to' => '\about\Model_AboutbannerImages',
            'key_to' => 'id',
            'cascade_save' => true,
            'cascade_delete' => false,
        ),
        'images_m' => array(
            'key_from' => 'image_m_id',
            'model_to' => '\about\Model_AboutbannerImages',
            'key_to' => 'id',
            'cascade_save' => true,
            'cascade_delete' => false,
        ),
        'images_s' => array(
            'key_from' => 'image_s_id',
            'model_to' => '\about\Model_AboutbannerImages',
            'key_to' => 'id',
            'cascade_save' => true,
            'cascade_delete' => false,
        ),
    );
    
    private static $_images;

    public function get_status_name() {
        $flag = $this->status;
        return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
    }

    public function get_image_name() {
        if (empty(self::$_images)) {
            self::$_images = Model_AboutbannerImages::get_as_array();
        }
        $flag = $this->image_id;
        return isset(self::$_images[$flag]) ? self::$_images[$flag] : '-';
    }

    public function get_form_data_basic($image) {
        return array(
            'attributes' => array(
                'name' => 'frm_aboutbanners',
                'class' => 'form-horizontal',
                'role' => 'form',
                'action' => '',
                'method' => 'post',
            ),
            'hidden' => array(),
            'fieldset' => array(
                array(
                    'label' => array(
                        'label' => 'Image Large',
                        'id' => 'image_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'image_id',
                        'value' => $this->image_id,
                        'options' => $image,
                        'attributes' => array(
                            'class' => 'form-control bootstrap-select',
                            'placeholder' => 'Image',
                            'data-live-search' => 'true',
                            'data-size' => '3',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Image Medium',
                        'id' => 'image_m_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'image_m_id',
                        'value' => $this->image_m_id,
                        'options' => $image,
                        'attributes' => array(
                            'class' => 'form-control bootstrap-select',
                            'placeholder' => 'Image',
                            'data-live-search' => 'true',
                            'data-size' => '3',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Image Small',
                        'id' => 'image_s_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'image_s_id',
                        'value' => $this->image_s_id,
                        'options' => $image,
                        'attributes' => array(
                            'class' => 'form-control bootstrap-select',
                            'placeholder' => 'Image',
                            'data-live-search' => 'true',
                            'data-size' => '3',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Banner Title',
                        'id' => 'banner_title',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'banner_title',
                        'value' => $this->banner_title,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Banner Title',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Status',
                        'id' => 'status',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'status',
                        'value' => $this->status,
                        'options' => $this->status_name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Status',
                            'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
            )
        );
    }

}
