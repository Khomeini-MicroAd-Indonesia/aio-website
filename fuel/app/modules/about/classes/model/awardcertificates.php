<?php
namespace About;

class Model_Awardcertificates extends \Orm\Model {
    
    private $status_name = array('InActive', 'Active');
    private $type_docs = array('Award', 'Certificate');
    private $image_path = 'media/awardcertificate/';

    protected static $_table_name = 'awardcertificates';

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'awardcertificates'=>array('before_insert'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_UpdatedAt' => array(
            'awardcertificates'=>array('before_update'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_Validation' => array(
            'awardcertificates'=>array('before_save')
        )
    );

    protected static $_properties = array(
        'id',
        'title_en' => array(
            'label' => 'Title(EN)',
            'validation' => array(
                'required',
                'max_length' => array(50),
            )
        ),
        'title_id' => array(
            'label' => 'Title(ID)',
            'validation' => array(
                'required',
                'max_length' => array(50),
            )
        ),
        'filename' => array(
            'label' => 'Image',
            'validation' => array(
                'required',
                'max_length' => array(150),
            )
        ),
        'desc_en' => array(
            'label' => 'Desc(EN)',
            'validation' => array(
                'max_length' => array(300),
            )
        ),
        'desc_id' => array(
            'label' => 'Desc(ID)',
            'validation' => array(
                'max_length' => array(300),
            )
        ),
        'type' => array(
            'label' => 'Type',
            'validation' => array(
                'required',
            )
        ),
        'status' => array(
            'label' => 'Status',
            'validation' => array(
                'required',
            )
        ),
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    );

    
    //private static $_images;
    
    public function get_status_name() {
        $flag = $this->status;
        return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
    }

    public function get_type_name() {
        $flag = $this->type;
        return isset($this->type_docs[$flag]) ? $this->type_docs[$flag] : '-';
    }

    public function get_image_path() {
            return $this->image_path;
    }

//    public function get_image_name() {
//        if (empty(self::$_images)) {
//            self::$_images = Model_AboutbannerImages::get_as_array();
//        }
//        $flag = $this->image_id;
//        return isset(self::$_images[$flag]) ? self::$_images[$flag] : '-';
//    }
    
    public function get_all_images() {
            if (file_exists(DOCROOT.$this->image_path)) {
                    $contents = \File::read_dir(DOCROOT.$this->image_path);
            } else {
                    $contents = array();
            }
            return $contents;
    }

    public static function get_as_array ($filter=array()) {
        $items = self::find('all', $filter);
        if (empty($items)) {
            $data = array();
        } else {
            foreach ($items as $item) {
            $data[$item->id] = $item->title_en;
            }
        }
        return $data;
    }
    
//    protected static $_belongs_to = array(
//        'images' => array(
//            'key_from' => 'image_id',
//            'model_to' => '\about\Model_AwardcertificateImages',
//            'key_to' => 'id',
//            'cascade_save' => true,
//            'cascade_delete' => false,
//        )
//    );
    

    public function get_form_data_basic() {
        return array(
            'attributes' => array(
                'name'       => 'frm_awardcertificates',
                'class'      => 'form-horizontal',
                'role'       => 'form',
                'action'     => '',
                'method'     => 'post',
            ),
            'hidden' => array(),
            'fieldset' => array(
                array(
                    'label' => array(
                        'label' => 'Title(EN)',
                        'id' => 'title_en',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                            'name' => 'title_en',
                            'value' => $this->title_en,
                            'attributes' => array(
                                'class' => 'form-control',
                                'placeholder' => 'Title(EN)',
                                'required' => '',
                            ),
                        'container_class' => 'col-sm-10'
                        )
                ),
                array(
                    'label' => array(
                        'label' => 'Title(ID)',
                        'id' => 'title_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                            'name' => 'title_id',
                            'value' => $this->title_id,
                            'attributes' => array(
                                'class' => 'form-control',
                                'placeholder' => 'Title(ID)',
                                'required' => '',
                            ),
                        'container_class' => 'col-sm-10'
                        )
                ),
                array(
                    'label' => array(
                        'label' => 'Desc(EN)',
                        'id' => 'desc_en',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                            'name' => 'desc_en',
                            'value' => $this->desc_en,
                            'attributes' => array(
                                'class' => 'form-control',
                                'placeholder' => 'Desc(EN)',
                                'required' => '',
                            ),
                        'container_class' => 'col-sm-10'
                        )
                ),
                array(
                    'label' => array(
                        'label' => 'Desc(ID)',
                        'id' => 'desc_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                            'name' => 'desc_id',
                            'value' => $this->desc_id,
                            'attributes' => array(
                                'class' => 'form-control',
                                'placeholder' => 'Desc(ID)',
                                'required' => '',
                            ),
                        'container_class' => 'col-sm-10'
                        )
                ),
                array(
                    'label' => array(
                        'label' => 'Image',
                        'id' => 'filename',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select_image_picker' => array(
                        'name' => 'filename',
                        'value' => $this->filename,
                        'options' => $this->get_all_images(),
                        'attributes' => array(
                            'class' => 'form-control image-picker',
                        ),
                        'container_class' => 'col-sm-10',
                        'image_url' => \Uri::base().$this->image_path,
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Type',
                        'id' => 'type',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'type',
                        'value' => $this->type,
                        'options' => $this->type_docs,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Type',
                            'required' => ''
                    ),
                    'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Status',
                        'id' => 'status',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'status',
                        'value' => $this->status,
                        'options' => $this->status_name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Status',
                            'required' => ''
                    ),
                    'container_class' => 'col-sm-10'
                    )
                ),
            )
        );
    }
}
