<?php

namespace Product;

class Productutil {

    /**
     * get pocari campaign
     */
    public static function get_pocari_campaign($slug) {

        $details = \Product\Model_Products::query()
                ->related('banners')
                ->where('slug', $slug)
                ->where('status', 1)
                ->get();

        $data = array();

        foreach ($details as $detail) {

            $data[] = array(
                'title' => $detail->title,
                'meta_title' => $detail->meta_title,
                'meta_desc' => $detail->meta_desc,
                'banner' => $detail->banners->filename
            );
        }
        return $data;
    }
    
    /**
     * get product overview
     */
    
    public static function get_product_overview($slug){
        
        $banners = \Homebanner\Model_Homebanners::query()
                ->related('images')
                ->related('logos')
                ->where('slug', $slug)
                ->where('status', 1)
                ->get();
                
        $data = array();
        //$post_date;
        foreach ($banners as $banner){
            $temp = 'content_'.\Config::get('language');
            $data[] = array(
                'slug'        => $banner->slug,
                'content'     => $banner->$temp 
            );
        }
        return $data;
    }
    
    /**
     * get product nutrition
     */
    
    public static function get_product_nutrition($product_id){
        
        $products = \Nutrition\Model_Nutritions::query()
                ->where('product_id', $product_id)
                ->where('status', 1)
                ->get();
                
        $data = array();
        
        foreach ($products as $product){
            
            $lang = \Config::get('language');
            $desc = 'desc_'.$lang;
            
            $data[] = array(
                'id'        => $product->id,       
                'name'      => $product->name,
                'image'     => $product->image,
                'desc'      => $product->$desc 
            );
        }
        return $data;
    }

}
