<?php

namespace Achievement;

class Model_Achievements extends \Orm\Model {

    private $status_name = array('InActive', 'Active');
    private $image_path = 'media/achievement/';
    protected static $_table_name = 'achievements';
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'achivements' => array('before_insert'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_UpdatedAt' => array(
            'achivements' => array('before_update'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_Validation' => array(
            'achivements' => array('before_save')
        )
    );
    protected static $_properties = array(
        'id',
        'title' => array(
            'label' => 'Title',
            'validation' => array(
                'required',
                'max_length' => array(150),
            )
        ),
        'filename' => array(
            'label' => 'Achivement Image',
            'validation' => array(
                'max_length' => array(200),
            )
        ),
        'post_date' => array(
            'label' => 'Post Date',
            'validation' => array(
                'required',
                'valid_date' => array(
                    'format' => 'Y-m-d'
                ),
            )
        ),
        'status' => array(
            'label' => 'Status',
            'validation' => array(
                'required',
            )
        ),
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    );

    public function get_status_name() {
        $flag = $this->status;
        return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
    }

    public static function get_as_array($filter = array()) {
        $items = self::find('all', $filter);
        if (empty($items)) {
            $data = array();
        } else {
            foreach ($items as $item) {
                $data[$item->id] = $item->title;
            }
        }
        return $data;
    }

    public function get_image_path() {
        return $this->image_path;
    }

    public function get_all_images() {
        if (file_exists(DOCROOT . $this->image_path)) {
            $contents = \File::read_dir(DOCROOT . $this->image_path);
        } else {
            $contents = array();
        }
        return $contents;
    }

    public function get_form_data_basic() {
        return array(
            'attributes' => array(
                'name' => 'frm_achivements',
                'class' => 'form-horizontal',
                'role' => 'form',
                'action' => '',
                'method' => 'post',
            ),
            'hidden' => array(),
            'fieldset' => array(
                array(
                    'label' => array(
                        'label' => 'Title',
                        'id' => 'ach_title',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'ach_title',
                        'value' => $this->title,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Achievement Title',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Post Date',
                        'id' => 'post_date',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'post_date',
                        'value' => $this->post_date,
                        'attributes' => array(
                            'class' => 'form-control mask-date',
                            'placeholder' => 'Post Date',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Achivement Image',
                        'id' => 'ach_image',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select_image_picker' => array(
                        'name' => 'ach_image',
                        'value' => $this->filename,
                        'options' => $this->get_all_images(),
                        'attributes' => array(
                            'class' => 'form-control image-picker',
                        ),
                        'container_class' => 'col-sm-10',
                        'image_url' => \Uri::base() . $this->image_path,
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Status',
                        'id' => 'status',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'status',
                        'value' => $this->status,
                        'options' => $this->status_name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Status',
                            'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
            )
        );
    }

}
