<?php
namespace Contactus;

class Model_Contactus extends \Orm\Model {
    
    protected static $_table_name = 'contact_us';

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'contact_us'=>array('before_insert'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_UpdatedAt' => array(
            'contact_us'=>array('before_update'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_Validation' => array(
            'contact_us'=>array('before_save')
        )
    );

    protected static $_properties = array(
        'id',
        'name' => array(
            'label' => 'Name',
            'validation' => array(
                'required',
                'max_length' => array(50),
            )
        ),
        'email' => array(
            'label' => 'Email',
            'validation' => array(
                'required',
                'max_length' => array(50),
            )
        ),
        'phone' => array(
            'label' => 'Phone',
            'validation' => array(
                'max_length' => array(50),
            )
        ),
        'fax' => array(
            'label' => 'Fax',
            'validation' => array(
                'max_length' => array(50),
            )
        ),
        'msg' => array(
            'label' => 'Message',
            'validation' => array(
                'required',
            )
        ),
        'created_at',
        'updated_at'
    );

    public function get_form_data_basic() {
        return array(
            'attributes' => array(
                'name' => 'frm_contact_us',
                'class' => 'form-horizontal',
                'role' => 'form',
                'action' => '',
                'method' => 'post',
            ),
            'hidden' => array(),
            'fieldset' => array(
                array(
                    'label' => array(
                        'label' => 'Name',
                        'id' => 'name',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'name',
                        'value' => $this->name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Name',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'email',
                        'id' => 'email',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'email',
                        'value' => $this->email,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'email',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'phone',
                        'id' => 'phone',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'phone',
                        'value' => $this->phone,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'phone',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'fax',
                        'id' => 'fax',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'fax',
                        'value' => $this->fax,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'fax',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Message',
                        'id' => 'msg',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'msg',
                        'value' => $this->msg,
                        'attributes' => array(
                            'class' => 'form-control ckeditor',
                            'placeholder' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                )
            )
        );
    }
}
