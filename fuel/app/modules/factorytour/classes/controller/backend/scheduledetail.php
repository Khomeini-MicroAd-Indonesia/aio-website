<?php
namespace Factorytour;

class Controller_Backend_Scheduledetail extends \Controller_Backend
{
	private $_module_url = 'backend/factorytour/scheduledetail';
	private $_menu_key = 'factorytour_scheduledetail';
        
	public function action_index() {
            
            $this->_data_template['schedule_list'] = Model_FactoryTourSchedules::find('all');
            
            return \Response::forge(\View::forge('factorytour::backend/list_schedule.twig', $this->_data_template, FALSE));
            
	}
	
	
}

