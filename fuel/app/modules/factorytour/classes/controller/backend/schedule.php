<?php

namespace Factorytour;

class Controller_Backend_Schedule extends \Controller_Rest
{
    public function before() {
        parent::before();
        $this->rest_format = 'json';
    }
    
    public function router($method, $params) {
        $request_method = \Request::main()->get_method();
        if (strcmp($request_method, 'POST') == 0 && strcmp($method, 'index') == 0) {
            return parent::router($method, $params);
        }
	$this->response(array('status'=> 0, 'error'=> 'Only POST method allowed'), 401);
    }
    
    public function post_index() {
        $schedules = \Input::post('schds');
        if (empty($schedules)) {
            return $this->response(array('status'=> 0, 'error'=> 'Data Format is not correct'), 400);
        }
        if (is_string($schedules)) {
            $schedules = json_decode($schedules, TRUE);
        }
        if (!is_array($schedules)) {
            return $this->response(array('status'=> 0, 'error'=> 'Data Format is not correct'), 400);
        }
        $cnt = 0;
        foreach ($schedules as $schedule) {
            $cnt += $this->_process($schedule);
        }
        return $this->response(array('status'=>1, 'proceed'=> $cnt), 200);
    }
    
    private function _process($schedule) {
        $val = 0;
        if (Model_FactoryTourAPI::check_data($schedule)) {
            $model_schedule = Model_FactoryTourAPI::get_model($schedule);
            if (Model_FactoryTourAPI::check_model($model_schedule)) {
                $model_schedule->save();
            }
            Model_FactoryTourAPI::create_log($model_schedule);
            $val = 1;
        }
        return $val;
    }
    
}
