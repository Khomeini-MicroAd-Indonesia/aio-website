<?php
namespace Factorytour;

class Controller_Backend_Schedulehistory extends \Controller_Backend
{
	private $_module_url = 'backend/factorytour/schedulehistory';
	private $_menu_key = 'factorytour_schedulehistory';
        
	public function action_index() {
            
            $this->_data_template['history_list'] = Model_FactoryTourVisitors::find('all');
            
            return \Response::forge(\View::forge('factorytour::backend/list_history.twig', $this->_data_template, FALSE));
            
	}
	
	
}

