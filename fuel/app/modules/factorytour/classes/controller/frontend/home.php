<?php
namespace Factorytour;

class Controller_Frontend_Home extends \Controller_Frontend
{
    private $_module_url = '';
    private $_menu_key = 'factorytour';
    private $_meta_slug = '/';

    public function before() {
        parent::before();
    }

    public function action_index() {
        $this->_factorytour_submission();
        $this->set_meta_info($this->_meta_slug.'explorion');
        $flag = \Session::get_flash('flag');
        $this->_data_template['flag'] = $flag;

        $this->_data_template['factory_schedules'] = Model_FactoryTourSchedules::query()->where('status',1)->where('factory',0)->order_by('visit_date')->order_by('shift')->get();
        return \Response::forge(\View::forge('factorytour::frontend/explorion.twig', $this->_data_template, FALSE));
    }
    
    public function action_factory_tour() {
        $this->_factorytour_submission();
        $this->set_meta_info($this->_meta_slug.'factory-tour');
        $flag = \Session::get_flash('flag');
        $this->_data_template['flag'] = $flag;
        $this->_data_template['factory_schedules'] = Model_FactoryTourSchedules::query()->where('status',1)->where('factory',1)->order_by('visit_date')->order_by('shift')->get();
        return \Response::forge(\View::forge('factorytour::frontend/factory_tour.twig', $this->_data_template, FALSE));
    }
    
    public function action_factory_tour_new() {
        
        $this->_factorytour_submission();

        $json = file_get_contents($this->_instagram_feed());
        $obj = json_decode($json, true, 512, JSON_BIGINT_AS_STRING);



        foreach ($obj['data'] as $post) {
            $pic_text = $post['caption']['text'];
            $pic_link = $post['link'];
            $pic_like_count = $post['likes']['count'];
            $pic_comment_count = $post['comments']['count'];
            $pic_src = str_replace("http://", "https://", $post['images']['standard_resolution']['url']);
            $pic_created_time = date("F j, Y", $post['caption']['created_time']);
            $pic_created_time = date("F j, Y", strtotime($pic_created_time . " +1 days"));
        }

        $this->_data_template['pic_obj']            = $obj['data'];
        $this->_data_template['pic_text']           = $pic_text;
        $this->_data_template['pic_link']           = $pic_link;
        $this->_data_template['pic_like_count']     = $pic_like_count;
        $this->_data_template['pic_comment_count']  = $pic_comment_count;
        $this->_data_template['pic_src']            = $pic_src;
        $this->_data_template['pic_created_time']   = $pic_created_time;

//        var_dump($obj['data']); exit;

        $this->set_meta_info($this->_meta_slug.'factory-tour-new');
        $flag = \Session::get_flash('flag');
        $this->_data_template['flag'] = $flag;
        $latest_album = Model_Albums::query()->where('status',1)->order_by('created_at', 'DESC')->get_one();
        $latest_photo = Model_FactoryTourImages::query()->where('status',1)->where('id_album',$latest_album['id'])->get();
        $i = 1;
        foreach($latest_photo as $k => $v){
            $visits[$i] = $v;
            $i = $i + 2;
        }
        $this->_data_template['latest_album'] = $visits;
        $this->_data_template['factory_explorion'] = \Factorytour\Factorytourutil::get_factorytour_data($this->_current_lang);
        $this->_data_template['factory_schedules'] = Model_FactoryTourSchedules::query()->where('status',1)->where('factory',1)->order_by('visit_date')->order_by('shift')->get();
        $this->_data_template['explore_schedules'] = Model_FactoryTourSchedules::query()->where('status',1)->where('factory',0)->order_by('visit_date')->order_by('shift')->get();
        $this->_data_template['active_explorion'] = "active";
        
        return \Response::forge(\View::forge('factorytour::frontend/factory_tour_new.twig', $this->_data_template, FALSE));
    }
    
    private function _send_email($post_data) {
        $data = array(
            'email_from' => \Config::get('config_basic.sender_email'),
            'email_subject' => '[Website aio.co.id] Factory Tour request message from ' . $post_data['name'],
            'email_reply_to' => array(
                'email' => $post_data['email'],
                'name' => $post_data['name']
            ), // Optional
            'email_data' => array(
                'base_url' => \Uri::base(),
                'name' => $post_data['name'],
                'institution' => $post_data['institution'],
                'visitor' => $post_data['visitor'],
                'address' => $post_data['address'],
                'landline' => $post_data['landline'],
                'mobile' => $post_data['mobile'],
                'email' => $post_data['email'],
                'message' => $post_data['message'],
                'factory' => strtoupper($post_data['factory']),
                'visit_date' => $post_data['visit_date'],
                'visit_date_full' => $post_data['visit_date_full'],
                'visit_shift' => $post_data['visit_shift'],
            ),
            'email_view' => 'factorytour::email/email_factorytour.twig',
            'mail_view' => 'factorytour::email/email_responder.twig',
        );
        
        if(strcasecmp($post_data['factory'], 'Kejayan') == 0){
            $to = \Config::get('config_basic.kejayan_factory_email');
        }else{
            $to = \Config::get('config_basic.sukabumi_factory_email');
        }
        
        $email = \Email::forge();

        // Set the from address
        $email->from($data['email_from'], 'Amerta Indah Otsuka');
        // Set the to address
        $email->to($to,'Factory Tour Division PT Amerta Indah Otsuka');
        // Set a subject
        $email->subject($data['email_subject']);
        // Add reply to
        $email->reply_to($data['email_data']['email'],$data['email_data']['name']);
        // And set the body.
        $email->html_body(\View::forge($data['email_view'], $data['email_data']));
        $email->send();

        $mail = \Email::forge();
        $mail->from($data['email_from'], 'Amerta Indah Otsuka');
        $mail->to($data['email_data']['email'],$data['email_data']['name']);
        $email->reply_to($to, 'Factory Tour Division Otsuka Indonesia');
        $mail->subject($data['email_subject']);
        $mail->html_body(\View::forge($data['mail_view'], $data['email_data']));
        $mail->send();
    }

    private function _factorytour_submission() {
        $_post_data = \Input::post();
        if (count($_post_data) > 0) {
            $_err_msg = [];
            $val = \Validation::forge('factorytour_validation');
            $val->add('name', 'Your Name')->add_rule('required');
            $val->add('email', 'Your Email')->add_rule('required')->add_rule('valid_email');
            $val->add('address' , 'Your Address')->add_rule('required');

            $val->set_message('required', 'Please fill in :label');
            $val->set_message('valid_email', 'Please fill in :label with valid email');
            $val->set_message('min_length', 'Please fill with min 30 character');
            if (!$val->run()) {
                foreach ($val->error() as $field => $error) {
                    $_err_msg[$field] = $error->get_message();
                }
            } else {
                $factoryvisit = new Model_FactoryTourVisitors();
                $factoryvisit->visit_date = $_post_data['visit_date'];
                $factoryvisit->visit_shift = $_post_data['visit_shift'];
                $factoryvisit->factory_name = $_post_data['factory'];
                $factoryvisit->factory_name = $_post_data['factory'];
                $factoryvisit->name = $_post_data['name'];
                $factoryvisit->institution = $_post_data['institution'];
                $factoryvisit->visitor = $_post_data['visitor'];
                $factoryvisit->address = $_post_data['address'];
                $factoryvisit->landline = $_post_data['landline'];
                $factoryvisit->mobile = $_post_data['mobile'];
                $factoryvisit->email = $_post_data['email'];
                $factoryvisit->message = $_post_data['message'];
                $factoryvisit->register_at = date('Y-m-d H:i:s');
                try {
                    $factoryvisit->save();
                    $this->_send_email($_post_data);
                    \Session::set_flash('flag',true);
                    \Response::redirect(\Uri::current());
                } catch(\Exception $ex){
                    $_err_msg[] = $ex->getMessage();
                }
            }
            $this->_data_template['err_msg'] = $_err_msg;
            $this->_data_template['post_data'] = $_post_data;
        }
    }

    private function _comment_submission() {
        $post_data = \Input::post();

        if(!empty($post_data)){
            $comment = new Model_Comments;
            $comment->name = $post_data['name'];
            $comment->comment = $post_data['message'];
            $comment->status = 0;
            $comment->save();
        }

        return \Response::forge(\View::forge('factorytour::frontend/gallery_detail.twig', $this->_data_template, FALSE));
    }
    
    public function action_gallery_detail() {
        $this->set_meta_info($this->_meta_slug);
//        $banners = Model_GalleryBanners::query()
//                ->where('status', 1)
//                ->limit(1)
//                ->get();
        $this->_data_template['banners'] = \Factorytour\Factorytourutil::get_gallery_detail();
        
        $testimonies = Model_Testimonials::query()
                ->where('status', 1)
                ->get();
        $this->_data_template['testimonies'] = $testimonies;
        
        $all_albums = Model_Albums::query()
                ->where('status', 1)
                ->order_by('created_at', 'desc')
                ->get();
        $this->_data_template['albums'] = $all_albums;
        
        $cover_image = Model_FactoryTourImages::query()
                ->where('status', 1)
                ->where('cover_album', 1)
                ->related('photos')
                ->get();
        $this->_data_template['images'] = $cover_image;
        
        $image_per_album = Model_FactoryTourImages::query()
                ->where('status', 1)
                ->related('photos')
                ->get();
        $this->_data_template['image_list'] = $image_per_album;
        
        $comments = Model_Comments::query()
                ->where('status', 1)
                ->get();
        $this->_data_template['comments'] = $comments;
        $this->_comment_submission();
        return \Response::forge(\View::forge('factorytour::frontend/gallery_detail.twig', $this->_data_template, FALSE));
    }
    
    public function action_change_image(){        
        $id_album = \Input::get('id');
        //print_r($id_album);exit();
        $photos = Model_FactoryTourImages::query()
                ->where('status', 1)
                ->where('id_album', $id_album)
                ->get();
        
        $temp = [];
        foreach($photos as $photo) {
            $temp[] = [
                'seq' => $photo->seq,
                'name' => $photo->filename,
                'cover' => $photo->cover_album,
            ];
        }
        return json_encode($temp); 
    }

    private function _instagram_feed() {

        $access_token = "1932917610.1677ed0.68df461a748e4bebbdeea778cc9ab426";
        $photo_count = 3;

        $json_link = "https://api.instagram.com/v1/users/self/media/recent/?";
        $json_link .= "access_token={$access_token}&count={$photo_count}";

        return $json_link;
    }
    
}
