<?php

namespace Factorytour;

class Controller_Frontend_Registration extends \Controller_Frontend
{
    
    private $_module_url = '';
    private $_menu_key = 'factorytour';
	
    public function before() {
        $this->_check_lang = false;
        parent::before();
    }
    
    public function action_register(){
        
        $factory = \Input::post('factory');
        $name = \Input::post('name');
        $institution = \Input::post('institution');
        $visitor = \Input::post('visitor');
        $address = \Input::post('address');
        $landline = \Input::post('landline');
        $mobile = \Input::post('mobile');
        $email = \Input::post('email');
        $message = \Input::post('message');
        $registered_at = date('Y-m-d H:i:s');
        
        $register = \Factorytour\Model_FactoryTourVisitors::forge(array(
            'factory_name' => $factory,
            'name' => $name,
            'institution' => $institution,
            'visitor' => $visitor,
            'address' => $address,
            'landline' => $landline,
            'mobile' => $mobile,
            'email' => $email,
            'message' => $message,
            'register_at' => $registered_at
        ));
        
        try {
            $register->save();
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            var_dump($message);
        }

        
    }
    
}