<?php
namespace Factorytour;

class Factorytourutil{

    /**
     * get Factory Tour data
     */
    
    public static function get_factorytour_data($lang){
        $factorytours = \Factorytour\Model_Factorytours::query()
                ->where('status', 1)
                ->get();

        $headline_lang = "headline_$lang";
        $content_lang = "content_$lang";
        
        $data = array();
       
        foreach ($factorytours as $factorytour){
            
            $total = str_word_count($factorytour->$content_lang);
            $word_total = explode(" ",$factorytour->$content_lang);
            $next_half = (count($word_total)+5)/2;
            $content_lang1 = implode(" ",array_splice($word_total,0,$next_half));
            $content_lang2 = implode(" ",array_splice($word_total,0,$next_half));  
            
            $data[] = array(
                'id'            => $factorytour->id,
                'title'         => $factorytour->title,
                'headline'      => $factorytour->$headline_lang,
                'link'          => $factorytour->link,
                'image'         => $factorytour->image,
                'content1'      => $content_lang1,
                'content2'      => $content_lang2,
            );
        }
        return $data;
    }
    
    /**
     * get Factory Tour Images
     */
    
    public static function get_factorytour_images(){
        $factorytourimages = \Factorytour\Model_Factorytourimages::query()
                ->where('status', 1)
                ->get();
                
        $data = array();
        
        foreach ($factorytourimages as $factorytourimage){
            
            $data[] = array(
                'filename'     => $factorytourimage->filename,
                'seq'          => $factorytourimage->seq, 
            );
        }
        return $data;
    }
    
    /**
     * get Factory Tour Side Images
     */
    
    public static function get_factorytour_side_images(){
        $factorytoursideimages = \Factorytour\Model_FactoryTourSideImages::query()
                ->where('status', 1)
                ->get();
                
        $data = array();
        
        foreach ($factorytoursideimages as $factorytoursideimage){
            
            $data[] = array(
                'filename'     => $factorytoursideimage->filename,
                'seq'          => $factorytoursideimage->seq, 
            );
        }
        return $data;
    }
    
    /**
     * get gallery detail detail
     */
    
    public static function get_gallery_detail(){
        $banners = \Factorytour\Model_GalleryBanners::query()
            ->where('status', 1)
            ->limit(1)
            ->get(); 
                       
        $data = array();
        //$post_date;
        foreach ($banners as $banner){
            $title = 'lead_content_'.\Config::get('language');
            
            $data[] = array(
                'title'        => $banner->$title,
                'banner'       => $banner->banner
            );
        }
        return $data;
    }
    
}

