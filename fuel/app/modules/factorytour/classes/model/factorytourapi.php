<?php

namespace Factorytour;

class Model_FactoryTourAPI extends \Model {

    public static function check_data($data) {
        $required_fields = array('factory', 'visit_date', 'shift', 'institution_name', 'status');
        foreach ($required_fields as $field) {
            if (!isset($data[$field])) {
                return FALSE;
            }
        }
        if (strtotime($data['visit_date']) < 0) {
            return FALSE;
        } elseif(strlen($data['institution_name']) <= 0) {
            return FALSE;
        }
        return TRUE;
    }
    
    public static function get_model($data) {
        $model = Model_FactoryTourSchedules::query()
                ->where('factory', $data['factory'])
                ->where('visit_date', $data['visit_date'])
                ->where('shift', $data['shift'])
                ->get_one();
        if (empty($model)) {
            $model = Model_FactoryTourSchedules::forge($data);
        } else {
            $model->institution_name = $data['institution_name'];
            $model->status = $data['status'];
        }
        return $model;
    }
    
    public static function check_model($model) {
        $val = TRUE;
        if (strcmp($model->get_factory_name(), '-') == 0) {
            $val = FALSE;
        } elseif (strcmp($model->get_shift_name(), '-') == 0) {
            $val = FALSE;
        } elseif (strcmp($model->get_status_name(), '-') == 0) {
            $val = FALSE;
        }
        return $val;
    }
    
    public static function create_log($model) {
        $year = date('Y');
        $month = date('m'); 
        $day = date('d');
        $basepath = DOCROOT.'report'.DS;
        if (!file_exists($basepath.$year)) {
            \File::create_dir($basepath,$year);
        }
        $basepath.=$year.DS;
        if (!file_exists($basepath.$month)) {
            \File::create_dir($basepath,$month);
        }
        $basepath.=$month.DS;
        $filename = $day.'.php';
        $filepath = $basepath.$filename;
        if (!\File::exists($filepath)) {
            \File::create($basepath, $filename, "<?php defined('COREPATH') or exit('No direct script access allowed'); ?>\n\n");
        }
        $content = date('Y-m-d H:i:s').' --> ID: '.$model->id.' ; Factory: '.$model->get_factory_name().' ('.$model->factory.') ; Shift: '.$model->get_shift_name().' ('.$model->shift.') ; Visit Date: '.$model->visit_date.' ; Institution Name: '.$model->institution_name.' ; Status: '.$model->get_status_name().' ('.$model->status.')'."\n";
        \File::append($basepath, $filename, $content);
    }

}
