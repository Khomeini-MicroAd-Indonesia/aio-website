<?php
namespace Factorytour;

class Model_FactoryTours extends \Orm\Model {
    private $status_name = array('InActive', 'Active');
    private $image_path = 'media/factorytour/';
    
    protected static $_table_name = 'factorytours';

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'factorytours'=>array('before_insert'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_UpdatedAt' => array(
            'factorytours'=>array('before_update'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_Validation' => array(
            'factorytours'=>array('before_save')
        )
    );
        
    protected static $_properties = array(
        'id',
        'title' => array(
            'label' => 'Title',
            'validation' => array(
                'max_length' => array(50),
            )
        ),
        'headline_id' => array(
            'label' => 'Headline ID',
            'validation' => array(
                'required'
            )
        ),
        'headline_en' => array(
            'label' => 'Headline EN',
            'validation' => array(
                'required',
            )
        ),
        'link' => array(
            'label' => 'Link',
            'validation' => array(
                'max_length' => array(150),
            )
        ),
        'image' => array(
            'label' => 'Image',
            'validation' => array(
                'required',
                'max_length' => array(50),
            )
        ),
        'content_id' => array(
            'label' => 'Content ID',
            'validation' => array()
        ),
        'content_en' => array(
            'label' => 'Content EN',
            'validation' => array()
        ),
        'status' => array(
            'label' => 'Status',
            'validation' => array(
                'required',
            )
        ),
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    );
    
//    protected static $_belongs_to = array(
//        'images' => array(
//            'key_from' => 'image_id',
//            'model_to' => '\Factorytour\Model_FactoryTourImages',
//            'key_to' => 'id',
//            'cascade_save' => true,
//            'cascade_delete' => false,
//        )
//    );
        
    //private static $_images;
    
    public function get_status_name() {
        $flag = $this->status;
        return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
    }
    
    public function get_image_path() {
        return $this->image_path;
    }

    public function get_all_images() {
        if (file_exists(DOCROOT.$this->image_path)) {
            $contents = \File::read_dir(DOCROOT.$this->image_path);
        } else {
            $contents = array();
        }
        return $contents;
    }
    
    public static function get_as_array ($filter=array()) {
        $items = self::find('all', $filter);
        if (empty($items)) {
            $data = array();
        } else {
            foreach ($items as $item) {
            $data[$item->id] = $item->name;
            }
        }
        return $data;
    }
	
    public function get_form_data_basic() {
        return array(
            'attributes' => array(
                'name' => 'frm_factory_tour',
                'class' => 'form-horizontal',
                'role' => 'form',
                'action' => '',
                'method' => 'post',
            ),
            'hidden' => array(),
            'fieldset' => array(
//                array(
//                    'label' => array(
//                        'label' => 'Image',
//                        'id' => 'image_id',
//                        'attributes' => array(
//                            'class' => 'col-sm-2 control-label'
//                        )
//                    ),
//                    'select' => array(
//                        'name' => 'image_id',
//                        'value' => $this->image_id,
//                        'options' => $image,
//                        'attributes' => array(
//                            'class' => 'form-control bootstrap-select',
//                            'placeholder' => 'Image',
//                            'data-live-search' => 'true',
//                            'data-size' => '3',
//                        ),
//                        'container_class' => 'col-sm-10'
//                    )
//                ),
                array(
                    'label' => array(
                        'label' => 'Title',
                        'id' => 'title',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'title',
                        'value' => $this->title,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Title',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Headline ID',
                        'id' => 'headline_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'headline_id',
                        'value' => $this->headline_id,
                        'attributes' => array(
                            'class' => 'form-control ckeditor',
                            'placeholder' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Headline EN',
                        'id' => 'headline_en',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'headline_en',
                        'value' => $this->headline_en,
                        'attributes' => array(
                            'class' => 'form-control ckeditor',
                            'placeholder' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Link',
                        'id' => 'link',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'link',
                        'value' => $this->link,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'URL',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Image',
                        'id' => 'image',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select_image_picker' => array(
                        'name' => 'image',
                        'value' => $this->image,
                        'options' => $this->get_all_images(),
                        'attributes' => array(
                            'class' => 'form-control image-picker',
                        ),
                        'container_class' => 'col-sm-10',
                        'image_url' => \Uri::base().$this->image_path,
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Content ID',
                        'id' => 'content_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'content_id',
                        'value' => $this->content_id,
                        'attributes' => array(
                            'class' => 'form-control ckeditor',
                            'placeholder' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Content EN',
                        'id' => 'content_en',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'content_en',
                        'value' => $this->content_en,
                        'attributes' => array(
                            'class' => 'form-control ckeditor',
                            'placeholder' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Status',
                        'id' => 'status',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'status',
                        'value' => $this->status,
                        'options' => $this->status_name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Status',
                            'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                    )
                )
            )
        );
    }
    
}
