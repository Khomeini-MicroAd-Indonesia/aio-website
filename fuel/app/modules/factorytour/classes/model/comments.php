<?php
namespace Factorytour;

class Model_Comments extends \Orm\Model {
    private $status_name = array('InActive', 'Active');
    private $status_comment = array('InActive', 'Active');
    private $image_path = 'media/gallery/';

    protected static $_table_name = 'gallery_comments';

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
                'factorytour_images'=>array('before_insert'),
                'mysql_timestamp' => true
        ),
        'Orm\Observer_UpdatedAt' => array(
                'factorytour_images'=>array('before_update'),
                'mysql_timestamp' => true
        ),
        'Orm\Observer_Validation' => array(
                'factorytour_images'=>array('before_save')
        )
    );

    protected static $_properties = array(
        'id',
        'name' => array(
            'label' => 'Name',
            'validation' => array(
                'required',
            )
        ),
        'comment' => array(
            'label' => 'Testimony',
            'validation' => array(
                'required',
            )
        ),
        'status' => array(
            'label' => 'Status',
            'validation' => array(
                'required',
            )
        ),
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    );


    public function get_status_name() {
            $flag = $this->status;
            return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
    }
    
       public function get_status_comment() {
            $flag = $this->status;
            return isset($this->status_comment[$flag]) ? $this->status_comment[$flag] : '-';
    }

    public function get_image_path() {
            return $this->image_path;
    }

    public function get_all_images() {
            if (file_exists(DOCROOT.$this->image_path)) {
                    $contents = \File::read_dir(DOCROOT.$this->image_path);
            } else {
                    $contents = array();
            }
            return $contents;
    }

    public static function get_as_array ($filter=array()) {
        $items = self::find('all', $filter);
        if (empty($items)) {
            $data = array();
        } else {
            foreach ($items as $item) {
            $data[$item->id] = $item->name;
            }
        }
        return $data;
    }

    public function get_form_data_basic() {
        return array(
            'attributes' => array(
                    'name' => 'frm_testimony',
                    'class' => 'form-horizontal',
                    'role' => 'form',
                    'action' => '',
                    'method' => 'post',
            ),
            'hidden' => array(),
            'fieldset' => array(
                array(
                    'label' => array(
                        'label' => 'Name',
                        'id' => 'name',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'name',
                        'value' => $this->name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Name',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Comment',
                        'id' => 'comment',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'comment',
                        'value' => $this->comment,
                        'attributes' => array(
                            'class' => 'form-control ckeditor',
                            'placeholder' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Status',
                        'id' => 'status',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'status',
                        'value' => $this->status,
                        'options' => $this->status_name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Status',
                            'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
            )
        );
    }
}
