<?php
namespace Factorytour;

class Model_FactoryTourSchedules extends \Orm\Model {
    
    private $shift_slot = array('-', '10:00 WIB', '13:00 WIB', '16:00 WIB');
    private $factories = array('Sukabumi', 'Kejayan');
    private $status_name = array('Available', 'Not Available');

    protected static $_table_name = 'factorytour_schedules';

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'factorytour_schedules'=>array('before_insert'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_UpdatedAt' => array(
            'factorytour_schedules'=>array('before_update'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_Validation' => array(
            'factorytour_schedules'=>array('before_save')
        )
    );
        
    protected static $_properties = array(
        'id',
        'factory' => array(
            'label' => 'Factory',
            'validation' => array(
                'required',
            )
        ),
        'institution_name' => array(
            'label' => 'Institution Name',
            'validation' => array(
                'required',
                'max_length' => array(150)
            )
        ),
        'visit_date' => array(
            'label' => 'Visit Date',
            'validation' => array(
                'required',
                'valid_date' => array(
                    'format' => 'Y-m-d'
                )
            )
        ),
        'shift' => array(
            'label' => 'Shift',
            'validation' => array(
                'required',
            )
        ),
        'status' => array(
            'label' => 'Status',
            'validation' => array(
                'required',
            )
        ),
        'created_at',
        'updated_at'
    );
    
    public function get_shift_name () {
        return isset($this->shift_slot[$this->shift]) ? $this->shift_slot[$this->shift] : '-';
    }
    
    public function get_factory_name () {
        return isset($this->factories[$this->factory]) ? $this->factories[$this->factory] : '-';
    }
    
    public function get_status_name () {
        return isset($this->status_name[$this->status]) ? $this->status_name[$this->status] : '-';
    }
    
}
