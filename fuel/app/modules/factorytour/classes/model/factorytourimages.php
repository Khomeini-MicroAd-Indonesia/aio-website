<?php
namespace Factorytour;

class Model_FactoryTourImages extends \Orm\Model {
    private $status_name = array('InActive', 'Active');
    private $as_cover = array('No', 'Yes');
    private $image_path = 'media/gallery/';

    protected static $_table_name = 'factorytour_images';

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
                'factorytour_images'=>array('before_insert'),
                'mysql_timestamp' => true
        ),
        'Orm\Observer_UpdatedAt' => array(
                'factorytour_images'=>array('before_update'),
                'mysql_timestamp' => true
        ),
        'Orm\Observer_Validation' => array(
                'factorytour_images'=>array('before_save')
        )
    );

    protected static $_properties = array(
        'id',
        'id_album',
        'name' => array(
            'label' => 'Image Name',
            'validation' => array(
                'required',
                'max_length' => array(50),
            )
        ),
        'filename' => array(
            'label' => 'Filename',
            'validation' => array(
                'required',
                'max_length' => array(50),
            )
        ),
        'status' => array(
            'label' => 'Status',
            'validation' => array(
                'required',
            )
        ),
        'seq' => array(
            'label' => 'Sequence',
            'validation' => array(
                'required',
            )
        ),
        'cover_album' => array(
            'label' => 'As Cover Album',
            'validation' => array(
                'required',
            )
        ),
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    );

//    protected static $_has_many = array(
//        'news' => array(
//            'key_from'          =>  'id',
//            'model_to'          =>  '\News\Model_News',
//            'key_to'            =>  'image_id',
//            'cascade_save'      =>  false,
//            'cascade_delete'    =>  false
//
//        )
//    );

    public function get_status_name() {
            $flag = $this->status;
            return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
    }
    
    
    public function get_as_cover() {
            $flag = $this->as_cover;
            return isset($this->as_cover[$flag]) ? $this->as_cover[$flag] : '-';
    }

    public function get_image_path() {
            return $this->image_path;
    }

    public function get_all_images() {
            if (file_exists(DOCROOT.$this->image_path)) {
                    $contents = \File::read_dir(DOCROOT.$this->image_path);
            } else {
                    $contents = array();
                    //$contents = "http://localhost:8888/MicroAd/tokyuland/assets/img/pdf-icon.png";
            }
            return $contents;
    }

    public static function get_as_array ($filter=array()) {
        $items = self::find('all', $filter);
        if (empty($items)) {
            $data = array();
        } else {
            foreach ($items as $item) {
            $data[$item->id] = $item->name;
            }
        }
        return $data;
    }

    public function get_form_data_basic($model_data = null) {
        return array(
            'attributes' => array(
                    'name' => 'frm_factorytour_image',
                    'class' => 'form-horizontal',
                    'role' => 'form',
                    'action' => '',
                    'method' => 'post',
            ),
            'hidden' => array(),
            'fieldset' => array(
                array(
                    'label' => array(
                        'label' => 'Album',
                        'id' => 'id_album',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'id_album',
                        'value' => $this->id_album,
                        'options' => $model_data['albums'],
                        'attributes' => array(
                            'class' => 'form-control bootstrap-select',
                            'placeholder' => 'Album Name',
                            'data-live-search' => 'true',
                            'data-size' => '3',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Image Name',
                        'id' => 'image_name',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'image_name',
                        'value' => $this->name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Image Name',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Status',
                        'id' => 'status',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'status',
                        'value' => $this->status,
                        'options' => $this->status_name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Status',
                            'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Seq',
                        'id' => 'image_seq',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'image_seq',
                        'value' => $this->seq,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => '0',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Image',
                        'id' => 'image_filename',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select_image_picker' => array(
                        'name' => 'image_filename',
                        'value' => $this->filename,
                        'options' => $this->get_all_images(),
                        'attributes' => array(
                            'class' => 'form-control image-picker',
                        ),
                        'container_class' => 'col-sm-10',
                        'image_url' => \Uri::base().$this->image_path,
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'As Cover Album',
                        'id' => 'cover_album',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'cover_album',
                        'value' => $this->cover_album,
                        'options' => $this->as_cover,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'As Cover Album',
                            'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
            )
        );
    }
    
    protected static $_belongs_to = array(
        'album' => array(
            'key_from' => 'id_album',
            'model_to' => 'Factorytour\\Model_Albums',
            'key_to' => 'id',
            'cascade_save' => true,
            'cascade_delete' => false,
        )
    );
    
    protected static $_has_many = array(
        'photos' => array(
            'key_from' => 'id',
            'model_to' => 'Factorytour\\Model_FactoryTourImages',
            'key_to' => 'id_album',
            'cascade_save' => true,
            'cascade_delete' => false,
        )
    );
    
}
