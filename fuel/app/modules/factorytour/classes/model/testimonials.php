<?php
namespace Factorytour;

class Model_Testimonials extends \Orm\Model {
    private $status_name = array('InActive', 'Active');
    private $image_path = 'media/gallery/';

    protected static $_table_name = 'testimonials';

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
                'factorytour_images'=>array('before_insert'),
                'mysql_timestamp' => true
        ),
        'Orm\Observer_UpdatedAt' => array(
                'factorytour_images'=>array('before_update'),
                'mysql_timestamp' => true
        ),
        'Orm\Observer_Validation' => array(
                'factorytour_images'=>array('before_save')
        )
    );

    protected static $_properties = array(
        'id',
        'name' => array(
            'label' => 'Name',
            'validation' => array(
                'required',
            )
        ),
        'institution' => array(
            'label' => 'Institution',
            'validation' => array(
                'required',
            )
        ),
        'image' => array(
            'label' => 'Image',
            'validation' => array(
                'required',
            )
        ),
        'testimony' => array(
            'label' => 'Testimony',
            'validation' => array(
                'required',
            )
        ),
        'status' => array(
            'label' => 'Status',
            'validation' => array(
                'required',
            )
        ),
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    );


    public function get_status_name() {
            $flag = $this->status;
            return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
    }

    public function get_image_path() {
            return $this->image_path;
    }

    public function get_all_images() {
            if (file_exists(DOCROOT.$this->image_path)) {
                    $contents = \File::read_dir(DOCROOT.$this->image_path);
            } else {
                    $contents = array();
            }
            return $contents;
    }

    public static function get_as_array ($filter=array()) {
        $items = self::find('all', $filter);
        if (empty($items)) {
            $data = array();
        } else {
            foreach ($items as $item) {
            $data[$item->id] = $item->name;
            }
        }
        return $data;
    }

    public function get_form_data_basic() {
        return array(
            'attributes' => array(
                    'name' => 'frm_testimony',
                    'class' => 'form-horizontal',
                    'role' => 'form',
                    'action' => '',
                    'method' => 'post',
            ),
            'hidden' => array(),
            'fieldset' => array(
                array(
                    'label' => array(
                        'label' => 'Name',
                        'id' => 'name',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'name',
                        'value' => $this->name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Name',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Institution',
                        'id' => 'institution',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'institution',
                        'value' => $this->institution,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Institution',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Testimony',
                        'id' => 'testimony',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'testimony',
                        'value' => $this->testimony,
                        'attributes' => array(
                            'class' => 'form-control ckeditor',
                            'placeholder' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Status',
                        'id' => 'status',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'status',
                        'value' => $this->status,
                        'options' => $this->status_name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Status',
                            'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Image',
                        'id' => 'image',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select_image_picker' => array(
                        'name' => 'image',
                        'value' => $this->image,
                        'options' => $this->get_all_images(),
                        'attributes' => array(
                            'class' => 'form-control image-picker',
                        ),
                        'container_class' => 'col-sm-10',
                        'image_url' => \Uri::base().$this->image_path,
                    )
                ),
            )
        );
    }
}
