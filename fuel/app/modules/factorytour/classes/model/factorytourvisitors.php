<?php
namespace Factorytour;

class Model_FactoryTourVisitors extends \Orm\Model {
	
        
	protected static $_table_name = 'factorytour_visitors';

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'factorytour_visitors'=>array('before_insert'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_UpdatedAt' => array(
			'factorytour_visitors'=>array('before_update'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_Validation' => array(
			'factorytour_visitors'=>array('before_save')
		)
	);
        
	protected static $_properties = array(
            'id',
            'factory_name' => array(
                'label' => 'Factory',
                'validation' => array(
                    'required',
                    'max_length' => array(50),
                )
            ),
            'name' => array(
                'label' => 'Name',
                'validation' => array(
                    'required',
                    'max_length' => array(50),
                )
            ),
            'institution' => array(
                'label' => 'Institution',
                'validation' => array(
                    'required',
                    'max_length' => array(50),
                )
            ),
            'visitor' => array(
                'label' => 'Visitor',
                'validation' => array(
                    'required',
                )
            ),
            'address' => array(
                'label' => 'Address',
                'validation' => array(
                    'required',
                )
            ),
            'landline' => array(
                'label' => 'Landline',
                'validation' => array(
                    'max_length' => array(50),
                )
            ),
            'mobile' => array(
                'label' => 'Mobile',
                'validation' => array(
                    'max_length' => array(50),
                )
            ),
            'email' => array(
                'label' => 'Email',
                'validation' => array(
                    'required',
                    'max_length' => array(50),
                )
            ),
            'message' => array(
                'label' => 'Message',
                'validation' => array(
                    'required',
                )
            ),
            'visit_date',
            'visit_shift',
            'register_at'
	);
	
//        public function get_status_name() {
//		$flag = $this->status;                
//		return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
//	}
        
    public static function get_as_array ($filter=array()) {
        $items = self::find('all', $filter);
        if (empty($items)) {
            $data = array();
        } else {
            foreach ($items as $item) {
            $data[$item->id] = $item->name;
            }
        }
        return $data;
    }
    
    public static function check_email($email) {
            return self::find('all', array(
                'where' => array(
                    array('email', $email)
                    )
            ));
    }
        
    public function get_form_data_basic() {
        return array(
            'attributes' => array(
                'name'       => 'frm_factorytour_visitors',
                'class'      => 'form-horizontal',
                'role'       => 'form',
                'action'     => '',
                'method'     => 'post',
            ),
            'hidden' => array(),
            'fieldset' => array(
                array(
                    'label' => array(
                        'label' => 'Factory',
                        'id' => 'factory_name',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'factory_name',
                        'value' => $this->factory_name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Factory',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Name',
                        'id' => 'name',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'name',
                        'value' => $this->name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Name',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Institution',
                        'id' => 'institution',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'institution',
                        'value' => $this->institution,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Institution',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Visitor',
                        'id' => 'visitor',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'visitor',
                        'value' => $this->visitor,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Visitor',
                            'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Address',
                        'id' => 'address',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'address',
                        'value' => $this->address,
                        'attributes' => array(
                            'class' => 'form-control ckeditor',
                            'placeholder' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Landline',
                        'id' => 'landline',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'landline',
                        'value' => $this->landline,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Landline',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Mobile',
                        'id' => 'mobile',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'mobile',
                        'value' => $this->mobile,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Mobile',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Email',
                        'id' => 'email',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'email',
                        'value' => $this->email,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Email',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Message',
                        'id' => 'message',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'message',
                        'value' => $this->message,
                        'attributes' => array(
                            'class' => 'form-control ckeditor',
                            'placeholder' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
            )
        );
    }
    
}
