<?php
namespace Factorytour;

class Model_GalleryBanners extends \Orm\Model {
    private $status_name = array('InActive', 'Active');
    private $image_path = 'media/gallery/';

    protected static $_table_name = 'gallery_banner';

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
                'factorytour_images'=>array('before_insert'),
                'mysql_timestamp' => true
        ),
        'Orm\Observer_UpdatedAt' => array(
                'factorytour_images'=>array('before_update'),
                'mysql_timestamp' => true
        ),
        'Orm\Observer_Validation' => array(
                'factorytour_images'=>array('before_save')
        )
    );

    protected static $_properties = array(
        'id',
        'lead_content_id' => array(
            'label' => 'Lead Content',
            'validation' => array(
                'required',
                'max_length' => array(255),
            )
        ),
        'lead_content_en' => array(
            'label' => 'Lead Content(EN)',
            'validation' => array(
                'required',
                'max_length' => array(255),
            )
        ),
        'banner' => array(
            'label' => 'Banner',
            'validation' => array(
                'required',
                'max_length' => array(255),
            )
        ),
        'status' => array(
            'label' => 'Status',
            'validation' => array(
                'required',
            )
        ),
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    );


    public function get_status_name() {
            $flag = $this->status;
            return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
    }

    public function get_image_path() {
            return $this->image_path;
    }

    public function get_all_images() {
            if (file_exists(DOCROOT.$this->image_path)) {
                    $contents = \File::read_dir(DOCROOT.$this->image_path);
            } else {
                    $contents = array();
            }
            return $contents;
    }

    public static function get_as_array ($filter=array()) {
        $items = self::find('all', $filter);
        if (empty($items)) {
            $data = array();
        } else {
            foreach ($items as $item) {
            $data[$item->id] = $item->name;
            }
        }
        return $data;
    }

    public function get_form_data_basic() {
        return array(
            'attributes' => array(
                    'name' => 'frm_gallery_banner',
                    'class' => 'form-horizontal',
                    'role' => 'form',
                    'action' => '',
                    'method' => 'post',
            ),
            'hidden' => array(),
            'fieldset' => array(
                array(
                    'label' => array(
                        'label' => 'Lead Content',
                        'id' => 'lead_content_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'lead_content_id',
                        'value' => $this->lead_content_id,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Lead Content',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Lead Content(EN)',
                        'id' => 'lead_content_en',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'lead_content_en',
                        'value' => $this->lead_content_en,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Lead Content(EN)',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Status',
                        'id' => 'status',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'status',
                        'value' => $this->status,
                        'options' => $this->status_name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Status',
                            'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Banner',
                        'id' => 'banner',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select_image_picker' => array(
                        'name' => 'banner',
                        'value' => $this->banner,
                        'options' => $this->get_all_images(),
                        'attributes' => array(
                            'class' => 'form-control image-picker',
                        ),
                        'container_class' => 'col-sm-10',
                        'image_url' => \Uri::base().$this->image_path,
                    )
                ),
            )
        );
    }
}
