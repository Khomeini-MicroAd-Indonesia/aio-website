<?php
namespace History;

class Historyutil{

    /**
     * get History data
     */
    
    public static function get_history_data(){
        $histories = \History\Model_Histories::query()
                ->related('images')
                ->where('status', 1)
                ->order_by('seq', 'ASC')
                ->get();
                
        $data = array();
        
        foreach ($histories as $history){
            $month = 'month_'.\Config::get('language');
            $title = 'title_'.\Config::get('language');
            $description = 'description_'.\Config::get('language');
            
            $data[] = array(
                'image'        => $history->images->filename, 
                'year'         => $history->year,
                'month'        => $history->$month,
                'title'        => $history->$title,
                'description'  => $history->$description,
                'seq'          => $history->seq,
            );
        }
        return $data;
    }
    
    
    
}

