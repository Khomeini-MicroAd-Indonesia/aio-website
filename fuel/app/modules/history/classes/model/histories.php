<?php
namespace History;

class Model_Histories extends \Orm\Model {
    private $status_name = array('InActive', 'Active');
    private $image_path = 'media/history/';

    protected static $_table_name = 'histories';

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'event'=>array('before_insert'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_UpdatedAt' => array(
            'event'=>array('before_update'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_Validation' => array(
            'event'=>array('before_save')
        )
    );

    protected static $_properties = array(
        'id',
        'image' => array(
            'label' => 'Image',
            'validation' => array(
                'required',
            )
        ),
        'year' => array(
            'label' => 'Year',
            'validation' => array(
                'required',
                'max_length' => array(4),
            )
        ),
        'month_id' => array(
            'label' => 'Month',
            'validation' => array(
                'max_length' => array(20),
            )
        ),
        'month_en' => array(
            'label' => 'Month(EN)',
            'validation' => array(
                'max_length' => array(20),
            )
        ),
        'title_id' => array(
            'label' => 'Title',
            'validation' => array(
                'required',
                'max_length' => array(100),
            )
        ),
        'title_en' => array(
            'label' => 'Title(EN)',
            'validation' => array(
                'required',
                'max_length' => array(100),
            )
        ),
        'description_id' => array(
                'label' => 'Description',
                'validation' => array()
        ),
        'description_en' => array(
            'label' => 'Description(EN)',
            'validation' => array()
        ),
        'status' => array(
            'label' => 'Status',
            'validation' => array(
                'required',
            )
        ),
        'seq' => array(
            'label' => 'Sequence',
            'validation' => array(
                'required',
            )
        ),
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    );
    
    private static $_images;
    
    public function get_status_name() {
        $flag = $this->status;
        return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
    }
    
    public function get_image_path() {
        return $this->image_path;
    }
	
    public function get_all_images() {
        if (file_exists(DOCROOT.$this->image_path)) {
            $contents = \File::read_dir(DOCROOT.$this->image_path);
        } else {
            $contents = array();
        }
        return $contents;
    }
    
    public function get_field_by_lang($field, $lang_id) {
        $val = '-';
        $field = $field.'_'.$lang_id;
        if (isset($this->$field)) {
            $val = $this->$field;
        }
        return $val;
    }

    public function get_form_data_basic() {
        return array(
            'attributes' => array(
                'name' => 'frm_history',
                'class' => 'form-horizontal',
                'role' => 'form',
                'action' => '',
                'method' => 'post',
            ),
            'hidden' => array(),
            'fieldset' => array(
                array(
                    'label' => array(
                        'label' => 'Image',
                        'id' => 'image',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select_image_picker' => array(
                        'name' => 'image',
                        'value' => $this->image,
                        'options' => $this->get_all_images(),
                        'attributes' => array(
                            'class' => 'form-control image-picker',
                        ),
                        'container_class' => 'col-sm-10',
                        'image_url' => \Uri::base().$this->image_path,
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Year',
                        'id' => 'year',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'year',
                        'value' => $this->year,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Year',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Month',
                        'id' => 'month_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'month_id',
                        'value' => $this->month_id,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Month'
                        ),
                    'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Month(EN)',
                        'id' => 'month_en',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'month_en',
                        'value' => $this->month_en,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Month(EN)'
                        ),
                    'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Title',
                        'id' => 'title_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'title_id',
                        'value' => $this->title_id,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Title',
                            'required' => '',
                        ),
                    'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Title(EN)',
                        'id' => 'title_en',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'title_en',
                        'value' => $this->title_en,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Title(EN)',
                            'required' => '',
                        ),
                    'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Description',
                        'id' => 'description_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'description_id',
                        'value' => $this->description_id,
                        'attributes' => array(
                            'class' => 'form-control ckeditor',
                            'placeholder' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                
                array(
                    'label' => array(
                        'label' => 'Description(EN)',
                        'id' => 'description_en',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'description_en',
                        'value' => $this->description_en,
                        'attributes' => array(
                            'class' => 'form-control ckeditor',
                            'placeholder' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Status',
                        'id' => 'status',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'status',
                        'value' => $this->status,
                        'options' => $this->status_name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Status',
                            'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Seq',
                        'id' => 'seq',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'seq',
                        'value' => $this->seq,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => '0',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
            )
        );
    }
}
