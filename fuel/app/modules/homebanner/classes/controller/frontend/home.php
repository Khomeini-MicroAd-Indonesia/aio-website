<?php
namespace News;

class Controller_Frontend_Home extends \Controller_Frontend
{
    private $_module_url = '';
    private $_menu_key = 'news';
    private $_meta_slug = '/';

    public function before() {
        parent::before();
    }

    public function action_index() {
        $this->set_meta_info($this->_meta_slug);
		$this->_data_template['frontend_menus'][$this->_menu_key]['menu_class_a'] = 'active';
		$selected_year = $this->param('selected_year');
		$latest_year = \News\Newsutil::get_news_latest_year();
		if (!is_numeric($selected_year)) {
                    $selected_year = $latest_year;
		}
		$this->_data_template['news_data'] = \News\Newsutil::get_news_data_group_year($this->_current_lang);
		$this->_data_template['news_selected_year'] = $selected_year;
		$this->_data_template['news_latest_year'] = $latest_year;
        $this->_data_template['banner_content'] = \Homebanner\Model_Homebanners::query()
            ->where('status' , 1);
        if (\Fuel\Core\Agent::is_mobiledevice())
        {
            return \Response::forge(\View::forge('news::frontend/m_news.twig', $this->_data_template, FALSE));
        }
        else
        {
            return \Response::forge(\View::forge('news::frontend/news.twig', $this->_data_template, FALSE));
        }
    }

}

