<?php
namespace Homebanner;

class Bannerutil{
	
    /**
     * get home banner 
     */
    
    public static function get_home_banner(){
        
        $banners = \Homebanner\Model_Homebanners::query()
                ->related('images')
                ->related('images_m')
                ->related('images_s')
                ->where('status', 1)
                ->get();
                
        $data = array();
        
        foreach ($banners as $banner){
            $title = 'title_'.\Config::get('language');
            $highlight = 'highlight_'.\Config::get('language');
            
            $data[] = array(
                'id'          => $banner->id,
                'image'       => $banner->images->filename,
                'image_m'     => $banner->images_m->filename,
                'image_s'     => $banner->images_s->filename,
                'title'       => $banner->$title,
                'highlight'   => $banner->$highlight,
                'date'        => $banner->date,
                'content'     => $banner->content,
                'title_en' => $banner->title_en,
                'content_en' => $banner->content_en,
                'text_position' => $banner->text_position
            );
        }
        
        return $data;
    }

    
    /**
     * get side image 
     */
    
    public static function get_side_image($lang){
        
        $images = \Homebanner\Model_HomebannerSideImages::query()
                ->where('status', 1)
                ->get();
        
        $teaser_lang = "teaser_$lang";
        
        $data = array();
        //$post_date;
        foreach ($images as $image){
            //$temp = 'content_'.\Config::get('language');
            $data[] = array(
                'id'          => $image->id,  
                'title'       => $image->title,
                'teaser'      => $image->$teaser_lang,
                'color'       => $image->color,  
                'image'       => $image->filename,
                'anchor'      => $image->link
            );
        }
        return $data;
    }
    
    public static function get_gallery() {

        $galleries = \Factorytour\Model_FactoryTourImages::query()
            ->where('status', 1)
            ->order_by('created_at', 'asc')
            ->limit(2)
            ->get();
        
        $data = [];
        
        foreach ($galleries as $gallery){
            //$temp = 'content_'.\Config::get('language');
            $data[] = array(
                'id'            => $gallery->id,  
                'name'          => $gallery->name,
                'filename'      => $gallery->filename, 
            );
        }
        return $data;
    }

    
}