<?php
namespace Homebanner;

class Model_HomebannerSideImages extends \Orm\Model {
	
    private $status_name = array('InActive', 'Active');
    private $image_path = 'media/homebanner/';

    protected static $_table_name = 'homebanner_sideimages';

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'homebanner_sideimage'=>array('before_insert'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_UpdatedAt' => array(
            'homebanner_sideimage'=>array('before_update'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_Validation' => array(
            'homebanner_sideimage'=>array('before_save')
        )
    );
        
    protected static $_properties = array(
        'id',
        'title' => array(
            'label' => 'Title',
            'validation' => array(
                'required',
                'max_length' => array(150),
            )
        ),
        'filename' => array(
            'label' => 'Filename',
            'validation' => array(
                'required',
                'max_length' => array(150),
            )
        ),
        'teaser_id' => array(
            'label' => 'Teaser ID',
            'validation' => array()
        ),
        'teaser_en' => array(
            'label' => 'Teaser EN',
            'validation' => array()
        ),
        'color' => array(
            'label' => 'Color',
            'validation' => array(
                'required',
                'max_length' => array(10),
            )
        ),
        'status' => array(
            'label' => 'Status',
            'validation' => array(
                'required',
            )
        ),
        'seq' => array(
            'label' => 'Sequence',
            'validation' => array(
                'required',
            )
        ),
        'link',
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    );
        
//    protected static $_has_many = array(
//        'homebanner' => array(
//            'key_from'          =>  'id',
//            'model_to'          =>  '\homebanner\Model_Homebanners',
//            'key_to'            =>  'image_id',
//            'cascade_save'      =>  false,
//            'cascade_delete'    =>  false
//        )
//    );
	
    public function get_status_name() {
        $flag = $this->status;
        return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
    }
	
    public function get_image_path() {
        return $this->image_path;
    }
	
    public function get_all_images() {
        if (file_exists(DOCROOT.$this->image_path)) {
            $contents = \File::read_dir(DOCROOT.$this->image_path);
        } else {
            $contents = array();
        }
        return $contents;
    }
        
    public static function get_as_array ($filter=array()) {
        $items = self::find('all', $filter);
        if (empty($items)) {
            $data = array();
        } else {
            foreach ($items as $item) {
            $data[$item->id] = $item->name;
            }
        }
        return $data;
    }
	
    public function get_form_data_basic() {
        return array(
            'attributes' => array(
                'name' => 'frm_homebanner_sideimage',
                'class' => 'form-horizontal',
                'role' => 'form',
                'action' => '',
                'method' => 'post',
            ),
            'hidden' => array(),
            'fieldset' => array(
                array(
                    'label' => array(
                        'label' => 'Title',
                        'id' => 'title',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'title',
                        'value' => $this->title,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Title',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Filename',
                        'id' => 'filename',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select_image_picker' => array(
                        'name' => 'filename',
                        'value' => $this->filename,
                        'options' => $this->get_all_images(),
                        'attributes' => array(
                            'class' => 'form-control image-picker',
                        ),
                        'container_class' => 'col-sm-10',
                        'image_url' => \Uri::base().$this->image_path,
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Teaser ID',
                        'id' => 'teaser_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'teaser_id',
                        'value' => $this->teaser_id,
                        'attributes' => array(
                            'class' => 'form-control ckeditor',
                            'placeholder' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Teaser EN',
                        'id' => 'teaser_en',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'teaser_en',
                        'value' => $this->teaser_en,
                        'attributes' => array(
                            'class' => 'form-control ckeditor',
                            'placeholder' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Color',
                        'id' => 'color',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'color',
                        'value' => $this->color,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Color',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Status',
                        'id' => 'status',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'status',
                        'value' => $this->status,
                        'options' => $this->status_name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Status',
                            'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Seq',
                        'id' => 'seq',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'seq',
                        'value' => $this->seq,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => '0',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Link',
                        'id' => 'link',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'link',
                        'value' => $this->link,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Link',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
            )
        );
    }
}
