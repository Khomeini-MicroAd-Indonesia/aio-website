<?php

namespace Homebanner;

class Model_Homebanners extends \Orm\Model {

    private $status_name = array('InActive', 'Active');
    private $position_options = array('TOP' => 'TOP', 'RIGHT' => 'RIGHT');
    protected static $_table_name = 'homebanners';
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'homebanners' => array('before_insert'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_UpdatedAt' => array(
            'homebanners' => array('before_update'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_Validation' => array(
            'homebanners' => array('before_save')
        )
    );
    protected static $_properties = array(
        'id',
        'image_id' => array(
            'label' => 'Image Original',
            'validation' => array(
                'required',
            )
        ),
        'image_m_id' => array(
            'label' => 'Image Medium',
            'validation' => array(
                'required',
            )
        ),
        'image_s_id' => array(
            'label' => 'Image Small',
            'validation' => array(
                'required',
            )
        ),
        'title_id' => array(
            'label' => 'Title(ID)',
            'validation' => array(
                'max_length' => array(100),
            )
        ),
        'title_en' => array(
            'label' => 'Title(EN)',
            'validation' => array(
                'max_length' => array(100),
            )
        ),
//        'press_title' => array(
//            'label' => 'Press Title',
//            'validation' => array(
//                'required',
//                'max_length' => array(50),
//            )
//        ),
        'highlight_en' => array(
            'label' => 'Highlight(EN)',
            'validation' => array(
                'max_length' => array(150),
            )
        ),
        'highlight_id' => array(
            'label' => 'Highlight(ID)',
            'validation' => array(
                'max_length' => array(150),
            )
        ),
        'date' => array(
            'label' => 'Date',
            'validation' => array()
        ),
        'text_position' => array(
            'label' => 'Text Position',
            'validation' => array(
                'required',
            )
        ),
        'content' => array(
            'label' => 'Content (Indonesia)',
            'validation' => array()
        ),
        'content_en' => array(
            'label' => 'Content (English)',
            'validation' => array()
        ),
        'status' => array(
            'label' => 'Status',
            'validation' => array(
                'required',
            )
        ),
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    );
    
    protected static $_belongs_to = array(
        'images' => array(
            'key_from' => 'image_id',
            'model_to' => '\homebanner\Model_HomebannerImages',
            'key_to' => 'id',
            'cascade_save' => true,
            'cascade_delete' => false,
        ),
        'images_m' => array(
            'key_from' => 'image_m_id',
            'model_to' => '\homebanner\Model_HomebannerImages',
            'key_to' => 'id',
            'cascade_save' => true,
            'cascade_delete' => false,
        ),
        'images_s' => array(
            'key_from' => 'image_s_id',
            'model_to' => '\homebanner\Model_HomebannerImages',
            'key_to' => 'id',
            'cascade_save' => true,
            'cascade_delete' => false,
        ),
    );
    
    private static $_images; //, $_images_m, $_images_s;

    public function get_status_name() {
        $flag = $this->status;
        return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
    }

    public function get_text_positions() {
        $flag = $this->text_position;
        return isset($this->position_options[$flag]) ? $this->position_options[$flag] : '-';
    }

    public function get_image_name() {
        if (empty(self::$_images)) {
            self::$_images = Model_HomebannerImages::get_as_array();
        }
        $flag = $this->image_id;
        return isset(self::$_images[$flag]) ? self::$_images[$flag] : '-';
    }

    public function get_form_data_basic($image) {
        return array(
            'attributes' => array(
                'name' => 'frm_homebanners',
                'class' => 'form-horizontal',
                'role' => 'form',
                'action' => '',
                'method' => 'post',
            ),
            'hidden' => array(),
            'fieldset' => array(
                array(
                    'label' => array(
                        'label' => 'Image Large',
                        'id' => 'image_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'image_id',
                        'value' => $this->image_id,
                        'options' => $image,
                        'attributes' => array(
                            'class' => 'form-control bootstrap-select',
                            'placeholder' => 'Image',
                            'data-live-search' => 'true',
                            'data-size' => '3',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Image Medium',
                        'id' => 'image_m_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'image_m_id',
                        'value' => $this->image_m_id,
                        'options' => $image,
                        'attributes' => array(
                            'class' => 'form-control bootstrap-select',
                            'placeholder' => 'Image',
                            'data-live-search' => 'true',
                            'data-size' => '3',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Image Small',
                        'id' => 'image_s_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'image_s_id',
                        'value' => $this->image_s_id,
                        'options' => $image,
                        'attributes' => array(
                            'class' => 'form-control bootstrap-select',
                            'placeholder' => 'Image',
                            'data-live-search' => 'true',
                            'data-size' => '3',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Text Position',
                        'id' => 'text_position',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'text_position',
                        'value' => $this->text_position,
                        'options' => $this->position_options,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Text Position',
                            'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Title(EN)',
                        'id' => 'title_en',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'title_en',
                        'value' => $this->title_en,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Title (English)',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Title(ID)',
                        'id' => 'title_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'title_id',
                        'value' => $this->title_id,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Title(ID)',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
//                array(
//                    'label' => array(
//                        'label' => 'Press Title',
//                        'id' => 'press_title',
//                        'attributes' => array(
//                            'class' => 'col-sm-2 control-label'
//                        )
//                    ),
//                    'input' => array(
//                        'name' => 'press_title',
//                        'value' => $this->press_title,
//                        'attributes' => array(
//                            'class' => 'form-control',
//                            'placeholder' => 'Press Title',
//                            'required' => '',
//                        ),
//                        'container_class' => 'col-sm-10'
//                    )
//                ),
                array(
                    'label' => array(
                        'label' => 'Highlight(EN)',
                        'id' => 'highlight_en',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'highlight_en',
                        'value' => $this->highlight_en,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Highlight(EN)',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Highlight(ID)',
                        'id' => 'highlight_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'highlight_id',
                        'value' => $this->highlight_id,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Highlight(ID)',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Date',
                        'id' => 'date',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'date',
                        'value' => $this->date,
                        'attributes' => array(
                            'class' => 'form-control mask-date',
                            'placeholder' => 'Date',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Content (Indonesia)',
                        'id' => 'content',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'content',
                        'value' => $this->content,
                        'attributes' => array(
                            'class' => 'form-control ckeditor',
                            'placeholder' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Content (English)',
                        'id' => 'content_en',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'content_en',
                        'value' => $this->content_en,
                        'attributes' => array(
                            'class' => 'form-control ckeditor',
                            'placeholder' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Status',
                        'id' => 'status',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'status',
                        'value' => $this->status,
                        'options' => $this->status_name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Status',
                            'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
            )
        );
    }

}
