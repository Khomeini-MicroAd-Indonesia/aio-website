<?php
namespace Explorion;

class Explorionutil{

    /**
     * get Explorion data
     */
    
    public static function get_explorion_data($lang){
        $explorions = \Explorion\Model_Explorions::query()
                ->related('images')
                ->where('status', 1)
                ->get();
        
        $title_lang = "title_$lang";
        $content_lang = "content_$lang";
        
        $data = array();
        
        foreach ($explorions as $explorion){
            
            $data[] = array(
                'image'        => $explorion->images->filename,
                'title'        => $explorion->$title_lang,
                'content'      => $explorion->$content_lang,
                'link'         => $explorion->link,
            );
        }
        return $data;
    }
    
}

