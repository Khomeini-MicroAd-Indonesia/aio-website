<?php
namespace factorytour;

class Controller_Frontend_Home extends \Controller_Frontend
{
    private $_module_url = '';
    private $_menu_key = 'factorytour';
    private $_meta_slug = '/';

    public function before() {
        parent::before();
    }

    public function action_index() {
        
        $this->_registration_submission();
        $this->set_meta_info($this->_meta_slug);
        $this->_data_template['explorion_data'] = \Explorion\Explorionutil::get_explorion_data($this->_current_lang);
        $this->_data_template['factory_data'] = \Factorytour\Factorytourutil::get_factorytour_data($this->_current_lang);
        $this->_data_template['factory_sideimages'] = \Factorytour\Factorytourutil::get_factorytour_side_images();
        
        return \Response::forge(\View::forge('factorytour::frontend/factory.twig', $this->_data_template, FALSE));
    }
    
    private function _send_email($post_data) {
        $data = array(
            'email_from' => 'khomeini@microad.co.id',//'website@lotte.co.id',
            //'email_from'    => \
            'email_to' => \Config::get('config_basic.contact_us_email_to'),
            'email_subject' => '[Website aio.co.id] Contact Us message from ' . $post_data['registration_name'],
            'email_reply_to' => array(
                'email' => $post_data['registration_email'],
                'name' => $post_data['registration_name']
            ), // Optional
            'email_data' => array(
                'base_url' => \Uri::base(),
                'contact_name' => $post_data['registration_name'],
                'contact_email' => $post_data['registration_email'],
                'contact_comment' => $post_data['registration_email'],
            ),
            'email_view' => 'pages::email/contact_us.twig',
        );
        var_dump($data['email_to']); exit;
        $email = \Email::forge();

        // Set the from address
        $email->from($data['email_from'], 'Amerta Indah Otsuka');
        // Set the to address
        $email->to($data['email_to'],'Factory Visit Division Amerta Indah Otsuka');
        // Set a subject
        $email->subject($data['email_subject']);
        // And set the body.
        $email->html_body(\View::forge($data['email_view'], $data['email_data']));
        $email->send();
        \Util_Email::queue_send_email($data);
    }
    
    private function _registration_submission() {
        $post_data = \Input::post();
        $post_file = \Input::file();
        $myemail = 'khomeini@microad.co.id';
        
        if(!empty($post_data)){
            $factory = $post_data['registration_factory'];
            $name = $post_data['registration_name'];
            $institution = $post_data['registration_institution'];
            $registrant = $post_data['registration_registrant'];
            $address = $post_data['registration_address'];
            $landline = $post_data['registration_landline'];
            $mobile = $post_data['registration_mobile'];
            $email = $post_data['registration_email'];
            $msg = $post_data['registration_message'];
            //$destination_path = DOCROOT.'media/';
            $success_message = 'Your registration has been submitted. Thanks!';
            $status_message = 1;
//            $target_path = $destination_path . basename($file['name']);
//            move_uploaded_file($file['tmp_name'], $target_path);
            
            
            $email_body = "You have received a new message. ".
            " Here are the details from:\n Applicant Name: $name to go to $factory \n ".
            "Institution: $institution\n ".
            "Registrant: $registrant\n ".
            "Address: $address\n ".
            "Telephone: $landline\n ".
            "Mobile: $mobile\n ".
            "Email: $email\n ".
            "Message: $msg\n ";
            
            $transport = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 465,'ssl')
                        ->setUsername($myemail)
                        ->setPassword('K1q2w3e@#');

            //Create the Mailer using your created Transport
            $mailer = \Swift_Mailer::newInstance($transport);
            
            
            //Create a message
            $message = \Swift_Message::newInstance($name)
              ->setFrom(array($myemail => 'Registration Form'))
              ->setTo(array($myemail))
              ->setBody($email_body);
            //$message->attach(\Swift_Attachment::fromPath($target_path));
            
            //Send the message
            $mailer->send($message);
            $this->_data_template['success_message'] = $success_message;            
            $this->_data_template['status_message'] = $status_message; 
        }  else {
            
        }
        $this->set_meta_info($this->_meta_slug);
        
        
        return \Response::forge(\View::forge('factorytour::frontend/factory.twig', $this->_data_template, FALSE));
       
        
    }
    public function action_gallery_detail() {
        $this->set_meta_info($this->_meta_slug);
        return \Response::forge(\View::forge('factorytour::frontend/gallery_detail.twig', $this->_data_template, FALSE));
    }
}

