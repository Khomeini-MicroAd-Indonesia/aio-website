<?php
namespace Distributor;

use Orm\Model;

class Controller_Frontend_Home extends \Controller_Frontend
{
    private $_module_url = '';
    private $_menu_key = 'distributor';
    private $_meta_slug = '/';

    public function before() {
        parent::before();
    }

    public function action_index() {
        $this->set_meta_info($this->_meta_slug);
        $provinces = Model_Provinces::query()
                ->where('status', 1)
                ->order_by('seq', 'asc')
                ->get();
        $this->_data_template['provinces'] = $provinces;
        return \Response::forge(\View::forge('distributor::frontend/distributor.twig', $this->_data_template, FALSE));
    }

    public function action_get_city() {
        $id_province = \Input::post('id_province');
        $cities = Model_Cities::query()
                ->where('status', 1)
                ->where('id_province', $id_province)
                ->related('distributors')
                ->get();
        $data = [];
        foreach($cities as $city) {
            $temp = [
                'id' => $city->id,
                'name' => $city->name
            ];
            foreach ($city->distributors as $dist) {
                $temp['distributors'][] = [
                    'id' => $dist->id,
                    'name' => $dist->name,
                    'address' => $dist->address,
                    'type' => $dist->get_type_name(),
                ];
            }
            $data[] = $temp;
        }
        return json_encode($data);
    }

    public function action_distributor() {
        $pro = Model_Provinces::query()->get();
        $province = Model_Cities::query()->related('distributors')->related('province')->get();
        // // echo "<pre>";
        // // var_dump($province);exit();
        // $distributor = Model_Distributors::find('all',['related' => 'city']);
        // // $city = Model_Cities::find('all');
        // // echo "<pre>";
        // // var_dump($province);exit();
        // // $distributor = Model_Cities::query()->related('distributors')->get();
        // // echo "<pre>";
        // // var_dump($distributor);exit();
        // // $this->_data_template['city'] = $city;
        // // echo "<pre>";var_dump($province);exit();
        // foreach ($province as $key => $value) {
        //   $nam = explode(" ",$value['name']);
        //     $provinsi[] = [
        //         'name'=> $value['name'],
        //         'names'=> join("",$nam),
        //       ];
        // };
        // echo "<pre>";var_dump($province);exit();
        foreach($province as $key => $value){
              foreach ($value['distributors'] as $k => $v) {
                # code...
                $branch[] = [
                  "nama"=>$v['name'],
                  "lokasi"=> $value['name'],
                  "latitude"=> $v['latitude'],
                  "longitude"=> $v['longitude'],
                  "alamat"=> $v['address'],
                  "phone_number"=> "",
                  "fax"=> ""
                ];
                // echo "<pre>";var_dump($v);exit();
              }

                  $provinsi[] = [
                      "area"=> $value['name'],
                      "area_latitude"=> "",
                      "area_longitude"=> "",
                      "alamat"=>"",
                      "phone_number"=> "",
                      "branch" => $branch
                    ];
              // $provi = array_unshift($provi,)
        }
        // echo "<pre>";var_dump(json_encode($provinsi));echo "</pre>";exit();
        $this->_data_template['province'] = $pro;//json_encode($provinsi,true);
        foreach ($province as $data => $key ) {
          foreach ($key['distributors'] as $val => $value) {
            //  $nam = explode(" ",$key['province']['name']);
          $e[] = array(
              'province' => $key['province']['name'],
              'kota' => $key['name'],
              'address' => $value['address'],
              'latitude' => $value['latitude'],
              'longitude' => $value['longitude']
          );
        };
      };
        $this->_data_template['total'] = $e;
        $this->_data_template['sub'] = $pro;
        // echo "<pre>";var_dump($pro);echo "</pre>";exit();
        // $this->_data_template['distributor'] = $distributor;
        $this->set_meta_info($this->_meta_slug);
        $this->_data_template['active_dist'] = "active";
        return \Response::forge(\View::forge('distributor::frontend/distribution_new.twig', $this->_data_template, FALSE));
    }

    public function action_getDistributor(){
        $provinsi = \Input::post();
        // var_dump($provinsi['province']);exit();
        if($provinsi['province'] == '#'){
            $total = Model_Cities::query()->related('distributors')->related('province')->get();
            // var_dump($total);exit();
            foreach ($total as $data => $key ) {
              foreach ($key['distributors'] as $val => $value) {
                // $nam = explode(" ",$key['province']['name']);
              $e[] = array(
                  'province' => $key['province']['name'],
                  'kota' => $key['name'],
                  'address' => $value['address'],
                  'latitude' => $value['latitude'],
                  'longitude' => $value['longitude']
              );
            };
          };
            return json_encode($e);
        }else{
            // var_dump($provinsi['province']);exit();
            $province = Model_Provinces::query()->where('name',$provinsi['province'])->get_one();
            if(empty($province)){
                $province = Model_Provinces::query()->where('name','like','%'.$provinsi['province'].'%')->get_one();
            }
            $total = Model_Cities::query()->related('distributors')->related('province')->where('id_province',$province['id'])->get();
            // var_dump($total);exit();
            foreach ($total as $data => $key ) {
              foreach ($key['distributors'] as $val => $value) {
              $e[] = array(
                  'province' => $key['province']['name'],
                  'kota' => $key['name'],
                  'address' => $value['address'],
                  'latitude' => $value['latitude'],
                  'longitude' => $value['longitude']
              );
            };
          };
            return json_encode($e);
        }
        // var_dump($provinsi);exit();
    }

    public function action_getProvince(){
        $provinsi = \Input::post();
        if(!empty($provinsi)){
            $lat = $provinsi['latitude'];
            $lng = $provinsi['longitude'];
            $dis = Model_Distributors::query()->where(['latitude'=>$lat,'longitude'=>$lng])->get_one();
            if(empty($dis)){
              $dis = Model_Distributors::query()->where('latitude',$lat)->or_where('longitude',$lng)->get_one();
            }
            $total = Model_Cities::query()->where('id',$dis['id_city'])->get_one();
            // var_dump($dis);var_dump($total);exit();
            // foreach ($total as $data => $key) {
            //   foreach ($dis as $val => $value) {
              $e[] = array(
                  'kota' => $total['name'],
                  'address' => $dis['address'],
                  'latitude' => $dis['latitude'],
                  'longitude' => $dis['longitude']
                );
            //   };
            // };
            // var_dump($data);var_dump($val);exit();
            // var_dump($e);exit();
            return json_encode($e);
        };
    }
}
