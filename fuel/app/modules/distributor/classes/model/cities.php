<?php

namespace Distributor;

class Model_Cities extends \Orm\Model {

    private $status_name = array('InActive', 'Active');

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'products' => array('before_insert'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_UpdatedAt' => array(
            'products' => array('before_update'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_Validation' => array(
            'products' => array('before_save')
        )
    );
    protected static $_properties = array(
        'id',
        'id_province' => array(
            'label' => 'Province',
            'validation' => array(
                'required',
            )
        ),
        'name' => array(
            'label' => 'Name',
            'validation' => array(
                'required',
            )
        ),
        'status' => array(
            'label' => 'Status',
            'validation' => array(
                'required',
            )
        ),
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    );

    public static function get_as_array($filter = array(), $first_empty=false) {
        $items = self::find('all', $filter);
        if (empty($items)) {
            $data = array();
        } else {
            if ($first_empty) {
                $data[''] = 'Please Select One';
            }
            foreach ($items as $item) {
                $data[$item->id] = $item->name;
            }
        }
        return $data;
    }

    public function get_status_name() {
        $flag = $this->status;
        return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
    }

    public function get_form_data_basic($model_data = null) {
        return array(
            'attributes' => array(
                'name' => 'frm_cities',
                'class' => 'form-horizontal',
                'role' => 'form',
                'action' => '',
                'method' => 'post',
            ),
            'hidden' => array(),
            'fieldset' => array(
                array(
                    'label' => array(
                        'label' => 'Province',
                        'id' => 'id_province',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'id_province',
                        'value' => $this->id_province,
                        'options' => $model_data['provinces'],
                        'attributes' => array(
                            'class' => 'form-control bootstrap-select',
                            'placeholder' => 'Province',
                            'data-live-search' => 'true',
                            'data-size' => '3',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Name',
                        'id' => 'name',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'name',
                        'value' => $this->name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Name',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Status',
                        'id' => 'status',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'status',
                        'value' => $this->status,
                        'options' => $this->status_name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Status',
                            'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
            )
        );
    }

    protected static $_belongs_to = array(
        'province' => array(
            'key_from' => 'id_province',
            'model_to' => 'Distributor\\Model_Provinces',
            'key_to' => 'id',
            'cascade_save' => true,
            'cascade_delete' => false,
        )
    );
    
    protected static $_has_many = array(
        'distributors' => array(
            'key_from' => 'id',
            'model_to' => 'Distributor\\Model_Distributors',
            'key_to' => 'id_city',
            'cascade_save' => true,
            'cascade_delete' => false,
        )
    );


}
