<?php

namespace Distributor;

class Model_Distributors extends \Orm\Model {

    private $status_name = array('InActive', 'Active');
    private $type_name = array('Branch', 'Distributor');

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'products' => array('before_insert'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_UpdatedAt' => array(
            'products' => array('before_update'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_Validation' => array(
            'products' => array('before_save')
        )
    );
    protected static $_properties = array(
        'id',
        'id_city' => array(
            'label' => 'City',
            'validation' => array(
                'required',
            )
        ),
        'name' => array(
            'label' => 'Name',
            'validation' => array(
                'required',
            )
        ),
        'type' => array(
            'label' => 'Type',
            'validation' => array(
                'required',
            )
        ),
        'address' => array(
            'label' => 'Address',
            'validation' => array(
                'required',
            )
        ),
        'latitude' => array(
            'label' => 'Latitude',
            'validation' => array(
                'required',
            )
        ),
        'longitude' => array(
            'label' => 'Longitude',
            'validation' => array(
                'required',
            )
        ),
        'status' => array(
            'label' => 'Status',
            'validation' => array(
                'required',
            )
        ),
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    );

    public static function get_as_array($filter = array(), $first_empty=false) {
        $items = self::find('all', $filter);
        if (empty($items)) {
            $data = array();
        } else {
            if ($first_empty) {
                $data[''] = 'Please Select One';
            }
            foreach ($items as $item) {
                $data[$item->id] = $item->name;
            }
        }
        return $data;
    }

    public function get_status_name() {
        $flag = $this->status;
        return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
    }

    public function get_type_name() {
        $flag = $this->type;
        return isset($this->type_name[$flag]) ? $this->type_name[$flag] : '-';
    }

    public function get_form_data_basic($model_data = null) {
        return array(
            'attributes' => array(
                'name' => 'frm_branches',
                'class' => 'form-horizontal',
                'role' => 'form',
                'action' => '',
                'method' => 'post',
            ),
            'hidden' => array(),
            'fieldset' => array(
                array(
                    'label' => array(
                        'label' => 'City',
                        'id' => 'id_city',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'id_city',
                        'value' => $this->id_city,
                        'options' => $model_data['cities'],
                        'attributes' => array(
                            'class' => 'form-control bootstrap-select',
                            'placeholder' => 'City',
                            'data-live-search' => 'true',
                            'data-size' => '3',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Name',
                        'id' => 'name',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'name',
                        'value' => $this->name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Name',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Latitude',
                        'id' => 'latitude',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'latitude',
                        'value' => $this->latitude,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Latitude',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Longitude',
                        'id' => 'longitude',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'longitude',
                        'value' => $this->longitude,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Longitude',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Type',
                        'id' => 'type',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'type',
                        'value' => $this->type,
                        'options' => $this->type_name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Type',
                            'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                        'label' => array(
                            'label' => 'Address',
                            'id' => 'address',
                            'attributes' => array(
                                'class' => 'col-sm-2 control-label'
                            )
                        ),
                        'textarea' => array(
                            'name' => 'address',
                            'value' => $this->address,
                            'attributes' => array(
                                'class' => 'form-control ckeditor',
                                'placeholder' => '',
                            ),
                            'container_class' => 'col-sm-10'
                        )
                    ),
                array(
                    'label' => array(
                        'label' => 'Status',
                        'id' => 'status',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'status',
                        'value' => $this->status,
                        'options' => $this->status_name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Status',
                            'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
            )
        );
    }

    protected static $_belongs_to = array(
        'city' => array(
            'key_from' => 'id_city',
            'model_to' => 'Distributor\\Model_Cities',
            'key_to' => 'id',
            'cascade_save' => true,
            'cascade_delete' => false,
        )
    );


}
