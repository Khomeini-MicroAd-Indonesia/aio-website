<?php
namespace Faq;

class Model_Faqs extends \Orm\Model {
    
    protected static $_table_name = 'faqs';
    private $category_name = array('General','Health','Product');
    const FAQS_GENERAL = 0;
    const FAQS_HEALTH = 1;
    const FAQS_PRODUCT = 2;

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'faqs'=>array('before_insert'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_UpdatedAt' => array(
            'faqs'=>array('before_update'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_Validation' => array(
            'faqs'=>array('before_save')
        )
    );

    protected static $_properties = array(
        'id',
        'product_id' => array(
            'label' => 'Product',
            'validation' => array(
                'required',
            )
        ),
        'category' => array(
            'label' => 'Category',
            'validation' => array(
                'required',
            )
        ),
        'title_id' => array(
            'label' => 'Title',
            'validation' => array(
                'required',
                'max_length' => array(150),
            )
        ),
        'title_en' => array(
            'label' => 'Title(EN)',
            'validation' => array(
                'max_length' => array(150),
            )
        ),
        'answer_id' => array(
            'label' => 'Answer',
            'validation' => array()
        ),
        'answer_en' => array(
            'label' => 'Answer(EN)',
            'validation' => array()
        ),
        'seq' => array(
            'label' => 'Seq',
            'validation' => array(
                'required',
            )
        ),
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    );
    
    protected static $_belongs_to = array(
        'products' => array(
            'key_from' => 'product_id',
            'model_to' => '\Product\Model_Products',
            'key_to' => 'id',
            'cascade_save' => true,
            'cascade_delete' => false,
        )
    );
    
    private static $_products;
    
    public function get_product_name(){
        if(empty(self::$_products)){
            self::$_products = \Product\Model_Products::get_as_array();
        }
        $flag = $this->product_id;
        return isset(self::$_products[$flag]) ? self::$_products[$flag] : '-';
    }
    
    public function get_category_name() {
        $flag = $this->category;
        return isset($this->category_name[$flag]) ? $this->category_name[$flag] : '-';
    }
    
    public function get_form_data_basic($product) {
        return array(
            'attributes' => array(
                'name' => 'frm_faqs',
                'class' => 'form-horizontal',
                'role' => 'form',
                'action' => '',
                'method' => 'post',
            ),
            'hidden' => array(),
            'fieldset' => array(
                array(
                    'label' => array(
                        'label' => 'Product',
                        'id' => 'product_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'product_id',
                        'value' => $this->product_id,
                        'options' => $product,
                        'attributes' => array(
                            'class' => 'form-control bootstrap-select',
                            'placeholder' => 'Product',
                            'data-live-search' => 'true',
                            'data-size' => '3',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Category',
                        'id' => 'category',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'category',
                        'value' => $this->category,
                        'options' => $this->category_name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Category',
                            'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Title',
                        'id' => 'title_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'title_id',
                        'value' => $this->title_id,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Title',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Title(EN)',
                        'id' => 'title_en',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'title_en',
                        'value' => $this->title_en,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Title(EN)',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Answer',
                        'id' => 'answer_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'answer_id',
                        'value' => $this->answer_id,
                        'attributes' => array(
                            'class' => 'form-control ckeditor',
                            'placeholder' => 'Answer',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Answer(EN)',
                        'id' => 'answer_en',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'answer_en',
                        'value' => $this->answer_en,
                        'attributes' => array(
                            'class' => 'form-control ckeditor',
                            'placeholder' => 'Answer(EN)',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Seq',
                        'id' => 'seq',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'seq',
                        'value' => $this->seq,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Seq',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
            )
        );
    }
}
