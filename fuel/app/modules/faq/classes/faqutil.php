<?php
namespace Faq;

class Faqutil{
    
    public static function get_faq_data($type){
        
        $title = 'title_'.\Config::get('language');
        $answer = 'answer_'.\Config::get('language');
        $faqs = \Faq\Model_Faqs::query()
            ->related('products')
            ->where('product_id', $type)
            ->order_by('seq', 'ASC')
            ->order_by('category', 'ASC')
            ->get();
        
        
        $data = array();
        $category_name = array('General', 'Health', 'Product');
        
        foreach ($faqs as $faq){
            
            $flag = $faq->category;
            $category = $category_name[$flag];
            
            $data[$category][] = array(
                'title'     => $faq->$title,
                'answer'    => $faq->$answer,
                'seq'       => $faq->seq
            );
            
        }
        return $data;
        
    }
    
}