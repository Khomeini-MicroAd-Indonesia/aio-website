<?php
namespace Event;

class Model_Events extends \Orm\Model {
	
        private $flag_status = array('Off','On');
        private $status_name = array('InActive', 'Active');
	
	protected static $_table_name = 'events';

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events'=>array('before_insert'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_UpdatedAt' => array(
			'events'=>array('before_update'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_Validation' => array(
			'events'=>array('before_save')
		)
	);
        
	protected static $_properties = array(
            'id',
            'slug' => array(
                    'label' => 'Event Slug',
                    'validation' => array(
                            'required',
                            'max_length' => array(100),
                    )
		),
            'banner_id' => array(
                    'label' => 'Banner',
                    'validation' => array(
                            'required',
                    )
            ),
            'image_id' => array(
                    'label' => 'Image',
                    'validation' => array(
                            'required',
                    )
            ),
            'meta_title' => array(
                    'label' => 'Meta Title',
                    'validation' => array(
                            'required',
                            'max_length' => array(50),
                    )
            ),
            'meta_desc' => array(
                    'label' => 'Meta Description',
                    'validation' => array(
                            'required',
                            'max_length' => array(150),
                    )
            ),
            'title' => array(
                    'label' => 'Event Title',
                    'validation' => array(
                            'required',
                            'max_length' => array(100),
                    )
            ),
            'flag' => array(
                'label' => 'Promo Flag',
                'validation' => array(
                    'required',
                )
            ),
            'teaser' => array(
                    'label' => 'Event Teaser',
                    'validation' => array(
                            'required',
                            'max_length' => array(110),
                    )
            ),
            'content' => array(
                    'label' => 'Event Content',
                    'validation' => array()
            ),
            'status' => array(
                    'label' => 'Status',
                    'validation' => array(
                            'required',
                    )
            ),
            'post_date' => array(
                    'label' => 'Post Date',
                    'validation' => array(
                            'required',
                            'valid_date' => array(
                                    'format' => 'Y-m-d'
                            )
                    )
            ),
            'created_by',
            'created_at',
            'updated_by',
            'updated_at'
	);
	
        protected static $_belongs_to = array(
            'banners' => array(
                'key_from' => 'banner_id',
                'model_to' => '\Event\Model_EventBanners',
                'key_to' => 'id',
                'cascade_save' => true,
                'cascade_delete' => false,
            ),
            'images' => array(
                'key_from' => 'image_id',
                'model_to' => '\Event\Model_EventImages',
                'key_to' => 'id',
                'cascade_save' => true,
                'cascade_delete' => false,
            )
        );
        
        private static $_banners, $_images;
        
        public function get_banner_name(){
            if(empty(self::$_banners)){
                self::$_banners = Model_EventBanners::get_as_array();
            }
            $flag = $this->image_id;
            return isset(self::$_images[$flag]) ? self::$_images[$flag] : '-';
        }
        
        public function get_image_name(){
            if(empty(self::$_images)){
                self::$_images = Model_EventImages::get_as_array();
            }
            $flag = $this->image_id;
            return isset(self::$_images[$flag]) ? self::$_images[$flag] : '-';
        }
        
        public function get_flag_name() {
            $flag = $this->flag;
            return isset($this->flag_status[$flag]) ? $this->flag_status[$flag] : '-';
        }
        
        public function get_status_name() {
		$flag = $this->status;                
		return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
	}
        
        public static function get_as_array ($filter=array()) {
            $items = self::find('all', $filter);
            if (empty($items)) {
                $data = array();
            } else {
                foreach ($items as $item) {
                $data[$item->id] = $item->name;
                }
            }
            return $data;
	}
        
	public function get_form_data_basic($banner, $image) {
		return array(
                            'attributes' => array(
                                'name'       => 'frm_events',
                                'class'      => 'form-horizontal',
                                'role'       => 'form',
                                'action'     => '',
                                'method'     => 'post',
			),
			'hidden' => array(),
			'fieldset' => array(
                            array(
                                'label' => array(
                                    'label' => 'Slug',
                                    'id' => 'slug',
                                    'attributes' => array(
                                        'class' => 'col-sm-2 control-label'
                                    )
                                ),
                                'input' => array(
                                    'name' => 'slug',
                                    'value' => $this->slug,
                                    'attributes' => array(
                                        'class' => 'form-control',
                                        'placeholder' => 'Slug',
                                        'required' => '',
                                    ),
                                    'container_class' => 'col-sm-10'
                                )
                            ),
                            array(
                                'label' => array(
                                    'label' => 'Banner',
                                    'id' => 'banner_id',
                                    'attributes' => array(
                                        'class' => 'col-sm-2 control-label'
                                    )
                                ),
                                'select' => array(
                                    'name' => 'banner_id',
                                    'value' => $this->banner_id,
                                    'options' => $banner,
                                    'attributes' => array(
                                        'class' => 'form-control bootstrap-select',
                                        'placeholder' => 'Banner',
                                        'data-live-search' => 'true',
                                        'data-size' => '3',
                                    ),
                                    'container_class' => 'col-sm-10'
                                )
                            ),
                            array(
                                'label' => array(
                                    'label' => 'Image',
                                    'id' => 'image_id',
                                    'attributes' => array(
                                        'class' => 'col-sm-2 control-label'
                                    )
                                ),
                                'select' => array(
                                    'name' => 'image_id',
                                    'value' => $this->image_id,
                                    'options' => $image,
                                    'attributes' => array(
                                        'class' => 'form-control bootstrap-select',
                                        'placeholder' => 'Image',
                                        'data-live-search' => 'true',
                                        'data-size' => '3',
                                    ),
                                    'container_class' => 'col-sm-10'
                                )
                            ),
                            array(
                                'label' => array(
                                    'label' => 'Meta Title',
                                    'id' => 'meta_title',
                                    'attributes' => array(
                                        'class' => 'col-sm-2 control-label'
                                    )
                                ),
                                'input' => array(
                                    'name' => 'meta_title',
                                    'value' => $this->meta_title,
                                    'attributes' => array(
                                        'class' => 'form-control',
                                        'placeholder' => 'Meta Title',
                                        'required' => '',
                                    ),
                                    'container_class' => 'col-sm-10'
                                )
                            ),
                            array(
                                'label' => array(
                                    'label' => 'Meta Description',
                                    'id' => 'meta_desc',
                                    'attributes' => array(
                                        'class' => 'col-sm-2 control-label'
                                    )
                                ),
                                'textarea' => array(
                                    'name' => 'meta_desc',
                                    'value' => $this->meta_desc,
                                    'attributes' => array(
                                        'class' => 'form-control',
                                        'placeholder' => 'Meta Description',
                                        'required' => '',
                                    ),
                                    'container_class' => 'col-sm-10'
                                )
                            ),
                            array(
                                'label' => array(
                                    'label' => 'Title',
                                    'id' => 'event_title',
                                    'attributes' => array(
                                        'class' => 'col-sm-2 control-label'
                                    )
                                ),
                                'input' => array(
                                    'name' => 'event_title',
                                    'value' => $this->title,
                                    'attributes' => array(
                                        'class' => 'form-control',
                                        'placeholder' => 'Title',
                                        'required' => '',
                                    ),
                                    'container_class' => 'col-sm-10'
                                )
                            ),
                            array(
                                'label' => array(
                                    'label' => 'Flag',
                                    'id' => 'promo_flag',
                                    'attributes' => array(
                                        'class' => 'col-sm-2 control-label'
                                    )
                                ),
                                'select' => array(
                                    'name' => 'flag',
                                    'value' => $this->flag_status,
                                    'options' => $this->flag_status,
                                    'attributes' => array(
                                        'class' => 'form-control',
                                        'placeholder' => 'Flag Status',
                                        'required' => ''
                                    ),
                                    'container_class' => 'col-sm-10'
                                )
                            ),
                            array(
                                'label' => array(
                                    'label' => 'Teaser',
                                    'id' => 'event_teaser',
                                    'attributes' => array(
                                        'class' => 'col-sm-2 control-label'
                                    )
                                ),
                                'input' => array(
                                    'name' => 'event_teaser',
                                    'value' => $this->teaser,
                                    'attributes' => array(
                                        'class' => 'form-control',
                                        'placeholder' => 'Teaser',
                                        'required' => '',
                                    ),
                                    'container_class' => 'col-sm-10'
                                )
                            ),
                            array(
                                'label' => array(
                                    'label' => 'Post Date',
                                    'id' => 'post_date',
                                    'attributes' => array(
                                            'class' => 'col-sm-2 control-label'
                                    )
                                ),
                                'input' => array(
                                    'name' => 'post_date',
                                    'value' => $this->post_date,
                                    'attributes' => array(
                                        'class' => 'form-control mask-date',
                                        'placeholder' => 'Post Date',
                                        'required' => '',
                                    ),
                                    'container_class' => 'col-sm-10'
                                )
                            ),
                            array(
                                'label' => array(
                                    'label' => 'Status',
                                    'id' => 'status',
                                    'attributes' => array(
                                        'class' => 'col-sm-2 control-label'
                                    )
                                ),
                                'select' => array(
                                    'name' => 'status',
                                    'value' => $this->status,
                                    'options' => $this->status_name,
                                    'attributes' => array(
                                        'class' => 'form-control',
                                        'placeholder' => 'Status',
                                        'required' => ''
                                    ),
                                    'container_class' => 'col-sm-10'
                                )
                            ),
                            array(
                                'label' => array(
                                    'label' => 'Event Content',
                                    'id' => 'event_content',
                                    'attributes' => array(
                                        'class' => 'col-sm-2 control-label'
                                    )
                                ),
                                'textarea' => array(
                                    'name' => 'event_content',
                                    'value' => $this->content,
                                    'attributes' => array(
                                        'class' => 'form-control ckeditor',
                                        'placeholder' => '',
                                    ),
                                    'container_class' => 'col-sm-10'
                                )
                            )   
                    )
		);
	}
}
