<?php
namespace Event;
use Fuel\Core\Pagination;
use Fuel\Core\Uri;

class Controller_Frontend_Home extends \Controller_Frontend
{
    private $_module_url = '';
    private $_menu_key = 'event';
    private $_meta_slug = '/';

    public function before() {
        parent::before();
    }

    public function action_index() {
        
        $this->set_meta_info($this->_meta_slug.$this->_current_lang.'/event');
        
        $event_sum = Model_Events::query()
            ->where('status',1)
            ->count();
        
        $config = array(
            'pagination_url' => Uri::current(),
            'total_items'    => $event_sum,
            'per_page'       => 3,
            'uri_segment'    => 'page',
            'show_first'    => 'true',
            'show_last'    => 'true',
        );
        
        $pagination = Pagination::forge('mypagination', $config);
        
        $selected_year = $this->param('selected_year');
        $selected_month = $this->param('selected_month');
        //var_dump($selected_month);exit;
        if (empty($selected_year)) {
            $selected_year = \Event\Eventutil::get_event_latest_year();
        }
        
        if (empty($selected_month)) {
            $selected_month = \Event\Eventutil::get_event_latest_month();
        }
        
        $this->_data_template['pagination'] = $pagination->render();
        $this->_data_template['event_list'] = \Event\Eventutil::get_event_data_group_year();
        $this->_data_template['event_display'] = \Event\Eventutil::get_event_data($pagination, $selected_year, $selected_month);
        $this->_data_template['event_selected_year'] = $selected_year;
        $this->_data_template['event_selected_month'] = $selected_month;
        
        
        return \Response::forge(\View::forge('event::frontend/event.twig', $this->_data_template, FALSE));
    }
    
    public function action_detail() {
        
        $this->set_meta_info($this->_meta_slug.$this->_current_lang.'/detail');
        $slug = $this->param('slug');
        
        $id = \Event\Eventutil::get_event_id($slug);
        
        $selected = \Event\Eventutil::get_event_detail($slug);
        
        $selected_year = $selected[0]['year'];
        $selected_month = $selected[0]['month'];
        
        if (strlen($selected[0]['meta_title']) > 0) {
            $this->_data_template['meta_title'] .= ' - ' . $selected[0]['meta_title'];
        }
        $this->_data_template['meta_desc'] = $selected[0]['meta_desc'];
        
        $this->_data_template['event_list'] = \Event\Eventutil::get_event_data_group_year();
        $this->_data_template['event_detail'] = $selected;
        $this->_data_template['promo_image'] = \Promo\Promoutil::get_promo_images();
        $this->_data_template['event_prev'] = \Event\Eventutil::get_event_prev($id, $selected_year,$selected_month);
        $this->_data_template['event_next'] = \Event\Eventutil::get_event_next($id, $selected_year,$selected_month);
        $this->_data_template['event_slide'] = \Event\Eventutil::get_event_slide($slug);
        $this->_data_template['event_publication'] = \Event\Eventutil::get_event_publication($slug);
        
        return \Response::forge(\View::forge('event::frontend/detail.twig', $this->_data_template, FALSE));
        
    }

}

