<?php
namespace Event;

class Eventutil{
	
    /**
     * get event by year
     */
    
    public static function get_event_latest_year(){
        
        $latest_year = date('Y');
        $event_archive = \Event\Model_Events::query()
            ->where('status', 1)
            ->order_by('post_date', 'DESC')
            ->get_one();
        
        if (!empty($event_archive)) {
            $latest_year = date('Y', strtotime($event_archive->post_date));
        }
        
        return $latest_year;
    }
    
    /**
     * get event by month of the year
     */
    
    public static function get_event_latest_month(){
        
        $latest_month = date('m');
        $event_archive = \Event\Model_Events::query()
            ->where('status', 1)
            ->order_by('post_date', 'DESC')
            ->get_one();
        
        if (!empty($event_archive)) {
            $latest_month = date('m', strtotime($event_archive->post_date));
        }
        
        return $latest_month;
    }
    
    /**
     * get event data
     */
    
    public static function get_event_data($pagination, $selected_year, $selected_month){
        
        $start_date = $selected_year.'-'.$selected_month.'-01';
        
        $end_date = $selected_year.'-'.$selected_month.'-'.cal_days_in_month(CAL_GREGORIAN, intval($selected_month), intval($selected_year));
        
        $events = \Event\Model_Events::query()
            ->related('images')
            ->where('status', 1)
            ->where('post_date', '>=',$start_date)
            ->where('post_date', '<=',$end_date)
            ->rows_offset($pagination->offset)
            ->rows_limit($pagination->per_page)
            ->order_by('post_date', 'DESC')
            ->get();
        
        
        $data = array();
        foreach ($events as $event){
            $year = date('Y', strtotime($event->post_date));
            
            $data[$year][] = array(
                'id'        => $event->id,
                'slug'      => $event->slug,
                'title'     => $event->title,
                'teaser'    => $event->teaser,
                'content'   => $event->content,
                'date'      => date('l, d F Y', strtotime($event->post_date)),
                'image'     => $event->images->filename
            );
            
        }
        return $data;
    }

    public static function get_event_data_group_year(){
            
        $event_archive = \Event\Model_Events::query()
            ->related('images')
            ->where('status', 1)
            ->order_by('post_date', 'DESC')
            ->get();

        $data = array();
        foreach ($event_archive as $event){
            $year = date('Y', strtotime($event->post_date));
            $month = date('m', strtotime($event->post_date));
            $data[$year][$month] = array(
                'id'        => $event->id,
                'slug'      => $event->slug,
                'title'     => $event->title,
                'teaser'    => $event->teaser,
                'content'   => $event->content,
                'date'      => date('l, d F Y', strtotime($event->post_date)),
                'image'     => $event->images->filename,
                'month_display' => date('F', strtotime($event->post_date))
            );
        }
        
        return $data;
    }
    
    /*
     *  Get Event Detail 
     */
    
    public static function get_event_detail($slug) {
        
        $events = \Event\Model_Events::query()
            ->related('banners')    
            ->related('images')
            ->where('status', 1)
            ->where('slug', $slug)
            ->order_by('post_date', 'DESC')
            ->get();
        
        
        $data = array();
        foreach ($events as $event){
            //$year = date('Y', strtotime($event->post_date));
            
            $data[] = array(
                'id'        => $event->id,
                'slug'      => $event->slug,
                'title'     => $event->title,
                'teaser'    => $event->teaser,
                'content'   => $event->content,
                'date'      => date('l, d F Y', strtotime($event->post_date)),
                'banner'    => $event->banners->filename,
                'image'     => $event->images->filename,
                'year'      => date('Y', strtotime($event->post_date)),
                'month'     => date('m', strtotime($event->post_date)),
                'meta_title'=> $event->meta_title,
                'meta_desc' => $event->meta_desc,
            );
            
        }
        return $data;
        
    }
    
    /**
     * Get Event Preview
     */
    
    public static function get_event_preview(){
        
        $events = \Event\Model_Events::query()
                ->related('images')
                ->where('flag', 1)
                ->where('status', 1)
                ->order_by('post_date', 'DESC')
                ->limit(4)
                ->get();
        
                
        $data = array();
        
        foreach ($events as $event){
            
            $data[] = array(
                'type'      => 'event',
                'slug'      => $event->slug,
                'title'     => $event->title,
                'teaser'    => $event->teaser,
                'image'     => $event->images->filename,
                'post_date' => $event->post_date,
            );
        }
        return $data;
    }
    
    /*
     *  Get Event ID
     */
    
    public static function get_event_id($slug) {
        
        //$start_date = $selected_year.'-'.$selected_month.'-01';
        
        //$end_date = $selected_year.'-'.$selected_month.'-'.cal_days_in_month(CAL_GREGORIAN, intval($selected_month), intval($selected_year));
        
        
        $events = \Event\Model_Events::query()
            ->where('status', 1)
            ->where('slug', $slug)
//            ->where('post_date', '>=',$start_date)
//            ->where('post_date', '<=',$end_date)
            ->order_by('post_date', 'DESC')
            ->get();
        
        
        $data = array();
        foreach ($events as $event){
            
            $data[] = array(
                'id'        => $event->id
            );
            
        }
        return $data;
        
    }
    
    /*
     *  Get Previous Event 
     */
    
    public static function get_event_prev($id, $selected_year, $selected_month) {
        
        $start_date = $selected_year.'-'.$selected_month.'-01';
        
        $end_date = $selected_year.'-'.$selected_month.'-'.cal_days_in_month(CAL_GREGORIAN, intval($selected_month), intval($selected_year));
        
        $events = \Event\Model_Events::query()
            ->where('status', 1)
            ->where('id', '<', $id)
            ->where('post_date', '>=',$start_date)
            ->where('post_date', '<=',$end_date)
            ->order_by('post_date', 'DESC')
            ->get();
        
        
        $data = array();
        foreach ($events as $event){
            
            $data[] = array(
                'id'        => $event->id,
                'slug'      => $event->slug,
                'title'     => $event->title,
                'teaser'    => $event->teaser,
                'content'   => $event->content,
                'date'      => date('l, d F Y', strtotime($event->post_date)),
                'banner'    => $event->banners->filename,
                'image'     => $event->images->filename
            );
            
        }
        return $data;
        
    }
    
    /*
     *  Get Next Event 
     */
    
    public static function get_event_next($id, $selected_year, $selected_month) {
        
        $start_date = $selected_year.'-'.$selected_month.'-01';
        
        $end_date = $selected_year.'-'.$selected_month.'-'.cal_days_in_month(CAL_GREGORIAN, intval($selected_month), intval($selected_year));
        
        $events = \Event\Model_Events::query()
            ->where('status', 1)
            ->where('id', '>', $id)
            ->where('post_date', '>=',$start_date)
            ->where('post_date', '<=',$end_date)
            ->order_by('post_date', 'DESC')
            ->get();
        
        
        $data = array();
        foreach ($events as $event){
            
            $data[] = array(
                'id'        => $event->id,
                'slug'      => $event->slug,
                'title'     => $event->title,
                'teaser'    => $event->teaser,
                'content'   => $event->content,
                'date'      => date('l, d F Y', strtotime($event->post_date)),
                'banner'    => $event->banners->filename,
                'image'     => $event->images->filename
            );
            
        }
        return $data;
        
    }
    
    
    /*
     *  Get Event Slide Images 
     */
    
    public static function get_event_slide($slug) {
        
        $events = \Event\Model_EventSlideImages::query()
            ->related('images')
            ->where('status', 1)
            ->where('slug', $slug)
            ->get();
        
        
        $data = array();
        foreach ($events as $event){
            
            $data[] = array(
                'desc'      => $event->desc,
                'image'     => $event->images->filename
            );
            
        }
        return $data;
        
    }
    
    /*
     *  Get Event Publications 
     */
    
    public static function get_event_publication($slug) {
        
        $events = \Event\Model_EventPublications::query()
            ->where('status', 1)
            ->where('slug', $slug)
            ->get();
        
        
        $data = array();
        foreach ($events as $event){
            
            $data[] = array(
                'url'      => $event->url
            );
            
        }
        return $data;
        
    }
    
}