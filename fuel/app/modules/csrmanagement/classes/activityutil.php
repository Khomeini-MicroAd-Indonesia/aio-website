<?php
namespace Csrmanagement;

class Activityutil{

    /**
     * get activity data
     */
    
    public static function get_activity_data(){
        $activities = \Csrmanagement\Model_Activities::query()
                ->where('status', 1)
                ->order_by('seq')
                ->order_by('date', 'desc')
                ->limit(3)
                ->get();
                
        $data = array();
        //$post_date;
        foreach ($activities as $activity){
            $title = 'title_'.\Config::get('language');
            $content = 'content_'.\Config::get('language');
            
            $data[] = array(
                'title'        => $activity->$title,
                'slug'         => $activity->slug,
                'img'          => $activity->image,
                'date'         => $activity->date,
                'content'      => $activity->$content
            );
        }
        return $data;
    }
    
    /**
     * get activity display
     */
    
    public static function get_activity_display(){
        $activities = \Csrmanagement\Model_Activities::query()
                ->where('status', 1)
                ->order_by('seq')
                ->order_by('date', 'desc')
                ->get();
                
        $data = array();
        //$post_date;
        foreach ($activities as $activity){
            $title = 'title_'.\Config::get('language');
            $content = 'content_'.\Config::get('language');
            
            $data[] = array(
                'category'     => $activity->category,
                'title'        => $activity->$title,
                'slug'         => $activity->slug,
                'image'        => $activity->image,
                'date'         => $activity->date,
                'content'      => $activity->$content
            );
        }
        return $data;
    }
    
}

