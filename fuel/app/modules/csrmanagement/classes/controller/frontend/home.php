<?php
namespace Csrmanagement;

class Controller_Frontend_Home extends \Controller_Frontend
{
    private $_module_url = '';
    private $_menu_key = 'csr';
    private $_meta_slug = '/';

    public function before() {
        parent::before();
    }

    public function action_index() {
        $this->set_meta_info($this->_meta_slug . $this->_current_lang . '/csr');
        $this->_data_template['activities'] = \Csrmanagement\Activityutil::get_activity_display();
        $this->_data_template['active_cerdaskan_bangsa'] = "active";
        return \Response::forge(\View::forge('csrmanagement::frontend/csr_cerdaskan_bangsa.twig', $this->_data_template, FALSE));
    }

    public function action_peduli_lingkungan() {
        $this->set_meta_info($this->_meta_slug . $this->_current_lang . '/csr');
        $this->_data_template['activities'] = \Csrmanagement\Activityutil::get_activity_display();
        $this->_data_template['active_peduli_lingkungan'] = "active";
        return \Response::forge(\View::forge('csrmanagement::frontend/csr_peduli_lingkungan.twig', $this->_data_template, FALSE));
    }

    public function action_sehatkan_bangsa() {
        $this->set_meta_info($this->_meta_slug . $this->_current_lang . '/csr');
        $this->_data_template['activities'] = \Csrmanagement\Activityutil::get_activity_display();
        $this->_data_template['active_sehatkan_bangsa'] = "active";
        return \Response::forge(\View::forge('csrmanagement::frontend/csr_sehatkan_bangsa.twig', $this->_data_template, FALSE));
    }

    public function action_shcb() {
        $this->set_meta_info($this->_meta_slug . $this->_current_lang . '/csr');
        $this->_data_template['activities'] = \Csrmanagement\Activityutil::get_activity_display();
        $this->_data_template['active_cerdaskan_bangsa'] = "active";
        $this->_data_template['active_csr'] = "active";
        return \Response::forge(\View::forge('csrmanagement::frontend/shcb.twig', $this->_data_template, FALSE));
    }

    public function action_shpl() {
        $this->set_meta_info($this->_meta_slug . $this->_current_lang . '/csr');
        $this->_data_template['activities'] = \Csrmanagement\Activityutil::get_activity_display();
        $this->_data_template['active_peduli_lingkungan'] = "active";
        $this->_data_template['active_csr'] = "active";
        return \Response::forge(\View::forge('csrmanagement::frontend/shpl.twig', $this->_data_template, FALSE));
    }

    public function action_shsb() {
        $this->set_meta_info($this->_meta_slug . $this->_current_lang . '/csr');
        $this->_data_template['activities'] = \Csrmanagement\Activityutil::get_activity_display();
        $this->_data_template['active_sehatkan_bangsa'] = "active";
        $this->_data_template['active_csr'] = "active";
        return \Response::forge(\View::forge('csrmanagement::frontend/shsb.twig', $this->_data_template, FALSE));
    }

}

