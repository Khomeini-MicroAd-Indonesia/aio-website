<?php

namespace Csrmanagement;

class Model_Activities extends \Orm\Model {
    
    private $category_name = array('Cerdaskan Bangsa', 'Peduli Lingkungan', 'Sehatkan Bangsa');
    private $status_name = array('InActive', 'Active');
    private $image_path = 'media/csr/';
    protected static $_table_name = 'activities';
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'activities' => array('before_insert'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_UpdatedAt' => array(
            'activities' => array('before_update'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_Validation' => array(
            'activities' => array('before_save')
        )
    );
    protected static $_properties = array(
        'id',
        'category' => array(
            'label' => 'Category',
            'validation' => array(
                'required',
            )
        ),
        'title_id' => array(
            'label' => 'Title',
            'validation' => array(
                'required',
                'max_length' => array(150),
            )
        ),
        'title_en' => array(
            'label' => 'Title(EN)',
            'validation' => array(
                'required',
                'max_length' => array(150),
            )
        ),
        'slug' => array(
            'label' => 'Slug',
            'validation' => array(
                'max_length' => array(200),
            )
        ),
        'date' => array(
            'label' => 'Date',
            'validation' => array(
                'valid_date' => array(
                    'format' => 'Y-m-d'
                )
            )
        ),
        'status' => array(
            'label' => 'Status',
            'validation' => array(
                'required',
            )
        ),
        'seq' => array(
            'label' => 'Sequence',
            'validation' => array(
                'required',
            )
        ),
        'content_id' => array(
            'label' => 'Content',
            'validation' => array()
        ),
        'content_en' => array(
            'label' => 'Content(EN)',
            'validation' => array()
        ),
        'highlight' => array(
            'label' => 'Highlight',
            'validation' => array(
                'max_length' => array(150),
            )
        ),
        'image' => array(
            'label' => 'Image',
            'validation' => array(
                'required',
                'max_length' => array(500),
            )
        ),
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    );
    
    public function get_image_path() {
        return $this->image_path;
    }

    public function get_all_images() {
        if (file_exists(DOCROOT.$this->image_path)) {
            $contents = \File::read_dir(DOCROOT.$this->image_path);
        } else {
            $contents = array();
        }
        return $contents;
    }
    
    public function get_category_name() {
        $flag = $this->category;
        return isset($this->category_name[$flag]) ? $this->category_name[$flag] : '-';
    }
    
    public function get_status_name() {
        $flag = $this->status;
        return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
    }

    public static function get_as_array($filter = array()) {
        $items = self::find('all', $filter);
        if (empty($items)) {
            $data = array();
        } else {
            foreach ($items as $item) {
                $data[$item->id] = $item->name;
            }
        }
        return $data;
    }

    public function get_form_data_basic($data=null) {
        return array(
            'attributes' => array(
                'name' => 'frm_csr_activity',
                'class' => 'form-horizontal',
                'role' => 'form',
                'action' => '',
                'method' => 'post',
            ),
            'hidden' => array(),
            'fieldset' => array(
                array(
                    'label' => array(
                        'label' => 'Category',
                        'id' => 'category',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'category',
                        'value' => $this->category,
                        'options' => $this->category_name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Category',
                            'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Title',
                        'id' => 'title_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'title_id',
                        'value' => $this->title_id,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Title',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Title(EN)',
                        'id' => 'title_en',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'title_en',
                        'value' => $this->title_en,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Title(EN)',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Slug',
                        'id' => 'slug',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'slug',
                        'value' => $this->slug,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Slug',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Date',
                        'id' => 'date',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'date',
                        'value' => $this->date,
                        'attributes' => array(
                            'class' => 'form-control mask-date',
                            'placeholder' => 'Date',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Highlight',
                        'id' => 'highlight',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'highlight',
                        'value' => $this->highlight,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Highlight',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Content',
                        'id' => 'content_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'content_id',
                        'value' => $this->content_id,
                        'attributes' => array(
                            'class' => 'form-control ckeditor',
                            'placeholder' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Content(EN)',
                        'id' => 'content_en',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'content_en',
                        'value' => $this->content_en,
                        'attributes' => array(
                            'class' => 'form-control ckeditor',
                            'placeholder' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Image',
                        'id' => 'image_filename',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select_image_picker' => array(
                        'name' => 'image_filename',
                        'value' => $this->image,
                        'options' => $this->get_all_images(),
                        'attributes' => array(
                            'class' => 'form-control image-picker',
                        ),
                        'container_class' => 'col-sm-10',
                        'image_url' => \Uri::base().$this->image_path,
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Status',
                        'id' => 'status',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'status',
                        'value' => $this->status,
                        'options' => $this->status_name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Status',
                            'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Seq',
                        'id' => 'seq',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'seq',
                        'value' => $this->seq,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => '0',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
            )
        );
    }

}
