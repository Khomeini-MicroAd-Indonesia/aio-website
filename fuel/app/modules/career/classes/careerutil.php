<?php
namespace Career;

class Careerutil{
	
    /**
     * get career data
     */
    
    public static function get_career_data($lang_id){
        
        $careers = \Career\Model_Careers::query()
                ->where('status', 1)
                ->order_by('seq','asc')
                ->get();
                
        $data = array();
        
        foreach ($careers as $career){
            $req = 'req_'.$lang_id;
            
            $data[] = array(
                'id'        => $career->id,
                'name'      => $career->name,
                'level'     => $career->level,
                'location'  => $career->location,
                'date'      => $career->date,
                'req'       => $career->$req,
                'seq'       => $career->seq
            );
        }
        return $data;
    }
    
    public static function get_content(){
        
        $careers = \Career\Model_Careercontents::query()
                ->where('status', 1)
                ->limit(1)
                ->get();
                
        $data = array();
        
        foreach ($careers as $career){
            
            $data[] = array(
                'id'        => $career->id,
                'link'      => $career->link,
                'image'     => $career->image,
                'image_m'   => $career->image_m,
                'image_s'   => $career->image_s
            );
        }
        return $data;
    }

    
}