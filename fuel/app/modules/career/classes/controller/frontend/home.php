<?php
namespace Career;

class Controller_Frontend_Home extends \Controller_Frontend
{
    private $_module_url = '';
    private $_menu_key = 'career';
    private $_meta_slug = '/';

    public function before() {
        parent::before();
    }

    public function action_index() {
        
        $this->_application_submission();
        $this->_data_template['success_message'] = \Session::get_flash('application_success_message');
        $this->set_meta_info($this->_meta_slug.$this->_current_lang.'/career');
        $this->_data_template['career_detail'] = \Career\Careerutil::get_career_data($this->_current_lang);
        $this->_data_template['career_content'] = \Career\Careerutil::get_content();
        
        return \Response::forge(\View::forge('career::frontend/career.twig', $this->_data_template, FALSE));
    }
    private function _send_email($type, $email_to, $email_data) {
           $email = \Email::forge();

           // Set the from address
           $email->from(\Config::get('config_basic.sender_email'), 'Amerta Indah Otsuka');
           
           if($type == 1){
           // Set the to address
                $email->to($email_to, 'HRD Division Amerta Indah Otsuka');
           }else{
                $email->to($email_to); 
           }
           // Set a subject
           $email->subject('Career Applicant - '.$email_data['name']);
           
           if($type == 1){
                $email->attach(DOCROOT.'media/career/files/'.$email_data['filename']);
           }
           // And set the body.
           if($type == 1){
                $email->html_body(\View::forge('career::email/email_career.twig', $email_data));
           }else{
                $email->html_body(\View::forge('career::email/email_responder.twig', $email_data));
           }
           $email->send();
    }
    
    private function _application_submission() {
        $post_data = \Input::post();
        $post_file = \Input::file();
        $flag = false;
        //var_dump(); exit;
        if(!empty($post_data) && !empty($post_file)){
            
            if(empty($post_data['g-recaptcha-response'])){

               \Session::set_flash('status', 'yes');
               \Session::set_flash('error_message', 'Please activate the captcha');
               
           }
           
            if($post_file['cv_file']['size']>2097152){

               \Session::set_flash('status', 'yes');
               \Session::set_flash('error_message', 'File should not be more than 2MB');
               
           }
           
           if($post_file['cv_file']['size']<=2097152 && !empty($post_data['g-recaptcha-response'])){
               
                 $position = $post_data['application_position'];
                 $name = $post_data['application_name'];
                 $place = $post_data['application_place'];
                 $date = $post_data['application_date'];
                 $dob = date("Y-m-d", strtotime($date));
                 $sex = $post_data['application_sex'];
                 $status = $post_data['application_status'];
                 $address = $post_data['application_address'];
                 $city = $post_data['application_city'];
                 $country = $post_data['application_country'];
                 $mobile = $post_data['application_mobile'];
                 $email_applicant = $post_data['application_email'];
                 $file = $post_file['cv_file'];
                 $destination_path = DOCROOT.'media'.DS.'career'.DS.'files'.DS;
                 $success_message = 'Your application has been submitted. Thanks!';
                 $status_message = 1;

                 //Renaming file uploaded
                 $path = $file['name'];
                 $ext = pathinfo($path, PATHINFO_EXTENSION);
                 $applicant_doc = "AIO_CV_"."$name".".".$ext;
                 $target_path = $destination_path . basename($applicant_doc);
                 $registered_at = date('Y-m-d H:i:s');
                 if (file_exists($destination_path)) {
                     move_uploaded_file($file['tmp_name'], $target_path);
                 }else{
                     mkdir($destination_path, 0777);
                     move_uploaded_file($file['tmp_name'], $target_path);
                 }

                 $register = \Career\Model_CareerApplicants::forge(array(
                     'position' => $position,
                     'name' => $name,
                     'birth_place' => $place,
                     'dob' => $dob,
                     'gender' => $sex,
                     'marriage_status' => $status,
                     'address' => $address,
                     'city'  => $city,
                     'country'   => $country,
                     'phone' => $mobile,
                     'email' => $email_applicant,
                     'filename'  => $applicant_doc,
                     'register_at'  => $registered_at,
                 ));

                 try {
                     $register->save();
                 } catch (Exception $exc) {
                     echo $exc->getTraceAsString();
                 }

                 $email_data = array(
                     'base_url' => \Uri::base(),
                     'position' => $position,
                     'name' => $name,
                     'birth_place' => $place,
                     'dob' => $dob,
                     'gender' => $sex,
                     'marriage_status' => $status,
                     'address' => $address,
                     'city'  => $city,
                     'country'   => $country,
                     'phone' => $mobile,
                     'email' => $email_applicant,
                     'filename'  => $applicant_doc,
                 );
                 \Session::set_flash('alert_status', 'no');
                 $this->_send_email(1, \Config::get('config_basic.career_email_to'),$email_data);
                 $this->_send_email(2, $email_applicant, $email_data);
                 $flag = true;
               
           } 
        }
        
        $this->set_meta_info($this->_meta_slug);


        $this->_data_template['flag'] = $flag;
        return \Response::forge(\View::forge('career::frontend/career.twig', $this->_data_template, FALSE));
       
        
    }

    public function action_career_new() {
        
        $this->_application_submission();
        //var_dump(\Session::get_flash('alert_status')); exit;
        $this->_data_template['success_message'] = \Session::get_flash('application_success_message');
        $this->_data_template['status'] = \Session::get_flash('status');
        $this->_data_template['error_message'] = \Session::get_flash('error_message');
        $this->set_meta_info($this->_meta_slug.$this->_current_lang.'/career-new');
        $this->_data_template['career_detail'] = \Career\Careerutil::get_career_data($this->_current_lang);
        $this->_data_template['career_content'] = \Career\Careerutil::get_content();
        $this->_data_template['active_career'] = "active";
        
        return \Response::forge(\View::forge('career::frontend/career_new.twig', $this->_data_template, FALSE));
    }

    public function action_life_at_otsuka_new() {
        
        $this->set_meta_info($this->_meta_slug.$this->_current_lang.'/life-at-otsuka-new');
        $this->_data_template['leadership_vision'] = \Lifeatotsuka\LAOutil::get_lao_leardership_vision();
        $this->_data_template['leadership_philosophy'] = \Lifeatotsuka\LAOutil::get_lao_philosophy();
        $this->_data_template['lao_data'] = \Lifeatotsuka\LAOutil::get_lao_data();
        $this->_data_template['active_career'] = "active";
        
        return \Response::forge(\View::forge('career::frontend/life_at_otsuka_new.twig', $this->_data_template, FALSE));
    }

}

