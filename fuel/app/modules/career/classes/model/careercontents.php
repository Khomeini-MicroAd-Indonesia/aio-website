<?php
namespace Career;

class Model_Careercontents extends \Orm\Model {
	private $status_name = array('InActive', 'Active');
        private $image_path = 'media/career/';
	
	protected static $_table_name = 'careercontents';

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'careers'=>array('before_insert'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_UpdatedAt' => array(
			'careers'=>array('before_update'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_Validation' => array(
			'careers'=>array('before_save')
		)
	);
        
	protected static $_properties = array(
		'id',
                'link' => array(
                    'label' => 'Link',
                    'validation' => array(
                        'required',
                        'max_length' => array(50),
                    )
                ),
                'image' => array(
                    'label' => 'Image',
                    'validation' => array(
                        'required',
                    )
                ),
                'image_m' => array(
                    'label' => 'Image for medium layout',
                    'validation' => array()
		),
                'image_s' => array(
                    'label' => 'Image for small layout',
                    'validation' => array(
                        'required',
                    )
                ),
                'status' => array(
                    'label' => 'Status',
                    'validation' => array(
                        'required',
                    )
		),
		'created_by',
		'created_at',
		'updated_by',
		'updated_at'
	);
	
        public function get_status_name() {
		$flag = $this->status;
		return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
	}
        
        public function get_image_path() {
		return $this->image_path;
	}
        
        public function get_all_images() {
		if (file_exists(DOCROOT.$this->image_path)) {
			$contents = \File::read_dir(DOCROOT.$this->image_path);
		} else {
			$contents = array();
		}
		return $contents;
	}
        
	public function get_form_data_basic() {
		return array(
                            'attributes' => array(
                                'name'       => 'frm_careercontents',
                                'class'      => 'form-horizontal',
                                'role'       => 'form',
                                'action'     => '',
                                'method'     => 'post',
			),
			'hidden' => array(),
			'fieldset' => array(
                            array(
                                'label' => array(
                                    'label' => 'Link',
                                    'id' => 'link',
                                    'attributes' => array(
                                        'class' => 'col-sm-2 control-label'
                                    )
                                ),
                                'input' => array(
                                    'name' => 'link',
                                    'value' => $this->link,
                                    'attributes' => array(
                                        'class' => 'form-control',
                                        'placeholder' => 'Link',
                                        'required' => '',
                                    ),
                                    'container_class' => 'col-sm-10'
                                )
                            ),
                            array(
                                'label' => array(
                                    'label' => 'Image',
                                    'id' => 'Image',
                                    'attributes' => array(
                                        'class' => 'col-sm-2 control-label'
                                    )
                                ),
                                'select_image_picker' => array(
                                    'name' => 'image',
                                    'value' => $this->image,
                                    'options' => $this->get_all_images(),
                                    'attributes' => array(
                                        'class' => 'form-control image-picker',
                                    ),
                                    'container_class' => 'col-sm-10',
                                    'image_url' => \Uri::base().$this->image_path,
                                )
                            ),
                            array(
                                'label' => array(
                                    'label' => 'Image Medium Layout',
                                    'id' => 'image_m',
                                    'attributes' => array(
                                        'class' => 'col-sm-2 control-label'
                                    )
                                ),
                                'select_image_picker' => array(
                                    'name' => 'image_m',
                                    'value' => $this->image_m,
                                    'options' => $this->get_all_images(),
                                    'attributes' => array(
                                        'class' => 'form-control image-picker',
                                    ),
                                    'container_class' => 'col-sm-10',
                                    'image_url' => \Uri::base().$this->image_path,
                                )
                            ),
                            array(
                                'label' => array(
                                    'label' => 'Image Small Layout',
                                    'id' => 'image_s',
                                    'attributes' => array(
                                        'class' => 'col-sm-2 control-label'
                                    )
                                ),
                                'select_image_picker' => array(
                                    'name' => 'image_s',
                                    'value' => $this->image_s,
                                    'options' => $this->get_all_images(),
                                    'attributes' => array(
                                        'class' => 'form-control image-picker',
                                    ),
                                    'container_class' => 'col-sm-10',
                                    'image_url' => \Uri::base().$this->image_path,
                                )
                            ),
                            array(
                                'label' => array(
                                    'label' => 'Status',
                                    'id' => 'status',
                                    'attributes' => array(
                                        'class' => 'col-sm-2 control-label'
                                    )
                                ),
                                'select' => array(
                                    'name' => 'status',
                                    'value' => $this->status,
                                    'options' => $this->status_name,
                                    'attributes' => array(
                                        'class' => 'form-control',
                                        'placeholder' => 'Status',
                                        'required' => ''
                                ),
                                'container_class' => 'col-sm-10'
                            )
                        ),    
                    )
		);
	}
}
