<?php
namespace Career;

class Model_CareerApplicants extends \Orm\Model {
	private $status_name = array('Belum Menikah', 'Menikah', 'Janda/Duda');
	
	protected static $_table_name = 'career_applicants';

	protected static $_observers = array(
		'Orm\Observer_Validation' => array(
			'careers'=>array('before_save')
		)
	);
        
	protected static $_properties = array(
		'id',
                'position' => array(
                    'label' => 'Posisi',
                    'validation' => array(
                        'required',
                        'max_length' => array(150),
                    )
		),
                'name' => array(
                    'label' => 'Nama',
                    'validation' => array(
                        'required',
                        'max_length' => array(255),
                    )
		),
                'birth_place' => array(
                    'label' => 'Tempat Lahir',
                    'validation' => array(
                        'required',
                        'max_length' => array(150),
                    )
                ),
                'dob' => array(
                    'label' => 'Tanggal Lahir',
                    'validation' => array(
                        'required',
                    )
                ),
                'gender' => array(
                    'label' => 'Jenis Kelamin',
                    'validation' => array(
                        'required',
                    )
		),
                'marriage_status' => array(
                    'label' => 'Status',
                    'validation' => array(
                        'required',
                    )
		),
                'address' => array(
                    'label' => 'Alamat',
                    'validation' => array(
                        'required',
//                        'readonly'
                        )
                ),
                'city' => array(
                    'label' => 'Kota',
                    'validation' => array(
                        'required',
                        )
                ),
                'country' => array(
                    'label' => 'Negara',
                    'validation' => array(
                        'required',
                    )
		),
                'phone' => array(
                    'label' => 'No. Telepon',
                    'validation' => array(
                        'required',
                    )
                ),
                'email' => array(
                    'label' => 'Email',
                    'validation' => array(
                        'required',
                    )
                ),
                'filename' => array(
                    'label' => 'Filename',
                    'validation' => array()
                ),
                'register_at',
	);
	
        public function get_status_name() {
		$flag = $this->status;
		return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
	}
        
        public static function get_as_array ($filter=array()) {
            $items = self::find('all', $filter);
            if (empty($items)) {
                $data = array();
            } else {
                foreach ($items as $item) {
                $data[$item->id] = $item->name;
                }
            }
            return $data;
	}
        
	public function get_form_data_basic() {
		return array(
                            'attributes' => array(
                                'name'       => 'frm_careers',
                                'class'      => 'form-horizontal',
                                'role'       => 'form',
                                'action'     => '',
                                'method'     => 'post',
			),
			'hidden' => array(),
			'fieldset' => array(
                            array(
                                'label' => array(
                                    'label' => 'Nama',
                                    'id' => 'name',
                                    'attributes' => array(
                                        'class' => 'col-sm-2 control-label'
                                    )
                                ),
                                'input' => array(
                                    'name' => 'name',
                                    'value' => $this->name,
                                    'attributes' => array(
                                        'class' => 'form-control',
                                        'placeholder' => 'Nama',
                                        'required' => '',
                                        'disabled' => true,
                                    ),
                                    'container_class' => 'col-sm-10'
                                )
                            ),
                            array(
                                'label' => array(
                                    'label' => 'Position',
                                    'id' => 'position',
                                    'attributes' => array(
                                        'class' => 'col-sm-2 control-label'
                                    )
                                ),
                                'input' => array(
                                    'name' => 'position',
                                    'value' => $this->position,
                                    'attributes' => array(
                                        'class' => 'form-control',
                                        'placeholder' => 'Position',
                                        'required' => '',
                                        'disabled' => true,
                                    ),
                                    'container_class' => 'col-sm-10'
                                )
                            ),
                            array(
                                'label' => array(
                                    'label' => 'Tempat Lahir',
                                    'id' => 'birth_place',
                                    'attributes' => array(
                                        'class' => 'col-sm-2 control-label'
                                    )
                                ),
                                'input' => array(
                                    'name' => 'birth_place',
                                    'value' => $this->birth_place,
                                    'attributes' => array(
                                        'class' => 'form-control',
                                        'placeholder' => 'Tempat Lahir',
                                        'required' => '',
                                        'disabled' => true,
                                    ),
                                    'container_class' => 'col-sm-10'
                                )
                            ),
                            array(
                                'label' => array(
                                    'label' => 'Tanggal Lahir',
                                    'id' => 'dob',
                                    'attributes' => array(
                                        'class' => 'col-sm-2 control-label'
                                    )
                                ),
                                'input' => array(
                                    'name' => 'dob',
                                    'value' => $this->dob,
                                    'attributes' => array(
                                        'class' => 'form-control',
                                        'placeholder' => 'Tanggal Lahir',
                                        'disabled' => true,
                                    ),
                                    'container_class' => 'col-sm-10'
                                )
                            ), 
                            array(
                                'label' => array(
                                    'label' => 'Jenis Kelamin',
                                    'id' => 'gender',
                                    'attributes' => array(
                                        'class' => 'col-sm-2 control-label'
                                    )
                                ),
                                'input' => array(
                                    'name' => 'gender',
                                    'value' => $this->gender,
                                    'attributes' => array(
                                        'class' => 'form-control',
                                        'placeholder' => '',
                                        'disabled' => true,
                                    ),
                                    'container_class' => 'col-sm-10'
                                )
                            ), 
                            array(
                                'label' => array(
                                    'label' => 'Alamat',
                                    'id' => 'address',
                                    'attributes' => array(
                                        'class' => 'col-sm-2 control-label'
                                    )
                                ),
                                'textarea' => array(
                                    'name' => 'address',
                                    'value' => $this->address,
                                    'attributes' => array(
                                        'class' => 'form-control ckeditor',
                                        'placeholder' => 'Alamat',
                                        'required' => '',
                                        'disabled' => true,
                                        'readonly' => true
                                    ),
                                    'container_class' => 'col-sm-10'
                                )
                            ),
                            array(
                                'label' => array(
                                    'label' => 'Kota',
                                    'id' => 'city',
                                    'attributes' => array(
                                        'class' => 'col-sm-2 control-label'
                                    )
                                ),
                                'input' => array(
                                    'name' => 'city',
                                    'value' => $this->city,
                                    'attributes' => array(
                                        'class' => 'form-control mask-date',
                                        'placeholder' => 'Kota',
                                        'required' => '',
                                        'disabled' => true,
                                    ),
                                    'container_class' => 'col-sm-10'
                                )
                            ),
                            array(
                                'label' => array(
                                    'label' => 'Status',
                                    'id' => 'marriage_status',
                                    'attributes' => array(
                                        'class' => 'col-sm-2 control-label'
                                    )
                                ),
                                'select' => array(
                                    'name' => 'status',
                                    'value' => $this->marriage_status,
                                    'options' => $this->status_name,
                                    'attributes' => array(
                                        'class' => 'form-control',
                                        'placeholder' => 'Status',
                                        'required' => '',
                                        'disabled' => true,
                                ),
                                'container_class' => 'col-sm-10'
                            )
                        ),
                        array(
                            'label' => array(
                                'label' => 'Negara',
                                'id' => 'country',
                                'attributes' => array(
                                    'class' => 'col-sm-2 control-label'
                                )
                            ),
                            'input' => array(
                                'name' => 'country',
                                'value' => $this->country,
                                'attributes' => array(
                                    'class' => 'form-control',
                                    'placeholder' => '0',
                                    'required' => '',
                                    'disabled' => true,
                                ),
                                'container_class' => 'col-sm-10'
                            )
                        ),
                        array(
                            'label' => array(
                                'label' => 'No. Telepon',
                                'id' => 'phone',
                                'attributes' => array(
                                    'class' => 'col-sm-2 control-label'
                                )
                            ),
                            'input' => array(
                                'name' => 'phone',
                                'value' => $this->phone,
                                'attributes' => array(
                                    'class' => 'form-control',
                                    'placeholder' => '0',
                                    'required' => '',
                                    'disabled' => true,
                                ),
                                'container_class' => 'col-sm-10'
                            )
                        ),
                        array(
                            'label' => array(
                                'label' => 'Email',
                                'id' => 'email',
                                'attributes' => array(
                                    'class' => 'col-sm-2 control-label'
                                )
                            ),
                            'input' => array(
                                'name' => 'email',
                                'value' => $this->email,
                                'attributes' => array(
                                    'class' => 'form-control',
                                    'placeholder' => '0',
                                    'required' => '',
                                    'disabled' => true,
                                ),
                                'container_class' => 'col-sm-10'
                            )
                        ),
                        array(
                            'label' => array(
                                'label' => 'File',
                                'id' => 'filename',
                                'attributes' => array(
                                    'class' => 'col-sm-2 control-label'
                                )
                            ),
                            'input' => array(
                                'name' => 'filename',
                                'value' => $this->filename,
                                'attributes' => array(
                                    'class' => 'form-control',
                                    'placeholder' => '0',
                                    'disabled' => true,
                                ),
                                'container_class' => 'col-sm-10'
                            )
                        ),    
                    )
		);
	}
}
