<?php
namespace Career;

class Model_Careers extends \Orm\Model {
	private $status_name = array('InActive', 'Active');
	
	protected static $_table_name = 'careers';

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'careers'=>array('before_insert'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_UpdatedAt' => array(
			'careers'=>array('before_update'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_Validation' => array(
			'careers'=>array('before_save')
		)
	);
        
	protected static $_properties = array(
		'id',
                'name' => array(
                    'label' => 'Name',
                    'validation' => array(
                        'required',
                        'max_length' => array(100),
                    )
		),
                'level' => array(
                    'label' => 'Level',
                    'validation' => array(
                        'required',
                        'max_length' => array(50),
                    )
                ),
                'location' => array(
                    'label' => 'Location',
                    'validation' => array(
                        'required',
                        'max_length' => array(50),
                    )
                ),
                'req_id' => array(
                    'label' => 'Requirement',
                    'validation' => array()
		),
                'req_en' => array(
                    'label' => 'Requirement(EN)',
                    'validation' => array()
		),
                'date' => array(
                    'label' => 'Date',
                    'validation' => array(
                        'required',
                        'valid_date' => array(
                            'format' => 'Y-m-d'
                        )
                    )
                ),
                'status' => array(
                    'label' => 'Status',
                    'validation' => array(
                        'required',
                    )
		),
                'seq' => array(
                    'label' => 'Sequence',
                    'validation' => array(
                        'required',
                    )
                ),
		'created_by',
		'created_at',
		'updated_by',
		'updated_at'
	);
	
        public function get_status_name() {
		$flag = $this->status;
		return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
	}
        
        public static function get_as_array ($filter=array()) {
            $items = self::find('all', $filter);
            if (empty($items)) {
                $data = array();
            } else {
                foreach ($items as $item) {
                $data[$item->id] = $item->name;
                }
            }
            return $data;
	}
        
	public function get_form_data_basic() {
		return array(
                            'attributes' => array(
                                'name'       => 'frm_careers',
                                'class'      => 'form-horizontal',
                                'role'       => 'form',
                                'action'     => '',
                                'method'     => 'post',
			),
			'hidden' => array(),
			'fieldset' => array(
                            array(
                                'label' => array(
                                    'label' => 'Name',
                                    'id' => 'name',
                                    'attributes' => array(
                                        'class' => 'col-sm-2 control-label'
                                    )
                                ),
                                'input' => array(
                                    'name' => 'name',
                                    'value' => $this->name,
                                    'attributes' => array(
                                        'class' => 'form-control',
                                        'placeholder' => 'Name',
                                        'required' => '',
                                    ),
                                    'container_class' => 'col-sm-10'
                                )
                            ),
                            array(
                                'label' => array(
                                    'label' => 'Level',
                                    'id' => 'level',
                                    'attributes' => array(
                                        'class' => 'col-sm-2 control-label'
                                    )
                                ),
                                'input' => array(
                                    'name' => 'level',
                                    'value' => $this->level,
                                    'attributes' => array(
                                        'class' => 'form-control',
                                        'placeholder' => 'Level',
                                        'required' => '',
                                    ),
                                    'container_class' => 'col-sm-10'
                                )
                            ),
                            array(
                                'label' => array(
                                    'label' => 'Location',
                                    'id' => 'location',
                                    'attributes' => array(
                                        'class' => 'col-sm-2 control-label'
                                    )
                                ),
                                'input' => array(
                                    'name' => 'location',
                                    'value' => $this->location,
                                    'attributes' => array(
                                        'class' => 'form-control',
                                        'placeholder' => 'Location',
                                        'required' => '',
                                    ),
                                    'container_class' => 'col-sm-10'
                                )
                            ),
                            array(
                                'label' => array(
                                    'label' => 'Requirement',
                                    'id' => 'req_id',
                                    'attributes' => array(
                                        'class' => 'col-sm-2 control-label'
                                    )
                                ),
                                'textarea' => array(
                                    'name' => 'req_id',
                                    'value' => $this->req_id,
                                    'attributes' => array(
                                        'class' => 'form-control ckeditor',
                                        'placeholder' => '',
                                    ),
                                    'container_class' => 'col-sm-10'
                                )
                            ), 
                            array(
                                'label' => array(
                                    'label' => 'Requirement(EN)',
                                    'id' => 'req_en',
                                    'attributes' => array(
                                        'class' => 'col-sm-2 control-label'
                                    )
                                ),
                                'textarea' => array(
                                    'name' => 'req_en',
                                    'value' => $this->req_en,
                                    'attributes' => array(
                                        'class' => 'form-control ckeditor',
                                        'placeholder' => '',
                                    ),
                                    'container_class' => 'col-sm-10'
                                )
                            ), 
                            array(
                                'label' => array(
                                    'label' => 'Date',
                                    'id' => 'date',
                                    'attributes' => array(
                                        'class' => 'col-sm-2 control-label'
                                    )
                                ),
                                'input' => array(
                                    'name' => 'date',
                                    'value' => $this->date,
                                    'attributes' => array(
                                        'class' => 'form-control mask-date',
                                        'placeholder' => 'Date',
                                        'required' => '',
                                    ),
                                    'container_class' => 'col-sm-10'
                                )
                            ),
                            array(
                                'label' => array(
                                    'label' => 'Status',
                                    'id' => 'status',
                                    'attributes' => array(
                                        'class' => 'col-sm-2 control-label'
                                    )
                                ),
                                'select' => array(
                                    'name' => 'status',
                                    'value' => $this->status,
                                    'options' => $this->status_name,
                                    'attributes' => array(
                                        'class' => 'form-control',
                                        'placeholder' => 'Status',
                                        'required' => ''
                                ),
                                'container_class' => 'col-sm-10'
                            )
                        ),
                        array(
                            'label' => array(
                                'label' => 'Seq',
                                'id' => 'seq',
                                'attributes' => array(
                                    'class' => 'col-sm-2 control-label'
                                )
                            ),
                            'input' => array(
                                'name' => 'seq',
                                'value' => $this->seq,
                                'attributes' => array(
                                    'class' => 'form-control',
                                    'placeholder' => '0',
                                    'required' => '',
                                ),
                                'container_class' => 'col-sm-10'
                            )
                        ),    
                    )
		);
	}
}
