<?php
namespace Product;

class Controller_Frontend_Home extends \Controller_Frontend
{
    private $_module_url = '';
    private $_menu_key = 'product';
    private $_meta_slug = '/';

    public function before() {
        parent::before();
    }

    public function action_index() {
        
        $this->set_meta_info($this->_meta_slug);
        $slug = $this->param('slug');
        $this->_data_template['campaign_pocari'] = \Product\Productutil::get_pocari_campaign($slug);
        
        return \Response::forge(\View::forge('product::frontend/pocari_sweat.twig', $this->_data_template, FALSE));
    }
    public function action_soyjoy() {
        
        $this->set_meta_info($this->_meta_slug);
        return \Response::forge(\View::forge('product::frontend/soyjoy.twig', $this->_data_template, FALSE));
    }
    public function action_ionessence() {
        
        $this->set_meta_info($this->_meta_slug);
        return \Response::forge(\View::forge('product::frontend/ionessence.twig', $this->_data_template, FALSE));
    }

}
