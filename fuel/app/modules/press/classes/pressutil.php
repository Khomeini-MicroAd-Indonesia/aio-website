<?php
namespace Press;

class Pressutil{


    /**
     * get press data
     */

    public static function get_press_data($status, $category){

        $title = 'title_'.\Config::get('language');
        $highlight = 'highlight_'.\Config::get('language');
        $content = 'content_'.\Config::get('language');

        if($status == 'yes'){

            if($category == 'news'){

                $presses = \Press\Model_Press::query()
                    ->where('status', 1)
                    ->where('category', 0)
                    ->or_where('category', 1)
                    ->order_by('date', 'DESC')
                    ->rows_limit(2)
                    ->get();

            }else{

                $presses = \Press\Model_Press::query()
                    ->where('status', 1)
                    ->where('category', 2)
                    ->order_by('date', 'DESC')
                    ->rows_limit(2)
                    ->get();

            }

        }else{

            if($category == 'news'){

                $presses = \Press\Model_Press::query()
                    ->where('status', 1)
                    ->where_open()
                    ->where('category',0)
                    ->or_where('category',1)
                    ->where_close()
                    ->order_by('date', 'DESC')
                    ->rows_limit(4)
                    ->get();

            }else{

                $presses = \Press\Model_Press::query()
                    ->where('status', 1)
                    ->where('category', 2)
                    ->order_by('date', 'DESC')
                    ->rows_limit(4)
                    ->get();

            }

        }

        $data = array();
        foreach ($presses as $press){
//            $category_name = \Press\Model_Press::;

            $data[] = array(
                'id'        => $press->id,
                'title'     => $press->$title,
                'highlight' => $press->$highlight,
                'date'      => $press->date,
                'content'   => $press->$content,
                'image'     => $press->image,
                'slug'      => $press->slug,
                'category'  => $press->get_category_name()
            );
        }
        return $data;
    }

    /**
     * get press article
     */

    public static function get_press_article($category, $latest){

        $title = 'title_'.\Config::get('language');
        $highlight = 'highlight_'.\Config::get('language');
        $content = 'content_'.\Config::get('language');

        if($category == 'news'){

            $presses = \Press\Model_Press::query()
                //->where('id', '!=', $latest->id)
                ->where('status', 1)
                ->where_open()
                ->where('category',0)
                ->or_where('category',1)
                ->where_close()
                ->order_by('date', 'DESC')
                ->rows_limit(4)
                ->offset(0)
                ->get();

        }else{

            $presses = \Press\Model_Press::query()
                //->where('id', '!=', $latest->id)
                ->where('status', 1)
                ->where('category', 2)
                ->order_by('date', 'DESC')
                ->rows_limit(4)
                ->offset(0)
                ->get();

        }

        $data = array();
        foreach ($presses as $press){

            $data[] = array(
                'id'        => $press->id,
                'title'     => $press->$title,
                'highlight' => $press->$highlight,
                'date'      => $press->date,
                'content'   => $press->$content,
                'image'     => $press->image,
                'slug'      => $press->slug,
                'category'  => $press->get_category_name()
            );
        }
        return $data;
    }

    /**
     * get press detail
     */

    public static function get_press_detail($slug){
        $title = 'title_'.\Config::get('language');
        $highlight = 'highlight_'.\Config::get('language');
        $content = 'content_'.\Config::get('language');
        $presses = \Press\Model_Press::query()
            ->where('status', 1)->where('slug', $slug)
            ->order_by('date', 'desc')
            ->get();
        $data = array();
        foreach ($presses as $press){
            $data[] = array(
                'title'     => $press->$title,
                'highlight' => $press->$highlight,
                'date'      => $press->date,
                'content'   => $press->$content,
                'image'     => $press->image,
                'slug'      => $press->slug
            );
        }
        return $data;
    }

    public static function get_press_list($slug,$category){

        $title = 'title_'.\Config::get('language');
        $highlight = 'highlight_'.\Config::get('language');
        $content = 'content_'.\Config::get('language');

        if($category == 'news'){

            $presses = \Press\Model_Press::query()
                ->where('status', 1)
                ->where_open()
                ->where('category',0)
                ->or_where('category',1)
                ->where_close()
                ->where('slug','!=',$slug)
                ->order_by('date', 'DESC')
                ->rows_limit(4)
                ->get();

        }else{

            $presses = \Press\Model_Press::query()
                ->where('status', 1)
                ->where('category', 2)
                ->where('slug','!=',$slug)
                ->order_by('date', 'DESC')
                ->rows_limit(4)
                ->get();

        }

        $data = array();
        foreach ($presses as $press){
//            $category_name = \Press\Model_Press::;

            $data[] = array(
                'id'        => $press->id,
                'title'     => $press->$title,
                'highlight' => $press->$highlight,
                'date'      => $press->date,
                'content'   => $press->$content,
                'image'     => $press->image,
                'slug'      => $press->slug,
                'category'  => $press->get_category_name()
            );
        }
        return $data;

    }

}
