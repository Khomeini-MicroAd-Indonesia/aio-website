<?php

namespace Press;

class Model_Press extends \Orm\Model {

    private $status_name = array('InActive', 'Active');
    private $category_name = array('Corporate','Brand','Press');
    const NEWS_CORPORATE = 0;
    const NEWS_BRAND = 1;
    const NEWS_PRESS = 2;
    private $image_path = 'media/press-room/';
    protected static $_table_name = 'press';
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'press' => array('before_insert'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_UpdatedAt' => array(
            'press' => array('before_update'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_Validation' => array(
            'press' => array('before_save')
        )
    );
    protected static $_properties = array(
        'id',
        'title_id' => array(
            'label' => 'Title',
            'validation' => array(
                'required',
                'max_length' => array(150),
            )
        ),
        'title_en' => array(
            'label' => 'Title(EN)',
            'validation' => array(
                'required',
                'max_length' => array(150),
            )
        ),
        'slug' => array(
            'label' => 'Slug',
            'validation' => array(
                'required',
                'max_length' => array(150),
            )
        ),
        'category' => array(
            'label' => 'Category',
            'validation' => array(
                'required',
            )
        ),
        'highlight_id' => array(
            'label' => 'Highlight',
            'validation' => array()
        ),
        'highlight_en' => array(
            'label' => 'Highlight(EN)',
            'validation' => array()
        ),
        'date' => array(
            'label' => 'Date',
            'validation' => array(
                'required',
                'valid_date' => array(
                    'format' => 'Y-m-d'
                )
            )
        ),
        'content_id' => array(
            'label' => 'Content',
            'validation' => array()
        ),
        'content_en' => array(
            'label' => 'Content(EN)',
            'validation' => array()
        ),
        'image' => array(
            'label' => 'Image',
            'validation' => array(
                'required',
                'max_length' => array(500),
            )
        ),
        'status' => array(
            'label' => 'Status',
            'validation' => array(
                'required',
            )
        ),
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    );
    
    public function get_image_path() {
        return $this->image_path;
    }

    public function get_all_images() {
        if (file_exists(DOCROOT.$this->image_path)) {
            $contents = \File::read_dir(DOCROOT.$this->image_path);
        } else {
            $contents = array();
        }
        return $contents;
    }
    
    public function get_category_name() {
        $flag = $this->category;
        return isset($this->category_name[$flag]) ? $this->category_name[$flag] : '-';
    }
    
    public function get_status_name() {
        $flag = $this->status;
        return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
    }

    public static function get_as_array($filter = array()) {
        $items = self::find('all', $filter);
        if (empty($items)) {
            $data = array();
        } else {
            foreach ($items as $item) {
                $data[$item->id] = $item->title_id;
            }
        }
        return $data;
    }

    public function get_form_data_basic() {
        return array(
            'attributes' => array(
                'name' => 'frm_press',
                'class' => 'form-horizontal',
                'role' => 'form',
                'action' => '',
                'method' => 'post',
            ),
            'hidden' => array(),
            'fieldset' => array(
                array(
                    'label' => array(
                        'label' => 'Category',
                        'id' => 'category',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'category',
                        'value' => $this->category,
                        'options' => $this->category_name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Category',
                            'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Title',
                        'id' => 'title_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'title_id',
                        'value' => $this->title_id,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Title',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Title(EN)',
                        'id' => 'title_en',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'title_en',
                        'value' => $this->title_en,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Title(EN)',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Slug',
                        'id' => 'slug',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'slug',
                        'value' => $this->slug,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Slug',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Highlight',
                        'id' => 'highlight_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'highlight_id',
                        'value' => $this->highlight_id,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Highlight',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Highlight(EN)',
                        'id' => 'highlight_en',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'highlight_en',
                        'value' => $this->highlight_en,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Highlight(EN)',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Date',
                        'id' => 'date',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'date',
                        'value' => $this->date,
                        'attributes' => array(
                            'class' => 'form-control mask-date',
                            'placeholder' => 'Date',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Content',
                        'id' => 'content_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'content_id',
                        'value' => $this->content_id,
                        'attributes' => array(
                            'class' => 'form-control ckeditor',
                            'placeholder' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Content(EN)',
                        'id' => 'content_en',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'content_en',
                        'value' => $this->content_en,
                        'attributes' => array(
                            'class' => 'form-control ckeditor',
                            'placeholder' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Image',
                        'id' => 'image_filename',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select_image_picker' => array(
                        'name' => 'image_filename',
                        'value' => $this->image,
                        'options' => $this->get_all_images(),
                        'attributes' => array(
                            'class' => 'form-control image-picker',
                        ),
                        'container_class' => 'col-sm-10',
                        'image_url' => \Uri::base().$this->image_path,
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Status',
                        'id' => 'status',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'status',
                        'value' => $this->status,
                        'options' => $this->status_name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Status',
                            'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
            )
        );
    }

}
