<?php
namespace Press;

use Fuel\Core\Input;

class Controller_Frontend_Home extends \Controller_Frontend
{
    private $_module_url = '';
    private $_menu_key = 'press';
    private $_meta_slug = '/';
    private $_per_page = 4;

    public function before() {
        parent::before();
    }

    public function action_index($category=\Press\Model_Press::NEWS_CORPORATE) {
        $this->set_meta_info($this->_meta_slug . $this->_current_lang . '/press-room');
        if ($category == \Press\Model_Press::NEWS_CORPORATE) {
            $this->_data_template['active_corporate'] = "active";
        }
        $model_page = \Pages\Model_Pages::query()
                ->where('url_path', '/'.$this->_current_lang.'/press-room-banner')
                ->where('status', 1)
                ->get_one();
        if (empty($model_page)){
            $model_page = \Pages\Model_Pages::query()
                ->where('url_path', '/press-room-banner')
                ->where('status', 1)
                ->get_one();
        }
        if (!empty($model_page)) {
            $this->_data_template['press_room_banner'] = $model_page->content;
        }

        $this->_data_template['press_list'] = \Press\Pressutil::get_press_detail($category);

        return \Response::forge(\View::forge('press::frontend/press.twig', $this->_data_template, FALSE));
    }

    public function action_brand_news() {
        $this->_data_template['active_brand'] = "active";
        return $this->action_index(\Press\Model_Press::NEWS_BRAND);
    }

    public function action_press_release() {
        $this->_data_template['active_press'] = "active";
        return $this->action_index(\Press\Model_Press::NEWS_PRESS);
    }

    public function action_detail() {
        $slug = $this->param('slug');
        $model_data = \Press\Model_Press::query()
            ->where('status', 1)
            ->where('slug', $slug)
            ->get_one();
        if (empty($model_data)) {
            \Response::redirect(\Uri::base().$this->_current_lang.'/our-event');
        }
        $tmp = "title_$this->_current_lang";
        $tmp_content = "content_$this->_current_lang";
        $tmp_highlight = "highlight_$this->_current_lang";
        $model_data->title = $model_data->$tmp;
        $model_data->content = $model_data->$tmp_content;
        $model_data->highlight = $model_data->$tmp_highlight;
        $this->_data_template['meta_title'] = \Config::get('config_basic.app_name').' - '.$model_data->title;
        $this->_data_template['meta_desc'] = $model_data->highlight;
        $this->_data_template['detail_data'] = $model_data;
        return \Response::forge(\View::forge('press::frontend/press_detail.twig', $this->_data_template, FALSE));
    }



    public function action_newss() {
        $flag = 0;
        $status = 'no';
        $category = 'news';
        $list = \Press\Pressutil::get_press_data($status, $category);
        $model = \Press\Model_Press::query()
            ->where_open()
                ->where('category',0)
                ->or_where('category',1)
            ->where_close()
            ->where('status',1);
        
        $list_total = ceil($model->count());
        //var_dump($list); exit;
        $last_id = $list[$list_total-1]['id'];
        
        $this->_data_template['flag'] = $flag;
        $this->_data_template['articles'] = $list;
        $this->_data_template['last_id'] = $last_id;
        $this->_data_template['list_total'] = $list_total;

        return \Response::forge(\View::forge('press::frontend/list.twig', $this->_data_template, FALSE));
    }
    
    public function action_news() {
        $flag = 0;
        $status = 'no';
        $category = 'news';
        
        //Latest News
        $latest = \Press\Model_Press::query()
            ->where('status',1)
            ->where_open()
                ->where('category',0)
                ->or_where('category',1)
            ->where_close()
            ->order_by('date','desc')
            ->get_one();
        
        if (!empty($latest)) {
            $this->_data_template['latest'] = $latest;

            $model = \Press\Model_Press::query()
                ->where_open()
                    ->where('category',0)
                    ->or_where('category',1)
                ->where_close()
                ->where('status',1);

//            $article_list = \Press\Model_Press::query()
//                ->where('status',1)
//                ->where_open()
//                    ->where('category',0)
//                    ->or_where('category',1)
//                ->where_close()
//                ->where('id', '!=', $latest->id)
//                ->order_by('date','desc');
            
            $article_list = \Press\Pressutil::get_press_article($category, $latest);
            $this->_data_template['articles'] = $article_list;
//                ->limit($this->_per_page)
//                ->offset(0)
//                ->get();
        }
        
        $this->_data_template['flag'] = $flag;
        $this->_data_template['per_page'] = $this->_per_page;
        $this->_data_template['list_page'] = ceil($model->count());
        $this->_data_template['active_news'] = "active";
        //var_dump(ceil($model->count())); exit;
        return \Response::forge(\View::forge('press::frontend/list.twig', $this->_data_template, FALSE));
    }
    
    public function action_press() {

        $flag = 1;
        $status = 'no';
        $category = 'press';
        
        //Latest News
        $latest = \Press\Model_Press::query()
            ->where('status',1)
            ->where('category',2)
            ->order_by('date','desc')
            ->get_one();
        
        if (!empty($latest)) {
            $this->_data_template['latest'] = $latest;

            $model = \Press\Model_Press::query()
                ->where('category',2)
                ->where('status',1);

//            $article_list = \Press\Model_Press::query()
//                ->where('status',1)
//                ->where_open()
//                    ->where('category',0)
//                    ->or_where('category',1)
//                ->where_close()
//                ->where('id', '!=', $latest->id)
//                ->order_by('date','desc');
            
            $article_list = \Press\Pressutil::get_press_article($category, $latest);
            $this->_data_template['articles'] = $article_list;
//                ->limit($this->_per_page)
//                ->offset(0)
//                ->get();
        }
        
        $this->_data_template['flag'] = $flag;
        $this->_data_template['per_page'] = $this->_per_page;
        $this->_data_template['list_page'] = ceil($model->count());
        $this->_data_template['active_news'] = "active";
        
        return \Response::forge(\View::forge('press::frontend/list.twig', $this->_data_template, FALSE));
    }
    
    public function action_load_list() {
        
        $curr_page = is_numeric(\Input::post('curr_page')) ? \Input::post('curr_page') : -1;
        $latest_id = is_numeric(\Input::post('latest_id')) ? \Input::post('latest_id') : -1;
        
        $flag = Input::post('flag');

        $title = 'title_'.\Config::get('language');
        $highlight = 'highlight_'.\Config::get('language');
        $content = 'content_'.\Config::get('language');

        if($flag == 1){
            
            $news = \Press\Model_Press::query()
                ->where('category',2)
                ->where('status', 1)
                ->where('id', '!=', $latest_id)
                ->order_by('date','desc')
                ->limit($this->_per_page)
                ->offset($curr_page*$this->_per_page)
                ->get();
            
        }else{

            $news = \Press\Model_Press::query()
                ->where_open()
                    ->where('category',0)
                    ->or_where('category',1)
                ->where_close()
                ->where('status', 1)
                ->where('id', '!=', $latest_id)
                ->order_by('date','desc')
                ->limit($this->_per_page)
                ->offset($curr_page*$this->_per_page)
                ->get();

        }
        var_dump($news);exit;
        $data = array();

        foreach($news as $press){
            $data[] = [
                'id'        => $press->id,
                'title'     => $press->$title,
                'highlight' => $press->$highlight,
                'date'      => $press->date,
                'content'   => $press->$content,
                'image'     => $press->image,
                'slug'      => $press->slug,
                'category'  => $press->get_category_name()
            ];
        }
        
        return json_encode($data);
    }

    public function action_detail_new() {
        $this->set_meta_info($this->_meta_slug);
        $slug = $this->param('slug');

        $list_detail = Model_Press::query()
            ->where('status',1)
            ->where('slug',$slug)
            ->get_one();

    //var_dump($list_detail); exit;
        if (empty($list_detail)) {
            throw new \HttpNotFoundException;
        }

        $this->_data_template['list_detail'] = $list_detail;

        $status = 'no';


        if($list_detail->category == 0 || $list_detail->category == 1 ){
            $category = "news";
        }
        else
        {
            $category = "press";
        }

        $this->_data_template['details'] = \Press\Pressutil::get_press_detail($slug);
        $this->_data_template['articles'] = \Press\Pressutil::get_press_list($slug, $category);
        $this->_data_template['active_news'] = "active";
        return \Response::forge(\View::forge('press::frontend/detail.twig', $this->_data_template, FALSE));
    }
}
