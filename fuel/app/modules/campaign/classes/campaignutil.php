<?php
namespace Campaign;

class Campaignutil{

    /**
     * get pocari campaign
     */
    public static function get_pocari_campaign($slug) {

        $details = \Campaign\Model_Campaigns::query()
                ->related('images')
                ->where('slug', $slug)
                ->where('status', 1)
                ->limit(1)
                ->get();

        $data = array();

        foreach ($details as $detail) {

            $data[] = array(
                'image' => $detail->images->filename,
                'link' => $detail->link
            );
        }
        return $data;
    }

    /**
     * get soyjoy campaign
     */
    public static function get_soyjoy_campaign($slug) {

        $details = \Campaign\Model_Campaigns::query()
                ->related('images')
                ->where('slug', $slug)
                ->where('status', 1)
                ->limit(1)
                ->get();

        $data = array();

        foreach ($details as $detail) {

            $data[] = array(
                'image' => $detail->images->filename,
                'link' => $detail->link,
                'title' => $detail->name
            );
        }
        return $data;
    }

    /**
     * get ionessence campaign
     */
    public static function get_ionessence_campaign($slug) {

        $details = \Campaign\Model_Campaigns::query()
                ->related('images')
                ->related('images_m')
                ->related('images_s')
                ->where('slug', $slug)
                ->where('status', 1)
                ->limit(1)
                ->get();

        $data = array();

        foreach ($details as $detail) {

            $data[] = array(
                'image'     => $detail->images->filename,
                'image_m'   => $detail->images_m->filename,
                'image_s'   => $detail->images_s->filename
            );
        }
        return $data;
    }


}
