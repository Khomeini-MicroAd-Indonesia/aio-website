<?php
namespace Campaign;

class Controller_Frontend_Home extends \Controller_Frontend
{
    private $_module_url = '';
    private $_menu_key = 'campaign';
    private $_meta_slug = '/';

    public function before() {
        parent::before();
    }

    public function action_index() {
        $slug = 'pocari-sweat';
        $this->set_meta_info($this->_meta_slug);
        $this->_data_template['campaign_pocari'] = \Campaign\Campaignutil::get_pocari_campaign($slug);
        
        return \Response::forge(\View::forge('campaign::frontend/pocari_sweat.twig', $this->_data_template, FALSE));
    }
    
    public function action_soyjoy() {
        $slug = 'soyjoy';
        $this->set_meta_info($this->_meta_slug);
        $this->_data_template['campaign_soyjoy'] = \Campaign\Campaignutil::get_soyjoy_campaign($slug);
        
        return \Response::forge(\View::forge('campaign::frontend/soyjoy.twig', $this->_data_template, FALSE));
    }
    public function action_ionessence() {
        $slug = 'ionessence';
        $this->set_meta_info($this->_meta_slug);
        $this->_data_template['campaign_ionessence'] = \Campaign\Campaignutil::get_ionessence_campaign($slug);
        
        return \Response::forge(\View::forge('campaign::frontend/ionessence.twig', $this->_data_template, FALSE));
    }

    public function action_pocari_new() {
        $product_id = 1;
        $this->set_meta_info($this->_meta_slug);
        
        $this->_data_template['pocari_products'] = \Product\Productutil::get_product_nutrition($product_id);
        $this->_data_template['active_brand'] = "active";
        
        return \Response::forge(\View::forge('campaign::frontend/pocari_sweat_new.twig', $this->_data_template, FALSE));
    }

    public function action_soyjoy_new() {
        $product_id = 2;
        $this->set_meta_info($this->_meta_slug);
        
        $this->_data_template['soyjoy_products'] = \Product\Productutil::get_product_nutrition($product_id);
        $this->_data_template['active_brand'] = "active";

        return \Response::forge(\View::forge('campaign::frontend/soyjoy_new.twig', $this->_data_template, FALSE));
    }
    public function action_ionessence_new() {
        $product_id = 3;
        $this->set_meta_info($this->_meta_slug);
        
        $this->_data_template['ionessence_products'] = \Product\Productutil::get_product_nutrition($product_id);
        $this->_data_template['active_brand'] = "active";

        return \Response::forge(\View::forge('campaign::frontend/ionessence_new.twig', $this->_data_template, FALSE));
    }

}