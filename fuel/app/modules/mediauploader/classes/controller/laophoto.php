<?php

namespace MediaUploader;

class Controller_LAOPhoto extends \Controller_Backend {

    private $_module_url = 'backend/media-uploader/lao-photo';
    private $_menu_key = 'media_uploader_laophoto';

    public function before() {
        parent::before();
        $this->authenticate();
        // Check menu permission
        if (!$this->check_menu_permission($this->_menu_key, 'read')) {
            // if not have an access then redirect to error page
            \Response::redirect(\Uri::base() . 'backend/no-permission');
        }
        $this->_data_template['meta_title'] = 'Media Uploader - LAO Photo';
        $this->_data_template['menu_parent_key'] = 'media_uploader';
        $this->_data_template['menu_current_key'] = $this->_menu_key;
    }

    public function action_index() {
        $this->_data_template['success_message'] = \Session::get_flash('success_message');
        $this->_data_template['error_message'] = \Session::get_flash('error_message');
        $this->_data_template['media_uploader_subtitle'] = 'LAO Photo';
        $this->_data_template['form_action_url'] = \Uri::base() . $this->_module_url . '/upload';
        $this->_data_template['information_text'] = '
			<ul>
				<li>Max filesize is 1 MB</li>
                                <li>Portrait Photo</li>
                                <li>Optimal Width 280px</li>
			</ul>
		';
        return \Response::forge(\View::forge('backend/jquery_image_uploader.twig', $this->_data_template, FALSE));
    }

    public function action_upload() {
        \Package::load('jqueryfileupload');
        $options = array(
            'script_url' => \Uri::base() . $this->_module_url . '/upload',
            'upload_dir' => DOCROOT . 'media/lao-photo/',
            'upload_url' => \Uri::base() . 'media/lao-photo/',
            'max_file_size' => 1024 * 1024, // 1 MB
            'image_versions' => array(
                '' => array(
                    'max_width' => 280,
                ),
                'thumbnail' => array(
                    'crop' => false,
                    'max_width' => 150,
                )
            ),
        );
        $upload_handler = new \JqueryFileUploadHandler($options);
    }

}
