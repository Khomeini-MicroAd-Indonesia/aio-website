<?php
namespace Lifeatotsuka;

class LAOutil{
	
    /**
     * get lao data
     */
    
    public static function get_lao_data(){
        
        $laos = \Lifeatotsuka\Model_Lifeatotsukas::query()
                ->where('status', 1)
                ->order_by('created_at', 'desc')
                ->limit(4)
                ->get();
        
       // $string = $loas[1]['story'];
        //$str = explode('.', $string);
//        var_dump($str); exit;
//        $count = count($laos);
        $count = 0;
        //var_dump($count); exit;
        $data = array();
        
        foreach ($laos as $lao){
            
            $story_lead = 'story_lead_'.\Config::get('language');
            $story = 'story_'.\Config::get('language');
            $count++;
            
            $data[] = array(
                'id'            => $lao->id,
                'name'          => $lao->name,
                'job'           => $lao->job,
                'location'      => $lao->location,
                'photo'         => $lao->photo,
                'story_lead'    => $lao->$story_lead,
                'story'         => $lao->$story,
                'count'         => $count
            );
        }
        return $data;
    }

    /**
     * get lao leadership vision 
     */
    
    public static function get_lao_leardership_vision(){
        
        $laos = \Lifeatotsuka\Model_Lifeatotsukavisions::query()
                ->where('status', 1)
                ->limit(1)
                ->get();
        
        
        $data = array();
        
        foreach ($laos as $lao){
            
            $content = 'content_'.\Config::get('language');
            
            $data[] = array(
                'id'            => $lao->id,
                'name'          => $lao->name,
                'image'         => $lao->image,
                'position'      => $lao->position,
                'content'       => $lao->$content,
            );
        }
        return $data;
    }
    
    /**
     * get lao leadership filosifi 
     */
    
    public static function get_lao_philosophy(){
        
        $laos = \Lifeatotsuka\Model_Lifeatotsukaphilosophies::query()
                ->where('status', 1)
                ->get();
        
        
        $data = array();
        
        foreach ($laos as $lao){
            
            $content = 'content_'.\Config::get('language');
            
            $data[] = array(
                'id'            => $lao->id,
                'title'         => $lao->title,
                'image'         => $lao->image,
                'content'       => $lao->$content,
            );
        }
        return $data;
    }
    
}