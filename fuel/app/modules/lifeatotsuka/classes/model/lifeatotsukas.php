<?php

namespace Lifeatotsuka;

class Model_Lifeatotsukas extends \Orm\Model {

    private $status_name = array('InActive', 'Active');
    private $photo_path = 'media/lao-photo/';
    protected static $_table_name = 'lifeatotsukas';
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'lifeatotsukas' => array('before_insert'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_UpdatedAt' => array(
            'lifeatotsukas' => array('before_update'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_Validation' => array(
            'lifeatotsukas' => array('before_save')
        )
    );
    protected static $_properties = array(
        'id',
        'name' => array(
            'label' => 'Name',
            'validation' => array(
                'required',
                'max_length' => array(50),
            )
        ),
        'age' => array(
            'label' => 'Age',
            'validation' => array(
                'required',
                'numeric_min' => array(1),
                'numeric_max' => array(100),
            )
        ),
        'job' => array(
            'label' => 'Job',
            'validation' => array(
                'max_length' => array(50)
            )
        ),
        'location' => array(
            'label' => 'Location',
            'validation' => array(
                'required',
                'max_length' => array(50),
            )
        ),
        'story_lead_id' => array(
            'label' => 'Story Lead',
            'validation' => array(
                'required',
                'max_length' => array(500),
            )
        ),
        'story_lead_en' => array(
            'label' => 'Story Lead(EN)',
            'validation' => array(
                'required',
                'max_length' => array(500),
            )
        ),
        'story_id' => array(
            'label' => 'Story',
            'validation' => array(
                'required',
            )
        ),
        'story_en' => array(
            'label' => 'Story(EN)',
            'validation' => array(
                'required',
            )
        ),
        'photo' => array(
            'label' => 'Photo',
            'validation' => array(
                'required',
                'max_length' => array(500),
            )
        ),
        'status' => array(
            'label' => 'Status',
            'validation' => array(
                'required',
            )
        ),
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    );
    
    public function get_photo_path() {
        return $this->photo_path;
    }

    public function get_all_photos() {
        if (file_exists(DOCROOT.$this->photo_path)) {
            $contents = \File::read_dir(DOCROOT.$this->photo_path);
        } else {
            $contents = array();
        }
        return $contents;
    }

    public function get_status_name() {
        $flag = $this->status;
        return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
    }

    public static function get_as_array($filter = array()) {
        $items = self::find('all', $filter);
        if (empty($items)) {
            $data = array();
        } else {
            foreach ($items as $item) {
                $data[$item->id] = $item->name;
            }
        }
        return $data;
    }

    public function get_form_data_basic() {
        return array(
            'attributes' => array(
                'name' => 'frm_lifeatotsukas',
                'class' => 'form-horizontal',
                'role' => 'form',
                'action' => '',
                'method' => 'post',
            ),
            'hidden' => array(),
            'fieldset' => array(
                array(
                    'label' => array(
                        'label' => 'Name',
                        'id' => 'name',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'name',
                        'value' => $this->name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Name',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Age',
                        'id' => 'age',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'age',
                        'value' => $this->age,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Age',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Job',
                        'id' => 'level',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'job',
                        'value' => $this->job,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Job',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Location',
                        'id' => 'location',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'location',
                        'value' => $this->location,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Location',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Story Lead',
                        'id' => 'story_lead_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'story_lead_id',
                        'value' => $this->story_lead_id,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Story Lead(EN)',
                        'id' => 'story_lead_en',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'story_lead_en',
                        'value' => $this->story_lead_en,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Story',
                        'id' => 'story_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'story_id',
                        'value' => $this->story_id,
                        'attributes' => array(
                            'class' => 'form-control ckeditor',
                            'placeholder' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Story(EN)',
                        'id' => 'story_en',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'story_en',
                        'value' => $this->story_en,
                        'attributes' => array(
                            'class' => 'form-control ckeditor',
                            'placeholder' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Photo',
                        'id' => 'photo_filename',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select_image_picker' => array(
                        'name' => 'photo_filename',
                        'value' => $this->photo,
                        'options' => $this->get_all_photos(),
                        'attributes' => array(
                            'class' => 'form-control image-picker',
                        ),
                        'container_class' => 'col-sm-10',
                        'image_url' => \Uri::base().$this->photo_path,
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Status',
                        'id' => 'status',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'status',
                        'value' => $this->status,
                        'options' => $this->status_name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Status',
                            'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
            )
        );
    }

    public static function checkStoryWords()
    {
        $post = \Input::post();

        $countStoryId = count(explode(' ', $post['story_id']));
        $countStoryEn = count(explode(' ', $post['story_en']));

        $error = 0;
        $message = null;
        if ( $countStoryId > 400 || $countStoryEn > 400 )
        {
            $message = 'Story: Maksimal kata yang dapat dimasukan adalah 400 kata';
            $error   = 1;
        }

        return ['error' => $error, 'message' => $message];
    }
}
