<?php

namespace Lifeatotsuka;

class Model_Lifeatotsukavisions extends \Orm\Model {

    private $status_name = array('InActive', 'Active');
    private $photo_path = 'media/lao-photo/';
    protected static $_table_name = 'lifeatotsuka_visions';
    
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'lifeatotsuka_visions' => array('before_insert'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_UpdatedAt' => array(
            'lifeatotsuka_visions' => array('before_update'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_Validation' => array(
            'lifeatotsuka_visions' => array('before_save')
        )
    );
    protected static $_properties = array(
        'id',
        'name' => array(
            'label' => 'Name',
            'validation' => array(
                'required',
                'max_length' => array(50),
            )
        ),
        'image' => array(
            'label' => 'Image',
            'validation' => array(
                'required',
                'max_length' => array(50),
            )
        ),
        'position' => array(
            'label' => 'Position',
            'validation' => array(
                'max_length' => array(50)
            )
        ),
        'content_id' => array(
            'label' => 'Content',
            'validation' => array()
        ),
        'content_en' => array(
            'label' => 'Content(EN)',
            'validation' => array()
        ),
        'status' => array(
            'label' => 'Status',
            'validation' => array(
                'required',
            )
        ),
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    );
    
    public function get_photo_path() {
        return $this->photo_path;
    }

    public function get_all_photos() {
        if (file_exists(DOCROOT.$this->photo_path)) {
            $contents = \File::read_dir(DOCROOT.$this->photo_path);
        } else {
            $contents = array();
        }
        return $contents;
    }

    public function get_status_name() {
        $flag = $this->status;
        return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
    }

    public static function get_as_array($filter = array()) {
        $items = self::find('all', $filter);
        if (empty($items)) {
            $data = array();
        } else {
            foreach ($items as $item) {
                $data[$item->id] = $item->name;
            }
        }
        return $data;
    }

    public function get_form_data_basic() {
        return array(
            'attributes' => array(
                'name' => 'frm_lifeatotsukas',
                'class' => 'form-horizontal',
                'role' => 'form',
                'action' => '',
                'method' => 'post',
            ),
            'hidden' => array(),
            'fieldset' => array(
                array(
                        'label' => array(
                            'label' => 'Name',
                            'id' => 'name',
                            'attributes' => array(
                                'class' => 'col-sm-2 control-label'
                            )
                        ),
                        'input' => array(
                                'name' => 'name',
                                'value' => $this->name,
                                'attributes' => array(
                                    'class' => 'form-control',
                                    'placeholder' => 'Name',
                                    'required' => '',
                                ),
                            'container_class' => 'col-sm-10'
                            )
                    ),
                    array(
                        'label' => array(
                            'label' => 'Image',
                            'id' => 'image',
                            'attributes' => array(
                                'class' => 'col-sm-2 control-label'
                            )
                        ),
                        'select_image_picker' => array(
                            'name' => 'image',
                            'value' => $this->image,
                            'options' => $this->get_all_photos(),
                            'attributes' => array(
                                'class' => 'form-control image-picker',
                            ),
                            'container_class' => 'col-sm-10',
                            'image_url' => \Uri::base().$this->photo_path,
                        )
                    ),
                    array(
                        'label' => array(
                            'label' => 'Position',
                            'id' => 'position',
                            'attributes' => array(
                                'class' => 'col-sm-2 control-label'
                            )
                        ),
                        'input' => array(
                            'name' => 'position',
                            'value' => $this->position,
                            'attributes' => array(
                                'class' => 'form-control',
                                'placeholder' => 'Position',
                                'required' => '',
                            ),
                        'container_class' => 'col-sm-10'
                        )
                    ),
                    array(
                        'label' => array(
                            'label' => 'Content',
                            'id' => 'content_id',
                            'attributes' => array(
                                'class' => 'col-sm-2 control-label'
                            )
                        ),
                        'textarea' => array(
                            'name' => 'content_id',
                            'value' => $this->content_id,
                            'attributes' => array(
                                'class' => 'form-control ckeditor',
                                'placeholder' => '',
                            ),
                            'container_class' => 'col-sm-10'
                        )
                    ),
                    array(
                        'label' => array(
                            'label' => 'Content(EN)',
                            'id' => 'content_en',
                            'attributes' => array(
                                'class' => 'col-sm-2 control-label'
                            )
                        ),
                        'textarea' => array(
                            'name' => 'content_en',
                            'value' => $this->content_en,
                            'attributes' => array(
                                'class' => 'form-control ckeditor',
                                'placeholder' => '',
                            ),
                            'container_class' => 'col-sm-10'
                        )
                    ), 
                    array(
                        'label' => array(
                            'label' => 'Status',
                            'id' => 'status',
                            'attributes' => array(
                                'class' => 'col-sm-2 control-label'
                            )
                        ),
                        'select' => array(
                            'name' => 'status',
                            'value' => $this->status,
                            'options' => $this->status_name,
                            'attributes' => array(
                                'class' => 'form-control',
                                'placeholder' => 'Status',
                                'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                        )
                    )
            )
        );
    }

}
