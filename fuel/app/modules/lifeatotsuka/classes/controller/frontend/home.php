<?php

namespace Lifeatotsuka;

use Factorytour\Model_Testimonials;

class Controller_Frontend_Home extends \Controller_Frontend {

    private $_module_url = '';
    private $_menu_key = 'lifeatotsuka';
    private $_meta_slug = '/';
    private $_per_page = 1;

    public function before() {
        parent::before();
    }

    public function action_index() {
        $this->set_meta_info($this->_meta_slug . $this->_current_lang . '/lifeatotsuka');
        $model_page = \Pages\Model_Pages::query()
                ->where('url_path', '/'.$this->_current_lang.'/lifeatotsuka-banner')
                ->where('status', 1)
                ->get_one();
        if (empty($model_page)){
            $model_page = \Pages\Model_Pages::query()
                ->where('url_path', '/lifeatotsuka-banner')
                ->where('status', 1)
                ->get_one();
        }
        if (!empty($model_page)) {
            $this->_data_template['lao_banner'] = $model_page->content;
        }
        $model = \Lifeatotsuka\Model_Lifeatotsukas::query()
            ->where('status', 1);
        $this->_data_template['stories'] = $model->order_by('created_at', 'DESC')
            ->limit($this->_per_page)
            ->offset(0)
            ->get();
        $this->_data_template['per_page'] = $this->_per_page;
        $this->_data_template['story_page'] = ceil($model->count());
        return \Response::forge(\View::forge('lifeatotsuka::frontend/lifeatotsuka.twig', $this->_data_template, FALSE));
    }
    public function action_get_testimony() {
        $offset = is_numeric(\Input::post('next_offset')) ? \Input::post('next_offset') : -1;
        $testimonies = Model_Lifeatotsukas::query()
            ->where('status', 1)
            ->order_by('created_at','DESC')
            ->limit($this->_per_page)
            ->offset($offset)
            ->get();
        $data = [];
        //var_dump($testimonies));exit;
        foreach($testimonies as $testimony) {
            $field_story_lead = "story_lead_$this->_current_lang";
            $field_story = "story_$this->_current_lang";
            $temp = [
                'id' => $testimony->id,
                'name' => $testimony->name,
                'age' => $testimony->age,
                'job' => $testimony->job,
                'location' => $testimony->location,
                'story_lead' => $testimony->$field_story_lead,
                'story' => $testimony->$field_story,
                'photo' => $testimony->photo
            ];
            $data[] = $temp;
        }
        return json_encode($data);
    }

}
